﻿using Google.JarResolver;
using UnityEditor;

[InitializeOnLoad]
public static class CustomHeyzapDependencies
{
	private static readonly string PluginName = "Heyzap";

	static CustomHeyzapDependencies()
	{
		PlayServicesSupport svcSupport =
			PlayServicesSupport.CreateInstance(PluginName, EditorPrefs.GetString("AndroidSdkRoot"), "ProjectSettings");

		svcSupport.DependOn("com.google.android.gms", "play-services-ads", "LATEST");

		svcSupport.DependOn("com.android.support", "support-v4", "26.1.0");
	}
}
