
//  Created by Draft Sama on 9/2/2559 BE.
//  Copyright © 2559 Lighting Bomb. All rights reserved.
//

#import "UnityShare.h"

@implementation UnityShare



-(BOOL)stringIsNilOrEmpty:(NSString*)aString {
    return !(aString && aString.length);
}




-(void)Share:(char *)filesPathJson subject:(char *)subject message:(char *)message{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    NSMutableArray *objectsToShare = [[NSMutableArray alloc] init];
    
    
    NSString *filesPathJsonString = [NSString stringWithUTF8String:filesPathJson];
    NSData *filesJsonData = [filesPathJsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *filesList = nil;
    
    filesList = [NSJSONSerialization JSONObjectWithData: filesJsonData options:NSJSONReadingMutableContainers error: nil];
    
    
    
    if(filesList != nil){
        
        for (NSString *filepath in filesList) {
            
            NSURL *url = [NSURL fileURLWithPath:filepath];
            //NSData *file = [NSData dataWithContentsOfURL:url];
            
            [objectsToShare addObject:url];
            
            
        }
        
        
    }
    
       
    if(message != nil){
    //Add Message
        NSString* messageObj = [NSString stringWithUTF8String:message];
        
        [objectsToShare addObject:messageObj];
        
    }
    
    
    
   
         UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //Add Subject
    if(subject != nil){
    
        [controller setValue:[NSString stringWithUTF8String:subject] forKey:@"subject"];
        
    }
    
    UIViewController *vc = UnityGetGLViewController();
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        controller.popoverPresentationController.sourceView = vc.view;
        controller.popoverPresentationController.sourceRect =  CGRectMake(UIScreen.mainScreen.bounds.size.width/2, UIScreen.mainScreen.bounds.size.height,0, 0);
    }
    
    
    
    [controller setCompletionHandler:^(NSString *act, BOOL done)
    {
        
        
        
        if ( done )
        {
            
            NSLog(@"Send %@ Completed",act);

        }
        else
        {
            NSLog(@"Send %@ Cancel",act);

            // didn't succeed. 
        }
    }];
    
    [vc presentViewController:controller animated:YES completion:nil];

    
}

-(void)ShareImageData:(char *)imageDataJson subject:(char *)subject message:(char *)message{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    NSMutableArray *objectsToShare = [[NSMutableArray alloc] init];

    
    
    
    NSString *imageDataJsonString = [NSString stringWithUTF8String:imageDataJson];
    NSData *imageDataJsonData = [imageDataJsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *dataList = nil;
    
   dataList = [NSJSONSerialization JSONObjectWithData: imageDataJsonData options:NSJSONReadingMutableContainers error: nil];
    

    
    
    if(dataList != nil){
        //Add Image
        
        for (NSString *stringData in dataList) {

        
        NSData *data = [[NSData alloc] initWithBase64EncodedString:stringData options:0];
        UIImage *image = [UIImage imageWithData:data];
        [objectsToShare addObject:image];
    
        }
    }
  
    if(message != nil){
        //Add Message
        NSString* messageObj = [NSString stringWithUTF8String:message];
        
        [objectsToShare addObject:messageObj];
        
    }

    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //Add Subject
    if(subject != nil){
        
        [controller setValue:[NSString stringWithUTF8String:subject] forKey:@"subject"];
        
    }
    UIViewController *vc = UnityGetGLViewController();
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        controller.popoverPresentationController.sourceView = vc.view;
       
         controller.popoverPresentationController.sourceRect =  CGRectMake(UIScreen.mainScreen.bounds.size.width/2, UIScreen.mainScreen.bounds.size.height * .75,0, 0); ;
    }
    
    
    [controller setCompletionHandler:^(NSString *act, BOOL done)
     {
         
         if ( done )
         {
             UnitySendMessage("GameController", "ShareDone", "sent");
         }
         else
         {
             UnitySendMessage("GameController", "ShareDone", "canceled");
         }
     }];

    
    [vc presentViewController:controller animated:YES completion:nil];

}




-(void)SocialShare:(char *)filesPathJson subject:(char*)subject  message:(char *)message socialApp:(char *)socialApp{
 
// Share Image only
    NSString* social = [NSString stringWithUTF8String:socialApp];
    
  
    
    if(social != nil){
        
         if ([SLComposeViewController isAvailableForServiceType:social]) {
             
              SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:social];
             
             
             
             if([mySLComposerSheet serviceType] == NULL){
                 
                 NSLog(@"[Native Share] %@  not install",social);
           
                 
                 [self Share:filesPathJson subject:subject message:message];
                 
                 return;
             }else{
                 
                 NSLog(@"%@",[mySLComposerSheet serviceType]);
                 
             }

             
             
             if(message != nil){
               
                 NSString *messageFinal = [NSString stringWithUTF8String:message];
                 [mySLComposerSheet setInitialText:messageFinal];
             }
             
            
             
             
             //ADD file
             NSString *filesPathJsonString = [NSString stringWithUTF8String:filesPathJson];
             NSData *filesJsonData = [filesPathJsonString dataUsingEncoding:NSUTF8StringEncoding];
             NSArray *filesList = nil;
             
             filesList = [NSJSONSerialization JSONObjectWithData: filesJsonData options:NSJSONReadingMutableContainers error: nil];
             
             
             
             if(filesList != nil){
                 
                 for (NSString *filepath in filesList) {
                     
             
                    
                    // NSLog(@"============= %@ ================",[filepath pathExtension]);
                     
                     if([[filepath pathExtension] isEqualToString:@"png"] || [[filepath pathExtension] isEqualToString:@"jpg"]|| [[filepath pathExtension] isEqualToString:@"gif"]){
                        [mySLComposerSheet addImage:[UIImage imageWithContentsOfFile:filepath]];
                     }else{
                         
                         [mySLComposerSheet addURL:[NSURL fileURLWithPath:filepath]];
                     }
                   
                     
                     
                 }
                 
                 
             }
             

             
//               NSString *urlFinal = [NSString stringWithUTF8String:url];
//             
//             [mySLComposerSheet addURL:[NSURL URLWithString:urlFinal]];


             
             [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                 
                 switch (result) {
                     case SLComposeViewControllerResultCancelled:
                         NSLog(@"Post Canceled");
                         break;
                     case SLComposeViewControllerResultDone:
                         NSLog(@"Post Sucessful");
                         break;
                         
                     default:
                         break;
                 }
             }];
             UIViewController *vc = UnityGetGLViewController();
             [vc presentViewController:mySLComposerSheet animated:YES completion:nil];
             
             
         }
        
        
        
    }
    
    
    
  
    
     
}

-(void)SocialShareImageData:(char *)imageDataJson subject:(char*)subject message:(char *)message socialApp:(char*)socialApp{
    NSString* social = [NSString stringWithUTF8String:socialApp];
    
   
    
    if(social != nil){
        
        if ([SLComposeViewController isAvailableForServiceType:social]) {
            
            SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:social];
            
            
            if([mySLComposerSheet serviceType] == NULL){
                
                NSLog(@"[Native Share] %@  not install",social);
                [self ShareImageData:imageDataJson subject:subject message:message];
                 return;
            }else{
                
                NSLog(@"%@",[mySLComposerSheet serviceType]);

            }
            
         
            
            if(message != nil){
            
                NSString *messageFinal = [NSString stringWithUTF8String:message];
                [mySLComposerSheet setInitialText:messageFinal];
            }
            
            
            //ADD file
            NSString *imageDataJsonString = [NSString stringWithUTF8String:imageDataJson];
            NSData *imageDataJsonData = [imageDataJsonString dataUsingEncoding:NSUTF8StringEncoding];
            NSArray *imageDataList = nil;
            
            imageDataList = [NSJSONSerialization JSONObjectWithData: imageDataJsonData options:NSJSONReadingMutableContainers error: nil];
            
            
            
            if(imageDataList != nil){
                
                for (NSString *stringData in imageDataList) {
                
                    NSData *data = [[NSData alloc] initWithBase64EncodedString:stringData options:0];
                 
                    [mySLComposerSheet addImage:[UIImage imageWithData:data]];
                    
                }
                
                
            }
            
            
            
            [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        NSLog(@"Post Canceled");
                        break;
                    case SLComposeViewControllerResultDone:
                        NSLog(@"Post Sucessful");
                        break;
                        
                    default:
                        break;
                }
            }];
            UIViewController *vc = UnityGetGLViewController();
            [vc presentViewController:mySLComposerSheet animated:YES completion:nil];
            
            
        }
        
        
        
    }
    

    
}

-(void)SendEmail:(char*)subject message:(char*)message attachFilesJson:(char*)attachFilesJson emailsJson:(char*)emailsJson{
    
    
    NSError *error = nil;
    NSString *emailsJsonString = [NSString stringWithUTF8String:emailsJson];
    NSString *attachFilesJsonString = [NSString stringWithUTF8String:attachFilesJson];
    NSString *emailSubject =[NSString stringWithUTF8String:subject];
    NSString *emailMessage =[NSString stringWithUTF8String:message];
    NSArray *toRecipents = nil;
    
    //Json Encode
   /*
    NSArray *arrayData = @[@"cinowacs@gamil.com",@"test@test.com"];
    NSData *jsonDataEncode = [NSJSONSerialization dataWithJSONObject:arrayData options:NSJSONWritingPrettyPrinted error:nil];
    jsonString = [[NSString alloc] initWithData:jsonDataEncode encoding:NSUTF8StringEncoding];
    
    
 
    NSLog(@"Json Encode ===== %@ =========",jsonString);
   
    */

    
    //Json Decode
   
    
    
   NSData *emailsJsonData = [emailsJsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    toRecipents = [NSJSONSerialization JSONObjectWithData: emailsJsonData options:NSJSONReadingMutableContainers error: &error];
    
    
    
    
    
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    
  
    if(emailSubject != nil){
        [mc setSubject:emailSubject];
    }
    if(emailMessage != nil){
        [mc setMessageBody:emailMessage isHTML:NO];
    }
    
 
     if(toRecipents != nil){
         [mc setToRecipients:toRecipents];
  
    }

    
    if(![self stringIsNilOrEmpty:attachFilesJsonString]){
    
    
    
    NSData *filesJsonData = [attachFilesJsonString dataUsingEncoding:NSUTF8StringEncoding];
    
   NSArray *filesList = [NSJSONSerialization JSONObjectWithData: filesJsonData options:NSJSONReadingMutableContainers error: &error];

    for (NSString *filepath in filesList) {
      
         NSURL *url = [NSURL fileURLWithPath:filepath];
        NSData *file = [NSData dataWithContentsOfURL:url];

        [mc addAttachmentData:file mimeType:@"*/*" fileName:  [url lastPathComponent]];

        
    }
    
    }
    
    
     // Present mail view controller on screen
    UIViewController *vc = UnityGetGLViewController();

    mc.mailComposeDelegate =self;
    [vc presentViewController:mc animated:YES completion:NULL];
    
}

-(void)SendEmailImageData:(char*)subject message:(char*)message imageDataJson:(char*)imageDataJson emailsJson:(char*)emailsJson{
    
    
    NSError *error = nil;
    NSString *emailsJsonString = [NSString stringWithUTF8String:emailsJson];
    NSString *imageDataJsonString = [NSString stringWithUTF8String:imageDataJson];
    NSString *emailSubject =[NSString stringWithUTF8String:subject];
    NSString *emailMessage =[NSString stringWithUTF8String:message];
    NSArray *toRecipents = nil;
    
    
    
    
    NSData *emailsJsonData = [emailsJsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    toRecipents = [NSJSONSerialization JSONObjectWithData: emailsJsonData options:NSJSONReadingMutableContainers error: &error];
    
    
    
    
    
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    
    
    if(emailSubject != nil){
        [mc setSubject:emailSubject];
    }
    if(emailMessage != nil){
        [mc setMessageBody:emailMessage isHTML:NO];
    }
    
    
    if(toRecipents != nil){
          [mc setToRecipients:toRecipents];
    }
    
    
    if(![self stringIsNilOrEmpty:imageDataJsonString]){
        
     
        NSData *imageDataJsonData = [imageDataJsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *dataList = nil;
        
        dataList = [NSJSONSerialization JSONObjectWithData: imageDataJsonData options:NSJSONReadingMutableContainers error: nil];
        
        
        
        
        if(dataList != nil){
            //Add Image
            
            for (NSString *stringData in dataList) {
                
                
                NSData *data = [[NSData alloc] initWithBase64EncodedString:stringData options:0];
                // NSURL *url = [NSURL fileURLWithPath:filepath];
                [mc addAttachmentData:data mimeType:@"image/*" fileName: @"AttachFile"];

             
            }
        }

        
        
            
            
        
        
    }
    
    
    // Present mail view controller on screen
    UIViewController *vc = UnityGetGLViewController();
    
    mc.mailComposeDelegate =self;
    [vc presentViewController:mc animated:YES completion:NULL];
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
      UIViewController *vc = UnityGetGLViewController();
        [vc dismissViewControllerAnimated:YES completion:NULL];
}
@end


static UnityShare* native = nil;
extern "C"
{
    
    
    
    void Unity_Share(char* filesPathJson,char* subject ,char* message ,char* emailsJson ,char* packageName){
        
        
        if(native == nil)
            native = [[UnityShare alloc] init];
     //   native->PopoverRect = CGRectMake(x, y, width, height);
        
        NSString *app = [NSString stringWithUTF8String:packageName];
        if([native stringIsNilOrEmpty:app]){
            [native Share:filesPathJson subject:subject message:message];
        }else if([app  isEqual: @"com.apple.mobilemail"]){
            [native SendEmail:subject message:message attachFilesJson:filesPathJson emailsJson:emailsJson];
        }else{
            
            [native SocialShare:filesPathJson subject:subject message:message socialApp:packageName];
        }
        
       
        
      
    }
    
    
    void Unity_ShareImageData(char* imageDataJson,char* subject,char* message ,char* emailsJson ,char* packageName){
        
        
        if(native == nil)
            native = [[UnityShare alloc] init];
        
        [native ShareImageData:imageDataJson subject:subject message:message];
    }

    
   
    
   
    
}
