﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class LocalizationData 
{
	public LocalizationItem[] items;
}

[System.Serializable]
public class LocalizationItem
{
	public string language;
	public string key;
	public string text;
}

public class LocalizationManager {

	public static Dictionary<string, string> localizationDictionary;

	public static string GetLocalizedText(string keyString) {
		
		if (localizationDictionary == null) {
			
			TextAsset textAsset = Resources.Load("Localization") as TextAsset;
			LocalizationData localizationData = JsonUtility.FromJson<LocalizationData>(textAsset.text);
			
			localizationDictionary = new Dictionary<string, string>();
			for (int i = 0; i < localizationData.items.Length; i++) {
				LocalizationItem localizationItem = localizationData.items[i];
				localizationDictionary.Add(localizationItem.language + "+" + localizationItem.key, localizationItem.text);
			}
		}
		
		if (localizationDictionary.ContainsKey(Application.systemLanguage + "+" + keyString)) {
			keyString = localizationDictionary[Application.systemLanguage + "+" + keyString];
		}
		else if (localizationDictionary.ContainsKey("English+" + keyString)) {
			keyString = localizationDictionary["English+" + keyString];
		}
		else {
			Debug.LogWarning("Localized text not found for key: " + keyString);
		}
		
		return keyString;
	}
}
