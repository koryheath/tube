﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KetchappSquareController : MonoBehaviour {
	
	public RectTransform fakeKetchappSquare;
	
	[HideInInspector]
	public bool animationEnabled;
	[HideInInspector]
	public float animationPercentage;
	
	public Vector2 position19x9;
	public Vector2 position16x9;
	public Vector2 position4x3;
	
	Vector2 targetPosition;
	Vector2 previousPosition;
	
	void Start() {
		float aspect = Screen.height / (float)Screen.width;
		if (aspect > 2.1f) {
			targetPosition = position19x9;
		}
		else if (aspect > 1.7f) {
			targetPosition = position16x9;
		}
		else {
			targetPosition = position4x3;
		}
		#if !UNITY_EDITOR
		fakeKetchappSquare.gameObject.SetActive(false);
		#else
		fakeKetchappSquare.gameObject.SetActive(true);
		#endif
	}
	
	void OnDisable()
	{
		#if !UNITY_EDITOR && !UNITY_ANDROID
		if (animationEnabled) {
			KetchappPromo.RemoveKetchappSquare();
		}
		#endif
	}
	
	void Update() {
		if (animationEnabled) {
			Vector2 position = new Vector2(Mathf.LerpUnclamped(1.1f, targetPosition.x, animationPercentage), targetPosition.y);
			if (position != previousPosition) {
				#if !UNITY_EDITOR && !UNITY_ANDROID
				KetchappPromo.ShowKetchappSquare(position.x, position.y);
				#endif
				#if UNITY_EDITOR
				fakeKetchappSquare.anchorMin = new Vector2(position.x, 1 - position.y);
				fakeKetchappSquare.anchorMax = new Vector2(position.x, 1 - position.y);
				#endif
			}
			previousPosition = position;
		}
	}
}
