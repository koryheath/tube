﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashPanel : MonoBehaviour {
	
	public Image image;
	
	System.Action midpointAction;
	
	public void Flash(Color color, System.Action midpointAction) {
		image.color = color;
		this.midpointAction = midpointAction;
		gameObject.SetActive(true);
	}
	
	public void FlashPanelMidpoint() {
		midpointAction();
	}
}
