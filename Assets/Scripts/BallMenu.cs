﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallMenu : MonoBehaviour {
	
	public GameController gameController;
	public BallConfig[] ballConfigs;
	public BallButton ballButtonPrefab;
	public RectTransform socialBalls;
	public RectTransform standardBalls;
	public GameObject gemVideoButton;
	public Animator gemVideoButtonAnimator;
	public bool shouldUnlockFacebook;
	public bool shouldUnlockInstagram;
	
	List<BallButton> ballButtons = new List<BallButton>();
	
	void Awake() {
		for (int i = 0; i < ballConfigs.Length; i++) {
			BallConfig ballConfig = ballConfigs[i];
			BallButton ballButton = Instantiate(ballButtonPrefab) as BallButton;
			ballButtons.Add(ballButton);
			ballButton.Configure(ballConfig);
			ballButton.transform.SetParent(ballButton.displayName == "facebookball" || ballButton.displayName == "instagramball" ?
				socialBalls : standardBalls, false);
			ballButton.buttonPressedAction = BallButtonPressed;
		}
	}
	
	void OnEnable() {
		for (int i = 0; i < ballButtons.Count; i++) {
			ballButtons[i].ball.transform.localRotation = ballConfigs[i].transform.localRotation;
		}
		UpdateUI();
	}
	
	void OnApplicationPause(bool pauseStatus) {
		if (pauseStatus == false) {
			if (shouldUnlockFacebook) {
				shouldUnlockFacebook = false;
				UnlockBall("facebookball");
			}
			if (shouldUnlockInstagram) {
				shouldUnlockInstagram = false;
				UnlockBall("instagramball");
			}
		}
	}

	public void BallButtonPressed(BallButton ballButton) {
		gameController.clickAudioSource.Play();
		if (ballButton.state == BallButtonState.selected) {
			gameController.gameCanvas.CloseMenu();
		}
		else if (ballButton.state == BallButtonState.unselected) {
			Settings.currentBall = ballButton.displayName;
			gameController.SetUpBall();
			UpdateUI();
		}
		else if (ballButton.displayName == "facebookball") {
			shouldUnlockFacebook = true;
			gameController.FacebookButtonPressed();
		}
		else if (ballButton.displayName == "instagramball") {
			shouldUnlockInstagram = true;
			gameController.InstagramButtonPressed();
		}
		else if (gameController.gameCanvas.gems >= ballButton.cost) {
			gameController.gameCanvas.gems -= ballButton.cost;
			gameController.buttonUnlockedAudioSource.Play();
			UnlockBall(ballButton.displayName);
		}
		else {
			gemVideoButtonAnimator.Play("GemVideoButtonBounce");
			gameController.buttonLockedAudioSource.Play();
		}
	}
	
	public void UnlockBall(string ballDisplayName) {
		Settings.SetBallPurchased(ballDisplayName, true);
		Settings.currentBall = ballDisplayName;
		gameController.SetUpBall();
		UpdateUI();
	}
	
	public void UpdateUI() {
		for (int i = 0; i < ballButtons.Count; i++) {
			BallButton ballButton = ballButtons[i];
			if (!Settings.IsBallPurchased(ballButton.displayName)) {
				ballButton.state = BallButtonState.unpurchased;
			}
			else if (Settings.currentBall == ballButton.displayName) {
				ballButton.state = BallButtonState.selected;
			}
			else {
				ballButton.state = BallButtonState.unselected;
			}
		}
	}
	
	public BallConfig GetBallConfig(string displayName) {
		for (int i = 0; i < ballConfigs.Length; i++) {
			if (ballConfigs[i].gameObject.name.ToLower() == displayName) {
				return ballConfigs[i];
			}
		}
		return null;
	}
}
