﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteAnimator : MonoBehaviour {
	
	public Palette palette;
	
	[HideInInspector]
	public Palette startPalette;
	
	private Palette _endPalette;
	public Palette endPalette {
		get {
			return _endPalette;
		}
		set {
			_endPalette = value;
			Shader.SetGlobalVectorArray("_UIColors", new Vector4[] {
				Color.Lerp(_endPalette.tubeTopColor, _endPalette.tubeBottomColor, .2f),
				Color.Lerp(_endPalette.obstacleTopColor, _endPalette.obstacleBottomColor, .5f)
			});
		}
	}
	
	[HideInInspector]
	public float t;
	
	public void SetToEndPalette() {
		palette.tubeTopColor = endPalette.tubeTopColor;
		palette.tubeBottomColor = endPalette.tubeBottomColor;
		palette.obstacleTopColor = endPalette.obstacleTopColor;
		palette.obstacleBottomColor = endPalette.obstacleBottomColor;
		palette.Apply();

	}
	
	void Update() {
		palette.tubeTopColor = Color.Lerp(startPalette.tubeTopColor, endPalette.tubeTopColor, t);
		palette.tubeBottomColor = Color.Lerp(startPalette.tubeBottomColor, endPalette.tubeBottomColor, t);
		palette.obstacleTopColor = Color.Lerp(startPalette.obstacleTopColor, endPalette.obstacleTopColor, t);
		palette.obstacleBottomColor = Color.Lerp(startPalette.obstacleBottomColor, endPalette.obstacleBottomColor, t);
		palette.Apply();
	}
}
