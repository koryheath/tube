﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveAnimator : MonoBehaviour {
	
	public void Activate() {
		gameObject.SetActive(true);
	}
	
	public void Deactivate() {
		gameObject.SetActive(false);
	}
	
	public void DestroySelf() {
		Destroy(gameObject);
	}
}
