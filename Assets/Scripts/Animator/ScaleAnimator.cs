﻿using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class ScaleAnimator: MonoBehaviour
{
	public Vector3 startScale;
	public Vector3 endScale;
	
	[HideInInspector]
	public float t;
	
	private RectTransform _rectTransform;
	private RectTransform rectTransform {
		get {
			if (_rectTransform == null) {
				_rectTransform = GetComponent<RectTransform>();
			}
			return _rectTransform;
		}
	}
	
	public void Update() {
		rectTransform.localScale = Vector3.LerpUnclamped(startScale, endScale, t);
	}
}
