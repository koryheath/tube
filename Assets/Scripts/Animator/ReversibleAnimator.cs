﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ReversibleAnimator : MonoBehaviour {
	
	public AnimationClip animationClip;
	
	private Animator _animator;
	private Animator animator {
		get {
			if (_animator == null) {
				_animator = GetComponent<Animator>();
			}
			return _animator;
		}
	}

	public bool visible {
		get {
			return animator.GetFloat("direction") > 0;
		}
		set {
			if (animator.isActiveAndEnabled) {

//				gameObject.SetActive (false);

				animator.SetFloat("direction", (value ? 1 : -1));
				AnimatorStateInfo animationState = animator.GetCurrentAnimatorStateInfo(0);
				animator.Play(animationClip.name, -1, Mathf.Clamp01(animationState.normalizedTime));
			}
		}
	}
}
