﻿using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class PositionAnimator : MonoBehaviour
{
	public Vector2 iPhoneXStartPosition;
	public Vector2 iPhoneXEndPosition;
	public Vector2 iPhoneStartPosition;
	public Vector2 iPhoneEndPosition;
	public Vector2 iPadStartPosition;
	public Vector2 iPadEndPosition;
	
	Vector3 startPosition;
	Vector3 endPosition;
	
	[HideInInspector]
	public float t;
	
	private RectTransform _rectTransform;
	private RectTransform rectTransform {
		get {
			if (_rectTransform == null) {
				_rectTransform = GetComponent<RectTransform>();
			}
			return _rectTransform;
		}
	}
	
	void Awake() {
		float aspect = Screen.height / (float)Screen.width;
		if (aspect > 2.1f) {
			startPosition.x = iPhoneXStartPosition.x;
			startPosition.y = iPhoneXStartPosition.y;
			endPosition.x = iPhoneXEndPosition.x;
			endPosition.y = iPhoneXEndPosition.y;
		}
		else if (aspect > 1.7f) {
			startPosition.x = iPhoneStartPosition.x;
			startPosition.y = iPhoneStartPosition.y;
			endPosition.x = iPhoneEndPosition.x;
			endPosition.y = iPhoneEndPosition.y;
		}
		else {
			startPosition.x = iPadStartPosition.x;
			startPosition.y = iPadStartPosition.y;
			endPosition.x = iPadEndPosition.x;
			endPosition.y = iPadEndPosition.y;
		}
	}
	
	void Update() {
		rectTransform.anchoredPosition = Vector3.LerpUnclamped(startPosition, endPosition, t);
	}
}
