﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourceAnimator : MonoBehaviour {
	
	public AudioSource audioSource;
	
	public void PlayAudioSource() {
		if (audioSource) {
			audioSource.Stop();
			audioSource.Play();
		}
	}
	
	public void StopAudioSource() {
		if (audioSource) {
			audioSource.Stop();
		}
	}
	
	public void PauseAudioSource() {
		if (audioSource) {
			audioSource.Pause();
		}
	}
	
	public void UnPauseAudioSource() {
		if (audioSource) {
			audioSource.UnPause();
		}
	}
}
