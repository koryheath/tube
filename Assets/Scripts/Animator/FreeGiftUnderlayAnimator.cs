﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeGiftUnderlayAnimator : MonoBehaviour {
	
	public float duration;
	public GameController gameController;
	public GameCanvas gameCanvas;
	public Animator freeGiftUnderlayAnimator;
	
	public void FadeInFinished() {
		gameCanvas.invisibleTouchBlocker.SetActive(false);
		gameController.Delay(duration, () => {
			freeGiftUnderlayAnimator.Play("FreeGiftUnderlayFadeOut");
		});
	}
	
	public void FadeOutFinished() {
		gameObject.SetActive(false);
	}
}
