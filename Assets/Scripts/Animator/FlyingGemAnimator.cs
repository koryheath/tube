﻿using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class FlyingGemAnimator : MonoBehaviour
{
	public System.Action finishedAction;
	public AudioSource gemAwardAudioSource;

	[HideInInspector]
	public Vector3 startPosition;
	[HideInInspector]
	public Vector3 endPosition;
	[HideInInspector]
	public Vector3 controlPosition;
	
	[HideInInspector]
	public float t;
	
	private RectTransform _rectTransform;
	private RectTransform rectTransform {
		get {
			if (_rectTransform == null) {
				_rectTransform = GetComponent<RectTransform>();
			}
			return _rectTransform;
		}
	}
	
	void Update() {
		float inverse = 1 - t;
		Vector3 a = (inverse * inverse) * startPosition;
		Vector3 b = 2 * inverse * t * controlPosition;
		Vector3 c = (t * t) * endPosition;
		rectTransform.localPosition = a + b + c;
	}
	
	void AnimationFinished() {
		if (gemAwardAudioSource != null) {
			gemAwardAudioSource.Play();
		}
		if (finishedAction != null) {
			finishedAction();
		}
		Destroy(gameObject);
	}
}
