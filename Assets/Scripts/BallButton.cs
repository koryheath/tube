﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BallButtonState {
	unpurchased,
	unselected,
	selected
}

public class BallButton : MonoBehaviour {
	
	public GameObject ball;
	public GameObject unpurchasedVisual;
	public GameObject halo;
	public GameObject facebook;
	public GameObject instagram;
	public GameObject gemCostParent;
	public Text costText;
	public BouncyButton bouncyButton;
	
	[HideInInspector]
	public System.Action<BallButton> buttonPressedAction;
	
	[HideInInspector]
	public string displayName;
	
	[HideInInspector]
	public int cost;
	
	private Material _ballMaterial;
	public Material ballMaterial {
		get {
			if (_ballMaterial == null) {
				_ballMaterial = ball.GetComponent<MeshRenderer>().material;
			}
			return _ballMaterial;
		}
	}
	
	void OnDestroy() {
		if (_ballMaterial != null) {
			Destroy(_ballMaterial);
		}
	}
	
	public void Configure(BallConfig ballConfig) {
		displayName = ballConfig.gameObject.name.ToLower();
		cost = ballConfig.cost;
		ballMaterial.mainTexture = ballConfig.mainTexture;
		costText.text = cost.ToString();
		if (displayName == "facebookball") {
			gemCostParent.SetActive(false);
			facebook.SetActive(true);
		}
		if (displayName == "instagramball") {
			gemCostParent.SetActive(false);
			instagram.SetActive(true);
		}
	}
	
	private BallButtonState _state;
	public BallButtonState state {
		get {
			return _state;
		}
		set {
			_state = value;
			unpurchasedVisual.SetActive(value == BallButtonState.unpurchased);
			halo.SetActive(value != BallButtonState.unpurchased);
			ball.gameObject.SetActive(value != BallButtonState.unpurchased);
		}
	}
	
	public void BallButtonPressed() {
		if (buttonPressedAction != null) {
			buttonPressedAction(this);
		}
	}
	
	void Update() {
		if (state == BallButtonState.selected) {
			ball.transform.Rotate(0, -Time.deltaTime * 70, 0, Space.World);
		}
	}
}
