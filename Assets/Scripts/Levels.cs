﻿using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levels {

	public static int intraLevelZSpacingIncrement = 0;
	public static float difficultyFactor = 1f;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Full Game Level Buckets
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static System.Action[] masterLevelList = {

		#if UNITY_IOS
		// Easy Levels
		SpiralChannelLevel,
		RotatingRingsLevel,
		CrazyRingsLevel,
		WideningSegmentsLevel,
		OppositeRotatingSegmentsLevel,

		GentlePathLevel,
		ArcEscalationLevel,
		LittleFansLevel,
		DomeForestLevel,
		RingGateLevel,

		SpringsLevel,
		NodeGraphLevel,
		RockingRingsLevel,
		FastRotatingRingsLevel,
		TeethLevel,

		CrystalsLevel,
		SpikyFlowersLevel,
		FlappingChannelLevel,
		JaggedPassageLevel,
		DolphinsLevel,

		WavingChannelLevel,
		TreeForestLevel,
		BouncingBallsLevel,

		// Medium Levels
		BigFansLevel,
		CrookedTeethLevel,
		AngledFansLevel,
		PendulumWheelLevel,
		DoublePathOpeningLevel,
		RunningRingsLevel,


		HypnotizeSpiralsLevel,
		WalkingSticksLevel,
		CheeriosLevel,
		ChargingMiceLevel,
		TrickyChoppersLevel,
		TetrominoesLevel,
		GuillotineLevel,
		BallSpitterLevel,
		BouldersLevel,
		SpikePillarsLevel,
		FlowersLevel,
		ArcheryLevel,
		CylinderForestLevel,
		AdjacentSegmentedRingsLevel,
		InvadersLevel,

		RotatingOutsideRingsLevel,
		TiltedGearsLevel,
		MazeLevel,
		RingDropLevel,
		FlyingCylindersLevel,
		BowlingLevel,
		RingLatticeLevel,

		BigFanTriosLevel,
		RingBuildingLevel,
		StalactitesLevel,
		RevolvingDoorLevel,
		PathOpeningLevel,

		ThreeTwoGatesLevel,
		DoorCloseLevel,
		RollingRingsLevel,
		RandomSwitchRingsLevel,

		JellyfishLevel,
		NewtonsCradleLevel,
		//RingSweepLevel,
		CrazyClocksLevel,
		ShiftingBallsRingsLevel,	
		HalfRingSlapChopLevel,
		ExtrudedRingLevel,
		SlalomLevel,
		AngledGatesLevel,
		ShuttersLevel,
		GunTurretsLevel,
		KeyholeLevel,
		SwordsLevel,
		RotatingPanelDoorLevel,

		// Medium-Hard levels
		VirusLevel,
		WavyArmsLevel,
		MovingPendulumLevel,
		CirclingMiceLevel,
		//PlutoniumDoorsLevel,
		RunawayTiresLevel,

		RockingTriangleLevel,
		//RussianRouletteLevel,
		ArcChoppersLevel,
		ClosingHalvesLevel,
		FullSwingRingsLevel,
		ThumpersLevel,
		OddlyRockingSegmentsLevel,
		RockingSquareLevel,
		TrickyTrianglesLevel,
		ChainChompLevel,
		PaddleWheelLevel,
		SpinningJaggedPassageLevel,
		PachinkoLevel,
		//SandwormTeethLevel,
		RainStickLevel,
		EqualizerLevel,
		CrosshatchLevel,
		DoorOpenLevel,
		TrappedLevel,
		
		CeilingSlamLevel,
		JackInBoxesLevel,
		TrenchRun2Level,
		BaodingBallsLevel,
		CubeShiftLevel,
		ChargingRingsLevel,
		OverlappingSegmentedRingsLevel,
		BuzzSawsLevel,
		FlippingRingsLevel,
		SpiralStaircaseLevel,
		PhasersLevel,
		RouletteLevel,
		//HoleInOneWindmillLevel,
		SqueezeRingsLevel,
		NailsLevel,
		MarblesLevel,
		SpikyMinesLevel,
		HalfCircleSpiralLevel,
		WakaWakaWakaLevel,
		//GiantBoulderLevel,
		//CubeWarpLevel,
		PingPongLevel,
		//RandomDoorSwitchLevel,
		FidgetSpinnersLevel,
		CubeSphereLevel,
		BouncingBallStarLevel,
		CrazyCubesLevel,
		GyroscopeLevel,

		//HalfCylinderGatesLevel,	
		//DoubleRippleLevel,
		RestrictedDodgingLevel,
		//ViciousCyclesLevel,
		
		// Hard levels
		//WobbleRingsLevel,
		//TrippyWavyPathLevel,
		//SunSpikesLevel,
		RandomFlyingFishLevel,
		//StartStopSegmentsLevel,
		RotatingStar8PointLevel,
		//RockSlideLevel,
		FloorFansLevel,
		//RotatingStarLevel,
		TopplingCylindersLevel,
		//InvisiblePathLevel,
		ShardsLevel,
		//TakeCoverLevel,
		//FloatySpinnyFansLevel,
		#elif UNITY_ANDROID
		// Easy Levels
		SpiralChannelLevel,
		RotatingRingsLevel,
		WideningSegmentsLevel,
		OppositeRotatingSegmentsLevel,

		ArcEscalationLevel,
		LittleFansLevel,
		DomeForestLevel,
		RingGateLevel,

		SpringsLevel,
		NodeGraphLevel,
		//RockingRingsLevel,
		FastRotatingRingsLevel,
		TeethLevel,

		SpikyFlowersLevel,
		FlappingChannelLevel,
		JaggedPassageLevel,
		DolphinsLevel,

		WavingChannelLevel,
		TreeForestLevel,
		BouncingBallsLevel,

		// Medium Levels
		BigFansLevel,
		CrookedTeethLevel,
		AngledFansLevel,
		PendulumWheelLevel,
		DoublePathOpeningLevel,
		RunningRingsLevel,


		WalkingSticksLevel,
		CheeriosLevel,
		ChargingMiceLevel,
		TrickyChoppersLevel,
		TetrominoesLevel,
		GuillotineLevel,
		BallSpitterLevel,
		BouldersLevel,
		SpikePillarsLevel,
		FlowersLevel,
		ArcheryLevel,
		//CylinderForestLevel,
		AdjacentSegmentedRingsLevel,

		RotatingOutsideRingsLevel,
		TiltedGearsLevel,
		MazeLevel,
		RingDropLevel,
		FlyingCylindersLevel,
		BowlingLevel,
		RingLatticeLevel,

		BigFanTriosLevel,
		RingBuildingLevel,
		StalactitesLevel,
		RevolvingDoorLevel,
		PathOpeningLevel,

		ThreeTwoGatesLevel,
		DoorCloseLevel,
		RollingRingsLevel,
		RandomSwitchRingsLevel,

		NewtonsCradleLevel,
		//RingSweepLevel,
		CrazyClocksLevel,
		ShiftingBallsRingsLevel,	
		HalfRingSlapChopLevel,
		ExtrudedRingLevel,
		SlalomLevel,
		AngledGatesLevel,
		ShuttersLevel,
		GunTurretsLevel,
		KeyholeLevel,
		SwordsLevel,
		RotatingPanelDoorLevel,

		// Medium-Hard levels
		VirusLevel,
		MovingPendulumLevel,
		CirclingMiceLevel,
		//PlutoniumDoorsLevel,
		RunawayTiresLevel,

		RockingTriangleLevel,
		//RussianRouletteLevel,
		ArcChoppersLevel,
		ClosingHalvesLevel,
		FullSwingRingsLevel,
		ThumpersLevel,
		OddlyRockingSegmentsLevel,
		RockingSquareLevel,
		TrickyTrianglesLevel,
		ChainChompLevel,
		PaddleWheelLevel,
		SpinningJaggedPassageLevel,
		PachinkoLevel,
		//SandwormTeethLevel,
		RainStickLevel,
		EqualizerLevel,
		CrosshatchLevel,
		DoorOpenLevel,
		TrappedLevel,
		
		CeilingSlamLevel,
		JackInBoxesLevel,
		TrenchRun2Level,
		BaodingBallsLevel,
		CubeShiftLevel,
		ChargingRingsLevel,
		OverlappingSegmentedRingsLevel,
		BuzzSawsLevel,
		FlippingRingsLevel,
		SpiralStaircaseLevel,
		PhasersLevel,
		RouletteLevel,
		//HoleInOneWindmillLevel,
		SqueezeRingsLevel,
		NailsLevel,
		MarblesLevel,
		SpikyMinesLevel,
		HalfCircleSpiralLevel,
		WakaWakaWakaLevel,
		//GiantBoulderLevel,
		//CubeWarpLevel,
		PingPongLevel,
		//RandomDoorSwitchLevel,
		FidgetSpinnersLevel,
		CubeSphereLevel,
		BouncingBallStarLevel,
		CrazyCubesLevel,
		GyroscopeLevel,

		//HalfCylinderGatesLevel,	
		//DoubleRippleLevel,
		RestrictedDodgingLevel,
		//ViciousCyclesLevel,
		
		// Hard levels
		//WobbleRingsLevel,
		RandomFlyingFishLevel,
		//StartStopSegmentsLevel,
		RotatingStar8PointLevel,
		//RockSlideLevel,
		FloorFansLevel,
		//RotatingStarLevel,
		TopplingCylindersLevel,
		ShardsLevel,
		//FloatySpinnyFansLevel,
		#endif
	};
	
	public static System.Action[] tutorialLevels = {
		SimpleRingsTutorialLevel,
		RotatingRingsTutorialLevel
	};
	
	public static System.Action[] fpsDroppingLevels = {
		GentlePathLevel,
		//TrippyWavyPathLevel,
		WavyArmsLevel,
		InvisiblePathLevel,
		SeaMonsterLevel,
		SurpriseVolleyLevel,
		CrazyRingsLevel
	};	
		
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Test Level Sets
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static System.Action[] testLevels1 = {

        //Lucas 4
	        ConvergentGatesLevel,
			VirusLevel,
	        BuzzSawsLevel,
			FlippingRingsLevel,
			GunTurretsLevel,

		//Lucas 5  //sent change request 1/25
			HypnotizeSpiralsLevel,  
			SwarmPathLevel, 

		//Lucas Batch 6
	        RingDropLevel,
	        BallSpitterLevel,
	        TrappedLevel,
	        PhasersLevel,
	        TrickyTrianglesLevel,
	        ThumpersLevel,

		//Lucas 7
			

			//Not sure what I think about these three:
			AngledFansLevel,



		//Lucas 8  //Sent lucas change request for these on 2/9

			BowlingLevel,
			SwordsLevel,
			RingBuildingLevel,
			
	     
	        

		//Lucas 9

	        //TrenchRunLevel,
	        //PhaserGunLevel,
	        //RouletteLevel,
	        //DuelingCannonsLevel,
	        //SpringsLevel,
	        //SqueezeRingsLevel,
	        //NailsLevel,
	        //CrystalsLevel,
	        //MarblesLevel,

	    //Lucas 10
			//EntropyBeastLevel,
	        //SpinningRingDropLevel,
	        //MovingPendulumLevel,
	        //CeilingSlamLevel,
	        //InvadersLevel,
	        //SimonSaysLevel,
	        //ArcheryLevel,
	        //MiniTubeLevel,
	        //SpikyMinesLevel,
	        //RotatingPanelDoorLevel,

	};

	public static System.Action[] testLevels2 = {
		
		//NewtonsCradleLevel,
		DolphinsLevel,

		/*
		EntropyBeastLevel,
		MovingPendulumLevel,
		SpikyMinesLevel,
		PhaserGunLevel,
		RouletteLevel,
		DuelingCannonsLevel,
		MarblesLevel,
		MirrorBallLevel,
		BowlingLevel,
		SwordsLevel,
		ChainChompLevel,
		NodeGraphLevel,
		PingPongLevel,
		BallSpitterLevel,
		TakeCoverLevel,
		JellyfishLevel,
		HypnotizeSpiralsLevel,
		BallVortexLevel,
		BouncingBallStarLevel,
		GunTurretsLevel,
		VirusLevel,
		PendulumWheelLevel,
		SeaMonsterLevel,
		BaodingBallsLevel,
		TreeForestLevel,
		DomeForestLevel,
		BouncingBallsLevel,
		*/
	};


	public static System.Action[] testLevels3 = {
		

	EmptyLevel,


	// ------------------------------------------------------------------------------------------
	// *************************************** ON DECK **********************************
	// ------------------------------------------------------------------------------------------
	//               These are ready for final review before adding to mix


	//Lucas 3
		CircleTunnelLevel, 
		HalfCircleSpiralLevel,
		CrazyClocksLevel,  //Made it harder, hour hand rotates backwards and faster than minute hand
		RussianRouletteLevel,  
		WakaWakaWakaLevel, //may be a copyright problem
		FloatySpinnyFansLevel, //this is one that feels super hard at first but seems to get fairly easy

	//Lucas 4
		BallVortexLevel, 
		GiantBoulderLevel,

	//Lucas 5
		TakeCoverLevel,
		ExtrudedRingLevel,  //Not in love with this, but might make a good easy-medium level
		SlalomLevel,
		DoubleRippleLevel, //May be too hard, let me know what you think
		HalfCylinderGatesLevel,
		FlowersLevel, //Should we space out the flowers a bit more?
		HypnotizeSpiralsLevel,  
		SwarmPathLevel,  //too hard still?

	//Lucas 6
		ThreeTwoGatesLevel,
		CheeriosLevel,
		CubeWarpLevel,
		CheeriosLevel,

	//Lucas 7
		RestrictedDodgingLevel,
		ChainChompLevel,
		TetrominoesLevel,  
		SquareHolesLevel, //pretty hard, may need to slow it down slightly
		FidgetSpinnersLevel,
		SpikyFlowersLevel, 
		SpiralStaircaseLevel,

	//Lucas 8
		CubeSphereLevel,
		AngledGatesLevel, 

		RingSweepLevel,  //good to go

	//Other
		ViciousCyclesLevel,  //mutant offspring of circle tunnel level
		FlyingCylindersLevel,
		SunSpikesLevel,
	};


	// ------------------------------------------------------------------------------------------
	// ***************************************LEVEL REPAIR SHOP**********************************
	// ------------------------------------------------------------------------------------------
	public static System.Action[] testLevels4 = {
		
		PlutoniumDoorsLevel,  
		CrookedTeethLevel,
		CylinderForestLevel,
		RandomDoorSwitchLevel,
		SurpriseVolleyLevel, //needs work
		SwingyCloseyPillars,
		SpinningBowlsLevel, 
		CollapsingZigZagChannelLevel, //semi-broken, should go back and forth, also always goes in same direction
		HoleInOneWindmillLevel, //looks crappy
		BigFansLevel,  //needs overhaul or delete
		RingGateLevel,  //Full size rings that turn slightly on Y axis when approached 
		AdjacentSegmentedRingsLevel,
		OneTruePathLevel,

		PlusFansLevel,

	
		PendulumWheelLevel, //removed from full game (1/28), seems to randomly kill me still
		TrippyWavyPathLevel, //needs a bit more differentiation from gentle path

		//Lucas 3
			//SpiderwebsLevel,

		//Lucas 4
			//RingLatticeLevel, //really good, but significant performace issues
			//BouncingBallStarLevel,

		//Lucas 5
			//CrazyCubesLevel, //shadows look a bit odd, but am not sure there's much we can do about it, and they don't look *bad*
			//JellyfishLevel,  //Needs testing on devices after performance update

		//Lucas 8
			//MirrorBallLevel,  //make a bit easier

	};

	// ------------------------------------------------------------------------------------------
	// ***************************************LEVEL PURGATORY**********************************
	// ------------------------------------------------------------------------------------------
	public static System.Action[] testLevels5 = {  

		//Interesting but highly problematic

		MiniArcSinChoppersLevel, //performance problems, too hard
		TrickyChoppersLevel, //performance problems; too hard
		SeaMonsterLevel, //performance problems
	};

		
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Levels
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void EmptyLevel(){
		z += 50;
	}




	public static void SimpleRingsTutorialLevel() {
		int numberOfRings = 2;

		for (int i = 0; i < numberOfRings; i++) {
			z += i == 0 ? 50 : 100;
			tubeAngle = tubeAngle + 180;
			CreateRing(270, 6, 0, 15);
		}
	}

	public static void RotatingRingsTutorialLevel() {
		int numberOfRings = 3;


		for (int i = 0; i < numberOfRings; i++) {
			z += i == 0 ? 0 : 100;
			RandomizeTubeAngle();
			TubeObject ring = CreateRing(250, 6, 0, 15);
			ring.tubeAngleEnd = ring.tubeAngle + randomRotation;
			ring.tubeAngleDuration = 9 - i;
		}
	}

	public static void EvilGrin() {
		
		TubeObject leftEye = null;
		TubeObject rightEye = null;
		TubeObject smile = null;

		z -= 200;
		leftEye = CreateRing(32, 18, 3, 5);
		leftEye.tubeAngle = 290;

		rightEye = CreateRing(29, 18, 3, 5);
		rightEye.tubeAngle = 70;

		smile = CreateRing(150, 5, 3, 5);
		smile.tubeAngle = 180;



		//Ring head = CreateRing(170, 5, 3, 5);


		z += 400; 
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas' Levels Batch 11
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void NewtonsCradleLevel()
    {
        int numberOfObstacles = 5;
        int numberOfBalls = 5;
        float sphereRadius = 4;
        float ringRadius = 5;
        float ringHoleRadius = 4;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 125;

            tubeAngle = 0;

			TubeObject ring = CreateRing(360, 20 - ringHoleRadius, 20 - ringRadius, ringRadius - ringHoleRadius);

			TubeObject rotationContainer = CreateContainer();
            rotationContainer.tubeAngle = Random.Range(0, 360);

            float animationStartDistance = Random.Range(50f, 100f);

            for (int j = 0; j < numberOfBalls; j++)
            {
				TubeObject swingContainer2 = CreateContainer();
                rotationContainer.AddChild(swingContainer2);

				TubeObject reverseContainer2 = CreateContainer();
                swingContainer2.AddChild(reverseContainer2);

				TubeObject swingContainer = CreateContainer();
                reverseContainer2.AddChild(swingContainer);

				TubeObject reverseContainer = CreateContainer();
                swingContainer.AddChild(reverseContainer);

				TubeObject ballContainer = CreateContainer();
                reverseContainer.AddChild(ballContainer);

                float wireLength = 20 - ringRadius - sphereRadius;

                TubeObject wire = CreateCylinder(1, wireLength, 1);
                ballContainer.AddChild(wire);
                wire.yPosition = -20 + ringRadius + wireLength / 2;

				TubeObject sphere = CreateSphere(sphereRadius * 2);
                ballContainer.AddChild(sphere);

                sphere.yPosition = -20 + sphereRadius;

                float ballOffset = 28;

                ballContainer.tubeAngle = j * ballOffset;

                if (j == 0 || j == numberOfBalls - 1)
                {
                    float endAngle = 150 * ((j == 0) ? 1 : -1);

                    for (int k = 0; k < 2; k++)
                    {
						TubeObject forward = (k == 0) ? swingContainer : swingContainer2;
						TubeObject backward = (k == 0) ? reverseContainer : reverseContainer2;
                    
                        forward.tubeAngleEnd = forward.tubeAngle + endAngle;
                        forward.tubeAngleDuration = .4f;
                        forward.tubeAngleCurve = EaseInQuad;
                        forward.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
                        forward.animationDistance = animationStartDistance - ((j == 0) ? 0 : forward.tubeAngleDuration * 2 * 100) + (k == 0 ? 0 : forward.tubeAngleDuration * 4 * 100);


                        backward.tubeAngleEnd = backward.tubeAngle - endAngle;
                        backward.tubeAngleDuration = forward.tubeAngleDuration;
                        backward.tubeAngleCurve = EaseOutQuad;
                        backward.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
                        backward.animationDistance = forward.animationDistance + backward.tubeAngleDuration * 100 + 2 ;
                    }
                }
            }
        }
    }

	public static void SnakesOnATubeLevel()
    {
        int numberOfObstacles = 3;
        int numberOfSpheres = 12;
        float travelDistance = 100;
        float animationDuration = 1.75f;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 200;

            tubeAngle = randomAngle;

            int reverse = (randomBool ? 1 : -1);
            int animationOffset = Random.Range(-10000, 10000);

            for (int j = 0; j < numberOfSpheres; j++)
            {
                float sphereRadius = (j == numberOfSpheres - 1) ? 3 : 2;

				TubeObject sphere = CreateSphere(sphereRadius * 2);
                sphere.yPosition = -20 + sphereRadius;

                sphere.zPositionEnd = sphere.zPosition + travelDistance;
                sphere.zPositionDuration = animationDuration;
                sphere.zPositionRepeatMode = RepeatMode.pingPong;
                sphere.zPositionCurve = EaseInOutSin;

                sphere.tubeAngleEnd = sphere.tubeAngle + 360 * reverse;
                sphere.tubeAngleDuration = animationDuration;
                sphere.tubeAngleRepeatMode = RepeatMode.loop;

                sphere.animationDistance = animationOffset + 5 * j;
            }

            float offset = 6;

			TubeObject nearOutsideCube = CreateCube(2, 10, 2);
            nearOutsideCube.zPosition -= offset;
            nearOutsideCube.yPosition = -20 + 5f / 2;

			TubeObject nearInsideCube = CreateCube(2, 10, 2);
            nearInsideCube.zPosition += offset;
            nearInsideCube.yPosition = -20 + 5f / 2;

			TubeObject middleInsideCube = CreateCube(2, 10, 2);
            middleInsideCube.zPosition += travelDistance / 2;
            middleInsideCube.xPosition -= offset;
            middleInsideCube.yPosition = 20 - 5f / 2;

			TubeObject middleOutsideCube = CreateCube(2, 10, 2);
            middleOutsideCube.zPosition += travelDistance / 2;
            middleOutsideCube.xPosition += offset;
            middleOutsideCube.yPosition = 20 - 5f / 2;

			TubeObject farInsideCube = CreateCube(2, 10, 2);
            farInsideCube.zPosition += travelDistance - offset;
            farInsideCube.yPosition = -20 + 5f / 2;

			TubeObject farOutsideCube = CreateCube(2, 10, 2);
            farOutsideCube.zPosition += travelDistance + offset;
            farOutsideCube.yPosition = -20 + 5f / 2;

            z += travelDistance;
        }
    }

	public static void MeatLockerLevel()
    {
        int numberOfObstacles = 20;
        float obstacleDistance = 30;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : obstacleDistance;

            tubeAngle = 0;

            int reverse = (randomBool ? 1 : -1);

			TubeObject ring = CreateRing(120, 1, 0, obstacleDistance);

			TubeObject swingingContainer = CreateContainer();
            swingingContainer.yPosition = 20;

            float xPosition = Random.Range(6f, 15f) * (randomBool ? 1 : -1);
            float height = 2 * 21 * Mathf.Sin(Mathf.Acos(Mathf.Abs(xPosition) / 21f));

	        TubeObject panel = CreateCubePillar(5, height, 1);
	        panel.shadingType = ShadingType.radial;
            swingingContainer.AddChild(panel);

            panel.yPosition = -height / 2;

            swingingContainer.xPosition = xPosition;
            swingingContainer.yRotation = Random.Range(-75f, 75f);

            swingingContainer.xRotation = -15 * reverse;
            swingingContainer.xRotationEnd = 15 * reverse;
            swingingContainer.xRotationDuration = 1.25f;
            swingingContainer.xRotationRepeatMode = RepeatMode.pingPong;
            swingingContainer.xRotationCurve = EaseInOutQuad;
            swingingContainer.animationDistance = Random.Range(-10000, 10000);
        }
    }


    public static void RunawayTiresLevel()
    {
        float numberOfObstacles = 30;

        float travelDistance = 400;
        float ringHoleRadius = 4;
        float ringRadius = 6;
        float ringWidth = 4;

        float angle = 0;

        tubeAngle = 0;

        for (float i = 0; i < numberOfObstacles; i++)
        {
	        z += (i == 0) ? (travelDistance * .6f) : 20 - (5 * (i / numberOfObstacles));

            angle = angle + Random.Range(60f, 300f);

            int reverse = (randomBool ? 1 : -1);

			TubeObject backAndForthContainer = CreateContainer();

			TubeObject bouncingContainer = CreateContainer();
            backAndForthContainer.AddChild(bouncingContainer);

	        TubeObject ring = CreateRing(360, 20 - ringHoleRadius, 20 - ringRadius, ringWidth);
	        ring.shadowType = ShadowType.directional;
            bouncingContainer.AddChild(ring);

            ring.yRotation += 90;

			bouncingContainer.yPosition = -20 + ringRadius;
            bouncingContainer.yPositionEnd = - ringRadius - Random.Range(2,4);
            bouncingContainer.yPositionDuration = Random.Range(.3f,.4f);
            bouncingContainer.yPositionRepeatMode = RepeatMode.pingPong;
            bouncingContainer.yPositionCurve = EaseOutQuad;
            bouncingContainer.animationDistance = 0;

            backAndForthContainer.tubeAngle = angle;
            backAndForthContainer.tubeAngleEnd = backAndForthContainer.tubeAngle - 45f * reverse;
            backAndForthContainer.tubeAngleRepeatMode = RepeatMode.pingPong;
            backAndForthContainer.tubeAngleDuration = bouncingContainer.yPositionDuration * 2;
            //bouncingContainer.tubeAngleCurve = EaseInOutQuad;
            backAndForthContainer.animationDistance = 0;

            ring.yRotation -= 10 * reverse;
            ring.yRotationEnd = ring.yRotation + 20 * reverse;
            ring.yRotationDuration = bouncingContainer.yPositionDuration * 2f;
            ring.yRotationRepeatMode = RepeatMode.pingPong;
            ring.yRotationCurve = EaseInOutQuad;

            ring.zPositionEnd = ring.zPosition - travelDistance;
            ring.zPositionDuration = 2;
            ring.zPositionRepeatMode = RepeatMode.triggeredOnce;
            ring.animationDistance = travelDistance - Random.Range(75,105);
    
        }

	    z -= 125;

    }

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas' Levels Batch 10
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void EntropyBeastLevel()
    {
        float travelDistance = 600;
        int numberOfBalls = 12;
        float ballRadius = 1.25f;
        float nucleusRadius = 2.5f;

        tubeAngle = 0;

        z += travelDistance;

		TubeObject blob = CreateContainer();

        blob.tubeAngleEnd = blob.tubeAngle + 360 * (randomBool ? 1 : -1);
        blob.tubeAngleDuration = 8;

        for (int i = 0; i < numberOfBalls; i++)
        {
			TubeObject movementContainer = CreateContainer();
            blob.AddChild(movementContainer);
            movementContainer.zPosition -= travelDistance;

            movementContainer.zPositionEnd = movementContainer.zPosition + travelDistance;
            movementContainer.zPositionDuration = travelDistance / 100f;
            movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
            movementContainer.animationDistance = 0;

            TubeObject expandContainer = CreateContainer();
            movementContainer.AddChild(expandContainer);
            expandContainer.tubeAngle = randomAngle;

            float scale = (40f - ballRadius) / (nucleusRadius * 2);

            expandContainer.xScale = expandContainer.yScale = expandContainer.zScale = 1;
            expandContainer.xScaleEnd = expandContainer.yScaleEnd = expandContainer.zScaleEnd = scale;
            expandContainer.xScaleDuration = expandContainer.yScaleDuration = expandContainer.zScaleDuration = travelDistance / 100f * .25f;
            expandContainer.xScaleRepeatMode = expandContainer.yScaleRepeatMode = expandContainer.zScaleRepeatMode = RepeatMode.triggeredPingPong;
            expandContainer.xScaleCurve = expandContainer.yScaleCurve = expandContainer.zScaleCurve = Linear;
            expandContainer.animationDistance = 0;

			TubeObject orbitContainer = CreateContainer();
            expandContainer.AddChild(orbitContainer);

            orbitContainer.tubeAngle = expandContainer.tubeAngle;
            orbitContainer.yPosition = nucleusRadius;
            orbitContainer.yPositionEnd = -nucleusRadius;
            orbitContainer.yPositionDuration = Random.Range(2.25f, 2.75f);
            orbitContainer.yPositionRepeatMode = RepeatMode.pingPong;
            orbitContainer.yPositionCurve = EaseInOutQuad;
            orbitContainer.animationDistance = Random.Range(-10000, 10000);

	        TubeObject sphere = CreateSphere(ballRadius * 2);
	        sphere.meshType = MeshType.sphereDense;
            orbitContainer.AddChild(sphere);

            sphere.shadingType = ShadingType.radial;

            sphere.xScaleEnd = sphere.yScaleEnd = sphere.zScaleEnd = (ballRadius * 2) / scale;
            sphere.xScaleDuration = sphere.yScaleDuration = sphere.zScaleDuration = travelDistance / 100f * .25f;
            sphere.xScaleRepeatMode = sphere.yScaleRepeatMode = sphere.zScaleRepeatMode = RepeatMode.triggeredPingPong;
            sphere.xScaleCurve = sphere.yScaleCurve = sphere.zScaleCurve = EaseOutQuad;
        }
    }

	public static void SpinningRingDropLevel()
    {
        int numberOfObstacles = 8;
        float rotationDistance = 200;
        float expandDistance = 100;

        float rotationAngle = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 80;

            tubeAngle = 0;
            rotationAngle += Random.Range(60, 300);

            float finalRotation = randomAngle;

            for (int j = 0; j < 4; j++)
            {
                TubeObject expandingContainer = CreateContainer();

				TubeObject ring = CreateRing(360, 8f, 0f, .5f);
                expandingContainer.AddChild(ring);

                ring.xRotation = 90;
                ring.xRotationEnd = ring.xRotation + 360;
                ring.xRotationDuration = (rotationDistance - expandDistance) / 100;
                ring.xRotationRepeatMode = RepeatMode.triggeredOnce;
                ring.xRotationCurve = EaseInOutQuad;

                ring.yRotationEnd = ring.yRotation + 360 + finalRotation;
                ring.yRotationDuration = (rotationDistance - expandDistance + 15) / 100;
                ring.yRotationRepeatMode = RepeatMode.triggeredOnce;
                ring.yRotationCurve = EaseInOutQuad;

                ring.animationDistance = rotationDistance;

                expandingContainer.tubeAngle = rotationAngle;

                expandingContainer.zRotation = (j >= 2) ? 90 : 0;

                expandingContainer.xScale = expandingContainer.yScale = expandingContainer.zScale = .25f;
                expandingContainer.xScaleEnd = expandingContainer.yScaleEnd = expandingContainer.zScaleEnd = 1;
                expandingContainer.xScaleDuration = expandingContainer.yScaleDuration = expandingContainer.zScaleDuration = expandDistance / 100f;
                expandingContainer.xScaleRepeatMode = expandingContainer.yScaleRepeatMode = expandingContainer.zScaleRepeatMode = RepeatMode.triggeredOnce;
                expandingContainer.xScaleCurve = expandingContainer.yScaleCurve = expandingContainer.zScaleCurve = EaseInQuad;

                if (j >= 2)
                {
                    expandingContainer.xPositionEnd = (j % 2 == 0) ? 24 : -24;
                    expandingContainer.xPositionDuration = expandingContainer.xScaleDuration;
                    expandingContainer.xPositionRepeatMode = RepeatMode.triggeredOnce;
                    expandingContainer.xPositionCurve = EaseInQuad;
                }
                else
                {
                    expandingContainer.yPositionEnd = (j % 2 == 0) ? 24 : -24;
                    expandingContainer.yPositionDuration = expandingContainer.xScaleDuration;
                    expandingContainer.yPositionRepeatMode = RepeatMode.triggeredOnce;
                    expandingContainer.yPositionCurve = EaseInQuad;
                }

                expandingContainer.animationDistance = expandDistance - 15;
            }
        }
    }


    public static void MovingPendulumLevel()
    {
        float travelDistance = 600;
        float sphereRadius = 3;

        tubeAngle = 0;

		TubeObject movementContainer = CreateContainer();

        movementContainer.zPositionEnd = movementContainer.zPosition + travelDistance;
        movementContainer.zPositionDuration = travelDistance / 100f;
        movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
        movementContainer.animationDistance = 15;

		TubeObject wall = CreateRing(170, 2, 0, 1 + movementContainer.animationDistance);
        movementContainer.AddChild(wall);
        wall.zPosition -= (movementContainer.animationDistance + 1) / 2;

		TubeObject pendulum = CreateContainer();
        movementContainer.AddChild(pendulum);
        pendulum.yPosition = 19;

        pendulum.tubeAngle = -wall.arc / 2 + 2;
        pendulum.tubeAngleEnd = wall.arc / 2 - 2;
        pendulum.tubeAngleDuration = Random.Range(2.5f, 3.5f);
        pendulum.tubeAngleRepeatMode = RepeatMode.pingPong;
        pendulum.tubeAngleCurve = EaseInOutQuad;
        pendulum.animationDistance = Random.Range(-10000, 10000);

		TubeObject secondPendulum = CreateContainer();
        pendulum.AddChild(secondPendulum);

        secondPendulum.xRotation = -wall.arc / 3;
        secondPendulum.xRotationEnd = wall.arc / 3;
	    secondPendulum.xRotationDuration = 1;
        secondPendulum.xRotationRepeatMode = RepeatMode.pingPong;
        secondPendulum.xRotationCurve = EaseInOutQuad;

		TubeObject sphere = CreateSphere(sphereRadius * 2);
        secondPendulum.AddChild(sphere);
        sphere.yPosition = -40 + sphereRadius;

	    TubeObject rope = CreateCylinder(1f, 40 - 2 - sphereRadius, 1f);
	    rope.shadowType = ShadowType.none;
        secondPendulum.AddChild(rope);
	    rope.yPosition = ((20 - 2) + (-20 + sphereRadius)) / 2 - (40 - 1 - sphereRadius) / 2;
        
	    TubeObject ropeShadow = CreateCylinder(1f, 15, 1f);
	    secondPendulum.AddChild(ropeShadow);
	    ropeShadow.shadingType = ShadingType.none;
	    ropeShadow.yPosition = sphere.yPosition + (ropeShadow.yScale / 2);

	    z += travelDistance + 20;
    }

    public static void CeilingSlamLevel()
    {
        int numberOfObstacles = 8;
        int numberOfPanels = 3;
        float segmentDistance = 100;
        float panelWidth = 40f / numberOfPanels;

        int gap = Random.Range(0, numberOfPanels);

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : segmentDistance;

            tubeAngle = 0;

	        gap = (gap + Random.Range(1, numberOfPanels)) % numberOfPanels;

            for (int j = 0; j < numberOfPanels; j++)
            {
                if (j != gap)
                {
	                TubeObject panel = CreateCubePillar(panelWidth, segmentDistance, 1);
	                panel.shadingType = ShadingType.radial;
	                panel.shadowType = ShadowType.radial;
	                
	                panel.xRotation = 90;
                    panel.xPosition = -20 + panelWidth / 2 + panelWidth * j;
	                panel.yPosition = 15.5f;

                    panel.yPositionEnd = -19;
                    panel.yPositionDuration = (segmentDistance / 100) / 2;
                    panel.yPositionRepeatMode = RepeatMode.triggeredPingPong;
                    panel.yPositionCurve = EaseInCubic;
                    panel.animationDistance = panel.yPositionDuration * 100 + (i + 2) * segmentDistance;
                }

            }
        }

        z += segmentDistance;
    }

	public static void InvadersLevel()
    {
        float numberOfObstacles = 5;
        int numberOfRows = 6;
        int numberOfColumns = 4;

        tubeAngle = 0;

        int gapEnemy = Random.Range(0, numberOfColumns);

        for (float k = 0; k < numberOfObstacles; k++)
        {
            z += (k == 0) ? 0 : 150;

            float animationDistanceOffset = Random.Range(-10000, 10000);

            for (int j = 0; j < numberOfRows; j++)
            {
                gapEnemy += Random.Range(1, numberOfColumns);

                while (gapEnemy >= numberOfColumns)
                {
                    gapEnemy -= numberOfColumns;
                }

                for (int i = 0; i < numberOfColumns; i++)
                {
                    if (i == gapEnemy)
                    {
                        continue;
                    }

					TubeObject enemy = CreateContainer();

					TubeObject body = CreateCube(6, 4, 2);
					TubeObject leftArm = CreateCube(3, 1, 1);
					TubeObject rightArm = CreateCube(3, 1, 1);
                    enemy.AddChild(body);
                    enemy.AddChild(leftArm);
                    enemy.AddChild(rightArm);

                    leftArm.zRotation = -30;
                    leftArm.xPosition = -2;
                    leftArm.yPosition = -3;

                    rightArm.zRotation = 30;
                    rightArm.xPosition = 2;
                    rightArm.yPosition = -3;

                    enemy.xPosition = -20 + (40f / (numberOfColumns)) * (i + .5f);

                    enemy.yPosition = 40 - (80f / (numberOfRows + 1));
                    enemy.yPositionEnd = enemy.yPosition - (80f / (numberOfRows + 1)) * (numberOfRows + .5f);
                    enemy.yPositionDuration = 6 - (3f * (k / (numberOfObstacles - 1f)));
                    enemy.yPositionRepeatMode = RepeatMode.loop;
                    enemy.animationDistance = (enemy.yPositionDuration * 100 / numberOfRows) * (j + 1) + animationDistanceOffset;
                }
            }
        }

        //z += travelDistance;
    }

	public static void SimonSaysLevel()
    {
        int numberOfObstacles = 4;

        int lastAngle = 90 * Random.Range(0, 4);
        int angleDifference = 0;

        for (int obstacleIndex = 1; obstacleIndex < numberOfObstacles; obstacleIndex++)
        {
            z += (obstacleIndex == 0) ? 0 : 120;

            int[] angles = new int[obstacleIndex + 1];

            for (int angleIndex = 0; angleIndex < angles.Length; angleIndex++)
            {
                if (angleIndex > 0)
                {
                    angles[angleIndex] = angles[angleIndex - 1] + 90 * (randomBool ? 1 : -1);
                }
                else
                {
                    angles[angleIndex] = lastAngle;
                }
            }

            lastAngle = angles[angles.Length - 1];
            angleDifference = lastAngle - angles[0];

            for (int stageIndex = 0; stageIndex < 2; stageIndex++)
            {
                z += (stageIndex == 0) ? 0 : 75;

                for (int ringIndex = 0; ringIndex <= obstacleIndex; ringIndex++)
                {
                    z += (stageIndex == 0 && ringIndex == 0) ? 0 : 50;

                    TubeObject ring = CreateRing(360, 4, 0, 1);

                    if (stageIndex == 0)
                    {
                        ring.tubeAngle = angles[ringIndex];
                    }
                    else
                    {
                        ring.tubeAngle = angleDifference + angles[ringIndex];
                    }

                    Debug.Log(ring.tubeAngle);

                    ring.arcEnd = 270;
                    ring.arcDuration = .125f;
                    ring.arcRepeatMode = RepeatMode.triggeredOnce;

                    if (stageIndex == 0 || ringIndex == 0)
                    {
                        ring.animationDistance = 1000;
                    }
                    else
                    {
                        ring.animationDistance = 50;
                    }
                }
            }
        }
    }

	public static void ArcheryLevel()
    {
        int numberOfObstacles = 3;

        float travelDistance = 250;
        int numberOfArrows = 10;

        float arrowLength = 30;
        float shaftWidth = .5f;
        float arrowheadLength = 6;
        float arrowheadWidth = 3;
        float arrowheadDepth = .2f;
        float fletchingWidth = 3;
        float fletchingLength = 8;
        float fletchingDepth = .1f;
        
        int[] arrowOrder = new int[numberOfArrows];

        for (int i = 0; i < arrowOrder.Length; i++)
        {
            arrowOrder[i] = i;
        }

        for (int i = 0; i < arrowOrder.Length; i++)
        {
            int index = Random.Range(i, arrowOrder.Length);
            int temp = arrowOrder[i];
            arrowOrder[i] = arrowOrder[index];
            arrowOrder[index] = temp;
        }

	    tubeAngle = 0;
	    z += 80;

        for (int j = 0; j < numberOfObstacles; j++)
        {
            z += (j == 0) ? 0 : 325;

            for (int i = 0; i < numberOfArrows; i++)
            {
				TubeObject arrowMovementContainer = CreateContainer();

				TubeObject flightDropContainer = CreateContainer();
                arrowMovementContainer.AddChild(flightDropContainer);
                flightDropContainer.zPosition += travelDistance / 2;

				TubeObject arrow = CreateContainer();
                flightDropContainer.AddChild(arrow);
                arrow.zPosition -= travelDistance / 2;

				TubeObject arrowSpinContainer = CreateContainer();
                arrow.AddChild(arrowSpinContainer);

				TubeObject shaft = CreateCylinder(shaftWidth, arrowLength - arrowheadLength, shaftWidth);
                arrowSpinContainer.AddChild(shaft);
                shaft.yPosition = -arrowheadLength / 2;

				TubeObject arrowhead = CreatePyramid(arrowheadWidth, arrowheadLength, arrowheadDepth);
                arrowSpinContainer.AddChild(arrowhead);
                arrowhead.yPosition = arrowLength / 2 - arrowheadLength / 2;

				TubeObject fletching1 = CreatePyramid(fletchingWidth, fletchingLength, fletchingDepth);
                arrowSpinContainer.AddChild(fletching1);
                fletching1.yPosition = -arrowLength / 2 + fletchingLength / 2 + 1;

				TubeObject fletching2 = CreatePyramid(fletchingWidth, fletchingLength, fletchingDepth);
                arrowSpinContainer.AddChild(fletching2);
                fletching2.yPosition = -arrowLength / 2 + fletchingLength / 2 + 1;
                fletching2.yRotation = 90;

                arrowSpinContainer.yRotation = 90;
                arrowSpinContainer.yRotationEnd = arrowSpinContainer.yRotation + Random.Range(-180,180); //360f * (randomBool ? 1 : -1);
                arrowSpinContainer.yRotationDuration = travelDistance / 100f;
                arrowSpinContainer.yRotationRepeatMode = RepeatMode.triggeredOnce;
                arrowSpinContainer.yRotationCurve = EaseInOutQuad;
                arrowSpinContainer.animationDistance = 50;

                arrow.yPosition = -15.5f;

                arrowMovementContainer.tubeAngle = (360f / numberOfArrows) * (i - .5f);
                arrowMovementContainer.tubeAngleEnd = arrowMovementContainer.tubeAngle + Random.Range(30f, 60f) * (randomBool ? 1 : -1);
                arrowMovementContainer.tubeAngleDuration = travelDistance / 100f;
                arrowMovementContainer.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
                arrowMovementContainer.tubeAngleCurve = EaseInOutQuad;

                arrowMovementContainer.yPositionEnd = Random.Range(6f, 12f);
                arrowMovementContainer.yPositionDuration = travelDistance / 100f / 2;
                arrowMovementContainer.yPositionRepeatMode = RepeatMode.triggeredOnce;
                arrowMovementContainer.yPositionCurve = EaseOutQuad;

                arrowMovementContainer.zPosition -= 8 * arrowOrder[i];
                arrowMovementContainer.zPositionEnd = arrowMovementContainer.zPosition + travelDistance;
                arrowMovementContainer.zPositionDuration = travelDistance / 100f;
                arrowMovementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
                arrowMovementContainer.zPositionCurve = EaseInCubic;
                arrowMovementContainer.animationDistance = arrowSpinContainer.animationDistance;

                flightDropContainer.yPositionEnd = -arrowMovementContainer.yPositionEnd - Random.Range(1,5);
                flightDropContainer.yPositionDuration = arrowMovementContainer.yPositionDuration;
                flightDropContainer.yPositionRepeatMode = RepeatMode.triggeredOnce;
                flightDropContainer.yPositionCurve = EaseInQuad;
                flightDropContainer.animationDistance = arrowMovementContainer.animationDistance;

                arrow.xRotation = 70;
                arrow.xRotationEnd = 110;
                arrow.xRotationDuration = travelDistance / 100f;
                arrow.xRotationRepeatMode = RepeatMode.triggeredOnce;
                arrow.animationDistance = 0;
            }
        }

        z += travelDistance;
    }

    public static void SpikyMinesLevel()
    {
        int numberOfObstacles = 5;

        float sphereRadius = 6;
        float pyramidWidth = 4;
        float pyramidHeight = 6;
        float explosionTime = .1f;
        float explosionDistance = 55;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 150;

            tubeAngle = 0;

			TubeObject mineShakeContainer = CreateContainer();

			TubeObject mine = CreateContainer();
            mineShakeContainer.AddChild(mine);

			TubeObject sphere = CreateSphere(sphereRadius * 2);
            mine.AddChild(sphere);
            sphere.shadingType = ShadingType.radial;

            sphere.xScaleEnd = sphere.yScaleEnd = sphere.zScaleEnd = 0;
            sphere.xScaleDuration = sphere.yScaleDuration = sphere.zScaleDuration = explosionTime;
            sphere.xScaleRepeatMode = sphere.yScaleRepeatMode = sphere.zScaleRepeatMode = RepeatMode.triggeredOnce;
            sphere.xScaleCurve = sphere.yScaleCurve = sphere.zScaleCurve = EaseInQuad;
            sphere.animationDistance = explosionDistance;

            for (int j = 0; j < 4; j++)
            {
                int numberOfSpikes = (j == 0 || j == 3) ? 1 : 6;

                for (int k = 0; k < numberOfSpikes; k++)
                {
					TubeObject rotationContainer = CreateContainer();
                    mine.AddChild(rotationContainer);

                    TubeObject pyramid = CreatePyramid(pyramidWidth, pyramidHeight, pyramidWidth);
                    rotationContainer.AddChild(pyramid);

                    pyramid.yPosition = sphereRadius;
                    rotationContainer.xRotation = 60 * j;
                    rotationContainer.yRotation = (360 / numberOfSpikes) * k;

                    float distance = 0;

                    if (j == 0 || j == 3)
                    {
                        distance = 20 - pyramid.yPosition;
                    }
                    else
                    {
                        if (k == 0 || k == 3)
                        {
                            distance = 40 - pyramid.yPosition;
                        }
                        else
                        {
                            distance = 22 - pyramid.yPosition;
                        }
                    }

                    distance = distance - pyramidHeight / 2 + 1;

                    pyramid.yPositionEnd = pyramid.yPosition + distance;
                    pyramid.yPositionDuration = explosionTime * (distance / (20 - pyramid.yPosition));
                    pyramid.yPositionRepeatMode = RepeatMode.triggeredOnce;
                    pyramid.yPositionCurve = EaseInQuad;
                    pyramid.animationDistance = explosionDistance;
                }
            }

            mine.tubeAngle = randomAngle;
            mine.tubeAngleEnd = mine.tubeAngle + 180f * 4 * (randomBool ? 1 : -1);
            mine.tubeAngleDuration = 8;
            mine.animationDistance = 850;
            mine.tubeAngleRepeatMode = RepeatMode.triggeredOnce;

            mineShakeContainer.xPositionShakingAmount = 2;
            mineShakeContainer.xPositionShakingDistance = 500;
            mineShakeContainer.animationDistance = explosionDistance;
        }
    }

	public static void RotatingPanelDoorLevel()
    {
        int numberOfObstacles = 3;
        int numberOfPanels = 3;
        float gapSize = 2f;
        float panelSize = (40f - (numberOfPanels + 1) * gapSize) / numberOfPanels;

        for (float i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 275;

            tubeAngle = 0;

            int reverse = (randomBool ? 1 : -1);
	        int shift = Random.Range(0, 4);

            for (int j = 0; j < 3; j++)
            {
				TubeObject rotationContainer = CreateContainer();

				TubeObject wallContainer = CreateContainer();
                rotationContainer.AddChild(wallContainer);
                rotationContainer.yPosition = 10;

                for (int k = 0; k < numberOfPanels; k++)
                {
                    if (k != j)
                    {
                    	int panelIndex = ((shift + k) % numberOfPanels);
                    	
                    	if (panelIndex == 1) {
	                    	TubeObject panel1 = CreateCubePillar(panelSize / 2, 30, 2);
	                    	wallContainer.AddChild(panel1);
	                    	panel1.xPosition = panelSize / 4;
	                    	panel1.shadowType = ShadowType.none;
	                    	TubeObject panel2 = CreateCubePillar(panelSize / 2, 30, 2);
	                    	wallContainer.AddChild(panel2);
	                    	panel2.xPosition = -panelSize / 4;
	                    	panel2.shadowType = ShadowType.none;
                    	}
                    	else {
	                    	TubeObject panel = CreateCubePillar(panelSize, 30, 2);
	                    	wallContainer.AddChild(panel);
	                    	panel.xPosition = (-20 + gapSize + panelSize / 2 + ((panelSize + gapSize) * panelIndex));
	                    	panel.xPosition *= reverse;
                    	}
                    }
                }

                wallContainer.yPosition = -15;

	            rotationContainer.xRotation = 120 * j;
	            rotationContainer.xRotation += -10;
                rotationContainer.xRotationEnd = rotationContainer.xRotation - 360f;
                rotationContainer.xRotationDuration = 3.5f - (i / (numberOfObstacles - 1));
            }

	        TubeObject hinge1 = CreateCylinderPillar(3, 19, 3);
	        hinge1.zRotation = 90;
	        hinge1.xPosition = -hinge1.yScale / 2;
	        hinge1.yPosition = 10;
	        hinge1.shadowType = ShadowType.radial;
	        TubeObject hinge2 = CreateCylinderPillar(3, 19, 3);
	        hinge2.zRotation = 90;
	        hinge2.xPosition = hinge2.yScale / 2;
	        hinge2.yPosition = 10;
	        hinge2.shadowType = ShadowType.radial;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas' Levels Batch 9
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void TrenchRun2Level()
    {
        int numberOfObstacles = 35;
        int trenchLength = 900;
        float trenchWidth = 30;
        float obstacleDistance = (float)trenchLength / numberOfObstacles;
        int numberOfSegments = (int)(trenchLength / obstacleDistance);

	    //float startingZ = z;

        int reverseRotation = randomBool ? 1 : -1;

        for (int i = 0; i < numberOfSegments; i++)
        {
            z += (i == 0) ? 0 : obstacleDistance;

			TubeObject trench = CreateContainer();

	        TubeObject leftWall = CreateCubePillar((40 - trenchWidth) / 2, 30, obstacleDistance);
	        TubeObject rightWall = CreateCubePillar((40 - trenchWidth) / 2, 30, obstacleDistance);
            trench.AddChild(leftWall);
            trench.AddChild(rightWall);
			
	        if (i > 0) {
	        	leftWall.yScale = rightWall.yScale = obstacleDistance;
	        	leftWall.zScale = rightWall.zScale = 30;
	        	leftWall.xRotation = rightWall.xRotation = 90;
	        }

	        leftWall.xPosition = -20 + ((40 - trenchWidth) / 4);
	        rightWall.xPosition = 20 - ((40 - trenchWidth) / 4);
            
	        float x = (trenchWidth / 6f) * (randomBool ? 1 : -1);
	        float angle = Random.Range(-80, 80);
            
	        TubeObject cylinder1 = CreateCylinderPillar(4, 20, 4);
	        trench.AddChild(cylinder1);
	        cylinder1.tubeAngle = angle;
	        cylinder1.xPosition = x;
	        cylinder1.yPosition = cylinder1.yScale / 2;

	        TubeObject cylinder2 = CreateCylinderPillar(4, 20, 4);
	        trench.AddChild(cylinder2);
	        cylinder2.tubeAngle = angle;
	        cylinder2.xPosition = x;
	        cylinder2.yPosition = -cylinder2.yScale / 2;

	        trench.tubeAngleEnd = trench.tubeAngle + 45f * reverseRotation;
            trench.tubeAngleDuration = trenchLength / 100f / 4;
            trench.tubeAngleRepeatMode = RepeatMode.pingPong;
            trench.tubeAngleCurve = EaseInOutQuad;
            trench.animationDistance = 25 + obstacleDistance * i;
        }

        z += obstacleDistance;
    }

    public static void TrenchRunLevel()
    {
        float trenchLength = 640;
        float minGapSize = 15f;

        float remainingTrenchLength = trenchLength;

        while (remainingTrenchLength > 0)
        {
            float obstacleDepth = 80;

            z += obstacleDepth / 2;

            float leftWallRight = -15;
            float rightWallLeft = 15;

            float leftWallRightEnd = Random.Range(leftWallRight, rightWallLeft - minGapSize);
            float rightWallLeftEnd = leftWallRightEnd + minGapSize;

            TubeObject leftSide = CreateCube(40, 40, obstacleDepth);
            leftSide.xPositionEnd = leftWallRight - 20;
            leftSide.xPosition = leftWallRightEnd - 20;

			TubeObject rightSide = CreateCube(40, 40, obstacleDepth);
            rightSide.xPositionEnd = rightWallLeft + 20;
            rightSide.xPosition = rightWallLeftEnd + 20;

            leftSide.xPositionDuration = rightSide.xPositionDuration = 1f;
            leftSide.xPositionRepeatMode = rightSide.xPositionRepeatMode = RepeatMode.pingPong;
            leftSide.animationDistance = rightSide.animationDistance = 20;

            z += obstacleDepth / 2;

            remainingTrenchLength -= obstacleDepth;
        }
    }

    public static void PhaserGunLevel()
    {
        float travelDistance = 600;

        tubeAngle = 0;

		TubeObject movementContainer = CreateContainer();

        movementContainer.zPositionEnd = movementContainer.zPosition + travelDistance;
        movementContainer.zPositionDuration = travelDistance / 100f;
        movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
        movementContainer.animationDistance = 0;

        TubeObject wall = CreateRing(170, 2, 0, 1);
        movementContainer.AddChild(wall);

		TubeObject gun = CreateContainer();
        movementContainer.AddChild(gun);
        gun.xScale = gun.yScale = gun.zScale = .4f;
        gun.yPosition = 16;

        gun.tubeAngle = -wall.arc / 2;
        gun.tubeAngleEnd = wall.arc / 2;
        gun.tubeAngleDuration = Random.Range(1.5f, 2.25f);
        gun.tubeAngleRepeatMode = RepeatMode.pingPong;
        gun.tubeAngleCurve = Linear;
        gun.animationDistance = Random.Range(-10000, 10000);

        TubeObject cube = CreateCube(1, 3, 2.5f);
        gun.AddChild(cube);
        cube.yPosition = 4;

        TubeObject cylinder1 = CreateCylinder(4, 10, 4);
        gun.AddChild(cylinder1);
        cylinder1.yPosition = -2.5f;

        TubeObject cylinder2 = CreateCylinder(1, 10, 1);
        gun.AddChild(cylinder2);
        cylinder2.yPosition = -12.5f;

        TubeObject sphere = CreateSphere(3f);
        gun.AddChild(sphere);
        sphere.yPosition = -17.5f;

        TubeObject projectile = CreateRing(360, 5, 0, 2f);
        gun.AddChild(projectile);

        projectile.xRotation = 90;

        projectile.yPosition = -18;
        projectile.yPositionEnd = -100;
        projectile.yPositionDuration = .5f;
        projectile.yPositionRepeatMode = RepeatMode.loop;
        projectile.yPositionCurve = Linear;

        projectile.xScale = projectile.yScale = .0625f;
        projectile.xScaleEnd = projectile.yScaleEnd = .25f;
        projectile.xScaleDuration = projectile.yScaleDuration = .15f;
        projectile.xScaleRepeatMode = projectile.yScaleRepeatMode = RepeatMode.pingPong;
        projectile.xScaleCurve = projectile.yScaleCurve = EaseInOutQuad;

        z += travelDistance;
    }

	public static void RouletteLevel()
    {
        int numberOfSegments = 6;
        float travelDistance = 600;
        float ballRadius = 2f;
        int openingIndex = Random.Range(0, numberOfSegments);

        tubeAngle = Random.Range(0, numberOfSegments) * (360f / numberOfSegments);

        TubeObject movementContainer = CreateContainer();

        movementContainer.zPositionEnd = movementContainer.zPosition + travelDistance;
        movementContainer.zPositionDuration = travelDistance / 100f;
        movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
        movementContainer.animationDistance = 20;

        for (int i = 0; i < numberOfSegments; i++)
        {
			TubeObject segment = CreateRing(360f / numberOfSegments - 10, 18, 0, 2);
            movementContainer.AddChild(segment);
            segment.tubeAngle = (360f / numberOfSegments) * i;

            if (i == openingIndex)
            {
                segment.yPositionEnd = 40f;
                segment.yPositionDuration = .25f;
                segment.yPositionRepeatMode = RepeatMode.triggeredOnce;
                segment.animationDistance = -travelDistance + 20;
            }

	        TubeObject wall = CreateRing(5, 8, 0, 4);
	        movementContainer.AddChild(wall);
	        wall.zPosition -= 3;
            wall.tubeAngle = (360f / numberOfSegments) * i + (180f / numberOfSegments);
        }

	    TubeObject ring = CreateRing(360, 9, 8, 4);
        movementContainer.AddChild(ring);
        ring.zPosition -= 3;

		TubeObject ball = CreateSphere(ballRadius * 2);
        movementContainer.AddChild(ball);
        ball.yPosition = 11 - ballRadius;
        ball.zPosition = -3;

        ball.tubeAngleEnd += openingIndex * (360f / numberOfSegments);
        ball.tubeAngleEnd += 360f * 4;
        ball.tubeAngleDuration = 6f;
        ball.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
        ball.tubeAngleCurve = EaseInOutQuad;

        z += travelDistance;
    }

    public static void DuelingCannonsLevel()
    {
        float cannonDistance = 150;
        float travelDistance = 600;
        float cannonBallSize = 3.2f;

        tubeAngle = 0;

		TubeObject movementContainer = CreateContainer();

        movementContainer.zPositionEnd = movementContainer.zPosition + travelDistance;
        movementContainer.zPositionDuration = travelDistance / 100f;
        movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
        movementContainer.animationDistance = -30 + cannonDistance / 2;

        for (int i = 0; i < 2; i++)
        {
            z += cannonDistance / 2;

			TubeObject cannon = CreateContainer();
            movementContainer.AddChild(cannon);

			TubeObject barrel = CreateRing(360, 4, 0, 30);
            cannon.AddChild(barrel);
            barrel.xScale = barrel.yScale = .1f;

			TubeObject barrelEnd = CreateRing(360, 5, 0, 1);
            cannon.AddChild(barrelEnd);
            barrelEnd.xScale = barrelEnd.yScale = .11f;
            barrelEnd.zPosition += 14.5f;

			TubeObject ring1 = CreateRing(360, 5, 0, .5f);
            cannon.AddChild(ring1);
            ring1.xScale = ring1.yScale = .1075f;
            ring1.zPosition += 4.75f;

			TubeObject ring2 = CreateRing(360, 5, 0, .5f);
            cannon.AddChild(ring2);
            ring2.xScale = ring2.yScale = .1075f;
            ring2.zPosition -= 6f;

			TubeObject ring3 = CreateRing(360, 5, 0, .5f);
            cannon.AddChild(ring3);
            ring3.xScale = ring3.yScale = .1075f;
            ring3.zPosition -= 14.75f;

			TubeObject barrelCap = CreateSphere(4);
            cannon.AddChild(barrelCap);
            barrelCap.zScale = 2f;
            barrelCap.zPosition -= 15f;

			TubeObject barrelKnob = CreateSphere(1.5f);
            cannon.AddChild(barrelKnob);
            barrelKnob.zPosition -= 16.5f;

			TubeObject cannonBase = CreateCube(barrel.xScale * 40, 2, 20);
            cannon.AddChild(cannonBase);
            cannonBase.yPosition = -2.75f;
            cannonBase.zPosition -= 5;

            cannon.yPosition = -16.5f;

            if (i == 1)
            {
                cannon.yRotation = 180;
            }

			TubeObject cannonball = CreateSphere(cannonBallSize);
            cannon.AddChild(cannonball);
            cannonball.zPosition -= 13;
            cannonball.yPosition = barrelEnd.yPosition;

            cannonball.zPositionEnd = cannonball.zPosition + 250;
            cannonball.zPositionDuration = .625f;
            cannonball.zPositionRepeatMode = RepeatMode.triggeredLoop;

            cannonball.yPositionEnd = -20 + cannonBallSize / 2 - cannon.yPosition;
            cannonball.yPositionDuration = cannonball.zPositionDuration;
            cannonball.yPositionRepeatMode = RepeatMode.triggeredLoop;
            cannonball.yPositionCurve = EaseInQuad;

            cannonball.xScaleEnd = cannonball.yScaleEnd = cannonball.zScaleEnd = 0;
            cannonball.xScaleDuration = cannonball.yScaleDuration = cannonball.zScaleDuration = cannonball.zPositionDuration;
            cannonball.xScaleRepeatMode = cannonball.yScaleRepeatMode = cannonball.zScaleRepeatMode = RepeatMode.triggeredLoop;
            cannonball.xScaleCurve = cannonball.yScaleCurve = cannonball.zScaleCurve = EaseInExpo;

            if (i == 0)
            {
                cannonball.animationDistance = -13 - 30;

                cannon.tubeAngleEnd = cannon.tubeAngle + Random.Range(300f, 420f);
            }
            else
            {
                cannonball.animationDistance = cannonDistance - 13 - 30;

                cannon.tubeAngleEnd = cannon.tubeAngle - Random.Range(300f, 420f);
            }

            cannon.tubeAngleDuration = Random.Range(3f, 5f);
            cannon.tubeAngleRepeatMode = RepeatMode.pingPong;
            cannon.tubeAngleCurve = EaseInOutQuad;

            cannon.zPosition += cannonDistance / 2 * (i == 1 ? 1 : -1);
        }

        z += travelDistance;
    }

    public static void SpringsLevel()
    {
        int numberOfObstacles = 6;
        int numberOfSpringRings = 7;
        float springRingHeight = .5f;
        float springRingGap = .5f;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 100;

            tubeAngle = 0;

            int reverse = (randomBool ? 1 : -1);

			TubeObject rotationContainer = CreateContainer();
			TubeObject spring = CreateContainer();

            float springHeight = springRingHeight * numberOfSpringRings + springRingGap * (numberOfSpringRings - 1);

            for (int j = 0; j < numberOfSpringRings; j++)
            {
	            TubeObject springRing = CreateRing(360, 4, 0, springRingHeight, 40);
                spring.AddChild(springRing);

                springRing.xRotation = 90;
                springRing.xScale = springRing.yScale = .5f;
                springRing.yPosition = 0 + springRingHeight / 2 + (springRingGap + springRingHeight) * (j - (numberOfSpringRings / 2));

                springRing.yPositionEnd = 0 + springRingGap * 8 * (j - (numberOfSpringRings / 2));
                springRing.yPositionDuration = .25f;
                springRing.yPositionRepeatMode = RepeatMode.pingPong;
                springRing.yPositionCurve = EaseInOutQuad;
            }

            rotationContainer.AddChild(spring);

            spring.yPosition = -20 + springHeight;
            spring.yPositionEnd = 20 - springHeight;
            spring.yPositionDuration = .5f;
            spring.yPositionRepeatMode = RepeatMode.pingPong;
            spring.yPositionCurve = EaseInOutQuad;

            rotationContainer.tubeAngle = randomAngle;
            rotationContainer.tubeAngleEnd = rotationContainer.tubeAngle + 360 * reverse;
            rotationContainer.tubeAngleDuration = spring.yPositionDuration * 18;
        }
    }

    public static void SqueezeRingsLevel()
    {
        int numberOfObstacles = 6;
        float ringRadius = 10;
        float ringEdge = 2;
        float ringDepth = 5;
        float scale = ringRadius / 20f;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 100;

            tubeAngle = randomAngle;

            int reverse = (randomBool ? 1 : -1);

            for (int j = 0; j < 6; j++)
            {
				TubeObject rotationContainer = CreateContainer();

				TubeObject ring = CreateRing(360, ringEdge / scale, 0, ringDepth / scale);
                rotationContainer.AddChild(ring);

                ring.xScale *= scale;
                ring.yScale *= scale;
                ring.zScale *= scale;
                ring.yPosition = -20;

                ring.xScaleEnd = ring.xScale;
                ring.xScale = ring.xScale * .25f;
                ring.xScaleDuration = 2;
                ring.xScaleRepeatMode = RepeatMode.pingPong;
                ring.animationDistance = 60 * j;

                rotationContainer.tubeAngle += 360 / (6f) * j;
                rotationContainer.tubeAngleEnd = rotationContainer.tubeAngle + 360f * reverse;
                rotationContainer.tubeAngleDuration = 12f;
            }
        }
    }

    public static void NailsLevel()
    {
        int numberOfObstacles = 24;
        float spikeLength = 30;
        float spikeWidth = 2;
        float headWidth = 10;
        float headHeight = 1;
        float nailDistance = 30;

        tubeAngle = 0;
        float tubeRotation = 0;
        float reverse = 1;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : nailDistance;

            if (i % 3 == 0)
            {
                tubeRotation = randomAngle;
                reverse = randomBool ? 1 : -1;
            }
            else
            {
                tubeRotation += 30 * reverse;
            }

            TubeObject pyramid = CreatePyramid(spikeWidth, spikeLength, spikeWidth);
            pyramid.zRotation = 180;

			TubeObject ring = CreateRing(360, 20, 0, headHeight);
            ring.xRotation = 90;
            ring.xScale = ring.yScale = headWidth / 40f;
            ring.yPosition = pyramid.yPosition + spikeLength / 2 + headHeight / 2;

			TubeObject nail = CreateContainer();
            nail.AddChild(pyramid);
            nail.AddChild(ring);

			TubeObject initialHammerContainer = CreateContainer();
            initialHammerContainer.AddChild(nail);

            initialHammerContainer.tubeAngle = tubeRotation;

            initialHammerContainer.yPosition = spikeLength + 10;
            initialHammerContainer.yPositionEnd = spikeLength / 2 + 10;
            initialHammerContainer.yPositionDuration = .5f;
            initialHammerContainer.yPositionRepeatMode = RepeatMode.triggeredOnce;
            initialHammerContainer.yPositionCurve = EaseOutQuad;
            initialHammerContainer.animationDistance = 130 + (i % 3 * nailDistance);

            nail.yPositionEnd = -60 + headHeight;
            nail.yPositionDuration = .5f;
            nail.yPositionRepeatMode = RepeatMode.triggeredOnce;
            nail.yPositionCurve = EaseInQuad;
            nail.animationDistance = 30 + (i % 3 * nailDistance);
        }
    }

	public static void CrystalsLevel()
    {
        int numberOfObstacles = 15;
        int spikeRow1Count = 5;
        int spikeRow2Count = 10;

        tubeAngle = 0;
        float tubeRotation = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            //z += (i == 0) ? 0 : 60;
	        z += (i == 0) ? 0 : 80 - i * 5;

            tubeRotation += Random.Range(60, 180);

            TubeObject crystal = CreateContainer();

            for (int j = 0; j < 1 + spikeRow1Count + spikeRow2Count; j++)
            {
                float spikeWidth = 0;
                float spikeHeight = 0;
                float spikeXAdjustment = 0;
                float spikeZRotation = 0;
                float spikeYRotation = 0;
                float pyramidHeight = 0;

                bool needsCollider = false;

                if (j == 0)
                {
                    spikeWidth = 4;
	                spikeHeight = 28;
                    spikeZRotation = 0;
                }
                else if (j <= spikeRow1Count)
                {
                    spikeWidth = Random.Range(2.5f, 3.5f);
	                spikeHeight = Random.Range(25f, 27f);
                    spikeXAdjustment = spikeWidth / 2;
                    spikeZRotation = Random.Range(8f, 16f);
                    spikeYRotation = 360f / spikeRow1Count * (j - 1);
                }
                else
                {
                    spikeWidth = Random.Range(1.5f, 2.5f);
                    spikeHeight = Random.Range(22f, 24f);
                    spikeXAdjustment = spikeWidth;
                    spikeZRotation = Random.Range(20f, 26f);
                    spikeYRotation = 360f / spikeRow2Count * (j - spikeRow1Count - 1);

                    needsCollider = true;
                }

                pyramidHeight = spikeWidth;

                float rectangleHeight = spikeHeight - pyramidHeight;

				TubeObject rotationContainer = CreateContainer();
				TubeObject spike = CreateContainer();

                rotationContainer.AddChild(spike);
                crystal.AddChild(rotationContainer);

	            TubeObject rectangle = CreateCubePillar(spikeWidth, rectangleHeight, spikeWidth);
	            TubeObject pyramid = CreatePyramidPillar(spikeWidth, pyramidHeight, spikeWidth);

                spike.AddChild(rectangle);
	            spike.AddChild(pyramid);
	            
	            rectangle.shadowType = ShadowType.radial;
	            pyramid.shadowType = ShadowType.radial;
                
	            rectangle.colliderType = needsCollider ? rectangle.colliderType : ColliderType.none;
	            pyramid.colliderType = ColliderType.none;

                pyramid.yPosition = spikeHeight / 2 - pyramidHeight / 2;
                rectangle.yPosition = pyramid.yPosition - pyramidHeight / 2 - rectangleHeight / 2;

                spike.yPosition = -20 + spikeHeight / 2;

                spike.zRotation = spikeZRotation;
                spike.xPosition = -Mathf.Sin(Mathf.Deg2Rad * spikeZRotation) * spikeHeight / 2 - spikeXAdjustment;
                spike.yPosition -= ((1 - Mathf.Cos(Mathf.Deg2Rad * spikeZRotation)) * spikeHeight / 2);

                rotationContainer.yRotation = spikeYRotation;
            }

            crystal.tubeAngle = tubeRotation;

            crystal.yPosition = -40;
            crystal.yPositionEnd = 0;
            crystal.yPositionDuration = 8;
            crystal.yPositionRepeatMode = RepeatMode.triggeredOnce;
            crystal.animationDistance = 550;
        }
    }

    public static void MarblesLevel()
    {
        int numberOfObstacles = 10;
        int numberOfSpheres = 5;
        float sphereRadius = 2;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 60;

            tubeAngle += Random.Range(60, 300);
            int reverse = randomBool ? 1 : -1;

            for (int j = 0; j < numberOfSpheres; j++)
            {
				TubeObject sphere = CreateSphere(sphereRadius * 2);
                sphere.xPosition = 20 - sphereRadius;
                sphere.tubeAngle += 12 * j * reverse;

                sphere.tubeAngleEnd = sphere.tubeAngle - 360 * reverse;
                sphere.tubeAngleDuration = 1 + (numberOfSpheres - j - 1) * .24f;
                sphere.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
                sphere.tubeAngleCurve = EaseInOutSin;
                sphere.animationDistance = 200 - j * 20;
            }
        }
    }

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas' Levels Batch 8
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void CubeSphereLevel()
    {
        int numberOfObstacles = 3;
        float centerGapSize = 28;

        float lineWidth = (40 - centerGapSize) / 2;
        float lineLength = (40 - centerGapSize) / 2;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 225;

            TubeObject rotationContainer1 = CreateContainer();
            TubeObject rotationContainer2 = CreateContainer();
            TubeObject rotationContainer3 = CreateContainer();

            rotationContainer1.AddChild(rotationContainer2);
            rotationContainer2.AddChild(rotationContainer3);

            for (int j = 0; j < 3; j++)
            {
                TubeObject wheel = CreateContainer();
                rotationContainer3.AddChild(wheel);

                for (int k = 0; k < 3; k++)
                {
                    if (j > 1 || k > 0)
                    {
                        TubeObject lineA = CreateCube(lineLength, lineWidth, lineWidth);
                        TubeObject lineB = CreateCube(lineLength, lineWidth, lineWidth);

                        wheel.AddChild(lineA);
                        wheel.AddChild(lineB);

                        lineA.tubeAngle = lineB.tubeAngle = 120 * k;

                        lineA.xPosition = -20 + lineLength / 2;
                        lineB.xPosition = 20 - lineLength / 2;
                    }
                }

                wheel.xRotation = wheel.xRotation = 120 * j;
            }

            rotationContainer1.tubeAngle = rotationContainer2.tubeAngle = rotationContainer3.tubeAngle = randomAngle;

            rotationContainer1.xRotationEnd = rotationContainer1.xRotation + 180 * (randomBool ? 1 : -1);
            rotationContainer1.xRotationDuration = .5f;
            rotationContainer1.xRotationRepeatMode = RepeatMode.triggeredOnce;
            rotationContainer1.animationDistance = 225;

            rotationContainer2.zRotationEnd = rotationContainer2.zRotation + 180 * (randomBool ? 1 : -1);
            rotationContainer2.zRotationDuration = .5f;
            rotationContainer2.zRotationRepeatMode = RepeatMode.triggeredOnce;
            rotationContainer2.animationDistance = 175;

            rotationContainer3.yRotationEnd = rotationContainer3.yRotation + 180 * (randomBool ? 1 : -1);
            rotationContainer3.yRotationDuration = .5f;
            rotationContainer3.yRotationRepeatMode = RepeatMode.triggeredOnce;
            rotationContainer3.animationDistance = 125;
        }
    }

	public static void RingSweepLevel()
	{
		tubeAngle = 0;
	    int numberOfObstacles = 5;
		int numberOfRingSegments = 5;
        float ringWidth = 2;
	    float gapSize = 0;
	    float obstacleGap = 30;
        float ringOpeningAngle = 75;
		float obstacleRingDepth = 120f / numberOfRingSegments;

        for (int i = 0; i < numberOfObstacles; i++)
        {
	        z += obstacleRingDepth / 2;
	        
	        for (int j = 0; j < numberOfRingSegments; j++, z += obstacleRingDepth) {
		        TubeObject obstacleRing = CreateRing(180, 2, 0, obstacleRingDepth);
		        obstacleRing.tubeAngle = ((i % 2 == 0) ? 90 : 270);
	        }
	        
	        z -= obstacleRingDepth / 2;
            bool flipOpeningSide = randomBool;

            if (i < numberOfObstacles - 1)
            {
            	z += obstacleGap / 2;
            	float ringRotationDuration = Random.Range(3f,5f);
                for (int j = 0; j < 2; j++)
                {
                    TubeObject ring = CreateRing(ringOpeningAngle, ringWidth, 0, ringWidth);
                    ring.xPosition += (gapSize / 2 + ringWidth / 2) * (j == 0 ? 1 : -1);
                    ring.yRotation = 90;
                    ring.zRotation += flipOpeningSide ? 180 : 0;
                    ring.zRotationEnd = ring.zRotation - 360f * (flipOpeningSide ? -1 : 1);
	                ring.zRotationDuration = ringRotationDuration;
                    ring.zRotationRepeatMode = RepeatMode.loop;
                    ring.animationDistance = 0;
                }
            	z += obstacleGap / 2;
            }
        }
    }

	public static void RingBuildingLevel()
    {

        int numberOfObstacles = 3;
        int numberOfSegments = 8;
        float segmentDistance = 20;
        float segmentHeight = 4;
        int[] segmentOrder = new int[numberOfSegments - 1];

        for (int i = 0; i < segmentOrder.Length; i++)
        {
            segmentOrder[i] = i;
        }

        for (int i = 0; i < segmentOrder.Length; i++)
        {
            int swapIndex = Random.Range(i, segmentOrder.Length);
            int temp = segmentOrder[i];
            segmentOrder[i] = segmentOrder[swapIndex];
            segmentOrder[swapIndex] = temp;
        }

        for (int j = 0; j < numberOfObstacles; j++)
        {
            z += (j == 0) ? 0 : 100;
            RandomizeTubeAngle();

            int rotationReverse = (randomBool ? 1 : -1);

            for (int i = 0; i < numberOfSegments - 1; i++)
            {
                z += (i == 0) ? 0 : segmentDistance;

                TubeObject ringSegment = CreateRing(360f / numberOfSegments, segmentHeight, 0, 2);
                ringSegment.tubeAngle = segmentOrder[i] * 360f / numberOfSegments;

                ringSegment.yPosition = segmentHeight + 1;
                ringSegment.yPositionEnd = 0;
                ringSegment.yPositionDuration = .25f;
                ringSegment.yPositionRepeatMode = RepeatMode.triggeredOnce;
                ringSegment.animationDistance = 200;

                TubeObject movementContainer = CreateContainer();
                movementContainer.AddChild(ringSegment);

                movementContainer.zPositionEnd = movementContainer.zPosition + (numberOfSegments - i) * segmentDistance;
                movementContainer.zPositionDuration = (movementContainer.zPositionEnd - movementContainer.zPosition) / 90f;
                movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
                movementContainer.zPositionCurve = EaseInOutQuad;
                movementContainer.animationDistance = 40;

                if (j == 2)
                {
                    movementContainer.tubeAngleEnd = movementContainer.tubeAngle + 180 * rotationReverse;
                    movementContainer.tubeAngleDuration = 1.25f;
                    movementContainer.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
                    movementContainer.tubeAngleCurve = EaseInOutQuad;
                    movementContainer.animationDistance = 40 + segmentDistance * i;
                }
            }
        }
    }

    public static void MirrorBallLevel()
    {
        int numberOfGates = 6;
        int numberOfSegments = 10;
        float gateDistance = 150;
        float excessTravelDistance = 50;
        float ballRadius = 2.25f;

        int[] selectedOpening = new int[numberOfGates];
        tubeAngle = 0;

        TubeObject ball = CreateSphere(ballRadius * 2);
        ball.yPosition = 20 - ballRadius;

        TubeObject previousContainer = null;

        for (int i = numberOfGates - 1; i >= 0; i--)
        {
            int tries = 10;

            do
            {
                selectedOpening[i] = Random.Range(0, numberOfSegments / 2);
                tries--;
            }
            while (i < numberOfGates - 1 && selectedOpening[i] == selectedOpening[i + 1] && tries > 0);
        }

        for (int i = numberOfGates - 1; i >= 0; i--)
        {
            TubeObject nextContainer = CreateContainer();

            float angleToTurn;

            if (previousContainer != null)
            {
                previousContainer.AddChild(nextContainer);

                if (i == 0)
                {
                    nextContainer.AddChild(ball);

                }

                nextContainer.zPosition -= gateDistance;
                nextContainer.zPositionEnd += nextContainer.zPosition + gateDistance;
                nextContainer.zPositionDuration = gateDistance / 100;
            }
            else
            {
                nextContainer.zPosition += gateDistance * (numberOfGates - 1);
                nextContainer.zPositionEnd = nextContainer.zPosition + gateDistance + excessTravelDistance;
                nextContainer.zPositionDuration = (gateDistance + excessTravelDistance) / 100;
            }

            nextContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;

            if (i == 0)
            {
                angleToTurn = selectedOpening[i] * (360 / numberOfSegments) - ball.tubeAngle;
            }
            else
            {
                angleToTurn = (selectedOpening[i] - selectedOpening[i - 1]) * (360 / numberOfSegments);
            }

            nextContainer.tubeAngleEnd = nextContainer.tubeAngle + angleToTurn;
            nextContainer.tubeAngleDuration = .75f * gateDistance / 100f;
            nextContainer.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
            nextContainer.tubeAngleCurve = EaseInOutQuad;

            nextContainer.animationDistance = 0;

            previousContainer = nextContainer;

            for (int j = 0; j < numberOfSegments; j++)
            {
                TubeObject ring = CreateRing(360f / numberOfSegments - 0f, 15, 0, 2);
                ring.zPosition += (i + 1) * gateDistance;
                ring.tubeAngle = 360f / numberOfSegments * j;

                if (j == selectedOpening[i] || j == selectedOpening[i] + (numberOfSegments / 2))
                {
                    ring.yPositionEnd += 25;
                    ring.yPositionRepeatMode = RepeatMode.triggeredOnce;

                    if (i == 0)
                    {
                        ring.animationDistance = gateDistance;
                        ring.yPositionDuration = .5f;
                    }
                    else
                    {
                        ring.animationDistance = 10;
                        ring.yPositionDuration = .1f;
                    }
                }
            }
        }

        z += numberOfGates * gateDistance + excessTravelDistance;
    }

	public static void BowlingLevel()
    {
        float tunnelLength = 300;
        float tunnelSegmentLength = 50;
        float pinDistance = 5;
        float pinAngle = 15;
        float pinCenterHeight = 8;
        float pinCenterWidth = 1.5f;
        float pinBottomWidth = 3.25f;
        float pinBottomHeight = 8;
        float pinTopWidth = 2;
        float pinTopHeight = 4;
        float pinFlyMinDistance = 40;
        float pinFlyMaxDistance = pinFlyMinDistance + 70f;
        float ballExcessTravelDistance = 50f;
        int tunnelSegments = (int)(tunnelLength / tunnelSegmentLength);
	    //int reverse = randomBool ? 1 : -1;

        tubeAngle = 0;

        TubeObject bowlingBall = CreateSphere(4.5f);

        bowlingBall.zPositionEnd = bowlingBall.zPosition + tunnelLength + ballExcessTravelDistance;
	    bowlingBall.zPositionDuration = (bowlingBall.zPositionEnd - bowlingBall.zPosition) / 100 * 1.25f;
        bowlingBall.zPositionRepeatMode = RepeatMode.triggeredOnce;
        bowlingBall.zPositionCurve = EaseOutQuad;
        bowlingBall.yPosition = -20 + 2.25f;
	    bowlingBall.animationDistance = 25;
        
	    TubeObject ring = CreateRing(290f, 2.0f, 0, 2.5f);
        ring.zPosition += 1.25f;


        for (int i = 0; i < tunnelSegments; i++)
        {
            z += (i == 0) ? tunnelSegmentLength / 2 : tunnelSegmentLength;

	        TubeObject leftWall = CreateCubePillar(2, tunnelSegmentLength, 2);
	        TubeObject rightWall = CreateCubePillar(2, tunnelSegmentLength, 2);
	        
	        rightWall.tubeAngle = 35;
	        leftWall.tubeAngle = -rightWall.tubeAngle;
	        
	        leftWall.xRotation = rightWall.xRotation = 90;
	        leftWall.yPosition = rightWall.yPosition = -20 + (leftWall.zScale / 2);
        }

        z += tunnelSegmentLength / 2;

        int pinNum = 0;

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < i + 1; j++)
            {
                pinNum++;

				TubeObject pinCenter = CreateCylinder(pinCenterWidth, pinCenterHeight, pinCenterWidth);

				TubeObject pinBottom = CreateSphere(1);
                pinBottom.xScale = pinBottom.zScale = pinBottomWidth;
                pinBottom.yScale = pinBottomHeight;
                pinBottom.yPosition = -.75f;
                Object.Destroy(pinBottom.GetComponent<SphereCollider>());
                pinBottom.gameObject.AddComponent<CapsuleCollider>();

				TubeObject pinTop = CreateSphere(1);
                pinTop.xScale = pinTop.zScale = pinTopWidth;
                pinTop.yScale = pinTopHeight;
                pinTop.yPosition = 4.75f;
                Object.Destroy(pinTop.GetComponent<SphereCollider>());
                pinTop.gameObject.AddComponent<CapsuleCollider>();

				TubeObject spinContainer = CreateContainer();
                spinContainer.AddChild(pinCenter);
                spinContainer.AddChild(pinBottom);
                spinContainer.AddChild(pinTop);

				TubeObject pin = CreateContainer();
                pin.AddChild(spinContainer);

                pin.yPosition = -20 + pinCenterHeight / 2;
                pin.tubeAngle = -(i / 2f) * pinAngle + pinAngle * j;
                pin.zPosition += i * pinDistance;

                pin.zRotationEnd = Random.Range(-90f, 270f);

                spinContainer.xRotationEnd = 180 * Random.Range(3, 8) * (randomBool ? 1 : -1) + randomAngle;

	            pin.yPositionEnd = (20 - pinBottomWidth / 2) * Mathf.Sin(Mathf.Deg2Rad * pin.zRotationEnd);
	            pin.xPositionEnd = (20 - pinBottomWidth / 2) * Mathf.Cos(Mathf.Deg2Rad * pin.zRotationEnd);
                pin.zPositionEnd = pin.zPosition + pinFlyMinDistance + ((pinFlyMaxDistance - pinFlyMinDistance) / 10f) * pinNum - pinDistance * i;
                pin.tubeAngleEnd = 0;

                pin.xRotationDuration = pin.yRotationDuration = pin.zRotationDuration =
                    pin.xPositionDuration = pin.yPositionDuration = pin.zPositionDuration =
                    pin.tubeAngleDuration = .75f;

                spinContainer.xRotationDuration = Random.Range(.75f, 2f);

                pin.xRotationRepeatMode = pin.yRotationRepeatMode = pin.zRotationRepeatMode =
                    pin.xPositionRepeatMode = pin.yPositionRepeatMode = pin.zPositionRepeatMode =
                    pin.tubeAngleRepeatMode = spinContainer.xRotationRepeatMode = RepeatMode.triggeredOnce;

                pin.xRotationCurve = pin.yRotationCurve = pin.zRotationCurve =
                    pin.xPositionCurve = pin.yPositionCurve = pin.zPositionCurve =
                    pin.tubeAngleCurve = spinContainer.xRotationCurve = EaseOutQuad;

	            pin.animationDistance = spinContainer.animationDistance = bowlingBall.animationDistance + 32;
            }
        }

        z += pinFlyMaxDistance;
    }

	public static void SwordsLevel()
    {
        int numberOfObstacles = 10;
        float bladeWidth = 1.5f;
        float bladeHeight = 20;
        float bladeDepth = .5f;
        float crossguardWidth = 5;
        float crossguardHeight = .5f;
        float crossguardDepth = 1;
        float gripWidth = 1;
        float gripHeight = 4;
        float pommelWidth = 1.5f;
        float computedSwordLength = bladeWidth + bladeHeight + crossguardHeight + gripHeight + pommelWidth / 2;
        float desiredSwordLength = 30f;

        tubeAngle = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 95 - (2*i);

	        TubeObject blade = CreateCubePillar(bladeWidth, bladeHeight, bladeDepth);
	        TubeObject tip = CreatePyramidPillar(bladeWidth, bladeWidth, bladeDepth);
	        TubeObject crossguard = CreateCubePillar(crossguardWidth, crossguardHeight, crossguardDepth);
	        TubeObject grip = CreateCylinderPillar(gripWidth, gripHeight, gripWidth);
	        TubeObject pommel = CreateSphere(pommelWidth);
			
	        blade.shadowType = ShadowType.radial;
	        tip.shadowType = ShadowType.radial;
	        crossguard.shadowType = ShadowType.radial;
	        grip.shadowType = ShadowType.radial;
	        pommel.shadowType = ShadowType.radial;

            tip.yPosition = bladeHeight / 2 + bladeWidth / 2;
            crossguard.yPosition = blade.yPosition - bladeHeight / 2 - crossguardHeight / 2;
            grip.yPosition = crossguard.yPosition - crossguardHeight / 2 - gripHeight / 2;
            pommel.yPosition = grip.yPosition - gripHeight / 2;

			TubeObject sword = CreateContainer();
            sword.AddChild(blade);
            sword.AddChild(tip);
            sword.AddChild(crossguard);
            sword.AddChild(grip);
            sword.AddChild(pommel);
            sword.xScale = sword.yScale = sword.zScale = desiredSwordLength / computedSwordLength;
            sword.yPosition -= ((pommel.yPosition - pommelWidth / 2) * sword.yScale);

            int animationType = Random.Range(0, 2);

	        TubeObject tubeAngleRotationContainer = CreateContainer();
	        tubeAngleRotationContainer.tubeAngle = randomRotation;
	        tubeAngleRotationContainer.tubeAngleEnd = tubeAngleRotationContainer.tubeAngle + 360 * (randomBool ? 1 : -1);
	        tubeAngleRotationContainer.tubeAngleRepeatMode = RepeatMode.loop;

            switch (animationType)
            {
                case 0: // swinging
                    int reverseSwing = randomBool ? 1 : -1;

	                TubeObject initialRotationContainer = CreateContainer();
                    initialRotationContainer.tubeAngle = randomAngle;
                    initialRotationContainer.yPosition = -20;

					TubeObject rotationContainer = CreateContainer();
                    initialRotationContainer.AddChild(rotationContainer);

                    initialRotationContainer.zRotationEnd = initialRotationContainer.zRotation - 41.5f * reverseSwing;
                    initialRotationContainer.zRotationDuration = .2f;
                    initialRotationContainer.zRotationRepeatMode = RepeatMode.triggeredOnce;
	                initialRotationContainer.zRotationCurve = EaseInOutSin;
                    initialRotationContainer.animationDistance = 45;

                    rotationContainer.AddChild(sword);
                    rotationContainer.zRotation = 0;
                    rotationContainer.zRotationEnd = rotationContainer.zRotation + 83 * reverseSwing;
                    rotationContainer.zRotationDuration = .3f;
                    
	                rotationContainer.zRotationCurve = EaseInOutSin;

                    rotationContainer.zRotationRepeatMode = RepeatMode.triggeredOnce;
	                rotationContainer.animationDistance = 15;
                    
					tubeAngleRotationContainer.AddChild(initialRotationContainer);
	                tubeAngleRotationContainer.tubeAngleDuration = Random.Range(6f, 9f);
	                
                    break;

                case 1: // stabbing

					TubeObject stabbingContainer = CreateContainer();
                    stabbingContainer.AddChild(sword);
                    stabbingContainer.tubeAngle = randomAngle;
                    stabbingContainer.yPosition -= 20;
                    stabbingContainer.yPositionEnd = stabbingContainer.yPosition + 25;
                    stabbingContainer.yPositionDuration = .2f;
                    stabbingContainer.yPositionCurve = EaseInOutCubic;

                    stabbingContainer.yPositionRepeatMode = RepeatMode.triggeredOnce;
                    stabbingContainer.animationDistance = 40;
				
	                tubeAngleRotationContainer.AddChild(stabbingContainer);
	                tubeAngleRotationContainer.tubeAngleDuration = Random.Range(10f, 15f);

                    break;
            }
        }
    }

	public static void AngledGatesLevel()
    {
        int numberOfObstacles = 3;
        int numberOfRings = 3;
        //float gapAngle = -30;
        float gapAngle = -90;
        float ringHeight = 4;
        float ringDepth = 1;
        //float ringDistance = 42;
        float ringDistance = 54;
        float ringRotation = 60;

        int overallReverseRotation = randomBool ? 1 : -1;

        z += 200;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 200;

            int reverseRotation = ((i % 2 == 0) ? 1 : -1) * overallReverseRotation;

			TubeObject rotationContainer = CreateContainer();

            for (int j = 0; j < numberOfRings; j++)
            {
                TubeObject ring1 = CreateRing((120 - gapAngle) / 2, ringHeight, 0, ringDepth);
				TubeObject ring2 = CreateRing((120 - gapAngle) / 2, ringHeight, 0, ringDepth);
				TubeObject ring3 = CreateRing((120 - gapAngle) / 2, ringHeight, 0, ringDepth);
                ring1.xScale = ring2.xScale = ring3.xScale = 2;
                ring2.tubeAngle += 120;
                ring3.tubeAngle += 240;

                ring1.yRotation = ring2.yRotation = ring3.yRotation = ringRotation * (j % 2 == 0 ? 1 : -1) * reverseRotation;

				TubeObject ring = CreateContainer();
                ring.AddChild(ring1);
                ring.AddChild(ring2);
                ring.AddChild(ring3);

                rotationContainer.AddChild(ring);

                ring.zPosition -= ringDistance * j;
            }

            rotationContainer.tubeAngleEnd = randomAngle;
            rotationContainer.tubeAngle = rotationContainer.tubeAngleEnd - 360 * reverseRotation;
            rotationContainer.tubeAngleDuration = 8;
            rotationContainer.animationDistance = rotationContainer.tubeAngleDuration * 100;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas' Levels Batch 7
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void SpikyFlowersLevel()
	{
		int numberOfObstacles = 15;
		int numberOfLayers = 3;
		int numberOfPetalsPerLayer = 4;
		float petalWidth = 4;
		float petalHeight = 20;
		float animationDuration = 1.25f;
		float animationDistance = 100;

		float previousAngle = 0;
        
		System.Func<float, float> curve = EaseOutExpo;
	    
		for (int i = 0; i < numberOfObstacles; i++)
		{
			z += (i == 0) ? 0 : 80 - (i * 4);

			TubeObject flower = CreateContainer();

			for (int j = 0; j < numberOfLayers; j++)
			{
				for (int k = 0; k < numberOfPetalsPerLayer; k++)
				{
					TubeObject petal = CreatePyramidPillar(petalWidth, petalHeight, .25f);
					flower.AddChild(petal);

					petal.xRotation = 105 + (15 / (numberOfLayers + 1)) * (j + 1);
					petal.xRotationEnd = (90 / (numberOfLayers + 1)) * (j + 1);

					petal.yPosition = petalHeight / 2 * Mathf.Cos(Mathf.Deg2Rad * petal.xRotationEnd) / 3f;
					petal.yPositionEnd = petalHeight / 2 * Mathf.Cos(Mathf.Deg2Rad * petal.xRotationEnd);

					petal.yScale = petalHeight * Mathf.Cos(Mathf.Deg2Rad * petal.xRotationEnd);

					petal.xRotationDuration = animationDuration;
					petal.xRotationRepeatMode = RepeatMode.triggeredOnce;
					petal.xRotationCurve = curve;

					petal.yPositionDuration = animationDuration;
					petal.yPositionRepeatMode = RepeatMode.triggeredOnce;
					petal.yPositionCurve = curve;

					petal.tubeAngle = (360 / numberOfPetalsPerLayer) * k;
					petal.tubeAngle += (180 / numberOfPetalsPerLayer) * j;

					petal.animationDistance = animationDistance;
				}
			}

			flower.xRotation = 90;
			flower.zRotation += 45;
			flower.yPosition = 13.25f;
			flower.yPositionEnd = 15f;
			flower.yPositionDuration = animationDuration;
			flower.yPositionRepeatMode = RepeatMode.triggeredOnce;
			flower.yPositionCurve = curve;
			flower.animationDistance = animationDistance;

			flower.tubeAngle = previousAngle + Random.Range(60, 180);

			previousAngle = flower.tubeAngle;
		}
	}
	
	public static void RestrictedDodgingLevel()
    {
        int numberOfObstacles = 40;
        float travelDistance = 600;
        float borderWidth = 4;
        float pyramidWidth = 4;
        float pyramidHeight = 8;

        tubeAngle = 0;

	    TubeObject startingRing = CreateRing(250, 2f, 0, borderWidth);

        TubeObject movementContainer = CreateContainer();
        movementContainer.AddChild(startingRing);

        movementContainer.zPositionEnd = movementContainer.zPosition + travelDistance;
        movementContainer.zPositionDuration = travelDistance / 100;
        movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
        movementContainer.tubeAngle = randomAngle;
        movementContainer.tubeAngleEnd = movementContainer.tubeAngle + 90f * (randomBool ? 1 : -1);
        movementContainer.tubeAngleDuration = 4;
        movementContainer.tubeAngleRepeatMode = RepeatMode.pingPong;
        movementContainer.tubeAngleCurve = EaseInOutQuad;

        float previousTubeAngle = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += travelDistance / numberOfObstacles;

            TubeObject pyramid = CreatePyramidPillar(pyramidWidth, pyramidHeight, pyramidWidth);
            pyramid.yPosition = -20 + pyramidHeight / 2;

            pyramid.tubeAngle = previousTubeAngle + Random.Range(45f, 315f);
            previousTubeAngle = pyramid.tubeAngle;
        }
    }

    public static void ChainChompLevel()
    {
        int numberOfObstacles = 10;
        int numberOfSmallSpheres = 4;
        float smallSphereRadius = 2;
        float largeSphereRadius = 8;

        System.Func<float, float> curve = EaseInQuint;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : Random.Range(55f,95f);

            float animationDistance = 35;
            float animationDuration = .3f;

            tubeAngle += Random.Range(60, 300);

            TubeObject rotationContainer = CreateContainer();

            for (int j = 0; j < numberOfSmallSpheres; j++)
            {
                TubeObject sphere = CreateSphere(smallSphereRadius * 2);

                sphere.yPosition = -40 + smallSphereRadius + smallSphereRadius * j;
                sphere.yPositionEnd = -20 + (40 - largeSphereRadius * 1.5f) / (numberOfSmallSpheres + 1) * (j + 1);
                sphere.yPositionDuration = animationDuration;
                sphere.yPositionRepeatMode = RepeatMode.triggeredPingPong;
                sphere.yPositionCurve = curve;
                sphere.animationDistance = animationDistance;

                rotationContainer.AddChild(sphere);
            }

            TubeObject largeSphere = CreateSphere(largeSphereRadius * 2);
            largeSphere.yPosition = -40 + smallSphereRadius * numberOfSmallSpheres + largeSphereRadius;
            largeSphere.yPositionEnd = 20 - (largeSphereRadius / 2);
            largeSphere.yPositionDuration = animationDuration;
			largeSphere.yPositionRepeatMode = RepeatMode.triggeredPingPong;
            largeSphere.yPositionCurve = curve;
            largeSphere.animationDistance = animationDistance;

			rotationContainer.AddChild(largeSphere);
			rotationContainer.tubeAngleEnd = rotationContainer.tubeAngle + randomRotation;
			rotationContainer.tubeAngleDuration = Random.Range(2f,10f);
			rotationContainer.tubeAngleRepeatMode = RepeatMode.loop;

        }

       
    }

	public static void AngledFansLevel()
    {
        int numberOfObstacles = 5;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 120 - (i * Random.Range(5,15));

            int reverse = (i % 2 == 0) ? 1 : -1;
            float animationDuration = Random.Range(5f, 7f);
            float animationDistance = Random.Range(-10000, 10000);

            CreateRing(360, 18, 17, 4);

            for (int j = 0; j < 4; j++)
            {
				TubeObject ring = CreateRing(80, 17, 0, 1);
                ring.tubeAngle = 90 * j;
                ring.yRotation = 45 * reverse;

                ring.tubeAngleEnd = ring.tubeAngle + 360f * reverse;
                ring.tubeAngleDuration = animationDuration - (2 * (i / numberOfObstacles));
                ring.animationDistance = animationDistance;

//                ring.xRotation = 15;
//                ring.xRotationEnd = 90;
//                ring.xRotationDuration = 1.5f -  (0.7f * ( i / 4));
//                ring.xRotationCurve = EaseInOutQuad;
//                ring.xRotationRepeatMode = RepeatMode.pingPong;

//				ring.xRotationEnd = 0;
//                ring.xRotationDuration = .5f;
//                ring.xRotationRepeatMode = RepeatMode.pingPong;

            }
        }
    }

	public static void NodeGraphLevel()
    {
        int numberOfObstacles = 3;
        float nodeRadius = 1.25f;
        int layer1Nodes = 3;
        float expandDuration = .5f;
        System.Func<float, float> curve = EaseInOutQuart;

        tubeAngle = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 200;

            TubeObject graphContainer = CreateContainer();

			TubeObject centerNode = CreateSphere(nodeRadius * 2);
            graphContainer.AddChild(centerNode);

            centerNode.xScaleEnd = centerNode.yScaleEnd = centerNode.zScaleEnd = 0;
            centerNode.xScaleDuration = centerNode.yScaleDuration = centerNode.zScaleDuration = expandDuration;
            centerNode.xScaleRepeatMode = centerNode.yScaleRepeatMode = centerNode.zScaleRepeatMode = RepeatMode.triggeredOnce;
            centerNode.animationDistance = 20;

            for (int j = 0; j < layer1Nodes; j++)
            {
				TubeObject layer1Node = CreateSphere(nodeRadius * 2);

				TubeObject layer1Container = CreateContainer();
                layer1Container.AddChild(layer1Node);
                graphContainer.AddChild(layer1Container);

                layer1Container.yPositionEnd = (20 - nodeRadius) * (1f / 3);
                layer1Container.yPositionDuration = expandDuration;
                layer1Container.yPositionRepeatMode = RepeatMode.triggeredOnce;
                layer1Container.yPositionCurve = curve;

                layer1Container.tubeAngleEnd = 120 * j + 45f;
                layer1Container.tubeAngleDuration = .01f;
                layer1Container.tubeAngleRepeatMode = RepeatMode.triggeredOnce;

                layer1Container.animationDistance = 200;

                for (int k = 0; k < 2; k++)
                {
					TubeObject layer2Node = CreateSphere(nodeRadius * 2);

					TubeObject layer2Container = CreateContainer();
                    layer2Container.AddChild(layer2Node);
                    layer1Container.AddChild(layer2Container);

                    layer2Container.yPositionEnd = (20 - nodeRadius) * (1f / 3);
                    layer2Container.yPositionDuration = expandDuration;
                    layer2Container.yPositionRepeatMode = RepeatMode.triggeredOnce;
                    layer2Container.yPositionCurve = curve;

                    layer2Container.tubeAngleEnd = 45 * (k == 0 ? 1 : -1);
                    layer2Container.tubeAngleDuration = .01f;
                    layer2Container.tubeAngleRepeatMode = RepeatMode.triggeredOnce;

                    layer2Container.animationDistance = 150;

                    for (int l = 0; l < 2; l++)
                    {
						TubeObject layer3Node = CreateSphere(nodeRadius * 2);

						TubeObject layer3Container = CreateContainer();
                        layer3Container.AddChild(layer3Node);
                        layer2Container.AddChild(layer3Container);

                        layer3Container.yPositionEnd = (20 - nodeRadius) * (1f / 3);
                        layer3Container.yPositionDuration = expandDuration;
                        layer3Container.yPositionRepeatMode = RepeatMode.triggeredOnce;
                        layer3Container.yPositionCurve = curve;

                        layer3Container.tubeAngleEnd = (l == 0 ? -20 : 20) + (k == 0 ? -20 : 20);
                        layer3Container.tubeAngleDuration = .01f;
                        layer3Container.tubeAngleRepeatMode = RepeatMode.triggeredOnce;

                        layer3Container.animationDistance = 100;
                    }
                }
            }

            graphContainer.tubeAngle = randomAngle;
            graphContainer.tubeAngleEnd = graphContainer.tubeAngle + 360 * (randomBool ? 1 : -1);
	        graphContainer.tubeAngleDuration = 6;
        }
    }

	public static void TetrominoesLevel()
    {
        int numberOfObstacles = 7;
        float depth = 2;

        int[] order = new int[7];

        for (int i = 0; i < order.Length; i++)
        {
            order[i] = i;
        }

        tubeAngle = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 100;

            TubeObject shape = CreateContainer();

            float sideLength;

            if (i % order.Length == 0)
            {
                for (int j = 0; j < order.Length; j++)
                {
                    int swapIndex = Random.Range(j, order.Length);
                    int temp = order[j];
                    order[j] = order[swapIndex];
                    order[swapIndex] = temp;
                }
            }

            int shapeType = order[i % order.Length];

            switch (shapeType)
            {
                case 0: // square
                    {
                        sideLength = 2 * Mathf.Sqrt(200);

	                    TubeObject top = CreateCubePillar(depth, sideLength, depth);
	                    TubeObject bottom = CreateCubePillar(depth, sideLength, depth);
						TubeObject left = CreateCubePillar(depth, sideLength, depth);
	                    TubeObject right = CreateCubePillar(depth, sideLength, depth);
						
	                    top.shadowType = ShadowType.radial;
	                    bottom.shadowType = ShadowType.radial;
	                    left.shadowType = ShadowType.radial;
	                    right.shadowType = ShadowType.radial;
	                    
	                    top.zRotation = 90;
	                    bottom.zRotation = 90;

                        top.yPosition = sideLength / 2 - depth / 2;
                        bottom.yPosition = -sideLength / 2 + depth / 2;
                        left.xPosition = -sideLength / 2 + depth / 2;
                        right.xPosition = sideLength / 2 - depth / 2;

                        shape.AddChild(top);
                        shape.AddChild(bottom);
                        shape.AddChild(left);
                        shape.AddChild(right);
                    }
                    break;
                case 1: // I
                    {
                        sideLength = 40 * Mathf.Sin(Mathf.Atan(.25f));

	                    TubeObject top0 = CreateCube(depth, 2 * sideLength, depth);
	                    TubeObject top1 = CreateCube(depth, 2 * sideLength, depth);
	                    TubeObject bottom0 = CreateCube(depth, 2 * sideLength, depth);
	                    TubeObject bottom1 = CreateCube(depth, 2 * sideLength, depth);
	                    TubeObject left = CreateCubePillar(depth, sideLength, depth);
						TubeObject right = CreateCubePillar(depth, sideLength, depth);

	                    top0.zRotation = 90;
	                    top1.zRotation = 90;
	                    bottom0.zRotation = 90;
	                    bottom1.zRotation = 90;
	                    
	                    top0.xPosition = -sideLength;
	                    top1.xPosition = sideLength;
	                    top0.yPosition = sideLength / 2 - depth / 2;
	                    top1.yPosition = sideLength / 2 - depth / 2;
	                    bottom0.xPosition = -sideLength;
	                    bottom1.xPosition = sideLength;
	                    bottom0.yPosition = -sideLength / 2 + depth / 2;
	                    bottom1.yPosition = -sideLength / 2 + depth / 2;
	                    left.xPosition = -sideLength * 2 + depth / 2;
                        right.xPosition = sideLength * 2 - depth / 2;

	                    shape.AddChild(top0);
	                    shape.AddChild(top1);
	                    shape.AddChild(bottom0);
	                    shape.AddChild(bottom1);
	                    shape.AddChild(left);
                        shape.AddChild(right);
                    }
                    break;
                case 2: // L
                case 3:
                    {
	                    sideLength = 17 * Mathf.Sin(Mathf.Atan(1 / 1.5f));

						TubeObject left = CreateCubePillar(depth, sideLength, depth);
	                    TubeObject middle = CreateCubePillar(depth, 2 * sideLength, depth);
	                    TubeObject hookLeft = CreateCubePillar(depth, sideLength + depth, depth);
	                    TubeObject top = CreateCubePillar(depth, sideLength, depth);
						TubeObject right = CreateCubePillar(depth, 2 * sideLength, depth);
	                    TubeObject bottom = CreateCubePillar(depth, 3 * sideLength, depth);
						
	                    middle.zRotation = 90;
	                    top.zRotation = 90;
	                    bottom.zRotation = 90;

	                    left.shadowType = ShadowType.radial;
	                    middle.shadowType = ShadowType.radial;
	                    hookLeft.shadowType = ShadowType.radial;
	                    top.shadowType = ShadowType.radial;
	                    right.shadowType = ShadowType.radial;
	                    bottom.shadowType = ShadowType.radial;
	                    
	                    left.xPosition = -sideLength * 1.5f + depth / 2;
                        left.yPosition = -sideLength * .5f;
                        middle.xPosition = -sideLength * .5f + depth / 2;
                        middle.yPosition = -depth / 2;
                        hookLeft.xPosition = sideLength * .5f + depth / 2;
                        hookLeft.yPosition = sideLength * .5f - depth / 2;
                        top.xPosition = sideLength;
                        top.yPosition = sideLength - depth / 2;
                        right.xPosition = sideLength * 1.5f - depth / 2;
	                    bottom.yPosition = -sideLength + depth / 2;
                        
	                    // Made the shape a bit smaller to avoid camera. Adjusting toward tube wall accordingly.
	                    float adjustFromCenter = -4f;
	                    left.yPosition += adjustFromCenter;
	                    middle.yPosition += adjustFromCenter;
	                    hookLeft.yPosition += adjustFromCenter;
	                    top.yPosition += adjustFromCenter;
	                    right.yPosition += adjustFromCenter;
	                    bottom.yPosition += adjustFromCenter;

                        shape.AddChild(left);
                        shape.AddChild(middle);
                        shape.AddChild(hookLeft);
                        shape.AddChild(top);
                        shape.AddChild(right);
                        shape.AddChild(bottom);

                        shape.xScale = (shapeType == 2) ? 1 : -1;
                    }
                    break;
                case 4: // T
                    {
                        sideLength = 20 * Mathf.Sin(Mathf.Atan(1 / 1.5f));

	                    TubeObject left = CreateCubePillar(depth, sideLength - depth * 2, depth);
						TubeObject leftTop = CreateCubePillar(depth, sideLength + depth, depth);
						TubeObject middleLeft = CreateCubePillar(depth, sideLength - depth, depth);
						TubeObject middleTop = CreateCubePillar(depth, sideLength, depth);
						TubeObject middleRight = CreateCubePillar(depth, sideLength - depth, depth);
						TubeObject rightTop = CreateCubePillar(depth, sideLength + depth, depth);
						TubeObject right = CreateCubePillar(depth, sideLength - depth * 2, depth);
	                    TubeObject bottom = CreateCubePillar(depth, sideLength * 3, depth);
	                    
	                    left.shadowType = ShadowType.radial;
	                    leftTop.shadowType = ShadowType.radial;
	                    middleLeft.shadowType = ShadowType.radial;
	                    middleTop.shadowType = ShadowType.radial;
	                    middleRight.shadowType = ShadowType.radial;
	                    rightTop.shadowType = ShadowType.radial;
	                    right.shadowType = ShadowType.radial;
	                    bottom.shadowType = ShadowType.radial;
						
	                    leftTop.zRotation = 90;
	                    middleTop.zRotation = 90;
	                    rightTop.zRotation = 90;
	                    bottom.zRotation = 90;

                        left.xPosition = -sideLength * 1.5f + depth / 2;
                        left.yPosition = -sideLength / 2;
                        leftTop.xPosition = -sideLength + depth / 2;
                        leftTop.yPosition = -depth / 2;
                        middleLeft.xPosition = -sideLength / 2 + depth / 2;
                        middleLeft.yPosition = sideLength / 2 - depth / 2;
                        middleTop.yPosition = sideLength - depth / 2;
                        middleRight.xPosition = sideLength / 2 - depth / 2;
                        middleRight.yPosition = sideLength / 2 - depth / 2;
                        rightTop.xPosition = sideLength - depth / 2;
                        rightTop.yPosition = -depth / 2;
                        right.xPosition = sideLength * 1.5f - depth / 2;
                        right.yPosition = -sideLength / 2;
                        bottom.yPosition = -sideLength + depth / 2;

                        shape.AddChild(left);
                        shape.AddChild(leftTop);
                        shape.AddChild(middleLeft);
                        shape.AddChild(middleTop);
                        shape.AddChild(middleRight);
                        shape.AddChild(rightTop);
                        shape.AddChild(right);
                        shape.AddChild(bottom);

                        shape.yPosition = 20 - sideLength - depth / 2;
                    }
                    break;
                case 5: // S
                case 6: // Z
                    {
                        sideLength = 20 * Mathf.Sin(Mathf.Atan(1 / 1.5f));

	                    TubeObject bottomLeft = CreateCubePillar(depth, sideLength - depth * 2, depth);
						TubeObject bottomTop = CreateCubePillar(depth, sideLength + depth, depth);
						TubeObject topLeft = CreateCubePillar(depth, sideLength - depth, depth);
						TubeObject top = CreateCubePillar(depth, sideLength * 2, depth);
						TubeObject topRight = CreateCubePillar(depth, sideLength - depth * 2, depth);
						TubeObject topBottom = CreateCubePillar(depth, sideLength + depth, depth);
						TubeObject bottomRight = CreateCubePillar(depth, sideLength - depth, depth);
	                    TubeObject bottom = CreateCubePillar(depth, sideLength * 2, depth);
						
	                    bottomLeft.shadowType = ShadowType.radial;
	                    bottomTop.shadowType = ShadowType.radial;
	                    topLeft.shadowType = ShadowType.radial;
	                    top.shadowType = ShadowType.radial;
	                    topRight.shadowType = ShadowType.radial;
	                    topBottom.shadowType = ShadowType.radial;
	                    bottomRight.shadowType = ShadowType.radial;
	                    bottom.shadowType = ShadowType.radial;
	                    
	                    bottomTop.zRotation = 90;
	                    top.zRotation = 90;
	                    topBottom.zRotation = 90;
	                    bottom.zRotation = 90;

                        bottomLeft.xPosition = -sideLength * 1.5f + depth / 2;
                        bottomLeft.yPosition = -sideLength / 2;
                        bottomTop.xPosition = -sideLength + depth / 2;
                        bottomTop.yPosition = -depth / 2;
                        topLeft.xPosition = -sideLength / 2 + depth / 2;
                        topLeft.yPosition = sideLength / 2 - depth / 2;
                        top.xPosition = sideLength / 2;
                        top.yPosition = sideLength - depth / 2;
                        topRight.xPosition = sideLength * 1.5f - depth / 2;
                        topRight.yPosition = sideLength / 2;
                        topBottom.xPosition = sideLength - depth / 2;
                        topBottom.yPosition = depth / 2;
                        bottomRight.xPosition = sideLength / 2 - depth / 2;
                        bottomRight.yPosition = -sideLength / 2 + depth / 2;
                        bottom.xPosition = -sideLength / 2;
                        bottom.yPosition = -sideLength + depth / 2;

                        shape.AddChild(bottomLeft);
                        shape.AddChild(bottomTop);
                        shape.AddChild(topLeft);
                        shape.AddChild(top);
                        shape.AddChild(topRight);
                        shape.AddChild(topBottom);
                        shape.AddChild(bottomRight);
                        shape.AddChild(bottom);

                        shape.xScale = (shapeType == 5) ? 1 : -1;
                    }
                    break;
            }

            shape.tubeAngle = randomAngle;
	        shape.tubeAngleEnd = shape.tubeAngle + 90 * (randomBool ? 1 : -1);
            shape.tubeAngleDuration = 1;
	        shape.tubeAngleCurve = EaseInOutCubic;
            shape.tubeAngleRepeatMode = RepeatMode.pingPong;
	        shape.animationDistance = Random.Range(-40f, 40f);
        }
    }

	public static void PingPongLevel()
    {
        float travelDistance = 600;
        float boxWidth = 24;
        float boxHeight = 24;
        float borderWidth = 2;
        float ballRadius = 2;

        tubeAngle = 0;

        TubeObject startingRing = CreateRing(270, 2f, 0, borderWidth);

	    TubeObject top = CreateCubePillar(borderWidth, boxWidth, borderWidth);
		TubeObject left = CreateCubePillar(borderWidth, boxHeight + borderWidth, borderWidth);
	    TubeObject right = CreateCubePillar(borderWidth, boxHeight + borderWidth, borderWidth);
		
	    top.shadowType = left.shadowType = right.shadowType = ShadowType.radial;
	    top.zRotation = 90;

        top.yPosition = -20 + boxHeight + borderWidth / 2;
        left.yPosition = -20 + boxHeight / 2 + borderWidth / 2;
        right.yPosition = -20 + boxHeight / 2 + borderWidth / 2;
        left.xPosition = -boxWidth / 2 - borderWidth / 2;
        right.xPosition = boxWidth / 2 + borderWidth / 2;

		TubeObject box = CreateContainer();
        box.AddChild(top);
        box.AddChild(left);
        box.AddChild(right);

		TubeObject sphere = CreateSphere(ballRadius * 2);
        sphere.yPosition = top.yPosition - borderWidth / 2 - ballRadius;
        sphere.xPosition = left.xPosition + borderWidth / 2 + ballRadius;

        sphere.yPositionEnd = top.yPosition - borderWidth / 2 - boxHeight + ballRadius;
        sphere.yPositionDuration = Random.Range(.5f, .75f);
        sphere.yPositionRepeatMode = RepeatMode.pingPong;
        sphere.xPositionEnd = right.xPosition - borderWidth / 2 - ballRadius;
        sphere.xPositionDuration = Random.Range(1f, 2f);
        sphere.xPositionRepeatMode = RepeatMode.pingPong;
        sphere.animationDistance = Random.Range(-10000, 10000);

		TubeObject movementContainer = CreateContainer();
        movementContainer.AddChild(startingRing);
        movementContainer.AddChild(box);
        movementContainer.AddChild(sphere);

        movementContainer.zPositionEnd = movementContainer.zPosition + travelDistance;
        movementContainer.zPositionDuration = travelDistance / 100;
        movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;

        z += travelDistance;
    }

    public static void SpiralStaircaseLevel()
    {
        int numberOfObstacles = 4;
	    int numberOfRings = 14;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 150;

            int reverse = randomBool ? 1 : -1;
            float randomAnimationDistance = Random.Range(-10000, 10000);

            for (int k = 0; k < numberOfRings; k++)
            {
	            TubeObject ring = CreateRing(90, 20, 0, 1, 30);
	            //ring.shadowType = ShadowType.directional;
	            ring.shadingType = ShadingType.directional;
	            
                ring.xRotation = 90;

                ring.zRotation = (360f / numberOfRings) * k;
                ring.zRotationEnd = ring.zRotation + 360 * reverse;
                ring.zRotationDuration = 2;
                ring.zRotationRepeatMode = RepeatMode.loop;

                ring.yPosition = -20 + (40f / (numberOfRings + 1)) * (k + 1);

                ring.animationDistance += randomAnimationDistance;
            }
        }
    }

    public static void SquareHolesLevel()
    {
        int numberOfObstacles = 3;

        int numberOfSlats = 3;
        float slatWidth = 40f / numberOfSlats;
        float gapSize = 10;
        float slatLength = 40 - gapSize;

        for (int i = 0; i < numberOfObstacles; i++)
        {
	        z += (i == 0) ? 0 : 200;
	        RandomizeTubeAngle();
	        
	        float animationDistance = randomBool ? 5 : 220;

            for (int j = 0; j < numberOfSlats; j++)
            {
	            TubeObject slatA = CreateCubePillar(slatWidth, slatLength, 1);
	            TubeObject slatB = CreateCubePillar(slatWidth, slatLength, 1);
	            
	            slatA.shadowType = slatB.shadowType = ShadowType.radial;

                slatA.xPosition = slatB.xPosition = -20 + slatWidth / 2 + slatWidth * j;
                slatA.yPosition = gapSize / 2;
                slatB.yPosition = slatA.yPosition + 40;

                slatA.yPositionEnd = slatA.yPosition - 40;
                slatB.yPositionEnd = slatB.yPosition - 40;
	            slatA.yPositionDuration = slatB.yPositionDuration = 3;
	            slatA.yPositionRepeatMode = slatB.yPositionRepeatMode = RepeatMode.loop;
                
	            // only works for these numberOfSlats and gapSize values
	            slatA.animationDistance = slatB.animationDistance = (100 * j) + animationDistance;
            }
        }
    }

    public static void FidgetSpinnersLevel()
    {
        int numberOfObstacles = 20;
        float centerRingHoleRadius = 1f;
        float centerRingRadius = 3;
        float centerRingHeight = 2;
        float outerRingRadius = 4;
        float outerRingHoleRadius = 1.5f;
        float outerRingHeight = 1;
        float outerRingPosition = 8;
        float connecterWidth = 4;
        float connecterHeight = 3;

        for (int i = 0; i < numberOfObstacles; i++)
        {
	        z += (i == 0) ? 0 : 45;

            TubeObject container = CreateContainer();

            TubeObject centerRing = CreateRing(360, 20 - centerRingHoleRadius, 20 - centerRingRadius, centerRingHeight, 60);
            TubeObject outerRing1 = CreateRing(360, 20 - outerRingHoleRadius, 20 - outerRingRadius, outerRingHeight, 60);
            TubeObject outerRing2 = CreateRing(360, 20 - outerRingHoleRadius, 20 - outerRingRadius, outerRingHeight, 60);
            TubeObject outerRing3 = CreateRing(360, 20 - outerRingHoleRadius, 20 - outerRingRadius, outerRingHeight, 60);
            TubeObject connector1 = CreateCube(connecterWidth, connecterHeight, outerRingHeight);
            TubeObject connector2 = CreateCube(connecterWidth, connecterHeight, outerRingHeight);
            TubeObject connector3 = CreateCube(connecterWidth, connecterHeight, outerRingHeight);

            outerRing1.yPosition = outerRingPosition;
            outerRing2.yPosition = outerRingPosition;
            outerRing3.yPosition = outerRingPosition;

            connector1.yPosition = (outerRingPosition - outerRingHoleRadius) / 2;
            connector2.yPosition = (outerRingPosition - outerRingHoleRadius) / 2;
            connector3.yPosition = (outerRingPosition - outerRingHoleRadius) / 2;
            outerRing1.tubeAngle = connector1.tubeAngle = 0;
            outerRing2.tubeAngle = connector2.tubeAngle = 120;
            outerRing3.tubeAngle = connector3.tubeAngle = 240;

            container.AddChild(centerRing);
            container.AddChild(outerRing1);
            container.AddChild(outerRing2);
            container.AddChild(outerRing3);
            container.AddChild(connector1);
            container.AddChild(connector2);
            container.AddChild(connector3);

            container.xRotation = 90;
            container.yPosition = -16;
            container.tubeAngle = randomAngle;

            container.zRotationEnd = container.zRotation + 360f * (i % 2 == 0 ? 1 : -1);
            container.zRotationDuration = 1;
            container.tubeAngleEnd = container.tubeAngle + Random.Range(180f, 360f) * (randomBool ? 1 : -1);
            container.tubeAngleDuration = 2;
            container.tubeAngleRepeatMode = RepeatMode.pingPong;
            container.tubeAngleCurve = EaseInOutQuad;
        }
    }




	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas' Levels Batch 6
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void ThreeTwoGatesLevel()
    {
        int numberOfObstacles = 4;
        float gapAngle = 60;
        float ringHeight = 4;
        float ringDepth = 1;
        float ringDistance = 30;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 175;

            int reverseRotation = randomBool ? 1 : -1;

            TubeObject ringA1 = CreateRing(120 - gapAngle, ringHeight, 0, ringDepth);
            TubeObject ringA2 = CreateRing(120 - gapAngle, ringHeight, 0, ringDepth);
            TubeObject ringA3 = CreateRing(120 - gapAngle, ringHeight, 0, ringDepth);
            ringA2.tubeAngle += 120;
            ringA3.tubeAngle += 240;

            TubeObject ringB1 = CreateRing(180 - gapAngle, ringHeight, 0, ringDepth);
            TubeObject ringB2 = CreateRing(180 - gapAngle, ringHeight, 0, ringDepth);
            ringB2.tubeAngle += 180;

            TubeObject ringA = CreateContainer();
            ringA.AddChild(ringA1);
            ringA.AddChild(ringA2);
            ringA.AddChild(ringA3);

            TubeObject ringB = CreateContainer();
            ringB.AddChild(ringB1);
            ringB.AddChild(ringB2);

            TubeObject movementContainer = CreateContainer();
            movementContainer.AddChild(ringB);

            movementContainer.zPosition -= ringDistance;
            movementContainer.zPositionEnd = movementContainer.zPosition + ringDistance * 2;
            movementContainer.zPositionDuration = .5f;
	        movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
	        movementContainer.zPositionCurve = EaseInOutQuad;
            movementContainer.animationDistance = 50;

            ringA.tubeAngleEnd = randomAngle;
            ringA.tubeAngle = ringA.tubeAngleEnd - 360 * reverseRotation;
            ringA.tubeAngleDuration = 6;
            ringA.animationDistance = ringA.tubeAngleDuration * 100;

            ringB.tubeAngleEnd = ringA.tubeAngle + 90;
            ringB.tubeAngle = ringB.tubeAngleEnd - 360 * reverseRotation;
            ringB.tubeAngleDuration = 9;
            ringB.animationDistance = ringB.tubeAngleDuration * 100;
        }
    }

    public static void RingDropLevel()
    {
        int numberOfObstacles = 6;

        float dropDistance = 140;
        float startingScale = .25f;
        float dropDuration = .4f;
        float expandDuration = .4f;
        float rotation = 0;

        tubeAngle = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 120;

            TubeObject ring = CreateRing(315, 10, 0, 2 * startingScale);
            ring.xScale = ring.yScale = ring.zScale = startingScale;
            ring.yPosition = 20 - (startingScale * 20);
            ring.yPositionEnd = -20 + (startingScale * 20);
            ring.yPositionDuration = dropDuration;
            ring.yPositionRepeatMode = RepeatMode.triggeredOnce;
            ring.yPositionCurve = EaseInQuad;
            ring.animationDistance = dropDistance - i * 5;

            TubeObject rotationContainer = CreateContainer();

	        // First ring always expands, second ring always bounces.
	        if (i != 1 && (i == 0 || randomBool))
            {
                TubeObject resetContainer = CreateContainer();
                resetContainer.AddChild(ring);
                resetContainer.yPositionEnd = -ring.yPositionEnd;
                resetContainer.yPositionDuration = expandDuration;
                resetContainer.yPositionRepeatMode = RepeatMode.triggeredOnce;
                resetContainer.yPositionCurve = EaseOutQuart;
                resetContainer.animationDistance = ring.animationDistance - dropDuration * 100;

                TubeObject expandContainer = CreateContainer();
                expandContainer.AddChild(resetContainer);
                expandContainer.xScale = expandContainer.yScale = expandContainer.zScale = 1;
                expandContainer.xScaleEnd = expandContainer.yScaleEnd = expandContainer.zScaleEnd = 1 / startingScale;
                expandContainer.xScaleDuration = expandContainer.yScaleDuration = expandContainer.zScaleDuration = expandDuration;
                expandContainer.xScaleRepeatMode = expandContainer.yScaleRepeatMode = expandContainer.zScaleRepeatMode = RepeatMode.triggeredOnce;
                expandContainer.xScaleCurve = expandContainer.yScaleCurve = expandContainer.zScaleCurve = EaseInQuad;
                expandContainer.animationDistance = ring.animationDistance - dropDuration * 100;

                rotationContainer.AddChild(expandContainer);
            }
            else
            {
            	ring.animationDistance = dropDistance - 20;
            	TubeObject bounceContainer = CreateContainer();
	            bounceContainer.AddChild(ring);
	            bounceContainer.yPositionEnd = 13;
	            bounceContainer.yPositionDuration = .4f;
	            bounceContainer.yPositionRepeatMode = RepeatMode.triggeredPingPong;
	            bounceContainer.yPositionCurve = EaseOutCubic;
	            bounceContainer.animationDistance = ring.animationDistance - dropDuration * 100;
            	
                rotationContainer.AddChild(bounceContainer);
            }

            rotation += 30 * (randomBool ? 1 : -1);

            rotationContainer.tubeAngle = rotation;
        }
    }

    public static void BallSpitterLevel()
    {
        float ballDistance = 100;
        float travelDistance = 750;
        float mainSphereRadius = 10f;
        float mainSphereVolume = (4f / 3f) * Mathf.PI * Mathf.Pow(mainSphereRadius, 3);
        float smallSphereRadius = 2f;
        float smallSphereVolume = (4f / 3f) * Mathf.PI * Mathf.Pow(smallSphereRadius, 3);
        int numberOfSmallSpheres = (int)(mainSphereVolume / smallSphereVolume) - 1; // minus 1 because the main sphere will shrink and become the last small sphere

	    TubeObject mainSphere = CreateSphere(mainSphereRadius * 2);
	    mainSphere.shadowType = ShadowType.none;
        mainSphere.xScale = mainSphere.yScale = mainSphere.zScale = mainSphereRadius * 2;
        mainSphere.xScaleEnd = mainSphere.yScaleEnd = mainSphere.zScaleEnd = 0;
        mainSphere.xScaleDuration = mainSphere.yScaleDuration = mainSphere.zScaleDuration = travelDistance / 100;
        mainSphere.xScaleRepeatMode = mainSphere.yScaleRepeatMode = mainSphere.zScaleRepeatMode = RepeatMode.triggeredOnce;
        mainSphere.xScaleCurve = mainSphere.yScaleCurve = mainSphere.zScaleCurve = EaseInCubic;
	    mainSphere.zPositionEnd = mainSphere.zPosition + travelDistance;
	    mainSphere.zPositionDuration = travelDistance / 100;
	    mainSphere.zPositionRepeatMode = RepeatMode.triggeredOnce;
	    mainSphere.animationDistance = ballDistance;

        for (int i = 0; i < numberOfSmallSpheres; i++)
        {
	        //float waitDistance = ((float)(i + 1) / numberOfSmallSpheres) * travelDistance;
	        //float waitTime = travelDistance / 100 * ((float)(i + 1) / numberOfSmallSpheres);

            TubeObject smallSphere = CreateSphere(smallSphereRadius * 2);
            smallSphere.tubeAngle = randomAngle;
            smallSphere.zPosition += ((float)(i + 1) / numberOfSmallSpheres) * travelDistance + mainSphereRadius * 2;
            smallSphere.xScale = smallSphere.yScale = smallSphere.zScale = 0;
            smallSphere.xScaleEnd = smallSphere.yScaleEnd = smallSphere.zScaleEnd = smallSphereRadius * 2;
            smallSphere.xScaleDuration = smallSphere.yScaleDuration = smallSphere.zScaleDuration = .1f;
            smallSphere.xScaleRepeatMode = smallSphere.yScaleRepeatMode = smallSphere.zScaleRepeatMode = RepeatMode.triggeredOnce;
	        smallSphere.yPositionEnd = -19f;
            smallSphere.yPositionDuration = .25f;
            smallSphere.yPositionRepeatMode = RepeatMode.triggeredOnce;
            smallSphere.yPositionCurve = EaseInQuad;
            smallSphere.animationDistance = ballDistance;
        }

        z += travelDistance;
    }

    public static void TrappedLevel()
    {
        float travelDistance = 600;
        float rotationTime = 6;

        tubeAngle = 0;

	    CreateRing(315, 2f, 0, 1);
        TubeObject ring = CreateRing(360, 5, 0, 1);

        ring.xScale = ring.yScale = .4f;
        ring.yPosition = -18;

        ring.xRotationEnd = ring.xRotation + 180;
        ring.xRotationDuration = rotationTime;
        ring.xRotationRepeatMode = RepeatMode.triggeredOnce;

        ring.animationDistance = 15;

        TubeObject movementContainer = CreateContainer();
        movementContainer.AddChild(ring);

        movementContainer.zPositionEnd = movementContainer.zPosition + travelDistance;
        movementContainer.zPositionDuration = travelDistance / 100;
        movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;

        movementContainer.tubeAngleEnd = movementContainer.tubeAngle + Random.Range(30, 50) * (randomBool ? 1 : -1);
        movementContainer.tubeAngleDuration = travelDistance / 100 / 2;
        movementContainer.tubeAngleRepeatMode = RepeatMode.triggeredPingPong;
        movementContainer.tubeAngleCurve = EaseInOutQuad;

        z += travelDistance;
    }

	public static void CubeWarpLevel()
    {
        int numberOfObstacles = 10;
        int numberOfLines = 4;
        float cubeSize = 3.0f;

        float gap = (34 / (numberOfLines - 1));

        float rotation = randomAngle;
        int flip = 1;
        int animationDistance = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0 || i % 2 == 1) ? 0 : 160;

            rotation = (i % 2 == 0) ? randomAngle : (rotation + 180);
            flip = (i % 2 == 0) ? (randomBool ? 1 : -1) : flip;
            animationDistance = (i % 2 == 0) ? Random.Range(-10000, 10000) : animationDistance;

            for (int j = 0; j < numberOfLines; j++)
            {
                TubeObject cube = CreateCube(cubeSize, cubeSize, cubeSize);
                
                cube.xPosition = -17 + gap * j;

                cube.yPosition = 20 * Mathf.Sin(Mathf.Deg2Rad * (90 - Mathf.Rad2Deg * Mathf.Asin(Mathf.Abs(cube.xPosition) / 20))) - cubeSize;
                cube.yPositionEnd = -cube.yPosition;
                cube.yPositionDuration = 1.5f;
                cube.yPositionRepeatMode = RepeatMode.pingPong;
                cube.yPositionCurve = EaseInOutQuad;

                cube.xRotation = 90;
                cube.xRotationEnd = 90 + 360 * (randomBool ? 1 : -1);
                cube.xRotationDuration = 1;

                cube.xScale = cube.yScale = cube.zScale = cubeSize;
                cube.xScaleEnd = cube.yScaleEnd = cube.zScaleEnd = cubeSize / 2;
                cube.xScaleDuration = cube.yScaleDuration = cube.zScaleDuration = cube.yPositionDuration / 2;
                cube.xScaleRepeatMode = cube.yScaleRepeatMode = cube.zScaleRepeatMode = RepeatMode.pingPong;
                cube.xScaleCurve = cube.yScaleCurve = cube.zScaleCurve = EaseInOutQuad;


                cube.tubeAngle = rotation;
                cube.tubeAngleEnd = rotation + 360 * flip;
                cube.tubeAngleDuration = 6;
                cube.animationDistance = animationDistance;
            }
        }
    }

    public static void PhasersLevel()
    {
        int numberOfObstacles = 20;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 50;

            TubeObject ring = CreateRing(360, 4, 0, 1);
            ring.yRotation = 90;
            ring.xPosition = -21;

            ring.xPositionEnd = 21;
            ring.xPositionDuration = 2;

            ring.xScale = ring.yScale = .5f;
            ring.xScaleEnd = ring.yScaleEnd = 1;
            ring.xScaleDuration = ring.yScaleDuration = ring.xPositionDuration / 2;
            ring.xScaleRepeatMode = ring.yScaleRepeatMode = RepeatMode.pingPong;
            ring.animationDistance = -i * 25;
        }
    }

    public static void TrickyTrianglesLevel()
    {
        int numberOfObstacles = 4;
	    int numberOfSegments = 14;
        float segmentWidth = 0.75f;
        float segmentAngle = 360f / numberOfSegments;
        float edgeLength = Mathf.Sqrt(800 - 800 * Mathf.Cos(Mathf.Deg2Rad * segmentAngle));
	    float segmentLength = edgeLength * 1.6f * Mathf.Cos(Mathf.Deg2Rad * 45);

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 150;

            TubeObject container = CreateContainer();

            for (int j = 0; j < numberOfSegments; j++)
            {
                TubeObject segment = CreateCube(segmentWidth, segmentLength, segmentWidth);
                segment.tubeAngle = segmentAngle * j;
                segment.yPosition = -20 + segmentLength / 2;

	            segment.zRotation += 30f * (j % 2 == 0 ? 1 : -1);
                segment.zRotationEnd = segment.zRotation * -1;
                segment.zRotationDuration = 1;
                segment.zRotationRepeatMode = RepeatMode.pingPong;

                container.AddChild(segment);
            }

            container.tubeAngle = randomAngle;
            container.tubeAngleEnd = container.tubeAngle + 360 * (i % 2 == 0 ? 1 : -1);
            container.tubeAngleDuration = Random.Range(6f, 10f);

        }
    }

	public static void CheeriosLevel()
    {
        int numberOfObstacles = 20;
        float ringSize = 8;
        float ringWidth = 2;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 30;

            TubeObject ring = CreateRing(360, 20 - ringWidth, 20 - ringSize / 2, ringWidth);
            ring.yPosition = 20 - ringSize / 2;

            ring.tubeAngle = randomAngle;
            ring.tubeAngleEnd = ring.tubeAngle + 540 * (randomBool ? 1 : -1);
            ring.tubeAngleDuration = 3;
            ring.tubeAngleRepeatMode = RepeatMode.pingPong;
            ring.tubeAngleCurve = EaseInOutQuad;
            ring.animationDistance = 350;
        }
    }

    public static void ThumpersLevel()
    {
        int numberOfObstacles = 3;
        float thumperWidth = 10;
	    float thumperHeight = 41;
        float sideGap = 3f;
        float innerGap = (40 - (3 * thumperWidth) - (2 * sideGap)) / 2;

        for (int i = 0; i < numberOfObstacles; i++)
        {
	        z += (i == 0) ? 0 : 250;
            
	        bool reverse = randomBool;
	        int goodPillar = Random.Range(0, 3);
	        
            for (int j = 0; j < 3; j++)
            {
            	int pillarIndex = ((reverse ? 2 - j : j) + goodPillar) % 3;
            	float animationDistance = 60 + (pillarIndex * 40);
            	
	            TubeObject cylinder1 = CreateCylinderPillar(thumperWidth, thumperHeight / 2, thumperWidth);
	            cylinder1.shadingType = ShadingType.radial;
	            cylinder1.shadowType = ShadowType.radial;
	            //cylinder1.shadowType = j == 1 ? ShadowType.none : ShadowType.radial;
	            cylinder1.xPosition = -20 + sideGap + thumperWidth / 2 + (thumperWidth + innerGap) * j;
	            cylinder1.yPosition = 30 - (thumperHeight / 4);
                cylinder1.yPositionEnd =  - (thumperHeight / 4);
                cylinder1.yPositionDuration = .75f;
                cylinder1.yPositionRepeatMode = RepeatMode.pingPong;
                cylinder1.yPositionCurve = EaseInOutQuad;
	            cylinder1.animationDistance = animationDistance;
                
	            TubeObject cylinder2 = CreateCylinderPillar(thumperWidth, thumperHeight / 2, thumperWidth);
	            cylinder2.shadingType = ShadingType.radial;
	            cylinder2.shadowType = ShadowType.radial;
	            //cylinder2.shadowType = j == 1 ? ShadowType.none : ShadowType.radial;
	            cylinder2.xPosition = -20 + sideGap + thumperWidth / 2 + (thumperWidth + innerGap) * j;
	            cylinder2.yPosition = 30 + (thumperHeight / 4);
	            cylinder2.yPositionEnd =  (thumperHeight / 4);
	            cylinder2.yPositionDuration = .75f;
	            cylinder2.yPositionRepeatMode = RepeatMode.pingPong;
	            cylinder2.yPositionCurve = EaseInOutQuad;
	            cylinder2.animationDistance = animationDistance;
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas' Levels Batch 5
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void ExtrudedRingLevel()
    {
        int numberOfObstacles = 8;
        int numberOfRingSegments = 4;
        float obstacleDistance = 110;
        float animationTime = 1.25f;

        int oldExtrusionRing = -1;
        int newExtrusionRing = Random.Range(0, numberOfRingSegments);

        tubeAngle = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : obstacleDistance;

            if (i > 0)
            {
                newExtrusionRing = oldExtrusionRing + Random.Range(1, 3) * (randomBool ? 1 : -1);

                if (newExtrusionRing >= numberOfRingSegments)
                {
                    newExtrusionRing -= numberOfRingSegments;
                }

                if (newExtrusionRing < 0)
                {
                    newExtrusionRing += numberOfRingSegments;
                }
            }

            for (int j = 0; j < numberOfRingSegments; j++)
            {
                TubeObject ring = CreateRing(360 / numberOfRingSegments, 10, 0, 2, 60);
                ring.tubeAngle = (360 / numberOfRingSegments) * j;

                TubeObject showContainer = CreateContainer();

                if (j == newExtrusionRing)
                {
                    ring.arcEnd = (i == numberOfObstacles - 1) ? ring.arc : 360;
                    ring.arcDuration = animationTime * .75f;
                    ring.arcRepeatMode = RepeatMode.triggeredOnce;
                    ring.arcCurve = EaseInOutQuad;
                    ring.animationDistance = obstacleDistance * .75f;

                    TubeObject hideContainer = CreateContainer();
                    hideContainer.AddChild(ring);

                    hideContainer.xScale = hideContainer.yScale = 1;
                    hideContainer.xScaleEnd = hideContainer.yScaleEnd = (i == numberOfObstacles - 1) ? 1 : 10;
                    hideContainer.xScaleDuration = hideContainer.yScaleDuration = .0000001f;
                    hideContainer.xScaleRepeatMode = hideContainer.yScaleRepeatMode = RepeatMode.triggeredOnce;
                    hideContainer.animationDistance =  0;

                    showContainer.AddChild(hideContainer);

                    TubeObject movementContainer = CreateContainer();
                    movementContainer.AddChild(showContainer);

                    movementContainer.zPositionEnd = movementContainer.zPosition + obstacleDistance;
                    movementContainer.zPositionDuration = animationTime;
                    movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
                    movementContainer.zPositionCurve = EaseInOutQuad;
                    movementContainer.animationDistance = obstacleDistance;                   
                }
                else
                {
                        showContainer.AddChild(ring);
                }

                if (i > 0)
                {
                    showContainer.xScale = showContainer.yScale = 10;
                    showContainer.xScaleEnd = showContainer.yScaleEnd = 1;
                    showContainer.xScaleDuration = showContainer.yScaleDuration = .0000001f;
                    showContainer.xScaleRepeatMode = showContainer.yScaleRepeatMode = RepeatMode.triggeredOnce;
                    showContainer.animationDistance = obstacleDistance;
                }
            }

            oldExtrusionRing = newExtrusionRing;
        }

        z += 100;
    }

	public static void TakeCoverLevel()
    {
        int numberOfVolleys = 4;
        int numberOfRows = 10;
        float sphereWidth = 2.5f;
        float gap = (40 - numberOfRows * sphereWidth) / (numberOfRows - 1);
        float spawnDistance = 200;
        float barrierDistance = 80;
        float barrierAngle = 60;
        float barrierHeight = 12;
        float sphereAnimationDistance = 250;

        for (int i = 0; i < numberOfVolleys; i++)
        {
            z += (i == 0) ? 0 : 125;

            tubeAngle = 0;

            TubeObject rotationContainer = CreateContainer();

            for (int y = 0; y < numberOfRows; y++)
            {
                for (int x = 0; x < numberOfRows; x++)
                {
                    TubeObject movementContainer = CreateContainer();
                    rotationContainer.AddChild(movementContainer);

                    TubeObject sphere = CreateSphere(sphereWidth);
                    movementContainer.AddChild(sphere);

                    sphere.xPosition = -20 + sphereWidth / 2 + (sphereWidth + gap) * x;
                    sphere.yPosition = -20 + sphereWidth / 2 + (sphereWidth + gap) * y;
                    sphere.xScale = sphere.yScale = sphere.zScale = 0;
                    sphere.xScaleEnd = sphere.yScaleEnd = sphere.zScaleEnd = sphereWidth;
                    sphere.xScaleDuration = sphere.yScaleDuration = sphere.zScaleDuration = .5f;
                    sphere.xScaleRepeatMode = sphere.yScaleRepeatMode = sphere.zScaleRepeatMode = RepeatMode.triggeredOnce;
                    sphere.xScaleCurve = sphere.yScaleCurve = sphere.zScaleCurve = EaseOutQuad;
                    sphere.animationDistance = sphereAnimationDistance;

                    float radius = Mathf.Sqrt(sphere.xPosition * sphere.xPosition + sphere.yPosition * sphere.yPosition);
                    float angle = Mathf.Atan(-sphere.yPosition / sphere.xPosition) * Mathf.Rad2Deg;

                    if (sphere.xPosition < 0)
                    {
                        angle += 180;
                    }
                    else if (sphere.yPosition > 0)
                    {
                        angle += 360;
                    }

                    movementContainer.zPosition = movementContainer.zPosition + spawnDistance;

                    if (radius <= barrierHeight - sphereWidth || (angle < 270 - barrierAngle / 2 || angle > 270 + barrierAngle / 2))
                    {
                        movementContainer.zPositionEnd = movementContainer.zPosition - spawnDistance;
                        movementContainer.zPositionDuration = 1;
                    }
                    else
                    {
                        movementContainer.zPositionEnd = movementContainer.zPosition - barrierDistance;
                        movementContainer.zPositionDuration = (barrierDistance) / spawnDistance;
                    }

                    movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
                    movementContainer.animationDistance = 200;
                }
            }

            TubeObject barrier = CreateRing(barrierAngle, barrierHeight, 0, 4);
            rotationContainer.AddChild(barrier);

            barrier.zPosition = barrier.zPosition + (spawnDistance - barrierDistance);
            barrier.yPosition = 13;
            barrier.yPositionEnd = 0;
            barrier.yPositionDuration = .5f;
            barrier.yPositionRepeatMode = RepeatMode.triggeredOnce;
            barrier.animationDistance = sphereAnimationDistance - barrierDistance;

            rotationContainer.zRotation = randomAngle;
        }

        z += spawnDistance;
    }

	public static void CrazyCubesLevel()
    {
        int numberOfObstacles = 8;
        float edgeWidth = 2f;
        float edgeLength = 31; //Mathf.Sqrt(800);

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 100;

            float rotationDuration = 6;

	        TubeObject frontTop = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
            TubeObject frontBottom = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
            TubeObject backTop = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
            TubeObject backBottom = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
            TubeObject frontLeft = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
            TubeObject frontRight = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
            TubeObject backLeft = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
	        TubeObject backRight = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
	        TubeObject topLeft = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
	        TubeObject topRight = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
	        TubeObject bottomLeft = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);
	        TubeObject bottomRight = CreateCubePillar(edgeWidth, edgeLength, edgeWidth);

            TubeObject cubeContainer = CreateContainer();
            cubeContainer.xScale = cubeContainer.yScale = cubeContainer.zScale = 1;
            cubeContainer.AddChild(frontTop);
            cubeContainer.AddChild(frontBottom);
            cubeContainer.AddChild(backTop);
            cubeContainer.AddChild(backBottom);
            cubeContainer.AddChild(frontLeft);
            cubeContainer.AddChild(frontRight);
            cubeContainer.AddChild(backLeft);
            cubeContainer.AddChild(backRight);
	        cubeContainer.AddChild(topLeft);
	        cubeContainer.AddChild(topRight);
	        cubeContainer.AddChild(bottomLeft);
	        cubeContainer.AddChild(bottomRight);

	        frontTop.shadowType = ShadowType.radial;
	        //frontTop.shadingType = ShadingType.radial;
	        frontTop.yPosition = edgeLength / 2 - edgeWidth / 2;
	        frontTop.zPosition = frontTop.zPosition - (edgeLength - edgeWidth) / 2;
	        frontTop.zRotation = 90;
	        frontBottom.shadowType = ShadowType.radial;
	        frontBottom.shadingType = ShadingType.radial;
	        frontBottom.yPosition = -edgeLength / 2 + edgeWidth / 2;
            frontBottom.zPosition = frontBottom.zPosition - (edgeLength - edgeWidth) / 2;
	        frontBottom.zRotation = 90;
	        backTop.shadowType = ShadowType.radial;
	        //backTop.shadingType = ShadingType.radial;
	        backTop.yPosition = edgeLength / 2 - edgeWidth / 2;
            backTop.zPosition = backTop.zPosition + (edgeLength - edgeWidth) / 2;
	        backTop.zRotation = 90;
	        backBottom.shadowType = ShadowType.radial;
	        backBottom.shadingType = ShadingType.radial;
	        backBottom.yPosition = -edgeLength / 2 + edgeWidth / 2;
            backBottom.zPosition = backBottom.zPosition + (edgeLength - edgeWidth) / 2;
	        backBottom.zRotation = 90;
	        frontLeft.shadowType = ShadowType.radial;
	        //frontLeft.shadingType = ShadingType.radial;
	        frontLeft.xPosition = -edgeLength / 2 + edgeWidth / 2;
            frontLeft.zPosition = frontLeft.zPosition - (edgeLength - edgeWidth) / 2;
	        frontRight.shadowType = ShadowType.radial;
	        frontRight.xPosition = edgeLength / 2 - edgeWidth / 2;
            frontRight.zPosition = frontRight.zPosition - (edgeLength - edgeWidth) / 2;
	        backLeft.shadowType = ShadowType.radial;
	        //backLeft.shadingType = ShadingType.radial;
	        backLeft.xPosition = -edgeLength / 2 + edgeWidth / 2;
            backLeft.zPosition = backLeft.zPosition + (edgeLength - edgeWidth) / 2;
	        backRight.shadowType = ShadowType.radial;
	        backRight.xPosition = edgeLength / 2 - edgeWidth / 2;
	        backRight.zPosition = backRight.zPosition + (edgeLength - edgeWidth) / 2;
	        topLeft.shadowType = ShadowType.radial;
	        //topLeft.shadingType = ShadingType.radial;
	        topLeft.xPosition = -edgeLength / 2 + edgeWidth / 2;
            topLeft.yPosition = edgeLength / 2 - edgeWidth / 2;
	        topLeft.zPosition = topLeft.zPosition;
	        topLeft.xRotation = 90;
	        topRight.shadowType = ShadowType.radial;
	        //topRight.shadingType = ShadingType.radial;
	        topRight.xPosition = edgeLength / 2 - edgeWidth / 2;
            topRight.yPosition = edgeLength / 2 - edgeWidth / 2;
            topRight.zPosition = topRight.zPosition;
	        topRight.xRotation = 90;
	        bottomLeft.shadowType = ShadowType.radial;
	        //bottomLeft.shadingType = ShadingType.radial;
	        bottomLeft.xPosition = -edgeLength / 2 + edgeWidth / 2;
            bottomLeft.yPosition = -edgeLength / 2 + edgeWidth / 2;
            bottomLeft.zPosition = bottomLeft.zPosition;
	        bottomLeft.xRotation = 90;
	        bottomRight.shadowType = ShadowType.radial;
	        //bottomRight.shadingType = ShadingType.radial;
	        bottomRight.xPosition = edgeLength / 2 - edgeWidth / 2;
            bottomRight.yPosition = -edgeLength / 2 + edgeWidth / 2;
	        bottomRight.zPosition = bottomRight.zPosition;
	        bottomRight.xRotation = 90;

            cubeContainer.tubeAngle = randomAngle;
            //cubeContainer.xRotation = cubeContainer.xRotation + 360 * (randomBool ? 1 : -1);
            //cubeContainer.yRotation = cubeContainer.yRotation + 360 * (randomBool ? 1 : -1);
            cubeContainer.zRotation = cubeContainer.zRotation + 360 * (randomBool ? 1 : -1);
            //cubeContainer.xRotationDuration = cubeContainer.yRotationDuration = cubeContainer.zRotationDuration = rotationDuration;
			cubeContainer.zRotationDuration = rotationDuration;

            TubeObject squishContainer = CreateContainer();
            squishContainer.xScale = squishContainer.yScale = squishContainer.zScale = 1;

            squishContainer.AddChild(cubeContainer);

            squishContainer.xScaleEnd = squishContainer.yScaleEnd = squishContainer.zScaleEnd = squishContainer.xScale * 0.45f;
            squishContainer.xScaleDuration = squishContainer.yScaleDuration = squishContainer.zScaleDuration = rotationDuration / 8;
            squishContainer.xScaleRepeatMode = squishContainer.yScaleRepeatMode = squishContainer.zScaleRepeatMode = RepeatMode.pingPong;
            squishContainer.xScaleCurve = squishContainer.yScaleCurve = squishContainer.zScaleCurve = EaseOutQuad;

            squishContainer.animationDistance = cubeContainer.animationDistance = 0;
        }
    }

	public static void SlalomLevel()
    {
        int numberOfObstacles = 25;
        float[] angles = new float[numberOfObstacles];

        angles[0] = randomBool ? 0 : 180;
        angles[1] = randomBool ? 0 : 180;

        for (int i = 2; i < numberOfObstacles; i++)
        {
            if (angles[i - 2] == 0 && angles[i - 1] == 0)
            {
                angles[i] = 180;
            }
            else if (angles[i - 2] == 180 && angles[i - 1] == 180)
            {
                angles[i] = 0;
            }
            else
            {
                angles[i] = randomBool ? 0 : 180;

            }
        }

        int rotation = randomBool ? 1 : -1;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 55 - i;

            TubeObject container = CreateContainer();

            TubeObject ring = CreateRing(180, 1, 0, 1);
            container.AddChild(ring);

            ring.tubeAngle = angles[i];
            ring.topEnd = 4;
            ring.topDuration = Random.Range(.5f, 1.0f);
            ring.topRepeatMode = RepeatMode.pingPong;
            ring.topCurve = EaseInOutQuad;
            ring.animationDistance = Random.Range(-10000, 10000);

            container.tubeAngleEnd = container.tubeAngle + 45 * rotation;
            container.tubeAngleDuration = 1;
            container.tubeAngleRepeatMode = RepeatMode.pingPong;
            container.tubeAngleCurve = EaseInOutQuad;
        }
    }

	public static void DoubleRippleLevel()
    {
        int numberOfObstacles = 4;
        int numberOfSources = 2;
        int numberOfRings = 3;
		int animationOffset = randomBool ? 10 : 70;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 180;

            float angle = randomAngle;

            if (animationOffset == 10) {animationOffset = 70;} else { animationOffset = 10;}

            if (i == numberOfObstacles - 1) { numberOfRings += 1;}

            for (int j = 0; j < numberOfSources; j++)
            {
                for (int k = 0; k < numberOfRings; k++)
                {
                    TubeObject ring = CreateRing(360, 1, 0, 8);
                    ring.tubeAngle = j * 360 / numberOfSources + angle;
                    ring.xPosition = 20 / 3.0f;
                    ring.xScale = ring.yScale = ring.zScale = 0;

                    ring.xScaleEnd = ring.yScaleEnd = ring.zScaleEnd = 2;
                    ring.xScaleDuration = ring.yScaleDuration = ring.zScaleDuration = 10;
                    ring.xScaleRepeatMode = ring.yScaleRepeatMode = ring.zScaleRepeatMode = RepeatMode.loop;
                    ring.animationDistance = k * (100 * ring.xScaleDuration / numberOfRings) + animationOffset;

                }
            }
        }
    }

	public static void JellyfishLevel()
    {
	    tubeAngle = 0;
	    int numberOfJellyfish = 6;
        float bodyWidthMax = 15f;
        float bodyWidthMin = 10f;
        float bodyHeightMax = 8f;
        float bodyHeightMin = 3f;
        int numberOfTentacles = 7;
        int numberOfSpheresPerTentacle = 10;
        float sphereSize = 1.25f;
        float sphereGap = 2.25f;
	    float jellyfishYPosition = -21 - bodyHeightMax / 2;
	    float animationDuration = .5f;
	    float randomTubeAngle = randomAngle;

        for (int i = 0; i < numberOfJellyfish; i++)
        {
            z += (i == 0) ? 0 : 50;

            TubeObject jellyfish = CreateContainer();

	        TubeObject body = CreateEllipsoid(bodyWidthMax, bodyWidthMax, bodyWidthMax);
            jellyfish.AddChild(body);
            body.xScale = bodyWidthMax;
            body.xScaleEnd = bodyWidthMin;
            body.xScaleDuration = animationDuration;
            body.xScaleRepeatMode = RepeatMode.pingPong;
            body.xScaleCurve = EaseOutQuad;
            body.yScale = bodyHeightMin;
            body.yScaleEnd = bodyHeightMax;
            body.yScaleDuration = animationDuration;
            body.yScaleRepeatMode = RepeatMode.pingPong;
            body.yScaleCurve = EaseOutQuad;

            for (int j = 0; j < numberOfTentacles; j++)
            {
	            TubeObject rotationContainer = CreateContainer();
	            rotationContainer.yRotation = j * (360f / numberOfTentacles);
	            
	            for (int k = 0; k < numberOfSpheresPerTentacle; k++)
                {
                    jellyfish.AddChild(rotationContainer);

                    TubeObject sphere = CreateSphere(sphereSize);
                    rotationContainer.AddChild(sphere);
		            sphere.yPosition = -sphereGap - (sphereSize + sphereGap) * k;
		            sphere.xPosition = bodyWidthMax / 2 - 2f;
                    sphere.xPositionEnd = bodyWidthMin / 2 - 2f;
                    sphere.xPositionDuration = animationDuration;
                    sphere.xPositionRepeatMode = RepeatMode.pingPong;
                    sphere.xPositionCurve = EaseInOutQuad;
                    sphere.animationDistance = sphere.animationDistance - 16 * k;

                    if (k < numberOfSpheresPerTentacle - 1)
                    {
                    	sphere.colliderType = ColliderType.none;
                    }
                }
            }

	        jellyfish.xPosition = 1f;
            jellyfish.yPosition = jellyfishYPosition;
            jellyfish.yPositionEnd = jellyfish.yPosition + 50;
            jellyfish.yPositionDuration = 4;
            jellyfish.yPositionRepeatMode = RepeatMode.triggeredOnce;
            jellyfish.animationDistance = 330;
	        jellyfish.tubeAngle = randomTubeAngle;
	        randomTubeAngle += Random.Range(60,300);

        }
    }

    public static void HalfCylinderGatesLevel()
    {
        int numberOfObstacles = 3;
        int numberOfCylinders = 4;
	    float gapSize = 2.5f;
	    float cylinderHeight = (40 - gapSize * (numberOfCylinders + 1)) / numberOfCylinders;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 250;

	        tubeAngle = 0;
            
	        TubeObject container = CreateContainer();
	        container.yRotationEnd = -360;
	        container.yRotationDuration = 5;
	        container.animationDistance = Random.Range(0f, 500f);

            for (int j = 0; j < numberOfCylinders; j++)
            {
	            TubeObject ring = CreateRing(180, 20, 1, cylinderHeight);
	            container.AddChild(ring);
	            
	            ring.shadingType = ShadingType.directional;
	            ring.shadowType = ShadowType.directional;
	            
	            ring.xRotation = 90;
	            ring.zRotation = (360f / numberOfCylinders) * j;
	            ring.yPosition = 20 - gapSize - (cylinderHeight / 2) - ((gapSize + cylinderHeight) * j);
            }
        }

	    z += 20;
    }

	public static void HypnotizeSpiralsLevel()
    {
        int numberOfObstacles = 6;
        int numberOfArms = 4;
	    int numberOfSpheresPerArm = 25;
	    float innerY = -3;
        float sphereRadius = .75f;

        for (int i = 0; i < numberOfObstacles; i++)
        {
        	z += (i == 0) ? 0 : ((i % 2 == 0) ? 160 : 15);
            int reverse = (i % 2 == 0) ? 1 : -1;

	        TubeObject spiral = CreateContainer();
	        spiral.tubeAngle = randomAngle;
	        spiral.tubeAngleEnd = spiral.tubeAngle - 360f * reverse;
	        spiral.tubeAngleDuration = 12 - i;
	        
	        for (int j = 0; j < numberOfSpheresPerArm; j++) {
	        	
	        	float t = j / (numberOfSpheresPerArm - 1f);
	        	if (reverse == -1) {
	        		t = 1 - t;
	        	}
	        	
	        	float y = Mathf.Lerp(-20 + sphereRadius, innerY, t);
	        	
	        	for (int k = 0; k < numberOfArms; k++, tubeAngle += (360f / numberOfArms) * reverse) {
		        	TubeObject sphere = CreateSphere(sphereRadius * 2);
		        	spiral.AddChild(sphere);
		        	sphere.zPosition = z - spiral.zPosition;
		        	sphere.yPosition = y;
		        	if (y - sphereRadius > -15) {
		        		sphere.colliderType = ColliderType.none;
		        	}
	        	}
	        	
	        	tubeAngle -= 4;
	        	z += 1;
	        }
        }
    }

	public static void FlowersLevel()
    {
        int numberOfObstacles = 10;
        int numberOfPetals = 6;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 70;

            tubeAngle = 0;

	        TubeObject flower = CreateContainer();
            
	        /*
	        TubeObject sphere = CreateSphere(1);
	        flower.AddChild(sphere);
	        TubeObject rod = CreateCube(.5f, 20, .5f);
	        flower.AddChild(rod);
	        rod.yPosition = rod.yScale / 2;
	        */

	        TubeObject stem = CreateRing(85, 1f, 0, .5f);
            flower.AddChild(stem);
	        stem.zRotation = 91;
	        stem.xScale = 1.02f;
	        stem.yScale = .64f;
	        stem.xPosition = 8.8f;
	        stem.yPosition = 13.8f;

            TubeObject flowerTop = CreateContainer();
            flower.AddChild(flowerTop);

            TubeObject centerRing = CreateRing(360, 19, 17, .5f, 60);
            flowerTop.AddChild(centerRing);

            for (int j = 0; j < numberOfPetals; j++)
            {
                float angle = (360 / numberOfPetals) * j;

                TubeObject petal = CreateRing(300, 18f, 17, .5f, 50);
                flowerTop.AddChild(petal);
                petal.tubeAngle = angle;
                petal.yPosition = 8.25f;
                petal.yScale = 3;
            }

	        flowerTop.xScale = flowerTop.yScale = .8f;
	        flowerTop.yPosition = 28;

	        flower.xScale = flower.yScale = flower.zScale = .1f;
	        flower.yPosition = -18.5f;

	        flower.tubeAngle = randomAngle;
	        flower.xScaleEnd = flower.yScaleEnd = flower.zScaleEnd = .9f;
            flower.xScaleDuration = flower.yScaleDuration = flower.zScaleDuration = .75f;
            flower.xScaleRepeatMode = flower.yScaleRepeatMode = flower.zScaleRepeatMode = RepeatMode.triggeredOnce;
            flower.xScaleCurve = flower.yScaleCurve = flower.zScaleCurve = EaseInOutCubic;
	        flower.animationDistance = 100;

			TubeObject flowerBase = CreateSphere(16);
	        flowerBase.tubeAngle = flower.tubeAngle + 1;
	        flowerBase.yPosition = -26;
        }
    }

    public static void SwarmPathLevel()
    {
        int numberOfObstacles = 1;
        int numberOfBallsPerSwarmSegment = 375;
        float lengthOfSwarmSegment = 250;
        int swarmSegments = 2;
        float ballRadius = 1.5f;
        float animationTime = 2f;
        System.Func<float, float> curve = EaseInOutQuad;

        int rotationRandomizer = randomBool ? 1 : -1;

        for (int k = 0; k < swarmSegments; k++) {
        	rotationRandomizer = rotationRandomizer * -1;

	        for (int i = 0; i < numberOfObstacles; i++)
	        {
	            float angleRandomizer = randomAngle;

				for (int j = 0; j < numberOfBallsPerSwarmSegment; j++)
	            {
	                float distanceFromCenter;
	                int numberOfRetries = 0;

					float zAdjustment = Random.Range(0, lengthOfSwarmSegment);

	                TubeObject sphere = CreateSphere(ballRadius * 2);
	                sphere.zPosition += zAdjustment;

	                do
	                {
	                    sphere.xPosition = Random.Range(-20 + ballRadius, 20 - ballRadius);
	                    sphere.yPosition = Random.Range(15, 20 - ballRadius);

	                    distanceFromCenter = Mathf.Sqrt(sphere.xPosition * sphere.xPosition + sphere.yPosition * sphere.yPosition) + ballRadius;

	                    numberOfRetries++;

	                    if (numberOfRetries > 10) // don't keep trying to place the ball in the circle forever
	                    {
	                        distanceFromCenter = Random.Range(-20 + ballRadius, 20 - ballRadius);
	                    }
	                }
	                while (distanceFromCenter > 20);

	                sphere.xPosition = 0;
	                sphere.yPosition = distanceFromCenter;
	                sphere.tubeAngle = randomAngle + angleRandomizer;

					sphere.tubeAngleEnd = Random.Range(0, 300) + angleRandomizer + (zAdjustment / lengthOfSwarmSegment) * 360 * rotationRandomizer;
	                sphere.tubeAngleDuration = animationTime;
	                sphere.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
	                sphere.tubeAngleCurve = curve;
	                sphere.yPositionEnd = Random.Range(12, 20 - ballRadius);
	                sphere.yPositionDuration = animationTime;
	                sphere.yPositionRepeatMode = RepeatMode.triggeredOnce;
	                sphere.yPositionCurve = curve;
	                sphere.animationDistance = 150;
	            }
	        }
			z += lengthOfSwarmSegment + 100;
	     }
		
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas' Levels Batch 4
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void BallVortexLevel()
    {
        int numberOfObstacles = 3;
        int numberOfOrbitals = 120;
        float ballRadius = 1f;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 200 + intraLevelZSpacingIncrement;
            tubeAngle = 0;

            TubeObject container = CreateContainer();

            for (int j = 0; j < numberOfOrbitals; j++)
            {
                float animationTime = j * 60;

                TubeObject orbitalContainer = CreateContainer();
                container.AddChild(orbitalContainer);

                TubeObject xMovementContainer = CreateContainer();
                orbitalContainer.AddChild(xMovementContainer);

                TubeObject ball = CreateSphere(ballRadius * 2);
                xMovementContainer.AddChild(ball);
                ball.yPosition = -3 - ballRadius;
                ball.yPositionEnd = -20 + ballRadius;
                ball.yPositionDuration = 4f;
                ball.yPositionRepeatMode = RepeatMode.pingPong;
                ball.yPositionCurve = EaseInOutSin;
                ball.animationDistance = 50 + animationTime;

                xMovementContainer.xPosition = -3 - ballRadius;
                xMovementContainer.xPositionEnd = -20 + ballRadius;
                xMovementContainer.xPositionDuration = 4f;
                xMovementContainer.xPositionRepeatMode = RepeatMode.pingPong;
                xMovementContainer.xPositionCurve = EaseInOutSin;

                xMovementContainer.animationDistance = 50 + animationTime;

                orbitalContainer.yRotation = j * (360 / numberOfOrbitals);
            }

            //float rotationAngle = Random.Range(90, 120) * (randomBool ? 1 : -1);
            //float rotationDuration = 4;

            container.xRotation = 90;
            container.tubeAngleEnd = randomRotation * 5;
            container.tubeAngleDuration = 40;
            container.tubeAngleRepeatMode = RepeatMode.pingPong;
            container.tubeAngleCurve = EaseInOutQuad;
            container.animationDistance = Random.Range(-10000, 10000);
        }
    }

    public static void BouncingBallStarLevel()
    {
        int numberOfObstacles = 5;
        int numberOfBalls = 20;
        float ballRadius = 2f;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 150;
            RandomizeTubeAngle();

            int reverse = (randomBool ? 1 : -1);

            for (int j = 0; j < numberOfBalls; j++)
            {
                TubeObject ball = CreateSphere(ballRadius * 2);
                ball.yPosition = 20 - ballRadius;
                ball.tubeAngle = ball.tubeAngle + j * (360 / numberOfBalls);

                ball.yPositionEnd = 3;
                ball.yPositionDuration = .6f;
                ball.yPositionRepeatMode = RepeatMode.pingPong;
                ball.yPositionCurve = EaseInOutSin;
                ball.animationDistance = j * 30 * reverse;
            }
        }
    }

    public static void GiantBoulderLevel()
    {
        int numberOfObstacles = 3;
        float travelDistance = 300;
        float travelDuration = 3;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 250 : 190f;
            RandomizeTubeAngle();

            TubeObject ball = CreateContainer();

            for (int j = 0; j < 18; j += 2)
            {
                float ringRadius = Mathf.Sqrt(400 - j * j);

                TubeObject ring = CreateRing(360, 20 - ringRadius + 1, 20 - ringRadius, 1);
                ball.AddChild(ring);
                ring.zPosition += j;

                if (j > 0)
                {
                    TubeObject ring2 = CreateRing(360, 20 - ringRadius + 1, 20 - ringRadius, 1);
                    ball.AddChild(ring2);
                    ring2.zPosition -= j;
                }
            }

            ball.xRotationEnd = ball.xRotation - 360f;
            ball.zPositionEnd = ball.zPosition - travelDistance;
            ball.zPositionDuration = travelDuration;
            ball.zPositionRepeatMode = RepeatMode.triggeredOnce;
            ball.animationDistance = 568;

            ball.xRotationDuration = (40 * Mathf.PI) / (travelDistance / travelDuration);
        }

        z -= 250;
    }

    public static void BuzzSawsLevel()
    {
        int numberOfSaws = 10;
        float travelDistance = 500;

        float sawWidth = 20;
        float sawThickness = .2f;

	    int numberOfBlades = 30;
        float bladeAngleDifference = 360f / numberOfBlades;
	    float bladeWidth = sawWidth * Mathf.Sin(Mathf.Deg2Rad * (bladeAngleDifference / 2));
	    float bladeHeight = bladeWidth;

        for (int i = 0; i < numberOfSaws; i++)
        {
            z += (i == 0) ? 300 : 40;
            RandomizeTubeAngle();
            int flip = randomBool ? 1 : -1;

            TubeObject sawContainer = CreateContainer();

	        TubeObject ring = CreateRing(360, 17, 20 - sawWidth / 2, sawThickness, 30);
	        ring.shadowType = ShadowType.directional;
            sawContainer.AddChild(ring);

            for (int j = 0; j < numberOfBlades; j++)
            {
	            TubeObject spike = CreatePyramid(sawThickness, bladeHeight, bladeWidth);
                sawContainer.AddChild(spike);
	            spike.yPosition = (sawWidth * .5f) + (bladeHeight * .45f);
                spike.yRotation = 90;
	            spike.tubeAngle = j * bladeAngleDifference;
	            spike.colliderType = ColliderType.none;
	            spike.shadowType = ShadowType.none;
	        }

            sawContainer.yPosition = -20;
            sawContainer.yRotation = 45 * flip;

	        sawContainer.zRotationEnd = sawContainer.zRotation - (360f * flip);
	        sawContainer.zRotationDuration = 1f;

            sawContainer.zPositionEnd = sawContainer.zPosition - travelDistance;
            sawContainer.zPositionDuration = 5;
            sawContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
			
            sawContainer.tubeAngleEnd = sawContainer.tubeAngle + travelDistance / ((Mathf.PI * 40) / 360) * flip;
            sawContainer.tubeAngleDuration = 5;
	        sawContainer.tubeAngleRepeatMode = RepeatMode.loop;

            sawContainer.animationDistance = travelDistance;
        }

        z -= 200;
    }

    public static void GunTurretsLevel()
    {
        float travelDistance = 600;

        z += 150;

        TubeObject movementContainer = CreateContainer();
		float turnAngle = Random.Range(120, 270) * (randomBool ? 1 : -1);
        float animationTime = (turnAngle * 8) / 360f;

        TubeObject guideRing = CreateRing(360, 6f, 5f, 5);
        movementContainer.AddChild(guideRing);

        for (int i = 0; i < 3; i++)
        {
			

            TubeObject cube = CreateCube(3, 1, 5);
            movementContainer.AddChild(cube);
            cube.tubeAngle = cube.tubeAngle + i * 120;
            cube.xPosition = 16;

            TubeObject cylinder1 = CreateCylinder(4, 10, 4);
            movementContainer.AddChild(cylinder1);
            cylinder1.tubeAngle = cylinder1.tubeAngle + i * 120;
            cylinder1.xRotation = 90;
            cylinder1.xPosition = 18f;
            cylinder1.zPosition = -2.5f;

            TubeObject cylinder2 = CreateCylinder(1, 10, 1);
            movementContainer.AddChild(cylinder2);
            cylinder2.tubeAngle = cylinder2.tubeAngle + i * 120;
            cylinder2.xRotation = 90;
            cylinder2.xPosition = 18f;
            cylinder2.zPosition = -12.5f;

            TubeObject sphere = CreateSphere(3f);
            movementContainer.AddChild(sphere);
            sphere.tubeAngle = sphere.tubeAngle + i * 120;
            sphere.xPosition = 18f;
            sphere.zPosition = -17.5f;


            for (int bulletIndex = 0; bulletIndex < 15; bulletIndex++)
            {
	            //TubeObject reverseRotationContainer = CreateContainer();

                TubeObject bullet = CreateCylinder(.8f, 6, .8f);
                movementContainer.AddChild(bullet);
                bullet.tubeAngle = bullet.tubeAngle + i * 120;
                bullet.xRotation = 90;
                bullet.xPosition = 17.25f;
                bullet.zPosition = -2.5f;
                bullet.zPositionEnd = -200;
	            bullet.zPositionDuration = .4f;
                bullet.zPositionRepeatMode = RepeatMode.triggeredLoop;
                bullet.animationDistance = 150 + 3.333f * bulletIndex;

            }
        }

       

        movementContainer.zPositionEnd = movementContainer.zPosition + travelDistance;
        movementContainer.zPositionDuration = 7f;
        movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
        movementContainer.tubeAngleEnd = movementContainer.tubeAngle + turnAngle;
        movementContainer.tubeAngleDuration = animationTime;
        movementContainer.tubeAngleRepeatMode = RepeatMode.pingPong;
        movementContainer.tubeAngleCurve = EaseInOutQuad;
        movementContainer.animationDistance = 150;

        z += travelDistance + 100;
    }

    public static void RingLatticeLevel()
    {
	    tubeAngle = 0;
	    int numberOfObstacles = 4;

	    float circleThickness = .5f;
        
	    float containerX = 0;
	    float containerY = 5;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 150 + intraLevelZSpacingIncrement;

			int numberOfRows = 9;

			float circleWidth = 80f / numberOfRows;

            TubeObject container = CreateContainer();

            for (int j = 0; j < numberOfRows; j++)
            {
                float yDistance = circleWidth * Mathf.Sin(Mathf.Deg2Rad * 60);
	            float yPosition = ((numberOfRows - 1) * .5f) * yDistance - j * yDistance;

                for (int k = 0; k < numberOfRows; k++)
                {
                    float bottom = 20 - circleWidth / 2;
	                float top = bottom + circleThickness;
                    
	                float xPosition = - 20 -
	                (circleWidth * numberOfRows/2 - circleWidth/2) +
	                (circleWidth * .75f) +
	                (k * circleWidth) -
	                ((j % 2 == 0) ? 0 : circleWidth / 2);
	                
	                // If the ring would be fully outside the tube, don't create it.
	                Vector2 circlePosition = new Vector2(xPosition + containerX, yPosition + containerY);
	                if (circlePosition.magnitude > 20 + (circleWidth / 2)) {
	                	continue;
	                }

	                TubeObject ring = CreateRing(360, top, bottom, circleThickness, 40);
	                ring.xPosition = xPosition;
                    ring.yPosition = yPosition;

                    container.AddChild(ring);
                }

	            container.tubeAngle = randomAngle;
                container.tubeAngleEnd = container.tubeAngle + 360f * (randomBool ? 1 : -1);
                container.tubeAngleDuration = 20;

	            container.xPosition = containerX;
	            container.yPosition = containerY;

//                container.xPositionEnd = container.xPosition + (circleWidth * numberOfRows / 4) + circleWidth;
//                container.xPositionDuration = 1;
//                container.xPositionRepeatMode = RepeatMode.pingPong;
//                container.xPositionCurve = EaseInOutQuad;
//                container.animationDistance = Random.Range(-100, 100);
            }
        }
    }

    public static void VirusLevel()
    {
        int numberOfObstacles = 3;
	    float armWidth = 1f;
	    float ballSize = 4f;
	    float armReduction = .5f;
	    tubeAngle = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 200;

	        TubeObject ballContainer = CreateContainer();
	        ballContainer.tubeAngle = randomAngle;
	        
	        float t = (1f + Mathf.Sqrt(5f)) / 2f;
	        Vector3[] points = new Vector3[32];
		
	        points[0] = new Vector3(-1,  t,  0).normalized;
	        points[1] = new Vector3( 1,  t,  0).normalized;
	        points[2] = new Vector3(-1, -t,  0).normalized;
	        points[3] = new Vector3( 1, -t,  0).normalized;

	        points[4] = new Vector3( 0, -1,  t).normalized;
	        points[5] = new Vector3( 0,  1,  t).normalized;
	        points[6] = new Vector3( 0, -1, -t).normalized;
	        points[7] = new Vector3( 0,  1, -t).normalized;

	        points[8] = new Vector3( t,  0, -1).normalized;
	        points[9] = new Vector3( t,  0,  1).normalized;
	        points[10] = new Vector3(-t,  0, -1).normalized;
	        points[11] = new Vector3(-t,  0,  1).normalized;
	        
	        // 5 faces around point 0
	        points[12] = Vector3.Lerp(Vector3.Lerp(points[0], points[11], .5f), points[5], .33f).normalized;
	        points[13] = Vector3.Lerp(Vector3.Lerp(points[0], points[5], .5f), points[1], .33f).normalized;
	        points[14] = Vector3.Lerp(Vector3.Lerp(points[0], points[1], .5f), points[7], .33f).normalized;
	        points[15] = Vector3.Lerp(Vector3.Lerp(points[0], points[7], .5f), points[10], .33f).normalized;
	        points[16] = Vector3.Lerp(Vector3.Lerp(points[0], points[10], .5f), points[11], .33f).normalized;
	        
	        // 5 adjacent faces
	        points[17] = Vector3.Lerp(Vector3.Lerp(points[1], points[5], .5f), points[9], .33f).normalized;
	        points[18] = Vector3.Lerp(Vector3.Lerp(points[5], points[11], .5f), points[4], .33f).normalized;
	        points[19] = Vector3.Lerp(Vector3.Lerp(points[11], points[10], .5f), points[2], .33f).normalized;
	        points[20] = Vector3.Lerp(Vector3.Lerp(points[10], points[7], .5f), points[6], .33f).normalized;
	        points[21] = Vector3.Lerp(Vector3.Lerp(points[7], points[1], .5f), points[8], .33f).normalized;
	        
	        // 5 faces around point 3
	        points[22] = Vector3.Lerp(Vector3.Lerp(points[3], points[9], .5f), points[4], .33f).normalized;
	        points[23] = Vector3.Lerp(Vector3.Lerp(points[3], points[4], .5f), points[2], .33f).normalized;
	        points[24] = Vector3.Lerp(Vector3.Lerp(points[3], points[2], .5f), points[6], .33f).normalized;
	        points[25] = Vector3.Lerp(Vector3.Lerp(points[3], points[6], .5f), points[8], .33f).normalized;
	        points[26] = Vector3.Lerp(Vector3.Lerp(points[3], points[8], .5f), points[9], .33f).normalized;
	        
	        // 5 adjacent faces
	        points[27] = Vector3.Lerp(Vector3.Lerp(points[4], points[9], .5f), points[5], .33f).normalized;
	        points[28] = Vector3.Lerp(Vector3.Lerp(points[2], points[4], .5f), points[11], .33f).normalized;
	        points[29] = Vector3.Lerp(Vector3.Lerp(points[6], points[2], .5f), points[10], .33f).normalized;
	        points[30] = Vector3.Lerp(Vector3.Lerp(points[8], points[6], .5f), points[7], .33f).normalized;
	        points[31] = Vector3.Lerp(Vector3.Lerp(points[9], points[8], .5f), points[1], .33f).normalized;
	        
	        float radius = 20 - (ballSize / 2);
	        
	        for (int j = 12; j < points.Length; j++) {
	        	
	        	TubeObject sphere = CreateSphere(ballSize);
	        	ballContainer.AddChild(sphere);
	        	
	        	Vector3 point = points[j] * radius;
	        	sphere.xPosition = point.x;
	        	sphere.yPosition = point.y;
	        	sphere.zPosition = point.z;
	        	
	        	Vector3 innerEndPoint = points[j] * .5f;
	        	
		        TubeObject arm = CreateCylinderPillar(armWidth, (point - innerEndPoint).magnitude, armWidth);
		        ballContainer.AddChild(arm);
	        	
		        Vector3 armCenter = Vector3.Lerp(point, innerEndPoint, .5f);
		        arm.xPosition = armCenter.x;
		        arm.yPosition = armCenter.y;
		        arm.zPosition = armCenter.z;
		        	
		        armCenter = Vector3.Lerp(point, innerEndPoint, .5f * armReduction);
		        arm.xPositionEnd = armCenter.x;
		        arm.yPositionEnd = armCenter.y;
		        arm.zPositionEnd = armCenter.z;
		        arm.yScaleEnd = arm.yScale * armReduction;
		        
		        arm.xPositionDuration = arm.yPositionDuration = arm.zPositionDuration = arm.yScaleDuration = 1.5f;
		        arm.xPositionRepeatMode = arm.yPositionRepeatMode = arm.zPositionRepeatMode = arm.yScaleRepeatMode = RepeatMode.triggeredOnce;
		        arm.xPositionCurve = arm.yPositionCurve = arm.zPositionCurve = arm.yScaleCurve = EaseOutExpo;
		        arm.animationDistance = -5 + arm.zPosition;
		        
		        Vector3 perpVector = Vector3.zero;
		        Vector3.OrthoNormalize(ref point, ref perpVector);
		        	
		        Quaternion rotation = Quaternion.LookRotation(perpVector, point);
		        Vector3 angles = rotation.eulerAngles;
		        arm.xRotation = angles.x;
		        arm.yRotation = angles.y;
		        arm.zRotation = angles.z;
	        }

	        ballContainer.xRotation = 0;
	        ballContainer.xRotationEnd = ballContainer.xRotation + 360f;
	        ballContainer.xRotationDuration = 10;

	        ballContainer.yRotation = 0;
	        ballContainer.yRotationEnd = ballContainer.yRotation + 360f;
	        ballContainer.yRotationDuration = 10;

	        ballContainer.zRotation = 0;
	        ballContainer.zRotationEnd = ballContainer.zRotation + 360f;
	        ballContainer.zRotationDuration = 10;
        }
    }

    public static void PendulumWheelLevel()
    {
	    tubeAngle = 0;

	    int numberOfObstacles = 5;
        int numberOfArms = 4;

	    float centerCylinderRadius = 2.25f;
        float sphereSize = 4;
        float stringWidth = 1;
	    float stringLength = 7;

        for (int i = 0; i < numberOfObstacles; i++)
        {
	        z += (i == 0) ? 0 : 125;

	        TubeObject centerRing = CreateRing(360, (20 - centerCylinderRadius) + stringWidth, 20 - centerCylinderRadius, stringWidth);

            int rotationReverse = randomBool ? 1 : -1;

            for (int j = 0; j < numberOfArms; j++)
            {
	            int reverse = randomBool ? 1 : -1;
                
	            TubeObject sphere1 = CreateSphere(sphereSize);
	            sphere1.yPosition = 9f;
	            sphere1.tubeAngle = j * 360 / numberOfArms;
	            
	            float topY = Mathf.Lerp(centerRing.top, centerRing.bottom, .5f);
	            float length = topY - sphere1.yPosition;

	            TubeObject string1 = CreateCubePillar(stringWidth, length, stringWidth);
                string1.tubeAngle = j * 360 / numberOfArms;
	            string1.yPosition = -20 + topY - (string1.yScale / 2);

                TubeObject string2 = CreateCubePillar(stringWidth, stringLength, stringWidth);
                string2.yPosition = 3.5f;

                TubeObject sphere2 = CreateSphere(sphereSize);
                sphere2.yPosition = 8f;

                TubeObject secondaryContainer = CreateContainer();
                secondaryContainer.AddChild(string2);
                secondaryContainer.AddChild(sphere2);
                secondaryContainer.yPosition = sphere1.yPosition;
                secondaryContainer.tubeAngle = j * 360 / numberOfArms;
	            secondaryContainer.zRotationEnd = secondaryContainer.zRotation + 360 * reverse;
	            secondaryContainer.zRotationDuration = Random.Range(1f, 2f);
	            secondaryContainer.zRotationRepeatMode = RepeatMode.loop;
	            secondaryContainer.animationDistance = 0;

                TubeObject primaryContainer = CreateContainer();
                primaryContainer.AddChild(string1);
                primaryContainer.AddChild(sphere1);
                primaryContainer.AddChild(secondaryContainer);
	            primaryContainer.zRotationEnd = primaryContainer.zRotation + 360 * rotationReverse;
	            primaryContainer.zRotationDuration = 2;
                primaryContainer.zRotationRepeatMode = RepeatMode.loop;
            }
        }
    }

    public static void FlippingRingsLevel()
    {
        int numberOfObstacles = 7;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 150;
            tubeAngle += Random.Range(60, 300);
            TubeObject ring = CreateRing(315, 6, 0, 2);
	        ring.xRotationEnd = -360;
	        ring.xRotationDuration = 1.8f;
        }
    }

    public static void ConvergentGatesLevel()
    {
        int numberOfObstacles = 3;
        float depth = 2;
        float closingDuration = .5f;
        System.Func<float, float> closingCurve = EaseInOutQuad;
        float animationDistanceDifference = 40;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 300;

            RandomizeTubeAngle();

            bool decisionA = randomBool; // left closes (true) or right closes (false)
            bool decisionB = randomBool; // top closes (true) or inside closes (false)
            bool decisionC = randomBool; // if top closed, left closes (true) or right closes (false); if inside closed, top closes (true) or bottom closes (false)

            TubeObject cube1 = CreateCubePillar(40, 20, depth);
            cube1.yPosition = 30;
            cube1.yPositionEnd = 10;
            cube1.yPositionDuration = closingDuration;
            cube1.yPositionRepeatMode = RepeatMode.triggeredOnce;
            cube1.yPositionCurve = closingCurve;
            cube1.animationDistance = 225;

            TubeObject cube2 = CreateCubePillar(20, 20, depth);
            cube2.yPosition = -10;
            cube2.xPosition = 30 * (decisionA ? -1 : 1);
            cube2.xPositionEnd = 10 * (decisionA ? -1 : 1);
            cube2.xPositionDuration = closingDuration;
            cube2.xPositionRepeatMode = RepeatMode.triggeredOnce;
            cube2.xPositionCurve = closingCurve;
            cube2.animationDistance = cube1.animationDistance - animationDistanceDifference;

            TubeObject cube3 = CreateCubePillar(decisionB ? 20 : 10, decisionB ? 10 : 20, depth);
            cube3.xPosition = decisionB ? (decisionA ? 30 : -30) : (decisionA ? 4 : -4);
            cube3.yPosition = decisionB ? -4 : -30;
            cube3.xPositionEnd = decisionB ? (decisionA ? 10 : -10) : cube3.xPosition;
            cube3.yPositionEnd = decisionB ? cube3.yPosition : -10;
            cube3.xPositionDuration = closingDuration;
            cube3.xPositionRepeatMode = RepeatMode.triggeredOnce;
            cube3.xPositionCurve = closingCurve;
            cube3.yPositionDuration = closingDuration;
            cube3.yPositionRepeatMode = RepeatMode.triggeredOnce;
            cube3.yPositionCurve = closingCurve;
            cube3.animationDistance = cube2.animationDistance - animationDistanceDifference;

            TubeObject cube4 = CreateCubePillar(10, 10, depth);
            cube4.xPosition = decisionA ? (decisionB ? (decisionC ? 4 : 14) : 25) : (decisionB ? (decisionC ? -14 : -4) : -25);
            cube4.yPosition = decisionB ? -25 : (decisionC ? -4 : -14);
            cube4.xPositionEnd = decisionB ? cube4.xPosition : (decisionA ? cube4.xPosition - 10 : cube4.xPosition + 10);
            cube4.yPositionEnd = decisionB ? cube4.yPosition + 10 : cube4.yPosition;
            cube4.xPositionDuration = closingDuration;
            cube4.xPositionRepeatMode = RepeatMode.triggeredOnce;
            cube4.xPositionCurve = closingCurve;
            cube4.yPositionDuration = closingDuration;
            cube4.yPositionRepeatMode = RepeatMode.triggeredOnce;
            cube4.yPositionCurve = closingCurve;
            cube4.xScaleEnd = decisionB ? 10 : 12;
            cube4.yScaleEnd = decisionB ? 12 : 10;
            cube4.xScaleDuration = closingDuration;
            cube4.xScaleRepeatMode = RepeatMode.triggeredOnce;
            cube4.xScaleCurve = closingCurve;
            cube4.yScaleDuration = closingDuration;
            cube4.yScaleRepeatMode = RepeatMode.triggeredOnce;
            cube4.yScaleCurve = closingCurve;
            cube4.animationDistance = cube3.animationDistance - animationDistanceDifference;
        }

        z += 100;
    }

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas' Levels Batch 3
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void HalfCircleSpiralLevel()
    {
        int numberOfObstacles = 3;
        int numberOfSegments = 7;

        float width = 1.5f;
        float depth = width;
        float sizeMultiplier = 1.25f;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += i == 0 ? 0 : 200;

            RandomizeTubeAngle();

            int reverse = randomBool ? 1 : -1;

            float previousSegmentXPosition = 0;
            float previousSegmentScale = 1;

            for (int j = 0; j < numberOfSegments; j++)
            {
                TubeObject ring;

                if (j == 0)
                {
                    ring = CreateRing(180, width, 0, depth);

                    previousSegmentXPosition = 0;
                    previousSegmentScale = 1;
                }
                else
                {
                    float scale = previousSegmentScale / sizeMultiplier;

                    ring = CreateRing(180, width / scale, 0, depth);
                    ring.xScale = ring.yScale = scale;

                    float xPosition = 0;

                    if (j % 2 == 0)
                    {
                        xPosition = previousSegmentXPosition + (previousSegmentScale * 20) * reverse - (scale * 40) * reverse / 2;
                    }
                    else
                    {
                        ring.xRotation = 180;
                        xPosition = previousSegmentXPosition - (previousSegmentScale * 20) * reverse + (scale * 40) * reverse / 2;
                    }

                    ring.xPosition = xPosition;

                    previousSegmentXPosition = xPosition;
                    previousSegmentScale = scale;
                }

                ring.tubeAngleEnd = ring.tubeAngle + 360f * reverse;
                ring.tubeAngleDuration = 1.5f;
            }
        }
    }

	public static void CrazyClocksLevel()
    {
        int numberOfObstacles = 5;
        float cylinderRadius = 3;
        float depth = 2;
        float handWidth = 2;
        float bigHandShaftLength = 16;
        float smallHandShaftLength = 13;
        float pointLength = 20 - bigHandShaftLength;
        float pointWidth = 4;
		float durationBase = 0;

        for (float i = 0; i < numberOfObstacles; i++)
        {
	        z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
            tubeAngle = 0;

            TubeObject ring = CreateRing(360, 20 - cylinderRadius + 1.5f, 20 - cylinderRadius, depth * 2);
            ring.zPosition -= depth / 2;

            TubeObject bigHandContainer = CreateContainer();

            TubeObject bigHandShaft = CreateCubePillar(handWidth, bigHandShaftLength - cylinderRadius/2, depth);
            bigHandContainer.AddChild(bigHandShaft);
            bigHandShaft.yPosition = bigHandShaftLength / 2 + cylinderRadius / 4;

            TubeObject bigHandPoint = CreatePyramidPillar(pointWidth, pointLength, depth);
            bigHandContainer.AddChild(bigHandPoint);
            bigHandPoint.yPosition = bigHandShaftLength + pointLength / 2;

            bigHandContainer.tubeAngleEnd = bigHandContainer.tubeAngle - 360f;
            durationBase = Random.Range(1,2);
            bigHandContainer.tubeAngleDuration = durationBase - (0.5f * durationBase)*(i/numberOfObstacles);
			bigHandContainer.tubeAngleDuration = Random.Range(1.5f, 2.5f)  ;
            bigHandContainer.animationDistance = Random.Range(0, 1000);

            TubeObject smallHandContainer = CreateContainer();

            TubeObject smallHandShaft = CreateCubePillar(handWidth, smallHandShaftLength - cylinderRadius / 2, depth);
            smallHandContainer.AddChild(smallHandShaft);
            smallHandShaft.yPosition = smallHandShaftLength / 2 + cylinderRadius / 4;
            smallHandShaft.zPosition -= depth;

            TubeObject smallHandPoint = CreatePyramidPillar(pointWidth, pointLength, depth);
            smallHandContainer.AddChild(smallHandPoint);
            smallHandPoint.yPosition = smallHandShaftLength + pointLength / 2;
            smallHandPoint.zPosition -= depth;

            smallHandContainer.tubeAngleEnd = smallHandContainer.tubeAngle + 360f;
            smallHandContainer.tubeAngleDuration = bigHandContainer.tubeAngleDuration / 2;
            smallHandContainer.animationDistance = Random.Range(0, 10000);
        }
    }

    public static void SpikePillarsLevel()
    {
        int numberOfObstacles = 6;

        float cylinderWidth = 10;
        float spikeWidth = 3;
        float spikeHeight = 6;
        int spikeRows = 8;
        int spikesPerRow = 4;

        float spikeRowGap = (40 - spikeRows * spikeWidth) / (spikeRows + 1)
;
        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += i == 0 ? 0 : 130 + intraLevelZSpacingIncrement;
            tubeAngle = 0;
            int reverse = (i % 2 == 0) ? 1 : -1;

            TubeObject container = CreateContainer();
	        TubeObject cylinder = CreateCylinderPillar(cylinderWidth, 30, cylinderWidth);
	        cylinder.yPosition = 20 - (cylinder.yScale / 2);
	        container.AddChild(cylinder);
	        cylinder = CreateCylinderPillar(cylinderWidth, 30, cylinderWidth);
	        cylinder.yPosition = -20 + (cylinder.yScale / 2);
	        container.AddChild(cylinder);

            for (int j = 0; j < spikeRows; j++)
            {
                for (int k = 0; k < spikesPerRow; k++)
                {
                    TubeObject spike = CreatePyramid(spikeWidth, spikeHeight, spikeWidth);
                    container.AddChild(spike);
                    spike.yRotation = (360f / spikesPerRow) * k;
                    spike.xPosition = (cylinderWidth / 2 + (spikeHeight - 1) / 2) * Mathf.Cos(Mathf.Deg2Rad * spike.yRotation);
                    spike.zPosition = spike.zPosition - (cylinderWidth / 2 + (spikeHeight - 1) / 2) * Mathf.Sin(Mathf.Deg2Rad * spike.yRotation);
                    spike.yPosition = -20 + spikeRowGap + spikeWidth / 2 + (spikeWidth + spikeRowGap) * j;
                    spike.zRotation = -90;
	                spike.shadowType = ShadowType.none;
                }
            }

            container.yRotationEnd = container.yRotation + 360f * reverse;
            container.yRotationDuration = .5f;
            container.xPosition = -20 + spikeHeight / 2 + cylinderWidth / 2;
            container.xPositionEnd = 20 - spikeHeight / 2 - cylinderWidth / 2;
            container.xPositionDuration = 1.4f; //1.8f - ( i / numberOfObstacles);
            container.xPositionCurve = EaseInOutQuad; 
            container.xPositionRepeatMode = RepeatMode.pingPong;
            container.animationDistance = Random.Range(0, 1000);
        }
    }

    public static void KeyholeLevel()
    {
        int numberOfObstacles = 5;

        for (int i = 0; i < numberOfObstacles; i++)
        {
	        z += i == 0 ? 0 : 150 - (i * 10) +  intraLevelZSpacingIncrement;
            tubeAngle += Random.Range(60,300);

	        TubeObject ring = CreateRing(0, 16, 0, 0, 55);
            ring.arcEnd = 330;
            ring.arcDuration = .5f;
            ring.arcRepeatMode = RepeatMode.triggeredOnce;
            ring.arcCurve = EaseOutQuad;

            ring.zScaleEnd = 2;
            ring.zScaleDuration = .5f;
            ring.zScaleRepeatMode = RepeatMode.triggeredOnce;
            ring.zScaleCurve = EaseOutQuad;

            ring.animationDistance = 135 - (i * 10);
        }
    }

    public static void RussianRouletteLevel()
    {
        int numberOfSegments = 6;
        float segmentArc = 55;
	    float pyramidSize = 18;
	    float zScale = 22;

        z += 250;
        int reverse = randomBool ? 1 : -1;

        tubeAngle = 0;

        TubeObject spinContainer = CreateContainer();

        int safeSegment = Random.Range(0, numberOfSegments);

        for (int j = 0; j < numberOfSegments; j++)
        {
            if (j != safeSegment)
            {
	            TubeObject obstacle = CreateRing(segmentArc, pyramidSize, 0, zScale / 2);
                obstacle.tubeAngle = obstacle.tubeAngle + j * (360f / numberOfSegments);
                spinContainer.AddChild(obstacle);
	            obstacle.zPosition = zScale / 4;
            }

	        TubeObject ring = CreateRing(segmentArc, pyramidSize, 0, zScale);
            ring.yPositionEnd = ring.yPosition;
            ring.yPosition = ring.yPosition + pyramidSize + 1;
            ring.yPositionDuration = .5f;
            ring.yPositionRepeatMode = RepeatMode.triggeredOnce;
            ring.animationDistance = 165;

            TubeObject dropContainer = CreateContainer();
            dropContainer.AddChild(ring);
            dropContainer.tubeAngle = dropContainer.tubeAngle + j * (360f / numberOfSegments);
            dropContainer.yPositionEnd = dropContainer.yPosition + pyramidSize + 1;
            dropContainer.yPositionDuration = .2f;
            dropContainer.yPositionRepeatMode = RepeatMode.triggeredOnce;
            dropContainer.animationDistance = -165;

            spinContainer.AddChild(dropContainer);
        }

        float randomSpin = (360f / numberOfSegments) * Random.Range(0, numberOfSegments);

        spinContainer.tubeAngleEnd = spinContainer.tubeAngle + (540f + randomSpin) * reverse;
        spinContainer.tubeAngleDuration = 2.5f;
        spinContainer.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
        spinContainer.tubeAngleCurve = EaseInOutQuad;
        spinContainer.animationDistance = 100;

        TubeObject movementContainer = CreateContainer();
        movementContainer.zPosition = movementContainer.zPosition - 200;
        movementContainer.zPositionEnd = movementContainer.zPosition + 200;
        movementContainer.zPositionDuration = 2.5f;
        movementContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;
        movementContainer.zPositionCurve = EaseInOutQuad;
        movementContainer.AddChild(spinContainer);
        movementContainer.animationDistance = 100;
    }

    public static void SpiderwebsLevel()
	{
		tubeAngle = 0;
		int numberOfObstacles = 5;
        int numberOfSpokes = 9;
        int numberOfSegments = 3;

        float webWidth = .5f;

        float arcDifference = 360f / numberOfSpokes;
		float segmentDifference = 20 / numberOfSegments;

        for (int i = 0; i < numberOfObstacles; i++)
        {
	        z += (i == 0) ? 0 : 150 - (i * 20);

	        TubeObject web = CreateContainer();
	        web.tubeAngle = randomAngle;
	        web.xScale = web.yScale = .9f;
	        web.yPosition = 2.2f;

            for (int j = 0; j < numberOfSpokes; j++)
            {
	            TubeObject spoke = CreateCubePillar(webWidth, 25, webWidth);
	            web.AddChild(spoke);
	            spoke.tubeAngle = j * arcDifference;
	            spoke.yPosition = spoke.yScale / 2;

                for (int k = 0; k < numberOfSegments; k++)
                {
                    float offset = segmentDifference * k;
                    float top = offset + webWidth;
                    float bottom = offset + 0;

                    float nonRingRadius = (20 - offset - webWidth) * Mathf.Sin(Mathf.Deg2Rad * (90 - arcDifference / 2));

                    TubeObject ring = CreateRing(arcDifference, top, bottom, webWidth);
                    ring.tubeAngle = spoke.tubeAngle;
                    ring.yPosition = -40 + webWidth + 2 * (20 - webWidth - nonRingRadius);

	                web.AddChild(ring);
                }
            }
        }
    }

    public static void ViciousCyclesLevel(){

		float numberOfRings = 10;
        float ringWidth = 4;
        float ringDepth = 4;
        float ringGap = 11;
        float ringDistance = 90;
        float duration = 1.5f;
	    //float ringTubeAngleChange = 15;
        int direction = randomBool ? 1 : -1;

		
		for (float j = 0; j < numberOfRings; j++)
        {
            z += (j == 0) ? 0 : ringDistance;

            TubeObject ring = CreateRing(360, ringGap / 2 + ringWidth, ringGap / 2, ringDepth);
            ring.yPosition -= ringGap / 2;
            ring.tubeAngle = tubeAngle;

       		ring.tubeAngleEnd = ring.tubeAngle - 360f * direction;
       		ring.tubeAngleCurve = EaseInOutQuad;
        	ring.tubeAngleDuration = duration - ((0.25f * duration) * (j / (numberOfRings-1)));
        	ring.animationDistance = Random.Range(0,300);

        	direction = randomBool ? 1 : -1;

        }

		z += ringDistance + 30;
        

    }

    public static void CircleTunnelLevel()
    {
        float numberOfRings = 10;

        float ringWidth = 3;
        float ringDepth = 3;
        float ringGap = 11;
        float ringDistance = 20;
	    //int directionChanges = 3;
	    //float duration = 1.5f;

        float ringTubeAngleChange = 15;

        int reverse = randomBool ? 1 : -1;

        for (int i = 0; i < numberOfRings; i++)
        {
            z += (i == 0) ? 0 : ringDistance;

            TubeObject ring = CreateRing(360, ringGap / 2 + ringWidth, ringGap / 2, ringDepth);
            ring.yPosition -= ringGap / 2;
            ring.tubeAngle = i * ringTubeAngleChange * reverse;
            ring.tubeAngleEnd = ring.tubeAngle - 360f * reverse;
            ring.tubeAngleDuration = 4;
        }

        reverse = reverse * -1;

         z += 100;

		for (int i = 0; i < numberOfRings; i++)
        {
            z += (i == 0) ? 0 : ringDistance - 10;

            TubeObject ring = CreateRing(360, ringGap / 2 + ringWidth, ringGap / 2, ringDepth);
            ring.yPosition -= ringGap / 2;
            ring.tubeAngle = i * ringTubeAngleChange * reverse;
            ring.tubeAngleEnd = ring.tubeAngle - 360f * reverse;
            ring.tubeAngleDuration = 2;
        }

		
    }

	public static void ShuttersLevel()
    {
	    tubeAngle = 0;
	    int numberOfObstacles = 4;
	    int numberOfShutters = 14;
	    float shutterWidth = 40f / numberOfShutters;

	    System.Func<float, float> curve = EaseInOutQuad;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 200;
	        float angle = randomAngle;
	        float animationDistance = Random.Range(0, 300f);
            
            for (int j = 0; j < numberOfShutters; j++)
            {
	            TubeObject rotationContainer = CreateContainer();
				
	            TubeObject obstacle = CreateCubePillar(shutterWidth * .7f, 40, .5f);
	            rotationContainer.AddChild(obstacle);
	            obstacle.shadowType = ShadowType.radial;
	            obstacle.yPosition = 20;
	            
	            rotationContainer.tubeAngle = angle;
	            rotationContainer.yPosition = -20;
	            rotationContainer.xPosition = -20 + shutterWidth / 2 + j * shutterWidth;

                rotationContainer.xRotation = -60;
                rotationContainer.xRotationEnd = 60;
                rotationContainer.xRotationDuration = 1.5f;
                rotationContainer.xRotationRepeatMode = RepeatMode.pingPong;
                rotationContainer.xRotationCurve = curve;
	            rotationContainer.animationDistance = animationDistance + j * 16;
            }
        }
    }

    public static void WakaWakaWakaLevel()
    {
    	RandomizeTubeAngle();

        int numberOfObstacles = 7;

        float pacmanHeight = 8;
        float pacmanDepth = 1;
        float pacmanArc = 250;

        int numberOfDots = 20;
        float dotWidth = 2.5f;

        float tubeAngleDuration = 3f; // don't change
        float dotAnimationStart = 25; // increase to start dot disappering sooner

        System.Func<float, float> dotCurve = EaseInOutExpo;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 90;
            int reverse = randomBool ? 1 : -1;
            tubeAngle += Random.Range(60,300);

            TubeObject pacman = CreateRing(pacmanArc, 19.999f, 20 - pacmanHeight / 2, pacmanDepth);
            pacman.shadowType = ShadowType.directional;
            pacman.zRotation = 90 * reverse;
            pacman.yPosition = -20 + pacmanHeight / 2;

            pacman.tubeAngle = tubeAngle + (reverse == 1 ? -61 : 142); // magic constants that align pacman to start of dots (don't know why)
            pacman.arcEnd = 360;
            pacman.arcDuration = .20f;
            pacman.arcRepeatMode = RepeatMode.pingPong;

            pacman.tubeAngleEnd = pacman.tubeAngle + 360f * reverse;
            pacman.tubeAngleDuration = tubeAngleDuration;
            pacman.tubeAngleRepeatMode = RepeatMode.loop;

            for (int j = 0; j < numberOfDots; j++)
            {
                TubeObject dot = CreateCylinder(dotWidth, pacmanDepth, dotWidth);

                dot.tubeAngle = tubeAngle + (360 / (numberOfDots - 1)) * j;
                dot.xRotation = 90;
                dot.yPosition = -20 + pacmanHeight * .5f;

                float scaleEnd = 0f;
                float scaleDuration = pacman.tubeAngleDuration * .5f;
                float animationDistance = (dotAnimationStart - (280 / numberOfDots) * j) * reverse;

                dot.xScaleEnd = scaleEnd;
                dot.xScaleDuration = scaleDuration;
                dot.xScaleRepeatMode = RepeatMode.pingPong;
                dot.xScaleCurve = dotCurve;
                dot.yScaleEnd = scaleEnd;
                dot.yScaleDuration = scaleDuration;
                dot.yScaleRepeatMode = RepeatMode.pingPong;
                dot.yScaleCurve = dotCurve;
                dot.zScaleEnd = scaleEnd;
                dot.zScaleDuration = scaleDuration;
                dot.zScaleRepeatMode = RepeatMode.pingPong;
                dot.zScaleCurve = dotCurve;
                dot.yPositionEnd = -15;
                dot.yPositionDuration = scaleDuration;
                dot.yPositionCurve = dotCurve;
                dot.yPositionRepeatMode = RepeatMode.pingPong;

                dot.animationDistance = animationDistance;
            }
        }
    }

    public static void FloatySpinnyFansLevel()
    {
        int numberOfObstacles = 5;
        float ringHeight = 2;
        float arc = 120;
        float animationDuration = 3f;
        float rotationDuration = 6f;
        float animationDistance = 100;

	    //System.Func<float, float> curve = EaseInQuad;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 150;

            tubeAngle += (Random.Range(60,300));

            int reverse = randomBool ? 1 : -1;

            TubeObject ring = CreateRing(arc, ringHeight, 0, ringHeight);
            ring.zRotation = 90 + arc / 2;
            ring.yRotationEnd = ring.xRotation + 360f;
            ring.yRotationDuration = animationDuration;
            ring.yRotationRepeatMode = RepeatMode.loop;
            ring.zRotationEnd = ring.zRotation + 360f * reverse;
            ring.zRotationRepeatMode = RepeatMode.loop;
            ring.zRotationDuration = rotationDuration;
            ring.animationDistance = animationDistance;

            TubeObject ring2 = CreateRing(arc, ringHeight, 0, ringHeight);
            ring2.zRotation = -90 + arc / 2;
            ring2.yRotationEnd = ring2.xRotation - 360f;
            ring2.yRotationDuration = animationDuration;
            ring2.yRotationRepeatMode = RepeatMode.loop;
            ring2.zRotationEnd = ring2.zRotation + 360f * reverse;
            ring2.zRotationRepeatMode = RepeatMode.loop;
            ring2.zRotationDuration = rotationDuration;
            ring2.animationDistance = animationDistance;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas' Levels Batch 2
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void HoleInOneLevel()
    {
        int numberOfRings = 3;
        for (int i = 0; i < numberOfRings; i++)
        {
            z += i == 0 ? 0 : 250;

            TubeObject ring = CreateRing(360, 16, -20, 10);
            ring.xScale = 2;
            ring.yScale = 2;
            ring.zScale = 2;
            ring.xPosition = 13f;

            ring.xScaleEnd = ring.yScaleEnd = 1.0f;

            ring.xPositionEnd = 17;
            ring.xPositionDuration = ring.xScaleDuration = ring.yScaleDuration = 1;
            ring.xPositionRepeatMode = ring.xScaleRepeatMode = ring.yScaleRepeatMode = RepeatMode.pingPong;
            ring.xPositionCurve = ring.xScaleCurve = ring.yScaleCurve = EaseInOutQuad;

            RandomizeTubeAngle();
            int reverse = randomBool ? -1 : 1;

            if (i == 0)
            {
                ring.tubeAngleEnd = ring.tubeAngle + randomRotation;
                ring.tubeAngleDuration = Random.Range(5f, 10f);
            }
            else if (i == 1)
            {
                ring.tubeAngleEnd = ring.tubeAngle + Random.Range(45f, 90f) * reverse;
                ring.tubeAngleDuration = 1;
                ring.tubeAngleCurve = EaseInOutSin;
                ring.tubeAngleRepeatMode = RepeatMode.pingPong;
            }
            else if (i == 2)
            {
                ring.tubeAngleEnd = ring.tubeAngle + Random.Range(540f, 720f) * reverse;
                ring.tubeAngleDuration = 2;
                ring.tubeAngleCurve = EaseOutQuad;
                ring.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
                ring.animationDistance = 200;
            }
        }
    }

    public static void HoleInOneWindmillLevel()
    {
	    int numberOfObstacles = 3;
		tubeAngle = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
	        z += i == 0 ? 0 : 200 + intraLevelZSpacingIncrement;
	        
     
	        //CreateBigFan(4, 17, 4, 2.5f, 4, 4, randomBool);
	        TubeObject tubeAngleSpinner = CreateContainer();
	        TubeObject cylinder = CreateCylinderPillar(2,20,2);
	        tubeAngleSpinner.AddChild(cylinder);
			TubeObject ring = CreateRing(360, 9f, -12, 2);
			tubeAngleSpinner.AddChild(ring);

	        cylinder.yPosition += cylinder.yScale / 2;
	        cylinder.zRotation = 45;
	        cylinder.zRotationEnd = cylinder.zRotation + randomRotation;
	        cylinder.zRotationDuration = 2.5f;

	        if (i < numberOfObstacles - 1) {
	        	cylinder.zRotationRepeatMode = RepeatMode.loop;
			} else {
				cylinder.zRotationRepeatMode = RepeatMode.pingPong;
				cylinder.zRotationCurve = EaseInOutQuad;
				cylinder.zRotationDuration += 0.5f;

			}
	        
	        cylinder.animationDistance = Random.Range(0,1000);

	        //ring.zPosition +=15;
	        ring.shadowType = ShadowType.none;
	        ring.yPosition = 10.25f;
	      

	        tubeAngleSpinner.tubeAngleEnd = ring.tubeAngle + randomRotation;
	        tubeAngleSpinner.tubeAngleDuration = Random.Range(7f, 10f);


        }
    }

    public static void EqualizerLevel()
    {
        int numberOfSheets = 5;
        int numberOfCylinders = 20;
        float cylinderWidth = .75f;

        float gap = (40 - (numberOfCylinders * cylinderWidth)) / (numberOfCylinders + 1);

        for (int i = 0; i < numberOfSheets; i++)
        {
            z += (i == 0) ? 0 : 150 + intraLevelZSpacingIncrement;
            float randomDelay = Random.Range(0, 300);
            int reverse = randomBool ? 1 : -1;

            RandomizeTubeAngle();

            TubeObject container = CreateContainer();
            container.tubeAngleEnd = container.tubeAngle + Random.Range(90f,180f) * (randomBool ? 1 : -1);
            container.tubeAngleDuration = 3f;
            container.tubeAngleRepeatMode = RepeatMode.pingPong;
            container.tubeAngleCurve = EaseInOutSin;

            for (int j = 0; j < numberOfCylinders; j++)
            {
                float xPosition = -20 + (j + 1) * gap + cylinderWidth / 2 + j * cylinderWidth;
                float cylinderHeight = 2 * Mathf.Sqrt(400 - Mathf.Abs(xPosition) * Mathf.Abs(xPosition));

                TubeObject cylinder = CreateCylinderPillar(cylinderWidth, cylinderHeight, cylinderWidth);
                cylinder.xPosition = xPosition;

                cylinder.yPositionEnd = cylinderHeight * .75f;
                cylinder.yPositionDuration = (numberOfCylinders / 12); // increase this number to speed up cylinders
                cylinder.yPositionRepeatMode = RepeatMode.pingPong;
                cylinder.yPositionCurve = EaseInQuad;

                cylinder.animationDistance = (j * 20 + randomDelay) * reverse; // decrease this number to speed up wave

                container.AddChild(cylinder);
            }
        }
    }

	public static void GuillotineLevel()
    {
        int numberOfInstances = 3;
        int startingPosition = 7;

        for (int i = 0; i < numberOfInstances; i++)
        {
            z += (i == 0) ? 100 : 300 + intraLevelZSpacingIncrement;
	        tubeAngle = 0;

//            //warning chop
//			TubeObject ring2 = CreateRing(180, 16, 0, 1);
//            ring2.xScale = 2;
//            ring2.yScale = 2;
//            ring2.yPosition = startingPosition;
//
//            ring2.yPositionEnd = -20;
//            ring2.yPositionDuration = .25f;
//			ring2.yPositionRepeatMode = RepeatMode.triggeredOnce;
//            ring2.yPositionCurve = EaseInQuad;
//            ring2.animationDistance = 200;
//	        ring2.shadowType = ShadowType.none;
//
//            TubeObject upContainer2 = CreateContainer();
//            upContainer2.AddChild(ring2);
//
//            upContainer2.yPositionEnd = startingPosition - ring2.yPositionEnd;
//            upContainer2.yPositionDuration = .25f;
//            upContainer2.yPositionRepeatMode = RepeatMode.triggeredOnce;
//            upContainer2.yPositionCurve = EaseInQuad;
//            upContainer2.animationDistance = 170;

			//actual chop

            TubeObject ring = CreateRing(180, 16, 0, 1);
            ring.xScale = 2;
            ring.yScale = 2;
            ring.yPosition = startingPosition;

            ring.yPositionEnd = -20;
            ring.yPositionDuration = .25f;
            ring.yPositionRepeatMode = RepeatMode.triggeredOnce;
            ring.yPositionCurve = EaseInExpo;
            ring.animationDistance = 35;
	        ring.shadowType = ShadowType.none;

            TubeObject upContainer = CreateContainer();
            upContainer.AddChild(ring);

            upContainer.yPositionEnd = startingPosition - ring.yPositionEnd;
            upContainer.yPositionDuration = .35f;
            upContainer.yPositionRepeatMode = RepeatMode.triggeredOnce;
            upContainer.yPositionCurve = EaseInOutExpo;
            upContainer.animationDistance = 10;

			




            TubeObject rotateContainer = CreateContainer();
            rotateContainer.AddChild(upContainer);
//            rotateContainer.AddChild(upContainer2);
            rotateContainer.tubeAngle = randomAngle;

            if (i == numberOfInstances - 1)
            {

                int reverse = randomBool ? 1 : -1;

                rotateContainer.tubeAngle = ring.tubeAngle;
                rotateContainer.tubeAngleEnd = ring.tubeAngle + 720f * reverse;
                rotateContainer.tubeAngleDuration = 3.75f;
                rotateContainer.tubeAngleCurve = EaseInOutSin;
                rotateContainer.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
                rotateContainer.animationDistance = 500;

                TubeObject reverseRotationContainer = CreateContainer();
                reverseRotationContainer.AddChild(rotateContainer);

                reverseRotationContainer.tubeAngleEnd = rotateContainer.tubeAngle - 180f * reverse;
                reverseRotationContainer.tubeAngleDuration = 1.25f;
                reverseRotationContainer.tubeAngleCurve = EaseInOutSin;
                reverseRotationContainer.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
                reverseRotationContainer.animationDistance = 150;

            }
            else
            {
                rotateContainer.tubeAngleEnd = rotateContainer.tubeAngle + (Random.Range(360,720) * (randomBool ? 1 : -1));
                rotateContainer.tubeAngleDuration = 4;
                rotateContainer.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
                rotateContainer.animationDistance = 500;
            }
        }
    }

    public static void WobbleRingsLevel()
    {
        int numberOfInstances = 5;
        int numberOfRings = 5;
        float ringWidth = Mathf.Sqrt(800 - 800 * Mathf.Cos(Mathf.Deg2Rad * (360f / numberOfRings))) * .99f;
		float animationDistance = 0;

        RandomizeTubeAngle();

        for (int i = 0; i < numberOfInstances; i++)
        {
            z += (i == 0) ? 0 : 100 + intraLevelZSpacingIncrement;

            int reverse = (i % 2 == 0) ? 1 : -1;
            tubeAngle += 180 / numberOfRings;

            for (int j = 0; j < numberOfRings; j++)
            {
	            TubeObject ring = CreateRing(180, 4, 0, 1);
	            ring.shadowType = ShadowType.directional;
                ring.xScale = ringWidth / 40;
                ring.yScale = ringWidth / 40;
                ring.yPosition = -20;
                ring.tubeAngle = tubeAngle + j * 360 / numberOfRings;

                ring.xRotation = 90;
                ring.xRotationEnd = -90f;
                ring.xRotationDuration = 2.5f;
                ring.xRotationRepeatMode = RepeatMode.pingPong;

				if (i == 0) {
					animationDistance = 180;
	            } else if (i == 1) {
					animationDistance = 0; 
	            } else if (i == 2) {
					animationDistance = j * (180 / numberOfRings);
	            } else if (i == 3) {
					animationDistance = - j * (180 / numberOfRings);
	            } else {
					animationDistance = Random.Range(-10000,10000);
	            }

                ring.animationDistance = (animationDistance) * reverse;
            }
        }
    }

    public static void CrosshatchLevel()
    {
        int numberOfInstances = 3;
	    float numberOfBlocks = 3;
        float blockWidth = 1.4f;
        float gap = (40 - numberOfBlocks * blockWidth) / (numberOfBlocks + 1);

        for (int i = 0; i < numberOfInstances; i++)
        {
	        z += (i == 0) ? 0 : 200 + intraLevelZSpacingIncrement;
	        
	        float duration = 2f;
	        float animationDistance = Random.Range(0, duration * 100);
            RandomizeTubeAngle();

            TubeObject rotatingContainer = CreateContainer();
	        rotatingContainer.tubeAngleEnd = rotatingContainer.tubeAngle + randomRotation;
            rotatingContainer.tubeAngleDuration = 15;
	        rotatingContainer.tubeAngleRepeatMode = RepeatMode.loop;
	        
	        tubeAngle = 0;
	        int centerIndex = -1;
	        if (numberOfBlocks % 2 == 1) {
	        	centerIndex = (int)(numberOfBlocks / 2f);
	        }

            for (int j = 0; j < numberOfBlocks; j++)
            {
            	if (j == centerIndex) {
	            	TubeObject cube = CreateCubePillar(blockWidth, 22, blockWidth);
	            	cube.yPosition = -tubeRadius + (cube.yScale / 2);
	            	rotatingContainer.AddChild(cube);
	            	cube = CreateCubePillar(blockWidth, 22, blockWidth);
	            	cube.yPosition = tubeRadius - (cube.yScale / 2);
	            	rotatingContainer.AddChild(cube);
            	}
            	else {
	            	TubeObject cube = CreateCubePillar(blockWidth, 40, blockWidth);
	            	cube.xPosition = -20 + gap + blockWidth / 2 + j * (blockWidth + gap) + (j - 1) * blockWidth;
	            	cube.xPositionEnd = blockWidth * (j - 1);
	            	cube.xPositionDuration = duration;
	            	cube.xPositionRepeatMode = RepeatMode.pingPong;
	            	cube.xPositionCurve = EaseInOutQuad;
	            	cube.animationDistance = animationDistance;
	            	rotatingContainer.AddChild(cube);
            	}
            }

            for (int j = 0; j < numberOfBlocks; j++)
            {
            	if (j == centerIndex) {
	            	TubeObject cube = CreateCubePillar(blockWidth, 22, blockWidth);
	            	cube.zRotation = 90;
	            	cube.xPosition = -tubeRadius + (cube.yScale / 2);
	            	rotatingContainer.AddChild(cube);
	            	cube = CreateCubePillar(blockWidth, 22, blockWidth);
	            	cube.zRotation = 90;
	            	cube.xPosition = tubeRadius - (cube.yScale / 2);
	            	rotatingContainer.AddChild(cube);
            	}
            	else {
	            	TubeObject cube = CreateCubePillar(blockWidth, 40, blockWidth);
	            	cube.zRotation = 90;
	            	cube.yPosition = -20 + gap + blockWidth / 2 + j * (blockWidth + gap) + (j - 1) * blockWidth;
	            	cube.yPositionEnd = blockWidth * (j - 1);
	            	cube.yPositionDuration = duration;
	            	cube.yPositionRepeatMode = RepeatMode.pingPong;
	            	cube.xPositionCurve = EaseInOutQuad;
	            	cube.animationDistance = animationDistance;
	            	rotatingContainer.AddChild(cube);
            	}
            }
        }
    }

    public static void TiltedGearsLevel()
    {
        int numberOfObstacles = 5;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += i == 0 ? 0 : 150 + intraLevelZSpacingIncrement;
            RandomizeTubeAngle();
            TubeObject fan = CreateBigFan(7, Random.Range(7, 10), randomBool);
            fan.xScale = 2.75f;
            fan.yScale = 2.75f;
            fan.zScale = 1.25f;
            fan.yPosition = 4;

            fan.yRotation = 45 * (randomBool ? 1 : -1);
        }
    }

	public static void SeaMonsterLevel()
	{
		int numberOfObstacles = 4;
		float sphereSize = 2f;
		int numberOfSpheres = 80;
		float gapSize = (40f - numberOfSpheres * sphereSize) / (numberOfSpheres - 1);
		int yDuration = 2;
		int xDuration = 4;
		int tubeAngleDuration = 6;

		for (int i = 0; i < numberOfObstacles; i++)
		{
			z += i == 0 ? 0 : 90;
			RandomizeTubeAngle();

			int reverse = randomBool ? 1 : -1;
			int rotationReverse = randomBool ? 1 : -1;

			for (int j = 0; j < numberOfSpheres; j++)
			{
				TubeObject sphere = CreateSphere(sphereSize);
				sphere.xPosition = -20 + sphereSize / 2 + (gapSize + sphereSize) * j;

				float chordLength = 2 * Mathf.Sqrt(400 - Mathf.Abs(sphere.xPosition) * Mathf.Abs(sphere.xPosition));

				sphere.yPosition = chordLength / 2 * reverse;
				sphere.yPositionEnd = -chordLength / 2 * reverse;
				sphere.yPositionDuration = yDuration;
				sphere.yPositionRepeatMode = RepeatMode.pingPong;
				sphere.yPositionCurve = EaseInOutSin;

				float initialZPosition = sphere.zPosition;
				sphere.zPosition = initialZPosition + chordLength / 2 * reverse;
				sphere.zPositionEnd = initialZPosition - chordLength / 2 * reverse;
				sphere.zPositionDuration = xDuration;
				sphere.zPositionRepeatMode = RepeatMode.pingPong;
				sphere.zPositionCurve = EaseInOutSin;

				sphere.tubeAngleEnd = sphere.tubeAngle + 360f * rotationReverse;
				sphere.tubeAngleDuration = tubeAngleDuration;
				sphere.tubeAngleRepeatMode = RepeatMode.loop;

				sphere.animationDistance = (j - numberOfSpheres / 2) * 4 * reverse;

				//Create flipped
				TubeObject sphere2 = CreateSphere(sphereSize);
				sphere2.xPosition = -20 + sphereSize / 2 + (gapSize + sphereSize) * j;

				sphere2.yPosition = -chordLength / 2 * reverse;
				sphere2.yPositionEnd = chordLength / 2 * reverse;
				sphere2.yPositionDuration = yDuration;
				sphere2.yPositionRepeatMode = RepeatMode.pingPong;
				sphere2.yPositionCurve = EaseInOutSin;

				initialZPosition = sphere2.zPosition;
				sphere2.zPosition = initialZPosition - chordLength / 2 * reverse;
				sphere2.zPositionEnd = initialZPosition + chordLength / 2 * reverse;
				sphere2.zPositionDuration = xDuration;
				sphere2.zPositionRepeatMode = RepeatMode.pingPong;
				sphere2.zPositionCurve = EaseInOutSin;

				sphere2.tubeAngleEnd = sphere2.tubeAngle + 360f * rotationReverse;
				sphere2.tubeAngleDuration = tubeAngleDuration;
				sphere2.tubeAngleRepeatMode = RepeatMode.loop;

				sphere2.animationDistance = (j - numberOfSpheres / 2) * 4 * reverse;
			}
		}
	}
	
	public static void BaodingBallsLevel()
    {
        int numberOfObstacles = 7;
        float sphereWidth = 18;
        float minTubeAngle = 50;
		float bounceDuration = 1;
        float rotationDuration = 4;
        System.Func<float, float> curve = EaseInOutSin;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 100 + intraLevelZSpacingIncrement;
            float startingAngle = randomAngle;

            TubeObject sphere1 = CreateSphere(sphereWidth);
            sphere1.yPosition = 20 - sphereWidth / 2;
            sphere1.tubeAngle = minTubeAngle + startingAngle;
            sphere1.tubeAngleEnd = 180 - minTubeAngle + startingAngle;
            sphere1.tubeAngleDuration = bounceDuration;
            sphere1.tubeAngleRepeatMode = RepeatMode.pingPong;
            sphere1.tubeAngleCurve = curve;

            TubeObject sphere2 = CreateSphere(sphereWidth);
            sphere2.yPosition = 20 - sphereWidth / 2;
            sphere2.tubeAngle = -minTubeAngle + startingAngle;
            sphere2.tubeAngleEnd = -180 + minTubeAngle + startingAngle;
            sphere2.tubeAngleDuration = bounceDuration;
            sphere2.tubeAngleRepeatMode = RepeatMode.pingPong;
            sphere2.tubeAngleCurve = curve;

            TubeObject rotationContainer = CreateContainer();
            rotationContainer.tubeAngleEnd = rotationContainer.tubeAngle + 360f;
            rotationContainer.tubeAngleDuration = rotationDuration;
            rotationContainer.tubeAngleRepeatMode = RepeatMode.loop;
            rotationContainer.AddChild(sphere1);
            rotationContainer.AddChild(sphere2);
        }
    }

	public static void SurpriseVolleyLevel()
    {
        int numberOfVolleys = 5;
        float pyramidWidth = 2.5f;
        float pyramidHeight = 6;
        float pyramidFallTime = .5f;

        float volleyWidth = 30;
        int startVolleyRows = 50;
        int volleyRowShortening = 3;
        int volleyColumns = 9;

        float gap = (40 - pyramidWidth * volleyColumns) / (volleyColumns - 1);
        float pyramidRandomness = gap / 3;

        for (int i = 0; i < numberOfVolleys; i++)
        {
            RandomizeTubeAngle();

            int gapPosition = Random.Range(0, volleyColumns - 1);
            int volleyRows = startVolleyRows - i * volleyRowShortening;

            for (int zIndex = 0; zIndex < volleyRows; zIndex++)
            {
                float zPositionOffset = (gap + pyramidWidth) * zIndex;
                float zPosition = gap + pyramidWidth / 2 + zPositionOffset;

                for (int xIndex = 0; xIndex < volleyColumns - 1; xIndex++)
                {
                    if (xIndex == gapPosition || xIndex == gapPosition + 1)
                    {
                        continue;
                    }

                    float zOffset = Random.Range(-pyramidRandomness, pyramidRandomness);

                    TubeObject pyramid = CreatePyramid(pyramidWidth, pyramidHeight, pyramidWidth);
                    pyramid.zRotation = 180;
                    pyramid.yRotation = randomAngle;
                    pyramid.xPosition = -volleyWidth / 2 - pyramidWidth / 2 + (gap + pyramidWidth) * xIndex + Random.Range(-pyramidRandomness, pyramidRandomness);
                    pyramid.yPosition = 20 + pyramidHeight / 2;
                    pyramid.zPosition = pyramid.zPosition + zPosition + zOffset;

                    pyramid.yPositionEnd = -20 - pyramidHeight;
                    pyramid.yPositionDuration = pyramidFallTime;
                    pyramid.yPositionRepeatMode = RepeatMode.triggeredOnce;
                    pyramid.yPositionCurve = EaseInQuad;
                    pyramid.animationDistance = 25 + (zPosition + zOffset) + zIndex * 3 + Random.Range(-5f, 5f);
                }
            }

            z += (gap + pyramidWidth / 2 + (gap + pyramidWidth) * volleyRows) * .75f;
        }
    }

    public static void FalseOpeningLevel()
    {
        int numberOfObstacles = 5;
        float ringHeight = 2;
        float ring1Depth = 5;
        float ring2Depth = 5;
       
        float animationDuration = 1.5f;
       
		tubeAngle = 0;

        for (int i = 0; i < numberOfObstacles; i++)
        {
            z += (i == 0) ? 0 : 125 + intraLevelZSpacingIncrement;

           

            //int randomizeGap = randomBool ? 1 : -1;

            TubeObject rotationContainer = CreateContainer();
            rotationContainer.tubeAngle = randomAngle;
            TubeObject ring1 = CreateContainer();

            TubeObject ring1Section1 = CreateRing(120, ringHeight, 0, ring1Depth);
			TubeObject ring1Section2 = CreateRing(120, ringHeight, 0, ring1Depth);

			ring1.AddChild(ring1Section1);
			ring1.AddChild(ring1Section2);

			rotationContainer.AddChild(ring1);

			ring1Section2.zPosition += ring1Depth + ring2Depth + 2;

			ring1Section1.arcEnd = 220;
			ring1Section1.arcDuration = animationDuration;
			ring1Section1.arcRepeatMode = RepeatMode.pingPong;
			ring1Section1.arcCurve = EaseInOutQuad;

			ring1Section2.arcEnd = 220;
			ring1Section2.arcDuration = animationDuration;
			ring1Section2.arcRepeatMode = RepeatMode.pingPong;
			ring1Section2.arcCurve = EaseInOutQuad;


			ring1.tubeAngleEnd = ring1.tubeAngle - 100;
			ring1.tubeAngleDuration = animationDuration;
			ring1.tubeAngleRepeatMode = RepeatMode.pingPong;
			ring1.tubeAngleCurve = EaseInOutQuad;
			ring1.animationDistance = 110 - (i * 10);



			TubeObject ring2 = CreateRing(120, ringHeight, 0, ring2Depth);

			ring2.tubeAngle = ring2.tubeAngle + 90;
			ring2.arcEnd = 220;
			ring2.arcDuration = animationDuration;
			ring2.arcRepeatMode = RepeatMode.triggeredOnce;
			ring2.arcCurve = EaseInOutExpo;
			ring2.tubeAngleEnd = ring2.tubeAngle + 100;
			ring2.tubeAngleDuration = animationDuration;
			ring2.tubeAngleRepeatMode = RepeatMode.pingPong;
			ring2.tubeAngleCurve = EaseInOutExpo;
			ring2.animationDistance = 140  - (i * 5);

			rotationContainer.AddChild(ring2);
			ring2.zPosition += ring1Depth + 1;

			if (i > 0) {
				rotationContainer.tubeAngleEnd = rotationContainer.tubeAngle + randomRotation;
				rotationContainer.tubeAngleDuration = 4;
				rotationContainer.tubeAngleRepeatMode = RepeatMode.loop;
				rotationContainer.animationDistance = 0;
	            //animationDistance = animationDistance - 5;
	        }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lucas Levels Batch 1
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void PachinkoLevel()
    {
        int numberOfRows = 10;
        float rowDistance = 50 + intraLevelZSpacingIncrement / 2;

        int numberOfPegsPerRow = 9;
        float pegWidth = 2;
        float pegMinHeight = 1;
        float pegMaxHeight = 12;

        float pegAngleOffset = (360 / numberOfPegsPerRow);

        for (int i = 0; i < numberOfRows; i++)
        {
            z += i == 0 ? 0 : rowDistance;
            float rowAngleOffset = 137.5f * (randomBool ? -1 : 1); // golden angle, randomized left or right

            rowDistance = rowDistance - 2;

            for (int j = 0; j < numberOfPegsPerRow; j++)
            {
                TubeObject peg = CreateCylinder(pegWidth, pegMinHeight, pegWidth);

                peg.tubeAngle = rowAngleOffset * i + j * pegAngleOffset;
                peg.yPosition = -20 + pegMaxHeight / 2;

                peg.yScaleEnd = pegMaxHeight;
                peg.yScaleDuration = .2f;
                peg.yScaleRepeatMode = RepeatMode.triggeredOnce;
                peg.yPositionCurve = EaseOutQuad;

                peg.animationDistance = 100;
            }
        }
    }

    public static void RainStickLevel()
    {
        int numberOfCylinders = 35;
        float cylinderDistance = 14 + (intraLevelZSpacingIncrement / 4);
        float minimumAngleChange = 25;
        float lastAngle = 0;
	    float cylinderWidth = 1;
	    float cylinderMaxHeight = 40;
	    
        for (int i = 0; i < numberOfCylinders; i++)
        {
	        z += i == 0 ? 0 : cylinderDistance;
	        TubeObject container = CreateContainer();
	        float angleChange = Random.Range(minimumAngleChange, 360 - minimumAngleChange);
	        container.tubeAngle = lastAngle + angleChange * (randomBool ? -1 : 1);
	        lastAngle = container.tubeAngle;
	        container.yPosition = -tubeRadius;
	        container.zRotation = 15 * (randomBool ? -1 : 1);
	        TubeObject tubeObject = CreateCubePillar(cylinderWidth, cylinderMaxHeight, cylinderWidth);
	        container.AddChild(tubeObject);
	        tubeObject.tubeAngle = 0;
	        tubeObject.yPosition = -(cylinderMaxHeight * .55f);
	        tubeObject.yPositionEnd = (cylinderMaxHeight * .49f);
	        tubeObject.yPositionDuration = .2f;
	        tubeObject.yPositionCurve = EaseOutQuad;
	        tubeObject.yPositionRepeatMode = RepeatMode.triggeredOnce;
	        tubeObject.animationDistance = Random.Range(90,110);
        }
    }

	public static void MazeLevel()
    {
        float segmentHeight = 2;
	    float narrowGapArc = 320;
	    float wideGapArc = 200;
	    float zScale = 12;
	    int numberOfNarrowGapSegments = 5;
	    int numberOfWideGapSegments = 5;
	    int numberOfNarrowGaps = 4;
	    
        float arcDifference = (narrowGapArc - wideGapArc) / 2;

	    for (int i = 0; i < numberOfNarrowGaps; i++)
	    {
		    for (int j = 0; j < numberOfNarrowGapSegments; j++) {
			    CreateRing(narrowGapArc, segmentHeight, 0, zScale);
			    z += zScale;
		    }
		    
		    if (i < numberOfNarrowGaps - 1) {
			    float tubeAngleAdjustment = arcDifference * (randomBool ? -1 : 1);
			    tubeAngle += tubeAngleAdjustment;
		    	for (int j = 0; j < numberOfWideGapSegments; j++) {
			    	CreateRing(wideGapArc, segmentHeight, 0, zScale);
			    	z += zScale;
		    	}
			    tubeAngle += tubeAngleAdjustment;
		    }
        }
    }


    public static void OneTruePathLevel()
    {
	    int numberOfPaths = 3;
	    int numberOfRings = 8;
	    float zScale = 20;
        float wallHeight = 4;
	    float choiceTime = (zScale * numberOfRings) + 100 + intraLevelZSpacingIncrement;
	    float arc = 360f / (numberOfPaths * 2); // For now, always make the ring segments and the spacing between them the same size

	    RandomizeTubeAngle();
	    
	    for (int i = 0; i < numberOfRings; i++) {
	    	z += i == 0 ? 0 : zScale;
		    for (int j = 0; j < numberOfPaths; j++, tubeAngle += (arc * 2)) {
			    CreateRing(arc, wallHeight, 0, zScale);
		    }
	    }
	    for (int i = 0; i < numberOfPaths - 1; i++) {
		    TubeObject ring = CreateRing(arc, wallHeight, 0, zScale);
		    
		    // Rings come up through tube wall - sometimes creates momentary artifacts.
		    /*
		    ring.tubeAngle += arc;
		    ring.yPosition = wallHeight + .5f;
		    ring.yPositionEnd = 0;
		    ring.yPositionDuration = .25f;
		    ring.yPositionRepeatMode = RepeatMode.triggeredOnce;
		    ring.yPositionCurve = EaseOutCubic;
		    */
		    
		    // Rings slide in like doors.
		    ring.tubeAngleEnd = ring.tubeAngle + (arc * (i == 0 ? -1 : 1));
		    ring.tubeAngleDuration = .25f;
		    ring.tubeAngleCurve = EaseOutCubic;
		    ring.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
		    
		    ring.animationDistance = choiceTime;
        }
    }

    public static void HalfRingSlapChopLevel()
    {
        int numberOfDoors = 4;
        float minX = 0;
        float maxX = 40;
        float duration = 1.1f * gameSpeed / 100;
        System.Func<float, float> curve = EaseInOutCubic;
        float durationDecrement = .1f;

        for (int i = 0; i < numberOfDoors; i++)
        {
            RandomizeTubeAngle();

            z += (i == 0) ? 0 : 150 + intraLevelZSpacingIncrement;
            bool flip = randomBool;

	        TubeObject leftDoor = CreateRing(180, 12, 0, 10, 30);
	        leftDoor.xPosition = flip ? maxX : minX;
	        leftDoor.xPositionEnd = flip ? minX : maxX;
	        leftDoor.xPositionDuration = duration;
            leftDoor.xPositionRepeatMode = RepeatMode.pingPong;
	        leftDoor.xPositionCurve = curve;
	        leftDoor.animationDistance = 15;

	        TubeObject rightDoor = CreateRing(180, 12, 0, 10, 30);
	        rightDoor.zRotation = 180;
	        rightDoor.xPosition = flip ? minX : maxX;
	        rightDoor.xPositionEnd = flip ? maxX : minX;
	        rightDoor.xPositionDuration = duration;
            rightDoor.xPositionRepeatMode = RepeatMode.pingPong;
            rightDoor.xPositionCurve = curve;
	        rightDoor.animationDistance = 15;
	        duration = duration - durationDecrement;
        }
    }

    public static void SandwormTeethLevel()
    {
        int numberOfWheels = 3;
        int numberOfSegments = 10;
	    float arc = 360 / numberOfSegments;

        for (int i = 0; i < numberOfWheels; i++)
        {
            z += i == 0 ? 0 : 175 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
	        for (int j = 0; j < numberOfSegments; j++, tubeAngle += arc)
            {
            	
	            //TubeObject ring = CreateRing(arc, 19.99f, 0, 10);
		        TubeObject tubeObject = CreatePyramidPillar(10, 20, 10);
		        tubeObject.yPosition = -45 + (tubeObject.yScale / 2);
		        tubeObject.yPositionEnd = -21 + (tubeObject.yScale / 2);
                tubeObject.yPositionDuration = .9f;
		        tubeObject.yPositionRepeatMode = RepeatMode.pingPong;
		        //tubeObject.yPositionCurve = EaseInOutQuart;
                tubeObject.animationDistance = (j * arc) * .3f;
            }
        }
    }

	

    public static void GyroscopeLevel()
    {
        int numberOfGates = 4;
	    float ringWidth = 2.5f;
	    float ringDepth = 1.5f;

        for (int i = 0; i < numberOfGates; i++)
        {
            z += (i == 0) ? 0 : 175 + intraLevelZSpacingIncrement;

            RandomizeTubeAngle();

            float rotation = randomRotation;

			// x axis
            TubeObject ringX = CreateRing(360, ringWidth, 0, ringDepth);
            ringX.xRotation = 0;
            ringX.xRotationEnd = ringX.xRotation + rotation;
            ringX.xRotationDuration = 3;
            ringX.animationDistance = 75;
			

			if (i >= 0) {
	            // y axis
	            TubeObject ringY = CreateRing(360, ringWidth * 2, ringWidth, ringDepth);
	            ringY.yRotation = 90;
	            ringY.yRotationEnd = ringY.yRotation + rotation;
	            ringY.yRotationDuration = 5;
	            ringY.animationDistance = 0;
            }

           

            if (i >= 1) {
				// z axis
	            TubeObject ringZ = CreateRing(360, ringWidth * 3, ringWidth * 2, ringDepth);
	            ringZ.xRotation = 0;
	            ringZ.yRotationEnd = ringZ.yRotation - rotation;
	            ringZ.yRotationDuration = 2;
	            ringZ.animationDistance = 37.5f;
	        }
			
	        if (i >=2) {
				// wild axis
	            TubeObject ringWild = CreateRing(360, ringWidth * 4, ringWidth * 3, ringDepth);
				ringWild.xRotation = 0;
//				ringWild.yRotationEnd = ringWild.yRotation - 360f;
//				ringWild.yRotationDuration = 1;

				ringWild.yRotation = 45;
				ringWild.xRotationEnd = ringWild.xRotation - 360f;
				ringWild.xRotationDuration = 1;
				ringWild.animationDistance = 37.5f;
	        }
        }
    }

    public static void BouldersLevel()
    {
        int numberOfBoulders = 16;
        float ringWidth = 4;
        float ringTop = 2;
	    float boulderScale = .49f;
	    int numberOfSlats = 30;
	    
	    z += 120;
	    RandomizeTubeAngle();
	    
        for (int i = 0; i < numberOfBoulders; i++)
        {
            z += (i == 0) ? 0 : 40;
	        tubeAngle += Random.Range(60f ,300f);
	        
            // x axis
	        TubeObject ringX1 = CreateRing(360, ringTop, 0, ringWidth, numberOfSlats);
            TubeObject ringX2 = CreateRing(360, ringTop, 0, ringWidth, numberOfSlats);
            TubeObject ringX3 = CreateRing(360, ringTop, 0, ringWidth, numberOfSlats);
            ringX1.xRotation = 0;
            ringX2.xRotation = 60;
	        ringX3.xRotation = 120;
	        ringX1.tubeAngle = 0;
	        ringX2.tubeAngle = 0;
	        ringX3.tubeAngle = 0;
	        ringX1.shadowType = ShadowType.directional;
	        ringX2.shadowType = ShadowType.directional;
	        ringX3.shadowType = ShadowType.directional;
	        
            // y axis
            TubeObject ringY1 = CreateRing(360, ringTop, 0, ringWidth, numberOfSlats);
            TubeObject ringY2 = CreateRing(360, ringTop, 0, ringWidth, numberOfSlats);
            TubeObject ringY3 = CreateRing(360, ringTop, 0, ringWidth, numberOfSlats);
            ringY1.yRotation = 0;
            ringY2.yRotation = 60;
            ringY3.yRotation = 120;
	        ringY1.tubeAngle = 0;
	        ringY2.tubeAngle = 0;
	        ringY3.tubeAngle = 0;
	        ringY1.shadowType = ShadowType.directional;
	        ringY2.shadowType = ShadowType.directional;
	        ringY3.shadowType = ShadowType.directional;
	        
	        TubeObject boulder = CreateContainer();
            boulder.AddChild(ringX1);
            boulder.AddChild(ringX2);
            boulder.AddChild(ringX3);
            boulder.AddChild(ringY1);
            boulder.AddChild(ringY2);
	        boulder.AddChild(ringY3);
	        
	        boulder.tubeAngle = 0;
            boulder.xScale = boulderScale;
            boulder.yScale = boulderScale;
            boulder.zScale = boulderScale;
	        boulder.xRotation = randomAngle;
	        boulder.yRotation = randomAngle;
	        boulder.zRotation = randomAngle;
	        
	        TubeObject container = CreateContainer();
	        container.AddChild(boulder);
	        container.yPosition = -(1 - boulderScale) * 20;

            float travelDistance = 200;
            float travelDuration = 4;
            container.zPositionEnd = z - travelDistance;
            container.zPositionDuration = travelDuration;
            container.zPositionRepeatMode = RepeatMode.triggeredOnce;
            container.animationDistance = 400;

            container.xRotationEnd = container.xRotation - 360;
            container.xRotationDuration = (40 * boulderScale * Mathf.PI) / (travelDistance / travelDuration);
//            container.tubeAngleEnd = container.tubeAngle + randomRotation;
//            container.tubeAngleDuration = 4; //Random.Range(1,2);
//            container.tubeAngleRepeatMode = RepeatMode.loop;
//            container.tubeAngleCurve = EaseInOutQuad;
        }
	    z -= 120;
    }
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Rotating Rings
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void RotatingRingsLevel() {
		int numberOfRings = 6;
		for (int i = 0; i < numberOfRings; i++) {
			z += i == 0 ? 0 : Random.Range(90,115) + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject ring = CreateRing(Random.Range(250,290), 6, 0, 15);
			ring.tubeAngleEnd = ring.tubeAngle + randomRotation;
			ring.tubeAngleDuration = Random.Range(4f, 6f);

		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Fast Rotating Rings Level
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void FastRotatingRingsLevel() {
		int numberOfRings = 7;
		float duration = 4;
		for (int i = 0; i < numberOfRings; i++) {
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject ring = CreateRing(360, 18, 0, 15);
			ring.tubeAngleEnd = ring.tubeAngle + (randomRotation * 10);
			ring.tubeAngleDuration = duration;
			ring.tubeAngleCurve = EaseInOutCubic;
			ring.tubeAngleRepeatMode = RepeatMode.triggeredOnce;

			ring.arcEnd = 270;
			ring.arcDuration = duration;
			ring.arcCurve = EaseInOutCubic;
			ring.arcRepeatMode = RepeatMode.triggeredOnce;

			ring.topEnd = 2;
			ring.topDuration = duration;
			ring.topCurve = EaseInOutCubic;
			ring.topRepeatMode = RepeatMode.triggeredOnce;


			ring.animationDistance = (ring.tubeAngleDuration) * 100;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Rocking Rings
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void RockingRingsLevel() {
		int numberOfRings = 7;
		for (int i = 0; i < numberOfRings; i++) {
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject ring = CreateRing(180, 10, 0, 3);
			ring.tubeAngleEnd = ring.tubeAngle + (randomBool ? 180 : -180);
			ring.tubeAngleDuration = 2f * gameSpeed / 100;
			ring.tubeAngleRepeatMode = RepeatMode.pingPong;
			ring.tubeAngleCurve = EaseInOutCubic;
			ring.animationDistance = Random.Range(0, ring.tubeAngleDuration * 100);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Full Swing Rings
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void FullSwingRingsLevel() {
		int numberOfRings = 5;
		for (int i = 0; i < numberOfRings; i++) {
			z += i == 0 ? 0 : 150 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject ring = CreateRing(290, 6, 0, 10);
			ring.tubeAngleEnd = ring.tubeAngle + randomRotation;
			ring.tubeAngleDuration = 2f;
			ring.tubeAngleRepeatMode = RepeatMode.pingPong;
			ring.tubeAngleCurve = EaseInOutExpo;
			ring.animationDistance = 400 - (i * 7);
		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Random Switch Rings
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void RandomSwitchRingsLevel() {
		int numberOfRings = 5;
		for (int i = 0; i < numberOfRings; i++) {
			z += i == 0 ? 0 : 125 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject ring = CreateRing(290, 6, 0, 10);
			ring.tubeAngleEnd = ring.tubeAngle + (Random.Range(90f, 180f) * (randomBool ? -1 : 1));
			ring.tubeAngleDuration = 1f;
			ring.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
			ring.tubeAngleCurve = EaseOutExpo;
			ring.animationDistance = 110;
			ring.tubeAngleShakingDistance = 250;
			ring.tubeAngleShakingAmount = 10;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Running Rings
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void RunningRingsLevel() {
		RandomizeTubeAngle();

		int numberOfRings = 8;
		float distance = 75 + intraLevelZSpacingIncrement;
		for (int i = 0; i < numberOfRings; i++) {
			
			tubeAngle += Random.Range(60,300);

			TubeObject ring = CreateRing(270, 6, 0, 15);
			ring.animationDistance = 50;
			ring.zPositionEnd = ring.zPosition + distance;
			ring.zPositionDuration = 1.25f;
			ring.zPositionCurve = EaseOutQuad;
			ring.zPositionRepeatMode = RepeatMode.triggeredOnce;
			if (i == 0) {
				ring.xPositionShakingDistance = 200;
				ring.xPositionShakingAmount = 2;
			}

			z += distance + 6;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Charging Rings
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void ChargingRingsLevel() {
		int numberOfRings = 5;
		float distanceToward = 400;
		float animationDistance = distanceToward * 1.2f;
		z += animationDistance * .8f;
		for (int i = 0; i < numberOfRings; i++) {
			z += i == 0 ? 0 : 170 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject container = CreateContainer();
			container.zPositionEnd = z + 125;
			container.zPositionDuration = 1;
			container.zPositionCurve = EaseInOutQuad;
			container.zPositionRepeatMode = RepeatMode.triggeredOnce;
			container.animationDistance = distanceToward * .97f;
			TubeObject ring = CreateRing(270, 6, 0, 15);
			container.AddChild(ring);
			ring.zPositionEnd = -distanceToward;
			ring.zPositionDuration = 2.5f;
			ring.zPositionCurve = EaseOutExpo;
			ring.zPositionRepeatMode = RepeatMode.triggeredOnce;
			ring.animationDistance = distanceToward * 1.2f;
		}
		z -= animationDistance * .4f;


	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Ring Channels
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void WavingChannelLevel() {
		RandomizeTubeAngle();
		int numberOfRings = 25;
		float ringLength = 20;
		for (int i = 0; i < numberOfRings; i++) {
			z += ringLength + 1;
			TubeObject ring = CreateRing(250, 4, 0, ringLength);
			ring.tubeAngleEnd = ring.tubeAngle + 100;
			ring.tubeAngleDuration = 1.5f;
			ring.tubeAngleCurve = EaseInOutQuad;
			ring.tubeAngleRepeatMode = RepeatMode.pingPong;
			ring.animationDistance = 1.5f * i * (ringLength + 1);
		}
	}
	
	public static void FlappingChannelLevel() {
		RandomizeTubeAngle();
		int numberOfRings = 15;
		float ringLength = 20;
		float gap = 35;
		float duration = .75f;
		for (int i = 0; i < numberOfRings; i++) {
			z += ringLength + 1;
			TubeObject ring = CreateRing(180, 4, 0, ringLength);
			ring.tubeAngleEnd = tubeAngle - (90 - (gap / 2));
			ring.tubeAngleDuration = duration;
			ring.tubeAngleCurve = EaseInOutQuad;
			ring.tubeAngleRepeatMode = RepeatMode.pingPong;
			ring.animationDistance = i * (ringLength + 7);
			ring = CreateRing(180, 4, 0, ringLength);
			ring.tubeAngleEnd = tubeAngle + (90 - (gap / 2));
			ring.tubeAngleDuration = duration;
			ring.tubeAngleCurve = EaseInOutQuad;
			ring.tubeAngleRepeatMode = RepeatMode.pingPong;
			ring.animationDistance = i * (ringLength + 7);
		}
	}



	public static void SpiralChannelLevel() {
		RandomizeTubeAngle();
		int numberOfRingsPerZag = 20;
		float ringLength = 10;
		float tubeAngleAdd = 10 * (randomBool ? -1 : 1);
		for (int i = 0; i < 3 ; i++) {
			
			tubeAngleAdd = tubeAngleAdd * -1;

			for (int j = 0; j < numberOfRingsPerZag; j++, tubeAngle += tubeAngleAdd){
				z += ringLength + 1;
				CreateRing(290, 4, 0, ringLength);
			}
		}
	}

	public static void CollapsingZigZagChannelLevel() {
		RandomizeTubeAngle();
		int numberOfZags = 5;
		int numberOfRingsPerZag = 12;
		float ringLength = 10;
		float startingTubeAngleAdd = 5;
		for (int j = 0; j < numberOfZags; j++){
			float tubeAngleAdd = startingTubeAngleAdd * -1;
			tubeAngleAdd = tubeAngleAdd < 0 ? tubeAngleAdd - 2 : tubeAngleAdd + 2;
			for (int i = 0; i < numberOfRingsPerZag; i++, tubeAngle += tubeAngleAdd) {
				z += ringLength + 1;
				TubeObject ring = CreateRing(260, 4, 0, ringLength);
				ring.arcEnd = 360;
				ring.animationDistance = 24;
				ring.arcDuration = .6f;
				ring.arcRepeatMode = RepeatMode.triggeredOnce;
				//ring.arcCurve = EaseInQuart;
			}
		}

	}

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Rocking Shapes
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void RockingTriangleLevel(){
		int numberOfObstacles = 12;
		RandomizeTubeAngle();
		for (int i = 0; i < numberOfObstacles; i++) {
			float spacing = i == 0 ? 0 : 50 + (intraLevelZSpacingIncrement / 5);
			z += spacing;
			TubeObject cubePillarTriangle = CreateCubePillarTriangle();
			cubePillarTriangle.tubeAngleEnd = cubePillarTriangle.tubeAngle + 180;
			cubePillarTriangle.tubeAngleDuration = 2.5f;
			cubePillarTriangle.tubeAngleRepeatMode = RepeatMode.pingPong;
			cubePillarTriangle.tubeAngleCurve = EaseInOutQuad;
			cubePillarTriangle.animationDistance = i * 56;
		}
	}
	
	private static TubeObject CreateCubePillarTriangle() {
		TubeObject cubePillarTriangle = CreateContainer();
		for (int i = 0; i < 3; i++) {
			TubeObject cubePillar = CreateCubePillar(3, 35, 3);
			cubePillar.tubeAngle = i * 120;
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -10;
			cubePillarTriangle.AddChild(cubePillar);
		}
		return cubePillarTriangle;
	}

	private static TubeObject CreateSkinnyCubePillarTriangle() {
		TubeObject cubePillarTriangle = CreateContainer();
		for (int i = 0; i < 3; i++) {
			TubeObject cubePillar = CreateCubePillar(2, 35, 2);
			cubePillar.tubeAngle = i * 120;
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -10;
			cubePillarTriangle.AddChild(cubePillar);
		}
		return cubePillarTriangle;
	}
	
	public static void RockingSquareLevel(){
		int numberOfObstacles = 12;
		RandomizeTubeAngle();
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 50 + intraLevelZSpacingIncrement / 2;
			TubeObject cubePillarSquare = CreateCubePillarSquare(3,29,3);
			cubePillarSquare.tubeAngleEnd = cubePillarSquare.tubeAngle + 140;
			cubePillarSquare.tubeAngleDuration = 2.5f;
			cubePillarSquare.tubeAngleRepeatMode = RepeatMode.pingPong;
			cubePillarSquare.tubeAngleCurve = EaseInOutQuad;
			cubePillarSquare.animationDistance = i * 56;
		}
	}
	
	private static TubeObject CreateCubePillarSquare(float x, float y, float z) {
		TubeObject cubePillarSquare = CreateContainer();
		for (int i = 0; i < 4; i++) {
			TubeObject cubePillar = CreateCubePillar(x, y, z);
			cubePillar.tubeAngle = i * 90;
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -13;
			cubePillarSquare.AddChild(cubePillar);
		}
		return cubePillarSquare;
	}

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Rotating Stars
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void RotatingStarLevel(){
		int numberOfObstacles = 8;
		float zIncrement = 150 + intraLevelZSpacingIncrement;
		for (float i = 0; i < numberOfObstacles; i++) {
			z += zIncrement - (20 * (i / numberOfObstacles));
			//z += i == 0 ? 0 : zIncrement;
			TubeObject cubePillarTriangle = CreateSkinnyCubePillarTriangle();
			cubePillarTriangle.tubeAngleEnd = (cubePillarTriangle.tubeAngle + 360);
			cubePillarTriangle.tubeAngleDuration = (6 - (i/numberOfObstacles));
			cubePillarTriangle.tubeAngleRepeatMode = RepeatMode.triggeredLoop;
			cubePillarTriangle.tubeAngleCurve = EaseInOutQuad;
			cubePillarTriangle.animationDistance = Random.Range(0,10000);
			
			TubeObject cubePillarTriangle2 = CreateSkinnyCubePillarTriangle();
			cubePillarTriangle2.tubeAngle = tubeAngle;
			cubePillarTriangle2.tubeAngleEnd = (cubePillarTriangle2.tubeAngle - 360);
			cubePillarTriangle2.tubeAngleDuration = (10 - (3 * (i/numberOfObstacles)));
			cubePillarTriangle2.tubeAngleRepeatMode = RepeatMode.triggeredLoop;
			cubePillarTriangle2.tubeAngleCurve = EaseInOutQuad;
			cubePillarTriangle2.animationDistance = Random.Range(0,10000);
			
		}
	}
	
	public static void RotatingStar8PointLevel(){
		int numberOfObstacles = 6;
		for (float i = 0; i < numberOfObstacles; i++) {
			RandomizeTubeAngle();
			z += i == 0 ? 0 : 150 - (i * 30 / numberOfObstacles);
			TubeObject cubePillarSquare = CreateCubePillarSquare(2,28,2);
			cubePillarSquare.tubeAngleEnd = cubePillarSquare.tubeAngle + 360;
			cubePillarSquare.tubeAngleDuration = (4 - (i/numberOfObstacles));
			cubePillarSquare.tubeAngleRepeatMode = RepeatMode.loop;
			cubePillarSquare.animationDistance = Random.Range(0,20);
			//cubePillarSquare.tubeAngleCurve = EaseInOutQuad;
			
			TubeObject cubePillarSquare2 = CreateCubePillarSquare(2,28,2);
			cubePillarSquare2.tubeAngle = tubeAngle;
			cubePillarSquare2.tubeAngleEnd = cubePillarSquare.tubeAngle - 360;
			cubePillarSquare2.tubeAngleDuration = (10 - (5 * (i/numberOfObstacles))) * gameSpeed / 100;
			cubePillarSquare2.tubeAngleRepeatMode = RepeatMode.loop;
			cubePillarSquare2.animationDistance = Random.Range(0,20);
			//cubePillarSquare2.tubeAngleCurve = EaseInOutQuad;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Forests
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void CylinderForestLevel() {

		float duration = 4;
		float numberOfObstacles = 30;
		float minHeight = 4;
		float maxHeight = 12;
		for (float i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 30 - (8 * (i / numberOfObstacles)) + (intraLevelZSpacingIncrement / 4);
			tubeAngle += Random.Range(45,160);

			TubeObject cylinder = CreateCylinder(2, minHeight, maxHeight);
			cylinder.yPosition = -20 + maxHeight / 2;
			cylinder.xRotation = 90;

			cylinder.yPositionEnd = -20 + 2;
			cylinder.yPositionDuration = 1;
			cylinder.yPositionCurve = EaseInOutQuad;
			cylinder.yPositionRepeatMode = RepeatMode.pingPong;

			cylinder.xScaleEnd = maxHeight;
			cylinder.xScaleDuration = 1;
			cylinder.xScaleCurve = EaseInOutQuad;
			cylinder.xScaleRepeatMode = RepeatMode.pingPong;


			cylinder.zScaleEnd = 2;
			cylinder.zScaleDuration = 1;
			cylinder.zScaleCurve = EaseInOutQuad;
			cylinder.zScaleRepeatMode = RepeatMode.pingPong;


			cylinder.tubeAngleEnd = cylinder.tubeAngle + (randomBool ? -45 : 45);
			cylinder.tubeAngleDuration = 1.5f - (0.3f * ( i / numberOfObstacles));
			cylinder.tubeAngleRepeatMode = RepeatMode.pingPong;
			cylinder.tubeAngleCurve = EaseInOutQuad;



			cylinder.animationDistance = Random.Range(0, 100);
		}
	}
	
	public static void TreeForestLevel() {
		for (int i = 0; i < 39; i++) {
			z += i == 0 ? 0 : 20 + (intraLevelZSpacingIncrement / 10);
			tubeAngle += Random.Range(25,335);
			TubeObject cylinder = CreateCylinder(2, 7, 2);
			cylinder.yPosition = -20 + (cylinder.yScale / 2);
			TubeObject sphere = CreateSphere(7);
			sphere.yPosition = -20 + cylinder.yScale;
			sphere.xScaleEnd = 14;
			sphere.xScaleDuration = 1;
			sphere.xScaleCurve = EaseInOutQuad;
			sphere.xScaleRepeatMode = RepeatMode.pingPong;
			sphere.yScaleEnd = sphere.xScaleEnd;
			sphere.yScaleDuration = 1;
			sphere.yScaleCurve = EaseInOutQuad;
			sphere.yScaleRepeatMode = RepeatMode.pingPong;
			sphere.zScaleEnd = sphere.xScaleEnd;
			sphere.zScaleDuration = 1f;
			sphere.zScaleCurve = EaseInOutQuad;
			sphere.zScaleRepeatMode = RepeatMode.pingPong;
			sphere.animationDistance = Random.Range(0, 100);
		}
	}
	
	public static void DomeForestLevel() {
		float minSize = 3;
		float maxSize = 20;
		RandomizeTubeAngle();
		for (int i = 0; i < 30; i++) {
			z += i == 0 ? 0 : 12 + intraLevelZSpacingIncrement / 4;
			tubeAngle += Random.Range(90f ,270f);
			float duration = Random.Range(1f, 2f);
			TubeObject sphere = CreateSphere(maxSize);
			sphere.yPosition = -20;
			sphere.xScaleEnd = minSize;
			sphere.xScaleDuration = duration;
			sphere.xScaleCurve = EaseInOutQuad;
			sphere.xScaleRepeatMode = RepeatMode.pingPong;
			sphere.yScaleEnd = minSize;
			sphere.yScaleDuration = duration;
			sphere.yScaleCurve = EaseInOutQuad;
			sphere.yScaleRepeatMode = RepeatMode.pingPong;
			sphere.zScaleEnd = minSize;
			sphere.zScaleDuration = duration;
			sphere.zScaleCurve = EaseInOutQuad;
			sphere.zScaleRepeatMode = RepeatMode.pingPong;
			sphere.animationDistance = Random.Range(0, duration * 100);
		}
	}
	
	public static void FlyingCylindersLevel() {
		float minSize = 3;
		float maxSize = 80;
		float height = 40;
		for (float i = 0; i < 30; i++) {
			z += i == 0 ? 0 : 24 - (8 * i / 30);
			RandomizeTubeAngle();

			TubeObject cylinder = CreateCylinderPillar(minSize, height, minSize);
			cylinder.shadowType = ShadowType.radial;
			cylinder.shadingType = ShadingType.radial;
			cylinder.yPosition = -tubeRadius - (height / 2) + 2;
			cylinder.xPosition = 5;
			cylinder.yPositionEnd = -cylinder.yPosition;
			cylinder.yPositionDuration = Random.Range(.5f,2) + Random.Range(.5f,2);
			cylinder.yPositionCurve = EaseInOutQuad;
			cylinder.yPositionRepeatMode = RepeatMode.pingPong;
//			cylinder.zScaleEnd = maxSize;
//			cylinder.zScaleDuration = duration;
//			cylinder.zScaleCurve = EaseInOutQuad;
//			cylinder.zScaleRepeatMode = RepeatMode.pingPong;
			cylinder.animationDistance = Random.Range(0, 1000);
		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Little Fans
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void LittleFansLevel() {
		int numberOfObstacles = 3;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			CreateLittleFans(4, 2);
		}
	}
	
	private static TubeObject CreateLittleFans(int numberOfFans, int numberOfBlades) {
		TubeObject fans = CreateContainer();
		float rotationDuration = 5f;
		bool fanRotationClockwise = randomBool;
		float animationDistance = Random.Range(0, rotationDuration * 100);
		for (int i = 0; i < numberOfFans; i++) {
			TubeObject fan = CreateFan(numberOfBlades, rotationDuration, animationDistance, fanRotationClockwise);
			fan.tubeAngle = i * (360f / numberOfFans);
			fan.yPosition = -10f;
			fans.AddChild(fan);
		}
		return fans;
	}
	
	private static TubeObject CreateFan(int numberOfBlades, float rotationDuration, float animationDistance, bool clockwise) {
		TubeObject fan = CreateContainer();
		for (int i = 0; i < numberOfBlades; i++) {
			TubeObject blade = CreateCubePillar(2, 25, 2);
			blade.zRotation = i * (180f / numberOfBlades);
			blade.tubeAngle = 0;
			fan.AddChild(blade);
		}
		fan.zRotationEnd = clockwise ? -360 : 360;
		fan.zRotationDuration = rotationDuration;
		fan.animationDistance = animationDistance;
		return fan;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Big Fans
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void BigFansLevel() {
		int numberOfObstacles = 4;
		int yScale = 15;
		int xScale = 3;
		int zScale = 3;
		float ringHeight = 3;


		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 150;
			RandomizeTubeAngle();
			float numberOfBlades = Random.Range(3 + i ,5 + i);
			float rotationDuration = Random.Range(6,8);

			bool rotation = randomBool;

			TubeObject fan = CreateContainer();

			float halfRingHeight = ringHeight / 2;
			if (yScale + halfRingHeight >= tubeRadius) {
				float cylinderDiameter = (tubeRadius - (yScale - halfRingHeight)) * 2;
				TubeObject cylinder = CreateCylinder(cylinderDiameter, zScale, cylinderDiameter);
				cylinder.xRotation = 90;
				fan.AddChild(cylinder);
			}
			else {
				TubeObject ring = CreateRing(360, yScale + halfRingHeight, yScale - halfRingHeight, zScale * 1.8f);
				fan.AddChild(ring);
			}
			for (int j = 0; j < numberOfBlades; j++) {
				TubeObject blade = CreateCubePillar(xScale, yScale - (halfRingHeight * .5f), zScale);
				fan.AddChild(blade);
				blade.yPosition = -20.5f + (blade.yScale / 2);
				blade.tubeAngle = j * (360f / numberOfBlades);
				blade.yRotation = 35;
			}
			fan.zRotationEnd = fan.zRotation + randomRotation;
			fan.zRotationDuration = rotationDuration;

			TubeObject oscillationContainer = CreateContainer();
			oscillationContainer.AddChild(fan);
			oscillationContainer.tubeAngleEnd = rotation ? -60 : 60;
			oscillationContainer.tubeAngleDuration = 2f + (numberOfBlades / 6);
			oscillationContainer.tubeAngleCurve = EaseInOutQuad;
			oscillationContainer.tubeAngleRepeatMode = RepeatMode.pingPong;
			oscillationContainer.animationDistance = Random.Range(0,1000);
		}
	}

	public static void BigFanTriosLevel() {
		int numberOfObstacles = 5;
//		int fansPerObstacle = 3;
//		float rotationDuration = Random.Range(8f,10f);
		bool direction = randomBool;

		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 175 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();

			CreateBigFan(3,15,3,3,35,Random.Range(4, 6), Random.Range(6f, 10f), direction);
			z += Random.Range(15,30);
			CreateBigFan(3,15,3,3,35,Random.Range(4, 4), Random.Range(4f, 6f), !direction);
			z += Random.Range(15,30);
			CreateBigFan(3,15,3,3,35,Random.Range(4, 6), Random.Range(6f, 10f), direction);

			direction = !direction;
	
		}
	}
	
	private static TubeObject CreateBigFan(
		float xScale,
		float yScale,
		float zScale,
		float ringHeight,
		float bladeRotation,
		int numberOfBlades,
		float rotationDuration,
		bool clockwise)
	{
		TubeObject fan = CreateContainer();
		
		float halfRingHeight = ringHeight / 2;
		if (yScale + halfRingHeight >= tubeRadius) {
			float cylinderDiameter = (tubeRadius - (yScale - halfRingHeight)) * 2;
			TubeObject cylinder = CreateCylinder(cylinderDiameter, zScale, cylinderDiameter);
			cylinder.xRotation = 90;
			fan.AddChild(cylinder);
		}
		else {
			TubeObject ring = CreateRing(360, yScale + halfRingHeight, yScale - halfRingHeight, zScale);
			fan.AddChild(ring);
		}
		for (int i = 0; i < numberOfBlades; i++) {
			TubeObject blade = CreateCubePillar(xScale, yScale - (halfRingHeight * .5f), zScale);
			fan.AddChild(blade);
			blade.yPosition = -20 + (blade.yScale / 2) - 0.5f;
			blade.tubeAngle = i * (360f / numberOfBlades);
			blade.yRotation = bladeRotation;
			blade.zPosition += 0.5f;
		}
		fan.zRotationEnd = clockwise ? 360 : -360;
		fan.zRotationDuration = rotationDuration;
		return fan;
	}
		
	private static TubeObject CreateBigFan(int numberOfBlades, float rotationDuration, bool clockwise) {
		return CreateBigFan(3, 15, 3, 3, 0, numberOfBlades, rotationDuration, clockwise);
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Crazy Rings
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void CrazyRingsLevel() {
		int numberOfObstacles = 6;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : Random.Range(95,110) + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject ring = CreateRing(270, 3, 0, 10);
			ring.tubeAngleEnd = ring.tubeAngle + randomRotation;
			ring.tubeAngleDuration = Random.Range(5f, 10f) / difficultyFactor;
			ring.arcEnd = 90;
			ring.arcCurve = EaseInOutQuad;
			ring.arcDuration = Random.Range(1f, 3f) / difficultyFactor;
			ring.arcRepeatMode = RepeatMode.pingPong;
			ring.topEnd = ring.top + 10;
			ring.topCurve = EaseInOutQuad;
			ring.topDuration = Random.Range(1f, 1.8f);
			ring.topRepeatMode = RepeatMode.pingPong;
			ring.animationDistance = Random.Range(0f, 1000f);
		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// SnakesLevel
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void SnakesLevel() {
		
		tubeAngle = 0;
		int numberOfObjects = 35;
		float angle = randomAngle;
		float angleChange = 180;
		float animationDistance = Random.Range(0, 300f);
		
		for (int i = 0; i < numberOfObjects; i++) {
			z += (i == 0) ? 0 : 10;
			TubeObject ellipsoid1 = CreateEllipsoid(15, 5, 10);
			ellipsoid1.yPosition = -20 + (ellipsoid1.yScale / 2);
			ellipsoid1.tubeAngle = angle;
			ellipsoid1.tubeAngleEnd = ellipsoid1.tubeAngle + angleChange;
			ellipsoid1.tubeAngleDuration = 2f;
			ellipsoid1.tubeAngleRepeatMode = RepeatMode.pingPong;
			ellipsoid1.tubeAngleCurve = EaseInOutExpo;
			ellipsoid1.animationDistance = animationDistance + (i * 20);
			
			TubeObject ellipsoid2 = CreateEllipsoid(15, 5, 8);
			ellipsoid2.yPosition = -20 + (ellipsoid1.yScale / 2);
			ellipsoid2.tubeAngle = angle + 180;
			ellipsoid2.tubeAngleEnd = ellipsoid2.tubeAngle + angleChange;
			ellipsoid2.tubeAngleDuration = 2.05f;
			ellipsoid2.tubeAngleRepeatMode = RepeatMode.pingPong;
			ellipsoid2.tubeAngleCurve = EaseInOutQuad;
			ellipsoid2.animationDistance = animationDistance + (i * 21);
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Teeth
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void TeethLevel() {
		float numberOfObstacles = 5;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 120 - (30f * (i / numberOfObstacles));
			RandomizeTubeAngle();
			TubeObject teeth = CreateTeeth(5 + Mathf.RoundToInt((1f * (i / numberOfObstacles))));
			teeth.tubeAngleEnd = teeth.tubeAngle + randomRotation;
			teeth.tubeAngleDuration = Random.Range(5, 10);

		}
	}

	private static TubeObject CreateTeeth(int numberOfTeeth) {
		TubeObject teeth = CreateContainer();
		float height = 22f;
		float width = 12;
		float scale = .2f;
		//float animationDistance = Random.Range(0, 100f);
		for (int i = 0; i < numberOfTeeth; i++, tubeAngle += (360 / numberOfTeeth)) {
			TubeObject tubeObject = CreatePyramidPillar(width, height, width);
			teeth.AddChild(tubeObject);
			tubeObject.xScaleEnd = width * scale;
			tubeObject.xScaleDuration = .8f;
			tubeObject.xScaleCurve = EaseInQuad;
			tubeObject.xScaleRepeatMode = RepeatMode.pingPong;
			tubeObject.yScaleEnd = height * scale;
			tubeObject.yScaleDuration = .8f;
			tubeObject.yScaleCurve = EaseInQuad;
			tubeObject.yScaleRepeatMode = RepeatMode.pingPong;
			tubeObject.zScaleEnd = width * scale;
			tubeObject.zScaleDuration = .8f;
			tubeObject.zScaleCurve = EaseInQuad;
			tubeObject.zScaleRepeatMode = RepeatMode.pingPong;
			tubeObject.yPosition = -23.5f + (tubeObject.yScale / 2);
			tubeObject.yPositionEnd = -20 + (tubeObject.yScaleEnd / 2);
			tubeObject.yPositionDuration = .8f;
			tubeObject.yPositionCurve = EaseInQuad;
			tubeObject.yPositionRepeatMode = RepeatMode.pingPong;
			tubeObject.animationDistance = Random.Range(0, 100f);
			tubeObject.yRotation = 30;
		}
		return teeth;
	}
	
	public static void CrookedTeethLevel() {
		int numberOfObstacles = 5;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 190;
			RandomizeTubeAngle();
			TubeObject teeth = CreateCrookedTeeth();
			teeth.tubeAngleEnd = teeth.tubeAngle + randomRotation;
			teeth.tubeAngleDuration = 6;
		}
	}
	
	private static TubeObject CreateCrookedTeeth() {
		TubeObject teeth = CreateContainer();
		int numberOfTeeth = 8;
		for (int i = 0; i < numberOfTeeth; i++) {
			TubeObject tubeObject = CreatePyramidPillar(1, 1, 10);
			tubeObject.tubeAngle = i * (360f / numberOfTeeth);
			teeth.AddChild(tubeObject);
			
			if ((i == 1) || (i == 6) ){
				tubeObject.yScale = 10;
				tubeObject.xScale = 5;
				tubeObject.zScale = 5;
				tubeObject.yPosition = -20;
				tubeObject.yPositionEnd = -5 - .1f;
			}
			else {
				tubeObject.yScale = 20;
				tubeObject.xScale = 10;
				tubeObject.yPosition = -25;
				tubeObject.yPositionEnd = -10 - .1f;
			}
			//tubeObject.zScale = 10;
			tubeObject.yPositionDuration = 1;
			tubeObject.yPositionCurve = EaseInOutCubic;
			tubeObject.yPositionRepeatMode = RepeatMode.pingPong;
			tubeObject.animationDistance = 120;
			tubeObject.yRotation = 30;
		}
		return teeth;
	}
		
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Ring Gate
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void RingGateLevel() {
		int numberOfObstacles = 6;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 100 - (2 * i) + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject ring = CreateRing(360, 3, 0, 2, 100);
			ring.animationDistance = 150 - (2 * i);
			ring.xRotation = 0;
			ring.xRotationEnd = 50;
			ring.xRotationDuration = .5f;
			ring.xRotationCurve = EaseInOutCubic;
			ring.xRotationRepeatMode = RepeatMode.triggeredOnce;
		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Jack In Boxes
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void JackInBoxesLevel() {
		RandomizeTubeAngle();
		CreateJackInBoxes(3,45,120);
		z += 45;
		CreateJackInBoxes(22,20 + (intraLevelZSpacingIncrement / 10),0);  
	}
	
	private static void CreateJackInBoxes(int numberOfBoxes, int zSpacing, int tubeAngleDynamism){
		for (int i = 0; i < numberOfBoxes; i++){
			z += i == 0 ? 0 : zSpacing;
			if (tubeAngleDynamism == 0){
				tubeAngle += Random.Range(90f, 270f);
			} else {
				tubeAngle = tubeAngle + tubeAngleDynamism;
			}
			CreateJackInBox();
		}
	}
	
	private static void CreateJackInBox(){
		float size = 19;
		float duration = .05f;
		TubeObject jack = CreateCylinder(5, 2.5f, 5);
		jack.yPosition = -20 + (jack.yScale / 2);
		jack.xScaleEnd = size;
		jack.xScaleDuration = duration;
		jack.xScaleRepeatMode = RepeatMode.triggeredOnce;
		jack.yScaleEnd = size / 3;
		jack.yScaleDuration = duration;
		jack.yScaleRepeatMode = RepeatMode.triggeredOnce;
		jack.zScaleEnd = size;
		jack.zScaleDuration = duration;
		jack.zScaleRepeatMode = RepeatMode.triggeredOnce;
		jack.yPositionEnd = -20 + (jack.yScaleEnd / 2);
		jack.yPositionDuration = duration;
		jack.yPositionRepeatMode = RepeatMode.triggeredOnce;
		jack.animationDistance = 30;
		jack.xPositionShakingDistance = 200;
		jack.xPositionShakingAmount = 3;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Bouncing Balls
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void BouncingBallsLevel() {
		for (int i = 0; i < 25; i++) {
			z += i == 0 ? 0 : 18;
			RandomizeTubeAngle();
			TubeObject sphere = CreateSphere(5);
			sphere.yPosition = -20 + (sphere.yScale / 2);
			sphere.yPositionEnd = -13;
			sphere.yPositionDuration = .4f;
			sphere.yPositionCurve = EaseOutCubic;
			sphere.yPositionRepeatMode = RepeatMode.pingPong;
			sphere.tubeAngleEnd = sphere.tubeAngle + randomRotation;
			sphere.tubeAngleDuration = Random.Range(3f, 6f);
			sphere.animationDistance = Random.Range(0, 40f);
		}
	}
	
	public static void ShiftingBallsRingsLevel() {
		int numberOfObstacles = 6;
		int minBalls = 2;
		int balls = minBalls;
		int maxBalls = 6;
		int minRings = 4;
		int rings = minRings;
		int maxRings = 6;

		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 150 - (10 * i);
			RandomizeTubeAngle();

			float rotation = randomRotation;
//			TubeObject sphereContainer = CreateContainer();

			float animationDistance = randomBool ? -10000 : 10000;

			for (int j = 0; j < balls; j++) {
				
				TubeObject sphere = CreateSphere(5);
//				sphereContainer.AddChild(sphere);
				sphere.tubeAngle = tubeAngle + j * (360f / balls);
				sphere.yPosition = -20 + (sphere.yScale / 2);
//				sphere.yPositionEnd = -13;
//				sphere.yPositionDuration = .4f;
//				sphere.yPositionCurve = EaseOutCubic;
//				sphere.yPositionRepeatMode = RepeatMode.pingPong;
				
				sphere.tubeAngleEnd = sphere.tubeAngle + (rotation / rings);
				sphere.tubeAngleDuration = Random.Range(1f, 1f);
				sphere.tubeAngleRepeatMode = RepeatMode.pingPong;
				sphere.animationDistance = animationDistance;
				sphere.tubeAngleCurve = EaseInOutExpo;
			}

			balls = Mathf.Min((balls + 2),maxBalls);

			for (int k = 0; k < rings; k++) {

				TubeObject ring = CreateRing(360, 6, 5, 3);

				ring.tubeAngle = tubeAngle + k * (360f / rings);
				ring.yPosition = -20 + (ring.yScale / 2);
				ring.yPositionEnd = -18;
				ring.yPositionDuration = 2.4f;
				ring.yPositionCurve = EaseOutCubic;
				ring.yPositionRepeatMode = RepeatMode.pingPong;
				ring.animationDistance = Random.Range(0, 0f);

				ring.tubeAngleEnd = ring.tubeAngle - rotation;
				ring.tubeAngleDuration = Random.Range(5f, 5f);
				ring.tubeAngleCurve = EaseInOutExpo;
				ring.tubeAngleRepeatMode = RepeatMode.pingPong;
				ring.animationDistance = animationDistance;



			}

			rings = Mathf.Min(rings + 2,maxRings);


		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Stalactites
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void StalactitesLevel() {
		RandomizeTubeAngle();
		for (int i = 0; i < 40; i++) {
			z += i == 0 ? 0 : 12;
			tubeAngle += Random.Range(90f ,270f);
			TubeObject pyramid = CreatePyramid(4, 10, 4);
			pyramid.yPosition = -20 + pyramid.yScale / 2;
			pyramid.yPositionEnd = 22 - pyramid.yScale / 2;
			pyramid.yPositionDuration = .4f * gameSpeed / 100;
			pyramid.yPositionCurve = EaseInCubic;
			pyramid.yPositionRepeatMode = RepeatMode.triggeredOnce;
			pyramid.animationDistance = 100;
			pyramid.xPositionShakingDistance = 250;
			pyramid.xPositionShakingAmount = 3;
		}
	}
	
	public static void SunSpikesLevel() {
		int numberOfWaves = 31;



		for (int i = 0; i < numberOfWaves + 1; i++) {
			z += i == 0 ? 0 : 60 - (10 * (i / numberOfWaves));
			RandomizeTubeAngle();

			int gapManager1 = Random.Range(0,180);
			int gapManager2 = gapManager1 + 180;

			for (int j = 1; j < (i * 2); j++){
				
				TubeObject pyramid = CreatePyramid(2.5f, 12, 2.5f);
				pyramid.yPosition = -2 + pyramid.yScale / 2;
				pyramid.yPositionEnd = 22 - pyramid.yScale / 2;
				pyramid.yPositionDuration = .15f;
				pyramid.yPositionCurve = EaseInCirc;
				pyramid.yPositionRepeatMode = RepeatMode.triggeredOnce;
				pyramid.animationDistance = 110 - (10 * (i / numberOfWaves));
				pyramid.xPositionShakingDistance = 200;
				pyramid.xPositionShakingAmount = 1;
				//pyramid.yRotation = 45;


				pyramid.tubeAngle = randomBool ? Random.Range(gapManager1,gapManager1 + 150) : Random.Range(gapManager2,gapManager2 + 150);

			}



			}
		}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Walking Sticks
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void WalkingSticksLevel() {
		int numberOfObstacles = 20;
		RandomizeTubeAngle();
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 30 + intraLevelZSpacingIncrement / 4;
			tubeAngle += Random.Range(90f, 270f);
			float rotation = randomRotation;
			float duration = Random.Range(1, 5);
			float height = 15;
			TubeObject tubeObject = CreateCylinderPillar(3, height, 3);
			tubeObject.zRotationEnd = rotation;
			tubeObject.zRotationDuration = duration;
			tubeObject.yPosition = -20 + (height / 2);
			tubeObject.yPositionEnd = -18;
			tubeObject.yPositionDuration = duration / 4;
			tubeObject.yPositionRepeatMode = RepeatMode.pingPong;
			tubeObject.yPositionCurve = EaseInSin;
			tubeObject.tubeAngleEnd = tubeObject.tubeAngle - rotation;
			tubeObject.tubeAngleDuration = duration * 3.1f;
		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Opposite Arc Gates
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void OppositeRotatingSegmentsLevel() {
		int numberOfObstacles = 7;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject ring = CreateRing(125, 5, 0, 10);
			ring.tubeAngle = randomAngle;
			ring.tubeAngleEnd = ring.tubeAngle + 360;
			ring.tubeAngleDuration = Random.Range(4, 8);
			z += 10;
			tubeAngle += 180;
			ring = CreateRing(135, 7, 0, 12);
			ring.tubeAngle = randomAngle;
			ring.tubeAngleEnd = ring.tubeAngle - 360;
			ring.tubeAngleDuration = Random.Range(6, 10);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Floor Fans
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void FloorFansLevel() {
		int numberOfObstacles = 25;
		RandomizeTubeAngle();
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 34 + (intraLevelZSpacingIncrement / 2);
			tubeAngle += Random.Range(90f, 270f);
			TubeObject ring = CreateRing(110, 2, 0, 3);
			ring.yRotationEnd = randomRotation;
			ring.yRotationDuration = Random.Range(2f, 4f);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Arc Choppers
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void ArcChoppersLevel() {
		int numberOfObstacles = 10;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 70 + intraLevelZSpacingIncrement;
			tubeAngle += Random.Range(60,300);
			TubeObject ring = CreateRing(170, 2, 0, 3, 100);
			ring.xRotationEnd = randomRotation;
			ring.xRotationDuration = Random.Range(2f, 5f) * gameSpeed / 100;
		}
	}

	public static void MiniArcSinChoppersLevel() {
		int numberOfObstacles = 10;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 85;
			RandomizeTubeAngle();
			//float rotation = randomRotation;
			float rotationDuration = Random.Range(2f, 5f);
			float animationDistance = Random.Range(0, rotationDuration * 100);
			TubeObject ring1 = CreateRing(70, 2, 0, 3, 100);
			ring1.xRotationEnd = randomRotation;
			ring1.xRotationDuration = rotationDuration;
			ring1.xRotationCurve = EaseInSin;
			ring1.animationDistance = animationDistance;
			TubeObject ring2 = CreateRing(70, 2, 0, 3, 100);
			ring2.tubeAngle = ring1.tubeAngle + ring1.arc;
			ring2.xRotationEnd = randomRotation;
			ring2.xRotationDuration = rotationDuration;
			ring2.xRotationCurve = EaseInSin;
			ring2.animationDistance = animationDistance;
		}
	}

	public static void TrickyChoppersLevel() {
		int numberOfObstacles = 10;
		for (float i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : (110 - (2*i));
			RandomizeTubeAngle();
			float duration = 3.5f - (1.5f * ( i / (numberOfObstacles - 1)));
			float animationDistance =  randomBool ? (-10 - (3 * i)) : (380 - (15 * i));
			float rotation = randomRotation;

			TubeObject ring1 = CreateRing(180, 2, 0, 3, 100);
			ring1.xRotationEnd = randomRotation;
			ring1.xRotationDuration = duration + 1f;
			ring1.xRotationCurve = EaseInOutQuart;
			ring1.arcEnd = 70;
			ring1.arcDuration = duration;
			ring1.arcCurve = EaseInOutCubic;
			ring1.arcRepeatMode = RepeatMode.pingPong;
			ring1.tubeAngleEnd = ring1.tubeAngle + rotation;
			ring1.tubeAngleDuration = duration + 1f;
			ring1.tubeAngleRepeatMode = RepeatMode.loop;
			ring1.animationDistance = animationDistance;
			 
			TubeObject ring2 = CreateRing(70, 2, 0, 3, 100);

			ring2.tubeAngle = ring1.tubeAngle + ring1.arc;
			ring2.xRotationEnd = ring1.xRotationEnd;
			ring2.xRotationDuration = duration  +  1f;
			ring2.xRotationCurve = EaseInOutQuart;
			ring2.arcEnd = 180;
			ring2.arcDuration = duration;
			ring2.arcCurve = EaseInOutCubic;
			ring2.arcRepeatMode = RepeatMode.pingPong;

			ring2.tubeAngleEnd = ring2.tubeAngle + rotation;
			ring2.tubeAngleDuration = duration + 2f;
			ring2.tubeAngleRepeatMode = RepeatMode.loop;
			ring2.animationDistance = animationDistance;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Mice
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void CirclingMiceLevel() {
		int numberOfObstacles = 30;
		float duration = 1;
		RandomizeTubeAngle();
		for (int i = 0; i < numberOfObstacles; i++) {
			float animationDistance = Random.Range(0, duration * 100);
			tubeAngle += Random.Range(90f, 270f);
			z += i == 0 ? 0 : 25 + intraLevelZSpacingIncrement / 5;
			TubeObject container = CreateContainer();
			container.zPositionEnd = z + 40;
			container.zPositionDuration = duration;
			container.zPositionRepeatMode = RepeatMode.pingPong;
			container.zPositionCurve = EaseInOutQuad;
			container.animationDistance = animationDistance + (duration * 50);
			TubeObject pyramid = CreatePyramid(4, 10, 4);
			container.AddChild(pyramid);
			pyramid.yPosition = -18.5f;
			pyramid.xRotation = 100;
			pyramid.yRotationEnd = pyramid.yRotation + 360;
			pyramid.yRotationDuration = duration * 2;
			pyramid.animationDistance = animationDistance;
			pyramid.tubeAngleEnd = pyramid.tubeAngle + 90;
			pyramid.tubeAngleDuration = duration;
			pyramid.tubeAngleRepeatMode = RepeatMode.pingPong;
			pyramid.tubeAngleCurve = EaseInOutQuad;
			pyramid.animationDistance = animationDistance;
		}
	}
	
	public static void ChargingMiceLevel() {
		z += 100;
		int numberOfObstacles = 30;
		float waveDuration = .08f;
		RandomizeTubeAngle();
		for (int i = 0; i < numberOfObstacles; i++) {
			
			z += 20 + (intraLevelZSpacingIncrement / 10);
			tubeAngle += Random.Range(60f, 300f);
			
			TubeObject container = CreateContainer();
			container.zPositionEnd = z - 400;
			container.zPositionDuration = .85f;
			container.zPositionRepeatMode = RepeatMode.triggeredOnce;
			container.tubeAngleEnd = container.tubeAngle + 20;
			container.tubeAngleDuration = waveDuration;
			container.tubeAngleRepeatMode = RepeatMode.triggeredPingPong;
			container.tubeAngleCurve = EaseInOutQuad;
			container.animationDistance = 200;
			container.xPositionShakingDistance = 400;
			container.xPositionShakingAmount = 2;
			
			TubeObject pyramid = CreatePyramid(4, 10, 4);
			container.AddChild(pyramid);
			pyramid.tubeAngle = 0;
			pyramid.yPosition = -18.5f;
			pyramid.xRotation = -100;
			pyramid.yRotation = 20;
			pyramid.yRotationEnd = -20;
			pyramid.yRotationDuration = waveDuration;
			pyramid.yRotationCurve = EaseOutInQuad;
			pyramid.yRotationRepeatMode = RepeatMode.triggeredPingPong;
			pyramid.animationDistance = container.animationDistance + (waveDuration * 50);
		}
		z -= 100;
	}

	public static void RockSlideLevel() {
		z += 600;
		int numberOfObstacles = 200;
		float waveDurationMidpoint = 6f;
		float sphereDiameter = 0;
		TubeObject rock = null;
		int waveLength = 750;
		for (float i = 0; i < numberOfObstacles; i++) {
			RandomizeTubeAngle();
			z += (10f + (intraLevelZSpacingIncrement / 10)) - (5 * (i / (i + 1)));
			sphereDiameter = Random.Range(2,6);
			rock = CreateSphere(sphereDiameter);
			rock.yPosition = -20f + (sphereDiameter / 2);
			rock.yPositionEnd = rock.yPosition + sphereDiameter / 2;
			rock.yPositionCurve = EaseOutCubic;
			rock.yPositionDuration = ((rock.yPositionEnd - rock.yPosition) / 15) * (gameSpeed / 100);
			rock.yPositionRepeatMode = RepeatMode.pingPong;
			
			rock.zPositionEnd = z - waveLength;
			rock.zPositionDuration = waveDurationMidpoint * (gameSpeed / 100) + Random.Range(-1,1) ;
			rock.zPositionRepeatMode = RepeatMode.triggeredOnce;
			rock.zPositionCurve = EaseOutExpo;
//			rock.tubeAngleEnd = rock.tubeAngle + Random.Range(-5,5);
//			rock.tubeAngleDuration = waveDuration;
//			rock.tubeAngleRepeatMode = RepeatMode.triggeredPingPong;
//			rock.tubeAngleCurve = EaseInOutQuad;
			rock.animationDistance = waveLength;
		}
		z -= 500;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Rolling Rings
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void RollingRingsLevel() {
		int numberOfObstacles = 5;
		int numberOfRings = 6;
		float ringScale = .36f;
		float ringRadius = tubeRadius * ringScale;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : Random.Range(95,110) + intraLevelZSpacingIncrement;
			float duration = Random.Range(5, 10);
			float rotation = randomRotation;
			RandomizeTubeAngle();
			TubeObject container = CreateContainer();
			container.tubeAngleEnd = container.tubeAngle + rotation;
			container.tubeAngleDuration = duration / difficultyFactor;
			for (int j = 0; j < numberOfRings; j++) {
				TubeObject ring = CreateRing(240, tubeRadius - ringRadius + 2, tubeRadius - ringRadius, 2);
				container.AddChild(ring);
				ring.shadowType = ShadowType.directional;
				ring.yPosition = -tubeRadius + ringRadius;
				ring.tubeAngle = 360 * (j / (float)numberOfRings);
				ring.zRotationEnd = -rotation;
				ring.zRotationDuration = duration * ringScale / difficultyFactor;
			}
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Doors
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void PlutoniumDoorsLevel() {
		int numberOfObstacles = 9;
		int numberOfDoors = 6;
		float top = 12;
		float bottom = 10;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : Random.Range(60,100);
			RandomizeTubeAngle();

			int animationDistance = randomBool ? 0 : 50;

			TubeObject rotationContainer = CreateContainer();

			for (int j = 0; j < numberOfDoors; j++, tubeAngle += 360f / numberOfDoors) {
				bool open = (j % 2 == 0);
				TubeObject ring = CreateRing(360f / numberOfDoors, top, open ? bottom : 0, 10);
				ring.bottomEnd = open ? 0 : bottom;
				ring.bottomDuration = .8f;
				ring.bottomCurve = EaseInOutExpo;
				ring.bottomRepeatMode = RepeatMode.pingPong;
				ring.animationDistance = animationDistance;
//				ring.tubeAngleShakingDistance = 160;
//				ring.tubeAngleShakingAmount = 8;
				rotationContainer.AddChild(ring);
			}

			if (i == (numberOfObstacles - 1)){
				rotationContainer.tubeAngleEnd = rotationContainer.tubeAngle + randomRotation;
				rotationContainer.tubeAngleDuration = 2f;
				rotationContainer.tubeAngleRepeatMode = RepeatMode.loop;
			}
		}
	}
	
	public static void DoorOpenLevel() {
		int numberOfObstacles = 20;
		float top = 12;
		float bottom = 11;
		float arc = 300;
		int tubeAngleTotalChange = 0;

		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 50 + intraLevelZSpacingIncrement;

			CreateRing(arc, top, 0, 4);
			tubeAngle += 180;

			TubeObject ring = CreateRing(360 - arc, top, 0, 3);
			ring.bottomEnd = bottom;
			ring.bottomDuration = .9f;
			ring.bottomCurve = EaseOutExpo;
			ring.bottomRepeatMode = RepeatMode.triggeredOnce;
			ring.animationDistance = i == 0 ? 120 : 120;
			tubeAngle += 180;
			int tubeAngleChange = randomBool ? Random.Range(30 + i,50 + i) : Random.Range(-30 - i,-50 - i);

			if (Mathf.Abs(tubeAngleChange + tubeAngleTotalChange) > 180) {
				tubeAngleChange = -tubeAngleChange;
			}

			tubeAngle += tubeAngleChange;
			tubeAngleTotalChange += tubeAngleChange;

			
		}
	}
	
	public static void DoorCloseLevel() {
		int numberOfObstacles = 3;
		int numberOfDoors = 6;
		float top = 12;
		float bottom = 10;
		List<TubeObject> doors = new List<TubeObject>();
		List<TubeObject> doorsToClose = new List<TubeObject>();
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 175 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			doors.Clear();
			for (int j = 0; j < numberOfDoors; j++, tubeAngle += 360f / numberOfDoors) {
				TubeObject door = CreateRing(360f / numberOfDoors, top, bottom, 10);
				doors.Add(door);
			}
			doorsToClose.Clear();
			doors.RemoveAt(0);
			int lastClosingIndex = Random.Range(1, numberOfDoors - 2);
			doorsToClose.Add(doors[lastClosingIndex]);
			doors.RemoveAt(lastClosingIndex);
			doors.Shuffle();
			doorsToClose.AddRange(doors);
			for (int j = 0; j < doorsToClose.Count; j++) {
				TubeObject door = doorsToClose[j];
				door.bottomEnd = 0;
				door.bottomDuration = .9f;
				door.bottomCurve = EaseOutExpo;
				door.bottomRepeatMode = RepeatMode.triggeredOnce;
				door.animationDistance = (100 + (j * 30));
			}
		}
	}
	
	public static void RandomDoorSwitchLevel() {
		int numberOfObstacles = 3;
		int numberOfDoors = 6;
		float top = 12;
		float bottom = 10;
		float arc = (360f / numberOfDoors) - 2;
		float switchSpeed = 70;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 200;
			RandomizeTubeAngle();
			List<int> indices = new List<int>();
			for (int j = 0; j < numberOfDoors; j++) {
				indices.Add(j);
			}
			indices.Shuffle();
			for (int j = 0; j < numberOfDoors; j++, tubeAngle += 360f / numberOfDoors) {
				
				TubeObject openingDoor = CreateRing(arc, top, 0, 10);
				openingDoor.bottomEnd = bottom;
				openingDoor.bottomCurve = EaseOutExpo;
				openingDoor.bottomRepeatMode = RepeatMode.triggeredOnce;
				openingDoor.bottomDuration = .9f;
				openingDoor.animationDistance = 90 + (indices[j] * switchSpeed);
				
				TubeObject closingDoor = CreateRing(arc, top, bottom, 10);
				closingDoor.bottomEnd = 0;
				closingDoor.bottomCurve = EaseOutExpo;
				closingDoor.bottomRepeatMode = RepeatMode.triggeredOnce;
				closingDoor.bottomDuration = .9f;
				closingDoor.animationDistance = openingDoor.animationDistance - (switchSpeed * 1.5f);
			}
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Passage
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void JaggedPassageLevel() {
		RandomizeTubeAngle();
		int numberOfObstacles = 20;
		for (int i = 0; i < numberOfObstacles; i++, tubeAngle += 5 * (randomBool ? -1 : 1)) {
			z += 20;
			TubeObject cubePillar = CreateCubePillar(14, 39, 20);
			cubePillar.shadingType = ShadingType.directional;
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -tubeRadius + (cubePillar.xScale / 2);
			tubeAngle += 180;
			cubePillar = CreateCubePillar(14, 39, 20);
			cubePillar.shadingType = ShadingType.directional;
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -tubeRadius + (cubePillar.xScale / 2);
		}
	}
	
	public static void SpinningJaggedPassageLevel() {
		
		int numberOfObstacles = 20;
		float duration = .3f;
		float slabLength = 23;
		float rockingAngle = 120;
		
		RandomizeTubeAngle();
		z += slabLength * numberOfObstacles;
		TubeObject container = CreateContainer();
		
		tubeAngle = 0;
		float currentZ = -slabLength / 2;
		
		for (int i = 0; i < numberOfObstacles; i++, tubeAngle += 6 * (randomBool ? -1 : 1), currentZ -= slabLength) {
			
			TubeObject cubePillar = CreateCubePillar(13, 39, slabLength - 1);
			container.AddChild(cubePillar);
			cubePillar.zPosition = currentZ;
			cubePillar.shadingType = ShadingType.directional;
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -tubeRadius + (cubePillar.xScale / 2);
			cubePillar.yPositionEnd = cubePillar.yPosition - 1;
			cubePillar.yPositionDuration = duration;
			cubePillar.yPositionRepeatMode = RepeatMode.pingPong;
			cubePillar.yPositionCurve = EaseInOutQuad;
			cubePillar.animationDistance = Random.Range(0, cubePillar.yPositionDuration * 100);

			cubePillar = CreateCubePillar(13, 39, slabLength - 1);
			container.AddChild(cubePillar);
			cubePillar.zPosition = currentZ;
			cubePillar.shadingType = ShadingType.directional;
			cubePillar.tubeAngle += 180;
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -tubeRadius + (cubePillar.xScale / 2);
			cubePillar.yPositionEnd = cubePillar.yPosition - 1;
			cubePillar.yPositionDuration = duration;
			cubePillar.yPositionRepeatMode = RepeatMode.pingPong;
			cubePillar.yPositionCurve = EaseInOutQuad;
			cubePillar.animationDistance = Random.Range(0, cubePillar.yPositionDuration * 100);
		}
		
		container.tubeAngleEnd = container.tubeAngle + (rockingAngle * (randomBool ? -1 : 1));
		container.tubeAngleDuration = 8 * gameSpeed / 100;
		container.tubeAngleRepeatMode = RepeatMode.pingPong;
		container.tubeAngleCurve = EaseInOutQuad;
		container.animationDistance = Random.Range(0, container.tubeAngleDuration * 200);
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Closing Halves
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void ClosingHalvesLevel() {
		float animationDistance = 100;
		float duration = .8f;
		float end = 15;
		float zScale = 15;
		int numberOfObstacles = 7;
		System.Func<float, float> curve = EaseInOutQuad;
		for (int i = 0; i < numberOfObstacles; i++) {
			RandomizeTubeAngle();
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			TubeObject cubePillar = CreateCubePillar(3, 21, zScale);
			cubePillar.shadingType = ShadingType.directional;
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -20 + cubePillar.xScale / 2;
			cubePillar.xScaleEnd = end;
			cubePillar.xScaleDuration = duration;
			cubePillar.xScaleCurve = curve;
			cubePillar.xScaleRepeatMode = RepeatMode.triggeredOnce;
			
			cubePillar.yScaleEnd = 39;
			cubePillar.yScaleDuration = duration * .8f;
			cubePillar.yScaleCurve = curve;
			cubePillar.yScaleRepeatMode = RepeatMode.triggeredOnce;
			
			cubePillar.yPositionEnd = -20 + (end / 2);
			cubePillar.yPositionDuration = duration;
			cubePillar.yPositionCurve = curve;
			cubePillar.yPositionRepeatMode = RepeatMode.triggeredOnce;
			cubePillar.animationDistance = animationDistance;
			
			tubeAngle += 180;
			cubePillar = CreateCubePillar(3, 21, zScale);
			cubePillar.shadingType = ShadingType.directional;
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -20 + cubePillar.xScale / 2;
			cubePillar.xScaleEnd = end;
			cubePillar.xScaleDuration = duration;
			cubePillar.xScaleCurve = curve;
			cubePillar.xScaleRepeatMode = RepeatMode.triggeredOnce;
			
			cubePillar.yScaleEnd = 39;
			cubePillar.yScaleDuration = duration * .8f;
			cubePillar.yScaleCurve = curve;
			cubePillar.yScaleRepeatMode = RepeatMode.triggeredOnce;
			
			cubePillar.yPositionEnd = -20 + (end / 2);
			cubePillar.yPositionDuration = duration;
			cubePillar.yPositionCurve = curve;
			cubePillar.yPositionRepeatMode = RepeatMode.triggeredOnce;
			cubePillar.animationDistance = animationDistance;
		}
	}
	
	
	public static void SwingyCloseyPillars() {
		float animationDistance = 120;
		float duration = .8f;
		float zScale = 5;
		int numberOfObstacles = 5;
		System.Func<float, float> curve = EaseInOutQuad;
		for (int i = 0; i < numberOfObstacles; i++) {
			RandomizeTubeAngle();
			z += i == 0 ? 0 : 180;

			TubeObject rotationContainer = CreateContainer();

			TubeObject cubePillar = CreateCubePillar(5, 80, zScale);

			rotationContainer.AddChild(cubePillar);

			cubePillar.shadingType = ShadingType.directional;
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -18;
			cubePillar.yPositionEnd = -cubePillar.xScale - 3f;
			cubePillar.yPositionDuration = duration;
			cubePillar.yPositionCurve = curve;
			cubePillar.yPositionRepeatMode = RepeatMode.triggeredOnce;
			cubePillar.animationDistance = animationDistance;


			tubeAngle += 180;
			TubeObject cubePillar2 = CreateCubePillar(5, 80, zScale);
			cubePillar2.shadingType = ShadingType.directional;
			cubePillar2.zRotation = 90;
			cubePillar2.yPosition = -18;
			cubePillar2.yPositionEnd = -cubePillar.xScale - 3f;
			cubePillar2.yPositionDuration = duration;
			cubePillar2.yPositionCurve = curve;
			cubePillar2.yPositionRepeatMode = RepeatMode.triggeredOnce;
			cubePillar2.animationDistance = animationDistance;

			rotationContainer.AddChild(cubePillar2);


			rotationContainer.yRotationEnd = 360;
			rotationContainer.yRotationDuration = 2f;
			rotationContainer.yRotationRepeatMode = RepeatMode.loop;
			rotationContainer.animationDistance = randomBool ? 50 : 100;

		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Paths
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void GentlePathLevel() {
		
		float zIncrement = 12 + (intraLevelZSpacingIncrement / 10);

		for (int i = 0; i < 30; i++) {

			z += i == 0 ? 0 : zIncrement + (intraLevelZSpacingIncrement / 20);
			tubeAngle = 24f * Mathf.Sin(i/2f) * Mathf.Pow(difficultyFactor, 0.5f);

			for (int j = 0; j < 16; j++, tubeAngle += 20) { //create cube ring at z with tubeAngle
				float duration = 1.8f;
				TubeObject cube = CreateCube(2, 9, 2);
				cube.yScaleEnd = 2;
				cube.yScaleDuration = duration;
				cube.yScaleCurve = EaseInOutQuad;
				cube.yScaleRepeatMode = RepeatMode.pingPong;
				cube.yPosition = -20 + (cube.yScale / 2);
				cube.yPositionEnd = -20 + (cube.yScaleEnd / 2);
				cube.yPositionDuration = duration;
				cube.yPositionCurve = EaseInOutQuad;
				cube.yPositionRepeatMode = RepeatMode.pingPong;
				cube.animationDistance = (i * zIncrement / 2);
			}
		}
	}

	public static void TrippyWavyPathLevel(){
		
		float zIncrement = 30 + (intraLevelZSpacingIncrement / 10);

		for (int i = 0; i < 25; i++) {

			z += i == 0 ? 0 : zIncrement + (intraLevelZSpacingIncrement / 20);
			tubeAngle = 26f * Mathf.Sin(i/2f);

			for (int j = 0; j < 12; j++, tubeAngle += 25) { 
				float duration = 1.8f;
				TubeObject cube = CreatePyramid(4f,12f,3f);
				cube.yScaleEnd = 2f;
				cube.yScaleDuration = duration;
				cube.yScaleCurve = EaseInOutQuad;
				cube.yScaleRepeatMode = RepeatMode.pingPong;
				cube.yPosition = -20 + (cube.yScale / 2);
				cube.yPositionEnd = -20 + (cube.yScaleEnd / 2);
				cube.yPositionDuration = duration;
				cube.yPositionCurve = EaseInOutQuad;
				cube.yPositionRepeatMode = RepeatMode.pingPong;
				cube.animationDistance = (i * zIncrement / 2);
				cube.yRotationEnd = 360;
				cube.yRotationRepeatMode = RepeatMode.loop;
				cube.yRotationDuration = 2;

				cube.tubeAngleEnd = cube.tubeAngle + 90 + (i);
				cube.tubeAngleDuration = 1f;
				cube.tubeAngleRepeatMode = RepeatMode.pingPong;
				cube.tubeAngleCurve = EaseInOutQuad;
			}
		}
	}

	public static void InvisiblePathLevel() {
		float zIncrement = 24;
		RandomizeTubeAngle();
		TubeObject pipe = null;

		for (int i = 0; i < 16; i++) {
			z += i == 0 ? 0 : zIncrement;
			tubeAngle = 30 * Mathf.Sin(i/2f);

			for (int j = 0; j < 8; j++, tubeAngle += 40) { //create cube ring at z with tubeAngle
				float duration = .9f;
				pipe = CreateCube(8, 10, 12);
				pipe.yPosition = 13 + pipe.yScale * 1.5f;
				pipe.yPositionEnd = 17;
				pipe.yPositionDuration = duration;
				pipe.yPositionCurve = EaseInOutQuad;
				pipe.yPositionRepeatMode = RepeatMode.pingPong;
				pipe.animationDistance = (i * zIncrement) - (4 * i) - 40;
			}
		}
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Arc Escalation
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void ArcEscalationLevel() {
		int numberOfSegments = 30;
		for (float i = 0; i < numberOfSegments; i++) {
			z += i == 0 ? 0 : (30f - (10f * (i / numberOfSegments)) + (intraLevelZSpacingIncrement / 4));
			RandomizeTubeAngle();
			TubeObject ring = CreateRing(Random.Range(10f, 80f) + Random.Range(10f,80f), Random.Range(3,6), 0,Random.Range(3,6));
			ring.tubeAngleEnd = ring.tubeAngle + randomRotation;
			ring.tubeAngleDuration = Random.Range(9,30) / (1.3f + (i/numberOfSegments));
		}
	}

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Segmented Rings
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void AdjacentSegmentedRingsLevel() {
		int numberOfObstacles = 3;
		int numberOfRings = 3;
		float duration = 7;
		float initialZ = z;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			float animationDistance = Random.Range(0, duration * 100);
			RandomizeTubeAngle();
			for (int j = 0; j < numberOfRings; j++) {
				z += 33;
				TubeObject segmentedRing = CreateSegmentedRing(6, 14);
				segmentedRing.tubeAngleEnd = segmentedRing.tubeAngle + ((i + j) % 2 == 0 ? 360 : -360);
				segmentedRing.tubeAngleDuration = duration;
				segmentedRing.animationDistance = animationDistance;
			}
		}
	}
	
	public static void OverlappingSegmentedRingsLevel() {
		int numberOfObstacles = 5;
		int numberOfRings = 2;
		float duration = 8;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			for (int j = 0; j < numberOfRings; j++) {
				TubeObject segmentedRing = CreateSegmentedRing(6, 15);
				segmentedRing.tubeAngleEnd = segmentedRing.tubeAngle + (j % 2 == 0 ? 360 : -360);
				segmentedRing.tubeAngleDuration = duration;
			}
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Path Opening Levels
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void PathOpeningLevel() {
		RandomizeTubeAngle();



//		float tubeAngleIncrementDirection = randomBool ? -1 : 1; 

		int tubeAngleIncrement = 0;
		int incrementDirection = randomBool ? -1 : 1;
		int ringZLength = 20;
		for (int i = 0; i < 4; i++){

			incrementDirection = incrementDirection * -1;

			for (int j = 0; j < 6; j++){
				z += ringZLength + 1;
				TubeObject ring = CreateRing(315, 4, 0, ringZLength);
				ring.tubeAngleEnd = ring.tubeAngle + randomRotation + (tubeAngleIncrement);
				ring.tubeAngleDuration = .9f * gameSpeed / 100;
				ring.tubeAngleCurve = EaseInOutQuad;
				ring.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
				ring.animationDistance = 150 * gameSpeed / 100;
				tubeAngleIncrement += (4 * incrementDirection);
			}
		}
	}
	
	public static void DoublePathOpeningLevel() {  // yes this code is rough :)
		RandomizeTubeAngle();

		TubeObject segment1 = null;
		TubeObject segment2 = null;
		float segmentArc = 135;
		float duration = 1f;
		int segmentZScale = 10;
		
		for (int i = 0; i < 20; i++){
			
			z += i == 0 ? 0 : (segmentZScale + 10);
			float animationDistance = i == 0 ? 190 : 120;

			segment1 = CreateRing(segmentArc,4,0,segmentZScale);
			segment1.tubeAngle = Random.Range(0,360);
			segment1.tubeAngleEnd = tubeAngle + (randomBool ? 360 : -360);
			segment1.tubeAngleDuration = duration;
			segment1.tubeAngleCurve = EaseInOutQuad;
			segment1.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
			segment1.animationDistance = animationDistance;
			
			segment2 = CreateRing(segmentArc,4,0,segmentZScale);
			segment2.tubeAngle = Random.Range(0,360);
			segment2.tubeAngleEnd = segment1.tubeAngleEnd + 180; //+ (2 * segmentOffset);
			segment2.tubeAngleDuration = duration;
			segment2.tubeAngleCurve = EaseInOutQuad;
			segment2.tubeAngleRepeatMode = RepeatMode.triggeredOnce;
			segment2.animationDistance = animationDistance;

			tubeAngle += Random.Range(-4,4);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Wavy Arms
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void WavyArmsLevel() {
		int numberOfObstacles = 3;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			CreateWavyArms(6, 25, 25);
		}
	}
	
	private static TubeObject CreateWavyArms(int numberOfBlades, float bottomArcSpread, float topArcSpread) {
		int numberOfSegments = 9;
		float height = 2;
		float duration = .9f;
		float animationDistance = Random.Range(0, duration * 100);
		TubeObject container = CreateContainer();
		for (int i = 0; i < numberOfBlades; i++) {
			for (int j = 0; j < numberOfSegments; j++) {
				TubeObject tubeObject = CreateCylinder(3, height, 3);
				container.AddChild(tubeObject);
				tubeObject.yPosition = -20 + (height / 2) + (height * j * .95f);
				float halfArcSpread = Mathf.Lerp(bottomArcSpread, topArcSpread, j / (numberOfSegments - 1f));
				tubeObject.tubeAngle = i * (360f / numberOfBlades) - halfArcSpread;
				tubeObject.tubeAngleEnd = i * (360f / numberOfBlades) + halfArcSpread;
				tubeObject.tubeAngleDuration = duration;
				tubeObject.tubeAngleCurve = EaseInOutQuad;
				tubeObject.tubeAngleRepeatMode = RepeatMode.pingPong;
				tubeObject.animationDistance = animationDistance + (j * 7);
			}
		}
		return container;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Overlapping Rocking Segments
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void OddlyRockingSegmentsLevel() {
		int numberOfObstacles = 5;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			CreateOddlyRockingSegments(4 + (3*i/numberOfObstacles));
		}
	}
	
	private static TubeObject CreateOddlyRockingSegments(int numberOfSegments) {
		float halfArcSpread = 360f / (numberOfSegments * 3);
		TubeObject container = CreateContainer();
		for (int i = 0; i < numberOfSegments; i++) {
			float duration = Random.Range(1f, 2.5f);
			TubeObject ring = CreateRing(18, 4, 0, 10);
			container.AddChild(ring);
			ring.tubeAngle = (i * (360f / numberOfSegments)) - halfArcSpread;
			ring.tubeAngleEnd = (i * (360f / numberOfSegments)) + halfArcSpread;
			ring.tubeAngleDuration = duration;
			ring.tubeAngleCurve = EaseInOutQuad;
			ring.tubeAngleRepeatMode = RepeatMode.pingPong;
			ring.animationDistance = Random.Range(0, duration * 100);
		}
		return container;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Start Stop Segments
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void StartStopSegmentsLevel() {
		int numberOfObstacles = 5;
		int numberOfSegments = 4;
		float duration = 1f;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject ring = CreateSegmentedRing(numberOfSegments, 360f / (numberOfSegments * 3), 5, 0, 10);
			ring.tubeAngleEnd = tubeAngle + ((360f / numberOfSegments) * (randomBool ? -1 : 1));
			ring.tubeAngleDuration = 1f;
			ring.tubeAngleCurve = EaseInOutCubic;
			ring.tubeAngleRepeatMode = RepeatMode.loop;
			ring.animationDistance = Random.Range(0, duration * 100);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Widening Segments
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void WideningSegmentsLevel() {
		float numberOfObstacles = 5;
		int numberOfSegments = 4;

		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : Random.Range(100,125) + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();

			TubeObject container = CreateContainer();
			for (int j = 0; j < numberOfSegments; j++){
				TubeObject ring = CreateRing(360f / numberOfSegments, 5, 0, 10);
				ring.tubeAngle = j * (360f / numberOfSegments);
				ring.arcEnd = 360f / (numberOfSegments * 3);
				ring.arcDuration = 1f;
				ring.arcCurve = EaseInOutCubic;
				ring.arcRepeatMode = RepeatMode.pingPong;
				ring.animationDistance = 110f - (20f * ( i / numberOfObstacles));
				container.AddChild(ring);
			}
			container.tubeAngleEnd = (tubeAngle + randomRotation);
			container.tubeAngleDuration = Random.Range(5, 10) / difficultyFactor;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Revolving Doors
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void RevolvingDoorLevel() {
		int numberOfObstacles = 3;
		for (int i = 0; i < numberOfObstacles; i++, tubeAngle += 5 * (randomBool ? -1 : 1)) {
			z += i == 0 ? 0 : 175 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject cubePillar = CreateCubePillar(10, 34, 2);
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -20 + cubePillar.xScale / 2;
			tubeAngle += 180;
			cubePillar = CreateCubePillar(10, 34, 2);
			cubePillar.zRotation = 90;
			cubePillar.yPosition = -20 + cubePillar.xScale / 2;
			
			TubeObject container = CreateContainer();
			
			cubePillar = CreateCubePillar(18, 21, 2);
			container.AddChild(cubePillar);
			cubePillar.shadingType = ShadingType.directional;
			cubePillar.tubeAngle = 90;
			cubePillar.yPosition = -20 + cubePillar.yScale / 2;
			cubePillar = CreateCubePillar(18, 21, 2);
			container.AddChild(cubePillar);
			cubePillar.shadingType = ShadingType.directional;
			cubePillar.tubeAngle = 90;
			cubePillar.yPosition = 20 - cubePillar.yScale / 2;
			
			container.xRotation = 90;
			container.xRotationEnd = container.xRotation + 360f;
			container.xRotationDuration = 3.5f;
		}
	}
	
	public static void PaddleWheelLevel() {
		int numberOfObstacles = 3;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 175 + intraLevelZSpacingIncrement;
			RandomizeTubeAngle();
			TubeObject container = CreateContainer();
			TubeObject ring = CreateRing(360, 12, 0, 3);
			ring.tubeAngle = 0;
			ring.shadingType = ShadingType.directional;
			ring.zRotation = 180;
			container.AddChild(ring);
			ring = CreateRing(360, 12, 0, 3);
			ring.tubeAngle = 0;
			ring.shadingType = ShadingType.directional;
			ring.xRotation = 90;
			ring.zRotation = 180;
			container.AddChild(ring);
			container.xPosition = .05f;
			container.xRotation = 57;
			container.xRotationEnd = container.xRotation + 360f;
			container.xRotationDuration = 3f;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Cube Shift
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void CubeShiftLevel() {
		float baseDuration = 0.75f;
		float adjustedDuration = 0f;
		float size = 3.5f;
		float radius = tubeRadius - (size / 2);
		for (int i = 0; i < 30; i++) {
			z += i == 0 ? 0 : 14 + intraLevelZSpacingIncrement / 4;
			RandomizeTubeAngle();
			TubeObject tubeObject = CreateCube(size, size, size);
			tubeObject.yPosition = -tubeRadius + (size / 2);
			tubeObject.xPositionCurve = EaseInOutCubic;
			tubeObject.yPositionCurve = tubeObject.xPositionCurve;
			tubeObject.zRotationCurve = tubeObject.xPositionCurve;
			tubeObject.xPositionRepeatMode = RepeatMode.pingPong;
			tubeObject.yPositionRepeatMode = RepeatMode.pingPong;
			tubeObject.zRotationRepeatMode = RepeatMode.pingPong;
			float angle =  randomBool ? Random.Range(0.8f, Mathf.PI * .8f) : Random.Range(Mathf.PI * 1.2f, Mathf.PI * 1.8f);
			tubeObject.xPositionEnd = Mathf.Sin(angle) * radius;
			tubeObject.yPositionEnd = Mathf.Cos(angle) * radius;
			tubeObject.zRotationEnd = -angle * Mathf.Rad2Deg;
			while (true) {
				float difference = tubeObject.zRotationEnd - tubeObject.zRotation;
				if (Mathf.Abs(difference) <= 45) {
					break;
				}
				tubeObject.zRotationEnd -= (90 * Mathf.Sign(difference));
			}

			adjustedDuration = 0.5f + baseDuration * Mathf.Pow((Mathf.Pow(tubeObject.xPositionEnd, 2) + Mathf.Pow(tubeObject.yPositionEnd,2)),0.5f) / radius;
			tubeObject.xPositionDuration = adjustedDuration;
			tubeObject.yPositionDuration = adjustedDuration;
			tubeObject.zRotationDuration = adjustedDuration;
			//tubeObject.animationDistance = Random.Range(0,1000);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Rotating Outside Rings
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void RotatingOutsideRingsLevel() {
		
		int numberOfObstacles = 3;
		int numberOfRings = 4;
		
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 100 + intraLevelZSpacingIncrement;
			float duration = Random.Range(5f, 8f);
			float rotation = randomRotation;
			float animationDistance = Random.Range(0, duration * 100);
			RandomizeTubeAngle();
			for (int j = 0; j < numberOfRings; j++) {
				TubeObject ring = CreateRing(220, 14, 4, 5);
				ring.shadowType = ShadowType.directional;
				ring.yPosition = -tubeRadius - 3;
				ring.tubeAngle = j * (360f / numberOfRings);
				ring.zRotation = ring.tubeAngle;
				ring.zRotationEnd = ring.tubeAngle + rotation;
				ring.zRotationDuration = duration;
				ring.animationDistance = animationDistance;
			}
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Flying Fish
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void RandomFlyingFishLevel() {
		int numberOfObstacles = 30;
		float duration = 2f;
		for (int i = 0; i < numberOfObstacles; i++) {
			RandomizeTubeAngle();
			z += i == 0 ? 0 : 20 + (intraLevelZSpacingIncrement / 4);
			TubeObject container = CreateContainer();
			container.yPosition = -tubeRadius;
			container.zRotation = 50;
			container.zRotationEnd = container.zRotation + 360;
			container.zRotationDuration = duration;

			TubeObject tubeObject = CreatePyramid(4, 10, 4);
			tubeObject.tubeAngle = 0;
			container.AddChild(tubeObject);
			tubeObject.yPosition = 15f;
			tubeObject.zRotation = 80;
			container.animationDistance = Random.Range(18,22);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Toppling Cylinders
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void TopplingCylindersLevel() {
		int numberOfObstacles = 30;
		float startingAngle = 8;
		float endingAngle = 60;
		for (int i = 0; i < numberOfObstacles; i++) {
			tubeAngle += Random.Range(60,300);
			z += i == 0 ? 0 : 30 + intraLevelZSpacingIncrement / 2;
			float direction = randomBool ? -1 : 1;
			TubeObject container = CreateContainer();
			container.yPosition = -tubeRadius;
			container.zRotation = startingAngle * direction;
			container.zRotationEnd = endingAngle * direction;
			container.zRotationDuration = .4f;
			container.zRotationCurve = EaseInCubic;
			container.zRotationRepeatMode = RepeatMode.triggeredOnce;
			container.animationDistance = 50;
			container.zRotationShakingDistance = 200;
			container.zRotationShakingAmount = 5;
			TubeObject tubeObject = CreateCylinderPillar(3, 20, 3);
			tubeObject.tubeAngle = 0;
			container.AddChild(tubeObject);
			tubeObject.yPosition = tubeObject.yScale / 2;
		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Plus Fans
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void PlusFansLevel() {
		tubeAngle = 0;
		int numberOfObstacles = 3;
		int numberOfFans = 4;
		float duration = .8f * gameSpeed / 100;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 175 + intraLevelZSpacingIncrement;
			float randomTubeAngle = randomAngle;
			float animationDistance = (-duration * 150);
			
			float zRotationEnd = randomBool ? -90 : 90;
			float tubeAngleFlag = randomBool ? -1 : 1;
			
			for (int j = 0; j < numberOfFans; j++, animationDistance += (duration * 100)) {
				
				TubeObject blade0 = CreateCubePillar(2.5f, 21, 2.5f);
				TubeObject blade1 = CreateCubePillar(2.5f, blade0.yScale, 2.5f);
				blade1.zRotation = 90;
				
				TubeObject container0 = CreateContainer();
				container0.AddChild(blade0);
				container0.AddChild(blade1);
				container0.zRotationEnd = zRotationEnd;
				container0.zRotationCurve = EaseInOutCubic;
				container0.zRotationRepeatMode = RepeatMode.triggeredOnce;
				container0.zRotationDuration = duration;
				container0.animationDistance = animationDistance;
				
				TubeObject container1 = CreateContainer();
				container1.AddChild(container0);
				container1.zRotationEnd = zRotationEnd;
				container1.zRotationCurve = EaseInOutCubic;
				container1.zRotationRepeatMode = RepeatMode.triggeredOnce;
				container1.zRotationDuration = duration;
				container1.animationDistance = animationDistance + (duration * 400);
				
				container1.tubeAngle = randomTubeAngle + ((360f / numberOfFans) * j * tubeAngleFlag);
				container1.yPosition = -tubeRadius + 8;
			}
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Shards Level
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void ShardsLevel() {
		int numberOfObstacles = 25;
		for (int i = 0; i < numberOfObstacles; i++) {
			RandomizeTubeAngle();
			z += i == 0 ? 0 : 40 + intraLevelZSpacingIncrement / 2;
			float angle = Random.Range(15, 35);
			TubeObject container = CreateContainer();
			container.yPosition = tubeRadius;
			container.zRotation = randomBool ? -angle : angle;
			TubeObject tubeObject = CreatePyramidPillar(Random.Range(3,8), 40, 6 + Random.Range(3,8));
			container.AddChild(tubeObject);
			tubeObject.tubeAngle = 0;
			tubeObject.yRotation = randomAngle;
			tubeObject.yPosition = -tubeObject.yScale - 10;   //(-tubeRadius * 2.6f);
			tubeObject.yPositionEnd = -20;
			tubeObject.yPositionDuration = 1;
			tubeObject.yPositionCurve = EaseInOutCubic;
			tubeObject.yPositionRepeatMode = RepeatMode.triggeredOnce;
			tubeObject.xPositionShakingDistance = 150;
			tubeObject.xPositionShakingAmount = 2;
			tubeObject.animationDistance = 120 * gameSpeed / 100;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Spinning Bowls Level
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void SpinningBowlsLevel() {
		int numberOfObstacles = 4;
		int numberOfRings = 3;
		for (int i = 0; i < numberOfObstacles; i++) {
			RandomizeTubeAngle();
			z += i == 0 ? 0 : 150 - (i * 20);
			for (int j = 0; j < numberOfRings; j++, tubeAngle += (360f / numberOfRings)) {
				TubeObject tubeObject = CreateRing(270, 4, 0, 10);
				tubeObject.shadowType = ShadowType.directional;
				tubeObject.xScale = .87f;
				tubeObject.yScale = .87f;
				tubeObject.xRotation = 90;
				tubeObject.yPosition = -tubeRadius + (tubeObject.zScale / 2);
				tubeObject.zRotationEnd = 360;
				tubeObject.zRotationDuration = .6f;
				tubeObject.zRotationRepeatMode = RepeatMode.loop;
				tubeObject.animationDistance = 14;

				if (j < i) {
					TubeObject pestle = CreateCylinder(4, 35, 4);
					pestle.yPosition = -20;
				}

			}
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Dolphins Level
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void DolphinsLevel() {
		z += 40;

		int numberOfDolphins = 50;
		RandomizeTubeAngle();
		
		float startMovingZ = z - 150;
		
		for (int i = 0; i < numberOfDolphins; i++) {

			int dolphinSpacing = 9 + intraLevelZSpacingIncrement / 10;
			z += i == 0 ? 0 : dolphinSpacing;  
			float rotation = randomRotation;
			
			tubeAngle += Random.Range(60f, 300f);

			TubeObject forwardContainer = CreateContainer();
			forwardContainer.zPositionEnd = forwardContainer.zPosition + 600;
			forwardContainer.zPositionDuration = (forwardContainer.zPositionEnd - forwardContainer.zPosition) / 40; 
			forwardContainer.zPositionCurve = EaseInOutQuad;
			forwardContainer.zPositionRepeatMode = RepeatMode.triggeredOnce;


			forwardContainer.tubeAngleEnd = tubeAngle + rotation;
			forwardContainer.tubeAngleDuration = 30;
			forwardContainer.tubeAngleRepeatMode = RepeatMode.loop;
			forwardContainer.animationDistance = z - startMovingZ;
			


			TubeObject surfaceContainer = CreateContainer();
			forwardContainer.AddChild(surfaceContainer);
			surfaceContainer.yPosition = -9;
			surfaceContainer.yPositionEnd = Random.Range(-1,2);
			surfaceContainer.yPositionDuration = 8;
			surfaceContainer.yPositionCurve = EaseOutQuad;
			surfaceContainer.yPositionRepeatMode = RepeatMode.triggeredPingPong;
			surfaceContainer.animationDistance = 275 + i * dolphinSpacing + Random.Range(0,50);

			float animationDistance = Random.Range(0, 200f);
			int jumpRandomizer = Random.Range(-2,3);
			float jumpingDuration = .5f + 0.05f * jumpRandomizer;



			TubeObject upDownContainer = CreateContainer();
			surfaceContainer.AddChild(upDownContainer);
			upDownContainer.tubeAngle = 0;
			upDownContainer.yPosition = -21;
			upDownContainer.yPositionEnd = -15 + jumpRandomizer;
			upDownContainer.yPositionDuration = jumpingDuration ;
			upDownContainer.yPositionCurve = EaseInOutSin;
			upDownContainer.yPositionRepeatMode = RepeatMode.pingPong;
			upDownContainer.animationDistance = animationDistance;
			
			TubeObject pyramid = CreatePyramid(4 + 0.25f * jumpRandomizer, 10 + 0.25f * jumpRandomizer, 4 + 0.25f * jumpRandomizer);
			upDownContainer.AddChild(pyramid);
			pyramid.tubeAngle = 0;
			pyramid.yRotation = 15 * (rotation < 0 ? -1 : 1);
			pyramid.xRotation = 135;
			pyramid.xRotationEnd = 45;
			pyramid.xRotationDuration = jumpingDuration;
			pyramid.xRotationCurve = EaseInOutSin;
			pyramid.xRotationRepeatMode = RepeatMode.pingPong;
			pyramid.animationDistance = animationDistance + (jumpingDuration * 30);
			
		}
		z += 600;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Nested Rocking Rings Level
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void NestedRockingRingsLevel() {
		
		int numberOfObstacles = 3;
		int numberOfRings = 5;
		float ringHeight = 2f;
		float startingRingBottom = 0;
		
		for (int i = 0; i < numberOfObstacles; i++) {
			RandomizeTubeAngle();
			z += i == 0 ? 0 : 150;
			float ringBottom = startingRingBottom;
			for (int j = 0; j < numberOfRings; j++, ringBottom += ringHeight) {
				TubeObject ring = CreateRing(360, ringBottom + ringHeight, ringBottom, 5);
				ring.yRotation = 90;
				ring.yRotationEnd = 270;
				ring.yRotationDuration = 1.5f;
				ring.yRotationRepeatMode = RepeatMode.pingPong;
				ring.yRotationCurve = EaseInOutQuad;
				ring.animationDistance = 60 - (j * 6);
			}
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Ellipsoid Shards (test)
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void EllipsoidShardsLevel() {
		int numberOfObstacles = 30;
		for (int i = 0; i < numberOfObstacles; i++) {
			RandomizeTubeAngle();
			z += i == 0 ? 0 : 30 + intraLevelZSpacingIncrement / 2;
			float angle = Random.Range(15, 35);
			TubeObject container = CreateContainer();
			container.yPosition = tubeRadius;
			container.zRotation = randomBool ? -angle : angle;
			TubeObject tubeObject = CreateEllipsoid(Random.Range(3,8), 40, 6 + Random.Range(3,8));
			container.AddChild(tubeObject);
			tubeObject.tubeAngle = 0;
			tubeObject.yRotation = randomAngle;
			tubeObject.yPosition = -tubeObject.yScale - 10;   //(-tubeRadius * 2.6f);
			tubeObject.yPositionEnd = -20;
			tubeObject.yPositionDuration = 1;
			tubeObject.yPositionCurve = EaseInOutCubic;
			tubeObject.yPositionRepeatMode = RepeatMode.triggeredOnce;
			tubeObject.xPositionShakingDistance = 150;
			tubeObject.xPositionShakingAmount = 2;
			tubeObject.animationDistance = 120 * gameSpeed / 100;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Key Rings Level
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void KeyRingsLevel() {
		
		int numberOfObstacles = 3;
		float dotSize = 6;
		float zScale = 3;
		tubeAngle = 0;
		for (int i = 0; i < numberOfObstacles; i++) {
			z += i == 0 ? 0 : 150;
			int flipped = randomBool ? -1 : 1;


			TubeObject centerRing = CreateRing(360, 11 , 10, zScale, 60);
			centerRing.yPosition = 8;

		
			TubeObject ring2Spinner = CreateContainer();
			ring2Spinner.yPosition = centerRing.yPosition;

			TubeObject ring2 = CreateRing(360, 11, 10, zScale, 60);
			ring2Spinner.AddChild(ring2);

			ring2.yPosition = -16;
			ring2.xScale = .8f;
			ring2.yRotation = 90;

			TubeObject ring3Spinner = CreateContainer();
			ring3Spinner.yPosition = centerRing.yPosition;

			TubeObject ring3 = CreateRing(360, 11, 10, zScale, 60);
			ring3Spinner.AddChild(ring3);
		
			ring3.yPosition = -16;
			ring3.xScale = .8f;
			ring3.yRotation = 90;


			float spinnerRotationEnd = 360;
			float spinnerDuration = 2f;
			float spinnerTubeAngle = 0f;

			ring2Spinner.zRotationEnd = spinnerRotationEnd;
			ring2Spinner.zRotationDuration = spinnerDuration;
			ring2Spinner.zRotationRepeatMode = RepeatMode.loop;
			ring2Spinner.zRotationCurve = EaseInOutQuad;

			ring2Spinner.animationDistance = 0;

			ring3Spinner.zRotationEnd = spinnerRotationEnd;
			ring3Spinner.zRotationDuration = spinnerDuration;
			ring3Spinner.zRotationRepeatMode = RepeatMode.loop;
			ring3Spinner.zRotationCurve = EaseInOutQuad;

			ring3Spinner.animationDistance = ring2Spinner.animationDistance + 10;
			z += 150;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Test
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void TestLevel() {
		z += 100;
	}
	
	/*
	private static TubeObject CreateAccordianRing(bool rotateClockwise, int tubeAngleEnd, float duration, int numberOfSegments, int segmentSpacing, int trigger) {
		TubeObject ring = CreateContainer();
		ring.tubeAngle = tubeAngle;

		for (int i = 0; i < numberOfSegments; i++, tubeAngle += 360 / numberOfSegments){
			TubeObject segment = CreateRing(1f / (numberOfSegments * segmentSpacing), 4, 0, 10); //probably should call CreateDefaultRing here or something
			segment.tubeAngleEnd = ring.tubeAngle + tubeAngleEnd; 
			segment.tubeAngleDuration = duration;
			segment.tubeAngleRepeatMode = RepeatMode.pingPong;
			ring.AddChild(segment);
		}


		return ring;
	}
	*/

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Basic Objects
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static float z;
	public static float tubeAngle;
	public static float tubeRadius = 20;
	public static float gameSpeed;
	
	public static bool randomBool {
		get {
			return (Random.value < 0.5f);
		}
	}
	
	public static int randomSign {
		get {
			return (Random.value < 0.5f ? -1 : 1);
		}
	}
	
	public static float randomAngle {
		get {
			return Random.Range(0, 360f);
		}
	}
	
	public static float randomRotation {
		get {
			return randomBool ? 360 : -360;
		}
	}
	
	public static void RandomizeTubeAngle() {
		tubeAngle = randomAngle;
	}
	
	public static TubeObject CreateContainer() {
		TubeObject container = TubeObject.GetInstance();
		container.zPosition = z;
		container.tubeAngle = tubeAngle;
		container.UpdateScale();
		return container;
	}
	
	public static TubeObject CreateSphere(float size) {
		TubeObject sphere = TubeObject.GetInstance();
		sphere.meshType = size <= 4 ? MeshType.sphereSparse : MeshType.sphereDense;
		sphere.colliderType = ColliderType.sphere;
		sphere.shadingType = ShadingType.directional;
		sphere.shadowType = ShadowType.directional;
		sphere.zPosition = z;
		sphere.tubeAngle = tubeAngle;
		sphere.xScale = size;
		sphere.yScale = size;
		sphere.zScale = size;
		return sphere;
	}
	
	public static TubeObject CreateEllipsoid(float xScale, float yScale, float zScale) {
		TubeObject ellipsoid = TubeObject.GetInstance();
		ellipsoid.meshType = Mathf.Max(Mathf.Max(xScale, yScale), zScale) <= 4 ? MeshType.sphereSparse : MeshType.sphereDense;
		ellipsoid.colliderType = ColliderType.ellipsoid;
		ellipsoid.shadingType = ShadingType.directional;
		ellipsoid.shadowType = ShadowType.directional;
		ellipsoid.zPosition = z;
		ellipsoid.tubeAngle = tubeAngle;
		ellipsoid.xScale = xScale;
		ellipsoid.yScale = yScale;
		ellipsoid.zScale = zScale;
		return ellipsoid;
	}
	
	public static TubeObject CreateCube(float xScale, float yScale, float zScale) {
		TubeObject cube = TubeObject.GetInstance();
		cube.meshType = MeshType.cube;
		cube.colliderType = ColliderType.cube;
		cube.shadingType = ShadingType.directional;
		cube.shadowType = ShadowType.directional;
		cube.zPosition = z;
		cube.tubeAngle = tubeAngle;
		cube.xScale = xScale;
		cube.yScale = yScale;
		cube.zScale = zScale;
		return cube;
	}
	
	public static TubeObject CreateCubePillar(float xScale, float yScale, float zScale) {
		TubeObject cubePillar = TubeObject.GetInstance();
		cubePillar.meshType = MeshType.cubePillar;
		cubePillar.colliderType = ColliderType.cube;
		cubePillar.shadingType = ShadingType.directional;
		cubePillar.shadowType = ShadowType.directional;
		cubePillar.zPosition = z;
		cubePillar.tubeAngle = tubeAngle;
		cubePillar.xScale = xScale;
		cubePillar.yScale = yScale;
		cubePillar.zScale = zScale;
		return cubePillar;
	}
	
	public static TubeObject CreatePyramid(float xScale, float yScale, float zScale) {
		TubeObject pyramid = TubeObject.GetInstance();
		pyramid.meshType = MeshType.pyramid;
		pyramid.colliderType = ColliderType.pyramid;
		pyramid.shadingType = ShadingType.directional;
		pyramid.shadowType = ShadowType.directional;
		pyramid.zPosition = z;
		pyramid.tubeAngle = tubeAngle;
		pyramid.xScale = xScale;
		pyramid.yScale = yScale;
		pyramid.zScale = zScale;
		return pyramid;
	}
	
	public static TubeObject CreatePyramidPillar(float xScale, float yScale, float zScale) {
		TubeObject pyramidPillar = TubeObject.GetInstance();
		pyramidPillar.meshType = MeshType.pyramidPillar;
		pyramidPillar.colliderType = ColliderType.pyramid;
		pyramidPillar.shadingType = ShadingType.directional;
		pyramidPillar.shadowType = ShadowType.directional;
		pyramidPillar.zPosition = z;
		pyramidPillar.tubeAngle = tubeAngle;
		pyramidPillar.xScale = xScale;
		pyramidPillar.yScale = yScale;
		pyramidPillar.zScale = zScale;
		return pyramidPillar;
	}
	
	public static TubeObject CreateCylinder(float xScale, float yScale, float zScale) {
		TubeObject cylinder = TubeObject.GetInstance();
		cylinder.meshType = MeshType.cylinder;
		cylinder.colliderType = ColliderType.cylinder;
		cylinder.shadingType = ShadingType.directional;
		cylinder.shadowType = ShadowType.directional;
		cylinder.zPosition = z;
		cylinder.tubeAngle = tubeAngle;
		cylinder.xScale = xScale;
		cylinder.yScale = yScale;
		cylinder.zScale = zScale;
		return cylinder;
	}
	
	public static TubeObject CreateCylinderPillar(float xScale, float yScale, float zScale) {
		TubeObject cylinderPillar = TubeObject.GetInstance();
		cylinderPillar.meshType = MeshType.cylinderPillar;
		cylinderPillar.colliderType = ColliderType.cylinder;
		cylinderPillar.shadingType = ShadingType.directional;
		cylinderPillar.shadowType = ShadowType.directional;
		cylinderPillar.zPosition = z;
		cylinderPillar.tubeAngle = tubeAngle;
		cylinderPillar.xScale = xScale;
		cylinderPillar.yScale = yScale;
		cylinderPillar.zScale = zScale;
		return cylinderPillar;
	}
	
	public static TubeObject CreateGem() {
		TubeObject gem = TubeObject.GetInstance();
		gem.meshType = MeshType.gem;
		gem.colliderType = ColliderType.gem;
		gem.shadingType = ShadingType.gem;
		gem.shadowType = ShadowType.directional;
		gem.zPosition = z;
		gem.tubeAngle = tubeAngle;
		gem.xScale = 5;
		gem.yScale = 4;
		gem.zScale = 5;
		gem.yPosition = -16;
		gem.yPositionEnd = -13;
		gem.yPositionDuration = .6f;
		gem.yPositionCurve = EaseInOutCubic;
		gem.yPositionRepeatMode = RepeatMode.pingPong;
		gem.yRotation = 0;
		gem.yRotationEnd = 360;
		gem.yRotationDuration = 2;
		return gem;
	}
	
	public static TubeObject CreateRing(float arc, float top, float bottom, float zScale) {
		return CreateRing(arc, top, bottom, zScale, (int)(Mathf.Max(1, (arc / 360) * 60)));
	}
	
	public static TubeObject CreateRing(float arc, float top, float bottom, float zScale, int numberOfSlats) {
		TubeObject ring = TubeObject.GetInstance();
		ring.numberOfSlats = numberOfSlats;
		ring.meshType = MeshType.ring;
		ring.colliderType = ColliderType.ring;
		ring.shadingType = ShadingType.radial;
		ring.shadowType = ShadowType.radial;
		ring.SetArcTopBottom(arc, top, bottom);
		ring.zPosition = z;
		ring.tubeAngle = tubeAngle;
		ring.zScale = zScale;
		return ring;
	}
	
	public static TubeObject CreateSegmentedRing(int numberOfSegments, float arc, float top, float bottom, float zScale) {
		TubeObject container = CreateContainer();
		for (int i = 0; i < numberOfSegments; i++){
			TubeObject ring = CreateRing(arc, top, bottom, zScale);
			ring.tubeAngle = i * (360f / numberOfSegments);
			container.AddChild(ring);
		}
		return container;
	}
	
	public static TubeObject CreateSegmentedRing(int numberOfSegments, float arc) {
		return CreateSegmentedRing(numberOfSegments, arc, 4, 0, 10);
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper Functions
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static string LevelDisplayName(System.Action levelFunction) {
		string levelName = levelFunction.Method.Name;
		string levelDisplayName = "";
		for (int i = 0; i < levelName.Length; i++) {
			char character = levelName[i];
			if (i > 0 && character >= 'A' && character <= 'Z') {
				levelDisplayName += " ";
			}
			levelDisplayName += character;
		}
		return levelDisplayName.Replace(" Level", "").ToUpper();
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Animation Curves
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static float Linear(float t) {
		return t;
	}
	
	public static float EaseInQuad(float t) {
		return t * t;
	}
	
	public static float EaseOutQuad(float t) {
		t = 1 - t;
		return 1 - (t * t);
	}
	
	public static float EaseInOutQuad(float t) {
		return t < .5 ? EaseInQuad(t * 2f) * .5f : (EaseOutQuad((t - .5f) * 2f) * .5f) + .5f;
	}
	
	public static float EaseOutInQuad(float t) {
		return t < .5 ? EaseOutQuad(t * 2f) * .5f : (EaseInQuad((t - .5f) * 2f) * .5f) + .5f;
	}
	
	public static float EaseInCubic(float t) {
		return t * t * t;
	}
	
	public static float EaseOutCubic(float t) {
		t = 1 - t;
		return 1 - (t * t * t);
	}
	
	public static float EaseInOutCubic(float t) {
		return t < .5 ? EaseInCubic(t * 2f) * .5f : (EaseOutCubic((t - .5f) * 2f) * .5f) + .5f;
	}
	
	public static float EaseInQuart(float t) {
		return t * t * t * t;
	}
	
	public static float EaseOutQuart(float t) {
		t = 1 - t;
		return 1 - (t * t * t * t);
	}
	
	public static float EaseInOutQuart(float t) {
		return t < .5 ? EaseInQuart(t * 2f) * .5f : (EaseOutQuart((t - .5f) * 2f) * .5f) + .5f;
	}
	
	public static float EaseInQuint(float t) {
		return t * t * t * t;
	}
	
	public static float EaseOutQuint(float t) {
		t = 1 - t;
		return 1 - (t * t * t * t);
	}
	
	public static float EaseInOutQuint(float t) {
		return t < .5 ? EaseInQuint(t * 2f) * .5f : (EaseOutQuint((t - .5f) * 2f) * .5f) + .5f;
	}
	
	public static float EaseInSin(float t) {
		return -Mathf.Cos(t * (Mathf.PI / 2)) + 1;	
	}
	
	public static float EaseOutSin(float t) {
		return Mathf.Sin(t * (Mathf.PI / 2));
	}
	
	public static float EaseInOutSin(float t) {
		return t < .5 ? EaseInSin(t * 2f) * .5f : (EaseOutSin((t - .5f) * 2f) * .5f) + .5f;
	}
	
	public static float EaseInCirc(float t) {
		return -(Mathf.Sqrt(1 - t * t) - 1);
	}
	
	public static float EaseOutCirc(float t) {
		return Mathf.Sqrt(1 - (t = t - 1) * t);
	}
	
	public static float EaseInOutCirc(float t) {
		return t < .5 ? EaseInCirc(t * 2f) * .5f : (EaseOutCirc((t - .5f) * 2f) * .5f) + .5f;
	}
	
	public static float expoShift = Mathf.Pow(2, -10);
	public static float expoRange = 1 - expoShift;
	public static float expoScale = 1 / expoRange;
	
	public static float EaseInExpo(float t) {
		return (Mathf.Pow(2, 10 * (t - 1)) - expoShift) * expoScale;
	}
	
	public static float EaseOutExpo(float t) {
		return (1 - (Mathf.Pow(2, -10 * t))) * expoScale;
	}
	
	public static float EaseInOutExpo(float t) {
		return t < .5 ? EaseInExpo(t * 2f) * .5f : (EaseOutExpo((t - .5f) * 2f) * .5f) + .5f;
	}
	
	public static float EaseInBack(float t) {
		return t * t * (2.4f * t - 1.4f);
	}
	
	public static float EaseOutBack(float t) {
		return (--t) * t * (2.4f * t + 1.4f) + 1f;
	}
	
	public static float EaseInOutBack(float t) {
		return t < .5 ? EaseInBack(t * 2f) * .5f : (EaseOutBack((t - .5f) * 2f) * .5f) + .5f;
	}
	
	public static float EaseInPerlinNoise(float t) {
		return ((Mathf.Clamp01(Mathf.PerlinNoise(t * 90, 0)) * 2) - 1) * t;
	}
	
	public static float PerlinNoise(float t) {
		return (Mathf.Clamp01(Mathf.PerlinNoise(t * 90, 0)) * 2) - 1;
	}

	public static System.Action GetLevelForLevelName(string levelName) {
		for (int i = 0; i < masterLevelList.Length; i++) {
			if (masterLevelList[i].Method.Name == levelName) {
				return masterLevelList[i];
			}
		}
		return null;
	}
	
	public static void DisplayWarnings() {
		
		/*
		List<System.Action> m = new List<System.Action>(masterLevelList);
		List<System.Action> z = new List<System.Action>(zLevelList);
		
		for (int i = 0; i < m.Count; i++) {
			if (!z.Contains(m[i])) {
				Debug.Log("not in spreadsheet -----------" + m[i].Method.Name);
			}
		}
		
		for (int i = 0; i < z.Count; i++) {
			if (!m.Contains(z[i])) {
				Debug.Log("not in master -----------" + z[i].Method.Name);
			}
		}
		*/
		
		List<System.Action> alreadyAppeared = new List<System.Action>();
		List<System.Action> duplicates = new List<System.Action>();
		
		// Check for duplicates in the master level list
		for (int i = 0; i < masterLevelList.Length; i++) {
			if (alreadyAppeared.Contains(masterLevelList[i])) {
				duplicates.Add(masterLevelList[i]);
			}
			else {
				alreadyAppeared.Add(masterLevelList[i]);
			}
		}
		
		if (duplicates.Count > 0) {
			Debug.LogWarning("Appears more than once in master list: " + DebugLevelList(duplicates.ToArray()));
		}
	}
	
	public static string DebugLevelList(System.Action[] levels) {
		string message = "";
		for (int i = 0; i < levels.Length; i++) {
			message += levels[i].Method.Name + " ";
		}
		return message;
	}
}
