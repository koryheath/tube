﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Palette : MonoBehaviour {
	
	public static Material tubeSegmentMaterial;
	
	public Color tubeTopColor;
	public Color tubeBottomColor;
	public Color obstacleTopColor;
	public Color obstacleBottomColor;
	
	public void Apply() {
		tubeSegmentMaterial.SetColor("_TopColor", tubeTopColor);
		tubeSegmentMaterial.SetColor("_BottomColor", tubeBottomColor);
		TubeObject.shadingMaterials[0].SetColor("_TopColor", obstacleTopColor);
		TubeObject.shadingMaterials[0].SetColor("_BottomColor", obstacleBottomColor);
		TubeObject.shadingMaterials[1].SetColor("_TopColor", obstacleTopColor);
		TubeObject.shadingMaterials[1].SetColor("_BottomColor", obstacleBottomColor);
	}
}
