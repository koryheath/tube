﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public enum GameStyle {
	fullGame,
	testLevels1,
	testLevels2,
	testLevels3,
	testLevels4,
	testLevels5,
	testLevels6
}

public class Settings : MonoBehaviour {
	
	public Dropdown gameStyleDropdown;
	
	public Text swipeMomentumText;
	public Slider swipeMomentumSlider;
	
	public Toggle adsToggle;
	public Toggle facebookAnalyticsToggle;
	public Toggle onScreenDebugInfoToggle;
	public Toggle shadowsToggle;
	public Toggle collisionsToggle;
	
	public Text versionText;
	
	public static int[] GetLevelInts(string parameterName, int defaultValue) {
		int[] levelInts = new int[Levels.masterLevelList.Length];
		for (int i = 0; i < levelInts.Length; i++) {
			levelInts[i] = PlayerPrefs.GetInt(parameterName + Levels.masterLevelList[i].Method.Name, defaultValue);
		}
		return levelInts;
	}
	
	public static void SetLevelInts(string parameterName, int[] levelInts) {
		for (int i = 0; i < levelInts.Length; i++) {
			PlayerPrefs.SetInt(parameterName + Levels.masterLevelList[i].Method.Name, levelInts[i]);
		}
	}
	
	public static int GetStarsForLevel(string levelName) {
		return PlayerPrefs.GetInt("Stars" + levelName, -1);
	}
	
	public static void SetStarsForLevel(string levelName, int stars) {
		PlayerPrefs.SetInt("Stars" + levelName, stars);
	}
	
	public static bool IsBallPurchased(string ballName) {
		return PlayerPrefs.GetInt(ballName + " Purchased", 0) == 1;
	}
	
	public static void SetBallPurchased(string ballName, bool value) {
		PlayerPrefs.SetInt(ballName + " Purchased", value ? 1 : 0);
	}
	
	public static string currentBall {
		get { return PlayerPrefs.GetString("CurrentBall", ""); }
		set { PlayerPrefs.SetString("CurrentBall", value); }
	}
	
	public static int numberOfTutorialLevelsPassed {
		get { return PlayerPrefs.GetInt("NumberOfTutorialLevelsPassed", 0); }
		set { PlayerPrefs.SetInt("NumberOfTutorialLevelsPassed", value); }
	}
	
	public static int bestScore {
		get { return PlayerPrefs.GetInt("BestScore", 0); }
		set { PlayerPrefs.SetInt("BestScore", value); }
	}
	
	public static int gems {
		get { return PlayerPrefs.GetInt("Gems", 0); }
		set { PlayerPrefs.SetInt("Gems", value); }
	}
	
	public static bool soundEnabled {
		get { return PlayerPrefs.GetInt("SoundEnabled", 1) == 1; }
		set { PlayerPrefs.SetInt("SoundEnabled", value ? 1 : 0); }
	}
	
	public static bool musicEnabled {
		get { return PlayerPrefs.GetInt("MusicEnabled", 1) == 1; }
		set { PlayerPrefs.SetInt("MusicEnabled", value ? 1 : 0); }
	}
	
	public static bool hapticEnabled {
		get { return PlayerPrefs.GetInt("HapticEnabled", 1) == 1; }
		set { PlayerPrefs.SetInt("HapticEnabled", value ? 1 : 0); }
	}
	
	public static bool controlsInverted {
		get { return PlayerPrefs.GetInt("ControlsInverted", 0) == 1; }
		set { PlayerPrefs.SetInt("ControlsInverted", value ? 1 : 0); }
	}
	
	public static GameStyle gameStyle {
		get { return (GameStyle)PlayerPrefs.GetInt("GameStyle", (int)GameStyle.fullGame); }
		set { PlayerPrefs.SetInt("GameStyle", (int)value); }
	}
	
	public static float swipeSensitivity {
		get { return PlayerPrefs.GetFloat("SwipeSensitivity", 130); }
		set { PlayerPrefs.SetFloat("SwipeSensitivity", value); }
	}
	
	public static int swipeMomentum {
		get { return PlayerPrefs.GetInt("SwipeMomentum", 0); }
		set { PlayerPrefs.SetInt("SwipeMomentum", value); }
	}
	
	public static bool facebookAnalyticsEnabled {
		get { return PlayerPrefs.GetInt("FacebookAnalyticsEnabled", 0) == 1; }
		set { PlayerPrefs.SetInt("FacebookAnalyticsEnabled", value ? 1 : 0); }
	}

	public static bool shadowsEnabled {
		get { return PlayerPrefs.GetInt("shadowsEnabled", 0) == 1; }
		set { PlayerPrefs.SetInt("shadowsEnabled", value ? 1 : 0); }
	}

	public static bool collisionsEnabled {
		get { return PlayerPrefs.GetInt("collisionsEnabled", 0) == 1; }
		set { PlayerPrefs.SetInt("collisionsEnabled", value ? 1 : 0); }
	}

	public static bool onScreenDebugInfoEnabled {
		get { return PlayerPrefs.GetInt("onScreenDebugInfoEnabled", 0) == 1; }
		set { PlayerPrefs.SetInt("onScreenDebugInfoEnabled", value ? 1 : 0); }
	}

	public static bool adsEnabled {
		get { return PlayerPrefs.GetInt("AdsEnabled", 1) == 1; }
		set { PlayerPrefs.SetInt("AdsEnabled", value ? 1 : 0); }
	}
	
	public static int gemMultiplier {
		get { return PlayerPrefs.GetInt("GemMultiplier", 1); }
		set { PlayerPrefs.SetInt("GemMultiplier", value); }
	}
	
	public static int freeGiftAwardedCount {
		get { return PlayerPrefs.GetInt("FreeGiftAwardedCount", 0); }
		set { PlayerPrefs.SetInt("FreeGiftAwardedCount", value); }
	}
	
	public static bool gdprShown {
		get { return PlayerPrefs.GetInt("gdprShown", 0) == 1; }
		set { PlayerPrefs.SetInt("gdprShown", value ? 1 : 0); }
	}
	
	public static bool challengeAttentionBounceFinished {
		get { return PlayerPrefs.GetInt("challengeAttentionBounceFinished", 0) == 1; }
		set { PlayerPrefs.SetInt("challengeAttentionBounceFinished", value ? 1 : 0); }
	}
	
	void Start () {
		versionText.text = "v" + Application.version.ToString();
		UpdateControls();
		UpdateText();
	}
	
	public void AddListeners() {
		gameStyleDropdown.onValueChanged.AddListener(delegate { UpdateSettings(); });
		swipeMomentumSlider.onValueChanged.AddListener(delegate { UpdateSettings(); });
		adsToggle.onValueChanged.AddListener(delegate { UpdateSettings(); });
		facebookAnalyticsToggle.onValueChanged.AddListener(delegate { UpdateSettings(); });
		onScreenDebugInfoToggle.onValueChanged.AddListener(delegate { UpdateSettings(); });
		shadowsToggle.onValueChanged.AddListener(delegate { UpdateSettings(); });
		collisionsToggle.onValueChanged.AddListener(delegate { UpdateSettings(); });
	}
	
	public void RemoveListeners() {
		gameStyleDropdown.onValueChanged.RemoveAllListeners();
		swipeMomentumSlider.onValueChanged.RemoveAllListeners();
		adsToggle.onValueChanged.RemoveAllListeners();
		facebookAnalyticsToggle.onValueChanged.RemoveAllListeners();
		onScreenDebugInfoToggle.onValueChanged.RemoveAllListeners();
		shadowsToggle.onValueChanged.RemoveAllListeners();
		collisionsToggle.onValueChanged.RemoveAllListeners();
	}
	
	public void UpdateControls() {
		RemoveListeners();
		gameStyleDropdown.value = (int)gameStyle;
		swipeMomentumSlider.value = swipeMomentum;
		adsToggle.isOn = adsEnabled;
		facebookAnalyticsToggle.isOn = facebookAnalyticsEnabled;
		onScreenDebugInfoToggle.isOn = onScreenDebugInfoEnabled;
		shadowsToggle.isOn = shadowsEnabled;
		collisionsToggle.isOn = collisionsEnabled;
		AddListeners();
	}
	
	public void UpdateSettings() {
		gameStyle = (GameStyle)gameStyleDropdown.value;
		swipeMomentum = (int)swipeMomentumSlider.value;
		adsEnabled = adsToggle.isOn;
		facebookAnalyticsEnabled = facebookAnalyticsToggle.isOn;
		onScreenDebugInfoEnabled = onScreenDebugInfoToggle.isOn;
		shadowsEnabled = shadowsToggle.isOn;
		collisionsEnabled = collisionsToggle.isOn;
		UpdateText();
	}
	
	void UpdateText() {
		swipeMomentumText.text = "Swipe Momentum: " + swipeMomentum;
	}
	
	public void LoadGameScene() {
		SceneManager.LoadScene("GameScene");
	}
	
	public void ResetEverything() {
		PlayerPrefs.DeleteAll();
		UpdateControls();
		UpdateText();
	}
	
	public void UnlockAllChallenges() {
		for (int i = 0; i < Levels.masterLevelList.Length; i++) {
			string levelName = Levels.masterLevelList[i].Method.Name;
			SetStarsForLevel(levelName, Mathf.Max(1, GetStarsForLevel(levelName)));
		}
	}

	public void onTestButtonClick()
	{
		print(ThirdPartyManager.checkStoragePermission () + "");


	}
}
