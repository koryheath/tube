﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePanel : MonoBehaviour {
	
	public GameController gameController;
	public Animator animator;
	
	public void OpenPanel() {
		gameObject.SetActive(false);
		gameObject.SetActive(true);
	}
	
	public void ContinueButtonPressed() {
		animator.Play("PausePanelFadeOut");
	}
	
	public void FadeOutComplete() {
		gameObject.SetActive(false);
		gameController.UnpauseGame();
	}
	
}
