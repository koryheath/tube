﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncyButton : MonoBehaviour {
	
	public AudioSource clickAudioSource;
	
	public void BouncyButtonDown() {
		if (clickAudioSource != null) {
			clickAudioSource.Play();
		}
	}
	
	public void BouncyButtonUp() {
		
	}
}
