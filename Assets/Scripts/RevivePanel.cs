﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if !UNITY_EDITOR
using Heyzap;
#endif

public class RevivePanel : MonoBehaviour {
	
	public System.Action reviveAction;
	public System.Action noReviveAction;
	public Image roundRectImage;
	public Animator panelAnimator;
	public CanvasGroup buttonPanelCanvasGroup;
	public bool alreadyEnabled;
	
	[HideInInspector]
	public float timerPercentage;
	
	private Material _roundRectMaterial;
	public Material roundRectMaterial {
		get {
			if (_roundRectMaterial == null) {
				_roundRectMaterial = Instantiate(roundRectImage.material);
				roundRectImage.material = _roundRectMaterial;
			}
			return _roundRectMaterial;
		}
	}
	
	void OnDestroy() {
		if (_roundRectMaterial != null) {
			Destroy(_roundRectMaterial);
		}
	}
	
	bool shouldPlayVideo;
	
	public void Reset() {
		alreadyEnabled = false;
		shouldPlayVideo = false;
		buttonPanelCanvasGroup.interactable = false;
		timerPercentage = 0;
		roundRectMaterial.SetFloat("_TimerPercentage", timerPercentage);
	}
	
	public void Enable() {
		alreadyEnabled = true;
		shouldPlayVideo = false;
		gameObject.SetActive(true);
	}
	
	public void Update() {
		roundRectMaterial.SetFloat("_TimerPercentage", timerPercentage);
	}
	
	public void StartTimer() {
		buttonPanelCanvasGroup.interactable = true;
		panelAnimator.Play("RevivePanelTimer");
	}
	
	public void OnContinueButtonPressed() {
		buttonPanelCanvasGroup.interactable = false;
		shouldPlayVideo = true;
		panelAnimator.Play("RevivePanelOut");
	}
	
	public void OnCancelButtonPressed() {
		buttonPanelCanvasGroup.interactable = false;
		shouldPlayVideo = false;
		panelAnimator.Play("RevivePanelOut");
	}
	
	public void OnPanelDismissed() {
		gameObject.SetActive(false);
		if (shouldPlayVideo) {
			#if !UNITY_EDITOR
			HZIncentivizedAd.SetDisplayListener(delegate(string adState, string adTag) {
				if (adState.Equals("failed") ||
					adState.Equals("fetch_failed") ||
					adState.Equals("incentivized_result_incomplete")) {
					HZIncentivizedAd.SetDisplayListener(null);
					noReviveAction();
				}
				else if (adState.Equals("incentivized_result_complete")) {
					HZIncentivizedAd.SetDisplayListener(null);
					reviveAction();
				}
			});
			HZIncentivizedAd.Show();
			#else
				reviveAction();
			#endif
		}
		else {
			noReviveAction();
		}
	}
}
