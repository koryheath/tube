﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTrail : MonoBehaviour {
	
	public float distanceFromTubeCenter = 20;
	public float maxLength = 33;
	public float headLength = 3;
	public float width = 2.1f;
	public int lengthVertices = 100;
	public int widthVertices = 4;
	public float smoothing = .75f;
	public float tilting = 20;
	
	MeshFilter meshFilter;
	Material meshMaterial;
	Vector2[] angles = new Vector2[50];
	float length;
	float halfAngle;
	float[] centerAngles;
	Vector3[] centerPoints;
	Quaternion[] rotations;
	Vector3[] vertices;
	
	void Awake() {
		meshFilter = GetComponent<MeshFilter>();
		meshMaterial = GetComponent<MeshRenderer>().material;
		centerAngles = new float[lengthVertices];
		centerPoints = new Vector3[lengthVertices];
		rotations = new Quaternion[lengthVertices];
		CreateMesh();
	}
	
	void OnDestroy() {
		Destroy(meshFilter.sharedMesh);
		Destroy(meshMaterial);
	}
	
	public void SetTextureAndColor(Texture texture, Color color) {
		meshMaterial.mainTexture = texture;
		meshMaterial.color = color;
	}
	
	public void Reset(float angle) {
		length = 0;
		for (int i = 0; i < angles.Length; i++) {
			angles[i] = new Vector2(angle * Mathf.Deg2Rad, 0);
		}
		UpdateMesh();
	}
	
	public void AddAngle(float angle, float deltaZ) {
		length = Mathf.Min(maxLength, length + deltaZ);
		for (int i = angles.Length -1; i > 0; i--) {
			angles[i] = angles[i - 1];
			angles[i].y -= deltaZ;
		}
		angles[0] = new Vector2(angle * Mathf.Deg2Rad, 0);
		UpdateMesh();
	}
	
	public void ReduceLength(float deltaZ) {
		length = Mathf.Max(0, length - deltaZ);
		UpdateMesh();
	}
	
	void UpdateMesh() {
		
		Mesh mesh = meshFilter.sharedMesh;
		
		// Calculate center points
		for (int i = 0; i < lengthVertices; i++) {
			float z = (i / (lengthVertices - 1f)) * -length;
			centerAngles[i] = GetAngleForZ(z);
			if (i > 0) {
				centerAngles[i] = Mathf.Lerp(centerAngles[i], centerAngles[i - 1], smoothing);
			}
			centerPoints[i] = new Vector3(Mathf.Sin(centerAngles[i]) * distanceFromTubeCenter, -Mathf.Cos(centerAngles[i]) * distanceFromTubeCenter, z);
		}
		
		// Calculate rotations
		for (int i = 1; i < lengthVertices; i++) {
			Vector3 forward = (centerPoints[i - 1] - centerPoints[i]).normalized;
			if (forward.x == 0 && forward.y == 0 && forward.z == 0) {
				forward = Vector3.forward;
			}
			//Vector3 upwards = (new Vector3(0, 0, centerPoints[i].z - tilting) - centerPoints[i]).normalized;
			Vector3 upwards = (new Vector3(0, 0, -tilting) - centerPoints[i]).normalized;
			rotations[i] = Quaternion.LookRotation(forward, upwards);
		}
		
		// Set first rotation
		rotations[0] = rotations[1];
		
		// Calculate mesh vertices
		int vertexIndex = 0;
		for (int i = 0; i < lengthVertices; i++) {
			Vector3 sideVector = rotations[i] * (Vector3.right * width);
			Vector3 leftPoint = centerPoints[i] - sideVector;
			Vector3 rightPoint = centerPoints[i] + sideVector;
			for (int x = 0; x < widthVertices; x++) {
				vertices[vertexIndex] = Vector3.Lerp(leftPoint, rightPoint, x / (widthVertices - 1f));
				vertexIndex++;
			}
		}
		
		mesh.vertices = vertices;
		
		CalculateHeadPercentage();
	}
	
	float GetAngleForZ(float z) {
		for (int i = 1; i < angles.Length; i++) {
			if (z > angles[i].y) {
				float t = Mathf.InverseLerp(angles[i - 1].y, angles[i].y, z);
				return Mathf.Lerp(angles[i - 1].x, angles[i].x, t);
			}
		}
		return angles[angles.Length - 1].x;
	}
	
	public void CalculateHeadPercentage() {
		float magnitude = 0;
		for (int i = 1; i < lengthVertices; i++) {
			float newMagnitude = magnitude + (centerPoints[i - 1] - centerPoints[i]).magnitude;
			if (headLength <= newMagnitude) {
				float t = Mathf.InverseLerp(magnitude, newMagnitude, headLength);
				float p = Mathf.Lerp((i - 1) / (lengthVertices - 1f), i / (lengthVertices - 1f), t);
				meshMaterial.SetFloat("_HeadPercentage", p);
				return;
			}
			magnitude = newMagnitude;
		}
		meshMaterial.SetFloat("_HeadPercentage", 1);
	}
	
	void CreateMesh() {
		
		vertices = new Vector3[widthVertices * lengthVertices];
		Vector2[] uv = new Vector2[vertices.Length];
		int[] triangles = new int[(widthVertices - 1) * (lengthVertices - 1) * 6];
		
		int vertexIndex = 0;
		int triangleVertex = 0;
		
		for (int y = 0; y < lengthVertices; y++) {
			for (int x = 0; x < widthVertices; x++, vertexIndex++) {
				uv[vertexIndex] = new Vector2(x / (widthVertices - 1f), 1 - (y / (lengthVertices - 1f)));
				if (x > 0 && y > 0) {
					triangles[triangleVertex] = vertexIndex - widthVertices - 1;
					triangleVertex++;
					triangles[triangleVertex] = vertexIndex - widthVertices;
					triangleVertex++;
					triangles[triangleVertex] = vertexIndex - 1;
					triangleVertex++;
					triangles[triangleVertex] = vertexIndex;
					triangleVertex++;
					triangles[triangleVertex] = vertexIndex - 1;
					triangleVertex++;
					triangles[triangleVertex] = vertexIndex - widthVertices;
					triangleVertex++;
				}
			}
		}
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.uv = uv;
		mesh.triangles = triangles;
		mesh.bounds = new Bounds(new Vector3(0, 0, -maxLength * .5f), new Vector3(distanceFromTubeCenter * 2, distanceFromTubeCenter * 2, maxLength));
		meshFilter.sharedMesh = mesh;
	}
}
