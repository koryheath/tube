﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpecificButtonsSettings : MonoBehaviour {

	public GameObject[] toDisableButtonsOnAndroid;


	// Use this for initialization
	void Start () {

		#if UNITY_ANDROID
		foreach(GameObject go in toDisableButtonsOnAndroid)
		{
			go.SetActive (false);
		}
		#endif
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
