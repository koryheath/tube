﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AndroidBackButton : MonoBehaviour {
	
	public Button backButton; 

	#if UNITY_ANDROID
	void Update () {
		if (backButton.isActiveAndEnabled && backButton.IsInteractable() && Input.GetKey(KeyCode.Escape)) {
			backButton.onClick.Invoke();
		}
	}
	#endif
}
