﻿#if UNITY_EDITOR

using UnityEditor;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshManager {
	
	[MenuItem("CONTEXT/MeshFilter/Save Mesh...")]
	public static void SaveMeshInPlace (MenuCommand menuCommand) {
		MeshFilter mf = menuCommand.context as MeshFilter;
		Mesh m = mf.sharedMesh;
		SaveMesh(m, m.name, false, false);
	}
	
	[MenuItem("CONTEXT/MeshFilter/Save Mesh As New Instance...")]
	public static void SaveMeshNewInstanceItem (MenuCommand menuCommand) {
		MeshFilter mf = menuCommand.context as MeshFilter;
		Mesh m = mf.sharedMesh;
		SaveMesh(m, m.name, true, false);
	}
	
	public static void SaveMesh (Mesh mesh, string name, bool makeNewInstance, bool optimizeMesh) {
		string path = EditorUtility.SaveFilePanel("Save Separate Mesh Asset", "Assets/Meshes", name, "asset");
		if (string.IsNullOrEmpty(path)) return;
		
		path = FileUtil.GetProjectRelativePath(path);
		
		Mesh meshToSave = (makeNewInstance) ? Object.Instantiate(mesh) as Mesh : mesh;
		
		if (optimizeMesh)
			MeshUtility.Optimize(meshToSave);
		
		/*
		Vector3[] vertices = meshToSave.vertices;
		for (int i = 0; i < vertices.Length; i++) {
			vertices[i] = vertices[i].normalized * .5f;
		}
		meshToSave.vertices = vertices;
		meshToSave.RecalculateBounds();
		*/
		
		AssetDatabase.CreateAsset(meshToSave, path);
		AssetDatabase.SaveAssets();
	}
	
	public static Mesh CreateCubeMesh(int gridSizeX, int gridSizeY, int gridSizeZ) {
		
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		List<int> triangles = new List<int>();
		
		// Front face
		for (int x = 0; x < gridSizeX - 1; x++) {
			for (int y = 0; y < gridSizeY - 1; y++) {
				float x0 = (x / (gridSizeX - 1f)) - .5f;
				float x1 = ((x + 1) / (gridSizeX - 1f)) - .5f;
				float y0 = (y / (gridSizeY - 1f)) - .5f;
				float y1 = ((y + 1) / (gridSizeY - 1f)) - .5f;
				Vector3 v0 = new Vector3(x0, y0, -.5f);
				Vector3 v1 = new Vector3(x0, y1, -.5f);
				Vector3 v2 = new Vector3(x1, y1, -.5f);
				Vector3 v3 = new Vector3(x1, y0, -.5f);
				CreateQuad(vertices, normals, triangles, v0, v1, v2, v3);
			}
		}
		
		// Back face
		for (int x = 0; x < gridSizeX - 1; x++) {
			for (int y = 0; y < gridSizeY - 1; y++) {
				float x0 = .5f - (x / (gridSizeX - 1f));
				float x1 = .5f - ((x + 1) / (gridSizeX - 1f));
				float y0 = (y / (gridSizeY - 1f)) - .5f;
				float y1 = ((y + 1) / (gridSizeY - 1f)) - .5f;
				Vector3 v0 = new Vector3(x0, y0, .5f);
				Vector3 v1 = new Vector3(x0, y1, .5f);
				Vector3 v2 = new Vector3(x1, y1, .5f);
				Vector3 v3 = new Vector3(x1, y0, .5f);
				CreateQuad(vertices, normals, triangles, v0, v1, v2, v3);
			}
		}
		
		// Right face
		for (int z = 0; z < gridSizeZ - 1; z++) {
			for (int y = 0; y < gridSizeY - 1; y++) {
				float z0 = (z / (gridSizeZ - 1f)) - .5f;
				float z1 = ((z + 1) / (gridSizeZ - 1f)) - .5f;
				float y0 = (y / (gridSizeY - 1f)) - .5f;
				float y1 = ((y + 1) / (gridSizeY - 1f)) - .5f;
				Vector3 v0 = new Vector3(.5f, y0, z0);
				Vector3 v1 = new Vector3(.5f, y1, z0);
				Vector3 v2 = new Vector3(.5f, y1, z1);
				Vector3 v3 = new Vector3(.5f, y0, z1);
				CreateQuad(vertices, normals, triangles, v0, v1, v2, v3);
			}
		}
		
		// Left face
		for (int z = 0; z < gridSizeZ - 1; z++) {
			for (int y = 0; y < gridSizeY - 1; y++) {
				float z0 = .5f - (z / (gridSizeZ - 1f));
				float z1 = .5f - ((z + 1) / (gridSizeZ - 1f));
				float y0 = (y / (gridSizeY - 1f)) - .5f;
				float y1 = ((y + 1) / (gridSizeY - 1f)) - .5f;
				Vector3 v0 = new Vector3(-.5f, y0, z0);
				Vector3 v1 = new Vector3(-.5f, y1, z0);
				Vector3 v2 = new Vector3(-.5f, y1, z1);
				Vector3 v3 = new Vector3(-.5f, y0, z1);
				CreateQuad(vertices, normals, triangles, v0, v1, v2, v3);
			}
		}
		
		// Top face
		for (int x = 0; x < gridSizeX - 1; x++) {
			for (int z = 0; z < gridSizeZ - 1; z++) {
				float x0 = (x / (gridSizeX - 1f)) - .5f;
				float x1 = ((x + 1) / (gridSizeX - 1f)) - .5f;
				float z0 = (z / (gridSizeZ - 1f)) - .5f;
				float z1 = ((z + 1) / (gridSizeZ - 1f)) - .5f;
				Vector3 v0 = new Vector3(x0, .5f, z0);
				Vector3 v1 = new Vector3(x0, .5f, z1);
				Vector3 v2 = new Vector3(x1, .5f, z1);
				Vector3 v3 = new Vector3(x1, .5f, z0);
				CreateQuad(vertices, normals, triangles, v0, v1, v2, v3);
			}
		}
		
		// Bottom face
		for (int x = 0; x < gridSizeX - 1; x++) {
			for (int z = 0; z < gridSizeZ - 1; z++) {
				float x0 = (x / (gridSizeX - 1f)) - .5f;
				float x1 = ((x + 1) / (gridSizeX - 1f)) - .5f;
				float z0 = .5f - (z / (gridSizeZ - 1f));
				float z1 = .5f - ((z + 1) / (gridSizeZ - 1f));
				Vector3 v0 = new Vector3(x0, -.5f, z0);
				Vector3 v1 = new Vector3(x0, -.5f, z1);
				Vector3 v2 = new Vector3(x1, -.5f, z1);
				Vector3 v3 = new Vector3(x1, -.5f, z0);
				CreateQuad(vertices, normals, triangles, v0, v1, v2, v3);
			}
		}
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.normals = normals.ToArray();
		mesh.triangles = triangles.ToArray();
		return mesh;
	}
	
	public static Mesh CreateDiamondMesh() {
		
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		List<int> triangles = new List<int>();
		
		Vector3 v0 = new Vector3(-.5f, 0, -.5f);
		Vector3 v1 = new Vector3(-.5f, 0, .5f);
		Vector3 v2 = new Vector3(.5f, 0, .5f);
		Vector3 v3 = new Vector3(.5f, 0, -.5f);
		Vector3 v4 = new Vector3(0, .5f, 0);
		Vector3 v5 = new Vector3(0, -.5f, 0);
		
		CreateTriangle(vertices, normals, triangles, v0, v1, v4);
		CreateTriangle(vertices, normals, triangles, v1, v2, v4);
		CreateTriangle(vertices, normals, triangles, v2, v3, v4);
		CreateTriangle(vertices, normals, triangles, v3, v0, v4);
		
		CreateTriangle(vertices, normals, triangles, v1, v0, v5);
		CreateTriangle(vertices, normals, triangles, v2, v1, v5);
		CreateTriangle(vertices, normals, triangles, v3, v2, v5);
		CreateTriangle(vertices, normals, triangles, v0, v3, v5);
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.normals = normals.ToArray();
		mesh.triangles = triangles.ToArray();
		return mesh;
	}
	
	public static Mesh CreatePyramidMesh() {
		
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		List<int> triangles = new List<int>();
		
		Vector3 v0 = new Vector3(-.5f, -.5f, -.5f);
		Vector3 v1 = new Vector3(-.5f, -.5f, .5f);
		Vector3 v2 = new Vector3(.5f, -.5f, .5f);
		Vector3 v3 = new Vector3(.5f, -.5f, -.5f);
		Vector3 v4 = new Vector3(0, .5f, 0);
		
		CreateTriangle(vertices, normals, triangles, v0, v1, v4);
		CreateTriangle(vertices, normals, triangles, v1, v2, v4);
		CreateTriangle(vertices, normals, triangles, v2, v3, v4);
		CreateTriangle(vertices, normals, triangles, v3, v0, v4);
		CreateQuad(vertices, normals, triangles, v3, v2, v1, v0);
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.normals = normals.ToArray();
		mesh.triangles = triangles.ToArray();
		return mesh;
	}
	
	public static Mesh CreatePyramidFineMesh(int sizeY) {
		
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		List<int> triangles = new List<int>();
		
		Vector3 v0 = new Vector3(-.5f, -.5f, -.5f);
		Vector3 v1 = new Vector3(.5f, -.5f, -.5f);
		Vector3 v2 = new Vector3(.5f, -.5f, .5f);
		Vector3 v3 = new Vector3(-.5f, -.5f, .5f);
		Vector3 v4 = new Vector3(0, .5f, 0);
		
		for (int i = 0; i < sizeY - 1; i++) {
			float percentage0 = i / (sizeY - 1f);
			float percentage1 = (i + 1) / (sizeY - 1f);
			Vector3 start0 = Vector3.Lerp(v0, v4, percentage0);
			Vector3 end0 = Vector3.Lerp(v0, v4, percentage1);
			Vector3 start1 = Vector3.Lerp(v1, v4, percentage0);
			Vector3 end1 = Vector3.Lerp(v1, v4, percentage1);
			Vector3 start2 = Vector3.Lerp(v2, v4, percentage0);
			Vector3 end2 = Vector3.Lerp(v2, v4, percentage1);
			Vector3 start3 = Vector3.Lerp(v3, v4, percentage0);
			Vector3 end3 = Vector3.Lerp(v3, v4, percentage1);
			CreateQuad(vertices, normals, triangles, start0, end0, end1, start1);
			CreateQuad(vertices, normals, triangles, start1, end1, end2, start2);
			CreateQuad(vertices, normals, triangles, start2, end2, end3, start3);
			CreateQuad(vertices, normals, triangles, start3, end3, end0, start0);
		}
		
		CreateQuad(vertices, normals, triangles, v0, v1, v2, v3);
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.normals = normals.ToArray();
		mesh.triangles = triangles.ToArray();
		return mesh;
	}
	
	public static Mesh CreateCylinderMesh(int sizeY) {
		int numberOfSpokes = 40;
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		List<int> triangles = new List<int>();
		for (int i = 0; i < numberOfSpokes; i++) {
			float angle0 = (Mathf.PI * 2) * (i / (float)numberOfSpokes);
			float angle1 = (Mathf.PI * 2) * ((i + 1) / (float)numberOfSpokes);
			Vector3 point0 = new Vector3(Mathf.Sin(angle0) * .5f, 0, Mathf.Cos(angle0) * .5f);
			Vector3 point1 = new Vector3(Mathf.Sin(angle1) * .5f, 0, Mathf.Cos(angle1) * .5f);
			Vector3 normal0 = point0.normalized;
			Vector3 normal1 = point1.normalized;
			Vector3 up = Vector3.up * .5f;
			Vector3 down = Vector3.down * .5f;
			// Top
			CreateTriangle(
				vertices,
				normals,
				triangles,
				Vector3.zero + down,
				point1 + down,
				point0 + down
			);
			// Bottom
			CreateTriangle(
				vertices,
				normals,
				triangles,
				Vector3.zero + up,
				point0 + up,
				point1 + up
			);
			// Sides
			for (int j = 0; j < sizeY - 1; j++) {
				float y0 = (j / (sizeY - 1f)) - .5f;
				float y1 = ((j + 1) / (sizeY - 1f)) - .5f;
				CreateQuad(
					vertices,
					normals,
					triangles,
					new Vector3(point0.x, y0, point0.z),
					new Vector3(point1.x, y0, point1.z),
					new Vector3(point1.x, y1, point1.z),
					new Vector3(point0.x, y1, point0.z),
					normal0,
					normal1,
					normal1,
					normal0
				);
			}
		}
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.normals = normals.ToArray();
		mesh.triangles = triangles.ToArray();
		return mesh;
	}
	
	public static Mesh CreateSphereMesh(int refineCount) {
		
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		List<int> triangles = new List<int>();
		
		// Icosohedron
		
		float t = (1f + Mathf.Sqrt(5f)) / 2f;
		
		Vector3 p0 = new Vector3(-1,  t,  0);
		Vector3 p1 = new Vector3( 1,  t,  0);
		Vector3 p2 = new Vector3(-1, -t,  0);
		Vector3 p3 = new Vector3( 1, -t,  0);

		Vector3 p4 = new Vector3( 0, -1,  t);
		Vector3 p5 = new Vector3( 0,  1,  t);
		Vector3 p6 = new Vector3( 0, -1, -t);
		Vector3 p7 = new Vector3( 0,  1, -t);

		Vector3 p8 = new Vector3( t,  0, -1);
		Vector3 p9 = new Vector3( t,  0,  1);
		Vector3 p10 = new Vector3(-t,  0, -1);
		Vector3 p11 = new Vector3(-t,  0,  1);
		
		
		// 5 faces around point 0
		CreateSphereTriangle(vertices, normals, triangles, p0, p11, p5, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p0, p5, p1, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p0, p1, p7, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p0, p7, p10, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p0, p10, p11, refineCount);

		// 5 adjacent faces
		CreateSphereTriangle(vertices, normals, triangles, p1, p5, p9, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p5, p11, p4, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p11, p10, p2, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p10, p7, p6, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p7, p1, p8, refineCount);

		// 5 faces around point 3
		CreateSphereTriangle(vertices, normals, triangles, p3, p9, p4, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p3, p4, p2, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p3, p2, p6, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p3, p6, p8, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p3, p8, p9, refineCount);

		// 5 adjacent faces
		CreateSphereTriangle(vertices, normals, triangles, p4, p9, p5, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p2, p4, p11, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p6, p2, p10, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p8, p6, p7, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p9, p8, p1, refineCount);
		
		
		/*
		// Tetrahedron
		
		Vector3 p0 = new Vector3(1,  1,  1);
		Vector3 p1 = new Vector3(-1,  1,  -1);
		Vector3 p2 = new Vector3(1, -1,  -1);
		Vector3 p3 = new Vector3(-1, -1,  1);
		
		CreateSphereTriangle(vertices, normals, triangles, p0, p2, p1, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p1, p2, p3, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p3, p2, p0, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p0, p1, p3, refineCount);
		*/
		
		
		/*
		// Bipyramid
		
		Vector3 p0 = new Vector3(0,  1,  0);
		
		float angle = 0;
		Vector3 p1 = new Vector3(Mathf.Sin(angle),  0,  Mathf.Cos(angle));
		angle = Mathf.PI * 2 * .33f;
		Vector3 p2 = new Vector3(Mathf.Sin(angle),  0,  Mathf.Cos(angle));
		angle = Mathf.PI * 2 * .66f;
		Vector3 p3 = new Vector3(Mathf.Sin(angle),  0,  Mathf.Cos(angle));
		
		Vector3 p4 = new Vector3(0,  -1,  0);
		
		CreateSphereTriangle(vertices, normals, triangles, p0, p1, p2, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p0, p2, p3, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p0, p3, p1, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p4, p2, p1, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p4, p3, p2, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p4, p1, p3, refineCount);
		*/


		/*
		
		// Octahedron
		
		Vector3 p0 = new Vector3(0,  1,  0);
		
		float angle = 0;
		Vector3 p1 = new Vector3(Mathf.Sin(angle),  0,  Mathf.Cos(angle));
		angle = Mathf.PI * 2 * .25f;
		Vector3 p2 = new Vector3(Mathf.Sin(angle),  0,  Mathf.Cos(angle));
		angle = Mathf.PI * 2 * .5f;
		Vector3 p3 = new Vector3(Mathf.Sin(angle),  0,  Mathf.Cos(angle));
		angle = Mathf.PI * 2 * .75f;
		Vector3 p4 = new Vector3(Mathf.Sin(angle),  0,  Mathf.Cos(angle));
		
		Vector3 p5 = new Vector3(0,  -1,  0);
		
		CreateSphereTriangle(vertices, normals, triangles, p0, p1, p2, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p0, p2, p3, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p0, p3, p4, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p0, p4, p1, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p5, p2, p1, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p5, p3, p2, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p5, p4, p3, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, p5, p1, p4, refineCount);
		*/


		
		Vector3[] vertexArray = vertices.ToArray();
		Vector3[] normalArray = vertices.ToArray();
		
		for (int i = 0; i < vertexArray.Length; i++) {
			normalArray[i] = vertexArray[i].normalized;
			vertexArray[i] = vertexArray[i].normalized * .5f;
		}
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertexArray;
		mesh.normals = normalArray;
		mesh.triangles = triangles.ToArray();
		return mesh;
	}
	
	public static void CreateSphereTriangle(
		List<Vector3> vertices,
		List<Vector3> normals,
		List<int> triangles,
		Vector3 p0,
		Vector3 p1,
		Vector3 p2,
		int refineCount)
	{
		p0.Normalize();
		p1.Normalize();
		p2.Normalize();
		if (refineCount <= 0) {
			CreateTriangle(vertices, normals, triangles, p0, p1, p2, Vector3.zero, Vector3.zero, Vector3.zero);
			return;
		}
		
		refineCount--;
		
		Vector3 m0 = Vector3.Lerp(p0, p1, .5f);
		Vector3 m1 = Vector3.Lerp(p1, p2, .5f);
		Vector3 m2 = Vector3.Lerp(p2, p0, .5f);
		
		CreateSphereTriangle(vertices, normals, triangles, p0, m0, m2, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, m0, p1, m1, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, m2, m1, p2, refineCount);
		CreateSphereTriangle(vertices, normals, triangles, m0, m1, m2, refineCount);
	}
	
	public static Mesh CreateGemMesh(int numberOfSpokes, float topRadius, float middleY) {
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		List<int> triangles = new List<int>();
		for (int i = 0; i < numberOfSpokes; i++) {
			float angle0 = (Mathf.PI * 2) * (i / (float)numberOfSpokes);
			float angle1 = (Mathf.PI * 2) * ((i + 1) / (float)numberOfSpokes);
			Vector3 topPoint0 = new Vector3(Mathf.Sin(angle0) * topRadius, .5f, Mathf.Cos(angle0) * topRadius);
			Vector3 topPoint1 = new Vector3(Mathf.Sin(angle1) * topRadius, .5f, Mathf.Cos(angle1) * topRadius);
			Vector3 middlePoint0 = new Vector3(Mathf.Sin(angle0) * .5f, middleY, Mathf.Cos(angle0) * .5f);
			Vector3 middlePoint1 = new Vector3(Mathf.Sin(angle1) * .5f, middleY, Mathf.Cos(angle1) * .5f);
			// Top
			CreateTriangle(vertices, normals, triangles, topPoint0, topPoint1, Vector3.up * .5f);
			// Middle
			CreateQuad(vertices, normals, triangles, topPoint0, middlePoint0, middlePoint1, topPoint1);
			// Bottom
			CreateTriangle(vertices, normals, triangles, middlePoint0, Vector3.down * .5f, middlePoint1);
		}
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.normals = normals.ToArray();
		mesh.triangles = triangles.ToArray();
		return mesh;
	}
	
	public static Mesh CreateTubeMesh() {
		
		float lightLevel = 255;
		float mediumLevel = 251;
		float darkLevel = 247;
		Color lightColor = new Color(lightLevel / 255, lightLevel / 255, lightLevel / 255, 1);
		Color mediumColor = new Color(mediumLevel / 255, mediumLevel / 255, darkLevel / 255, 1);
		Color darkColor = new Color(darkLevel / 255, darkLevel / 255, darkLevel / 255, 1);
		
		int numberOfColumns = 8;
		int numberOfSlatsPerColumn = 5;
		
		List<Vector3> vertices = new List<Vector3>();
		List<Color> colors = new List<Color>();
		List<int> triangles = new List<int>();
		
		for (int column = 0; column < numberOfColumns; column++) {
			
			float startAngle = (column / (float)numberOfColumns) * Mathf.PI * 2;
			float endAngle = ((column + 1) / (float)numberOfColumns) * Mathf.PI * 2;
			
			int columnType = (column % 4);
			
			if (columnType == 0 || columnType == 1) {
				CreateTubeMeshPanel(vertices, colors, triangles, startAngle, endAngle, numberOfSlatsPerColumn, -.5f, 0, mediumColor, mediumColor);
				CreateTubeMeshPanel(vertices, colors, triangles, startAngle, endAngle, numberOfSlatsPerColumn, 0, .5f, mediumColor, mediumColor);
			}
			else if (columnType == 2) {
				CreateTubeMeshPanel(vertices, colors, triangles, startAngle, endAngle, numberOfSlatsPerColumn, -.5f, 0, lightColor, darkColor);
				CreateTubeMeshPanel(vertices, colors, triangles, startAngle, endAngle, numberOfSlatsPerColumn, 0, .5f, darkColor, darkColor);
			}
			else if (columnType == 3) {
				CreateTubeMeshPanel(vertices, colors, triangles, startAngle, endAngle, numberOfSlatsPerColumn, -.5f, 0, lightColor, lightColor);
				CreateTubeMeshPanel(vertices, colors, triangles, startAngle, endAngle, numberOfSlatsPerColumn, 0, .5f, lightColor, darkColor);
			}
		}
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.colors = colors.ToArray();
		mesh.triangles = triangles.ToArray();
		return mesh;
	}
	
	static public void CreateTubeMeshPanel(
		List<Vector3> vertices,
		List<Color> colors,
		List<int> triangles,
		float startAngle,
		float endAngle,
		int numberOfSlats,
		float startZ,
		float endZ,
		Color color0,
		Color color1)
	{
		for (int slat = 0; slat < numberOfSlats; slat++) {
			
			float angle0 = Mathf.Lerp(startAngle, endAngle, slat / (float)numberOfSlats);
			float angle1 = Mathf.Lerp(startAngle, endAngle, (slat + 1) / (float)numberOfSlats);
			float x0 = Mathf.Sin(angle0) * .5f;
			float y0 = Mathf.Cos(angle0) * .5f;
			float x1 = Mathf.Sin(angle1) * .5f;
			float y1 = Mathf.Cos(angle1) * .5f;
			
			if (color0 == color1) {
				CreateQuad(
					vertices,
					colors,
					triangles,
					new Vector3(x0, y0, startZ),
					new Vector3(x1, y1, startZ),
					new Vector3(x1, y1, endZ),
					new Vector3(x0, y0, endZ),
					color0,
					color0,
					color0,
					color0
				);
			}
			else {
				float z0 = Mathf.Lerp(startZ, endZ, slat / (float)numberOfSlats);
				float z1 = Mathf.Lerp(startZ, endZ, (slat + 1) / (float)numberOfSlats);
				CreateQuad(
					vertices,
					colors,
					triangles,
					new Vector3(x0, y0, startZ),
					new Vector3(x1, y1, startZ),
					new Vector3(x1, y1, z1),
					new Vector3(x0, y0, z0),
					color0,
					color0,
					color0,
					color0
				);
				CreateQuad(
					vertices,
					colors,
					triangles,
					new Vector3(x0, y0, z0),
					new Vector3(x1, y1, z1),
					new Vector3(x1, y1, endZ),
					new Vector3(x0, y0, endZ),
					color1,
					color1,
					color1,
					color1
				);
			}

		}
	}
	
	static public Mesh CreateFinishLineMesh() {
		
		float lightLevel = 255;
		float darkLevel = 128;
		Color lightColor = new Color(lightLevel / 255, lightLevel / 255, lightLevel / 255, 1);
		Color darkColor = new Color(darkLevel / 255, darkLevel / 255, darkLevel / 255, 1);

		int numberOfSlats = 40;
		
		List<Vector3> vertices = new List<Vector3>();
		List<Color> colors = new List<Color>();
		List<int> triangles = new List<int>();
		
		for (int i = 0; i < numberOfSlats; i++) {
			
			float angle0 = (i / (float)numberOfSlats) * Mathf.PI * 2;
			float angle1 = ((i + 1) / (float)numberOfSlats) * Mathf.PI * 2;
			float x0 = Mathf.Sin(angle0) * .5f;
			float y0 = Mathf.Cos(angle0) * .5f;
			float x1 = Mathf.Sin(angle1) * .5f;
			float y1 = Mathf.Cos(angle1) * .5f;
			
			Color color0 = (i % 2 == 0 ? lightColor : darkColor);
			Color color1 = (i % 2 == 0 ? darkColor : lightColor);
			
			CreateQuad(
				vertices,
				colors,
				triangles,
				new Vector3(x0, y0, -.5f),
				new Vector3(x1, y1, -.5f),
				new Vector3(x1, y1, -.5f + (1f / 3f)),
				new Vector3(x0, y0, -.5f + (1f / 3f)),
				color0,
				color0,
				color0,
				color0
			);

			CreateQuad(
				vertices,
				colors,
				triangles,
				new Vector3(x0, y0, -.5f + (1f / 3f)),
				new Vector3(x1, y1, -.5f + (1f / 3f)),
				new Vector3(x1, y1, -.5f + (2f / 3f)),
				new Vector3(x0, y0, -.5f + (2f / 3f)),
				color1,
				color1,
				color1,
				color1
			);

			CreateQuad(
				vertices,
				colors,
				triangles,
				new Vector3(x0, y0, -.5f + (2f / 3f)),
				new Vector3(x1, y1, -.5f + (2f / 3f)),
				new Vector3(x1, y1, .5f),
				new Vector3(x0, y0, .5f),
				color0,
				color0,
				color0,
				color0
			);
		}
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.colors = colors.ToArray();
		mesh.triangles = triangles.ToArray();
		return mesh;
	}
	
	static public void CreateTriangle(
		List<Vector3> vertices,
		List<Vector3> normals,
		List<int> triangles,
		Vector3 v0,
		Vector3 v1,
		Vector3 v2,
		Vector3 n0,
		Vector3 n1,
		Vector3 n2)
	{
		triangles.Add(GetVertexIndex(vertices, normals, v0, n0));
		triangles.Add(GetVertexIndex(vertices, normals, v1, n1));
		triangles.Add(GetVertexIndex(vertices, normals, v2, n2));
	}
	
	static public void CreateQuad(
		List<Vector3> vertices,
		List<Vector3> normals,
		List<int> triangles,
		Vector3 v0,
		Vector3 v1,
		Vector3 v2,
		Vector3 v3,
		Vector3 n0,
		Vector3 n1,
		Vector3 n2,
		Vector3 n3)
	{
		CreateTriangle(vertices, normals, triangles, v0, v1, v2, n0, n1, n2);
		CreateTriangle(vertices, normals, triangles, v2, v3, v0, n2, n3, n0);
	}
	
	static public void CreateTriangle(
		List<Vector3> vertices,
		List<Color> colors,
		List<int> triangles,
		Vector3 v0,
		Vector3 v1,
		Vector3 v2,
		Color c0,
		Color c1,
		Color c2)
	{
		triangles.Add(GetVertexIndex(vertices, colors, v0, c0));
		triangles.Add(GetVertexIndex(vertices, colors, v1, c1));
		triangles.Add(GetVertexIndex(vertices, colors, v2, c2));
	}
	
	static public void CreateQuad(
		List<Vector3> vertices,
		List<Color> colors,
		List<int> triangles,
		Vector3 v0,
		Vector3 v1,
		Vector3 v2,
		Vector3 v3,
		Color c0,
		Color c1,
		Color c2,
		Color c3)
	{
		CreateTriangle(vertices, colors, triangles, v0, v1, v2, c0, c1, c2);
		CreateTriangle(vertices, colors, triangles, v2, v3, v0, c2, c3, c0);
	}
	
	static public void CreateTriangle(
		List<Vector3> vertices,
		List<Vector3> normals,
		List<int> triangles,
		Vector3 v0,
		Vector3 v1,
		Vector3 v2)
	{
		Vector3 normal = Vector3.Cross(v2 - v1, v0 - v1).normalized;
		triangles.Add(GetVertexIndex(vertices, normals, v0, normal));
		triangles.Add(GetVertexIndex(vertices, normals, v1, normal));
		triangles.Add(GetVertexIndex(vertices, normals, v2, normal));
	}
	
	static public void CreateQuad(
		List<Vector3> vertices,
		List<Vector3> normals,
		List<int> triangles,
		Vector3 v0,
		Vector3 v1,
		Vector3 v2,
		Vector3 v3)
	{
		CreateTriangle(vertices, normals, triangles, v0, v1, v2);
		CreateTriangle(vertices, normals, triangles, v2, v3, v0);
	}
	
	static public int GetVertexIndex(
		List<Vector3> vertices,
		List<Vector3> normals,
		Vector3 vertex,
		Vector3 normal)
	{
		for (int i = 0; i < vertices.Count; i++) {
			if (vertices[i] == vertex && normals[i] == normal) {
				return i;
			}
		}
		vertices.Add(vertex);
		normals.Add(normal);
		return vertices.Count - 1;
	}
	
	static public int GetVertexIndex(
		List<Vector3> vertices,
		List<Color> colors,
		Vector3 vertex,
		Color color)
	{
		for (int i = 0; i < vertices.Count; i++) {
			if (vertices[i] == vertex && colors[i] == color) {
				return i;
			}
		}
		vertices.Add(vertex);
		colors.Add(color);
		return vertices.Count - 1;
	}
}

#endif