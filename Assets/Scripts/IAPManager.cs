﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour, IStoreListener
{
	private static IStoreController m_StoreController;
	private static IExtensionProvider m_StoreExtensionProvider;
	
	public GameController gameController;
	public string noAdsProductID;
	public string gemDoublerProductID;
	public string tenjinID;
	
	List<string> purchasedProductTitles = new List<string>();
	
	private bool _activitySpinnerShowing;
	bool activitySpinnerShowing {
		get {
			return _activitySpinnerShowing;
		}
		set {
			if (_activitySpinnerShowing != value) {
				_activitySpinnerShowing = value;
				if (_activitySpinnerShowing) {
					gameController.gameCanvas.ShowActivitySpinnerWithTimeout(10);
				}
				else {
					gameController.gameCanvas.HideActivitySpinner();
				}
			}
		}
	}
	
	bool explicitlyPurchasing;
    
	void Start() {
		if (m_StoreController == null) {
			InitializePurchasing();
		}
	}
    
	public void InitializePurchasing() {
		if (IsInitialized()) {
			return;
		}
        
		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
		
//		builder.AddProduct(noAdsProductID, ProductType.NonConsumable);
//		builder.AddProduct(gemDoublerProductID, ProductType.NonConsumable);



		///// Added IDs for android.
		#if UNITY_IPHONE
		builder.AddProduct (noAdsProductID, ProductType.NonConsumable, new IDs () {
			{ noAdsProductID, AppleAppStore.Name },
			{ ThirdPartyManager.AndroidNOADSID, GooglePlay.Name }
		});

		builder.AddProduct (gemDoublerProductID, ProductType.NonConsumable, new IDs () {
			{ gemDoublerProductID, AppleAppStore.Name },
			{ ThirdPartyManager.AndroidGEMSDOUBLERID, GooglePlay.Name }
		});
		#elif UNITY_ANDROID

		builder.AddProduct (ThirdPartyManager.AndroidNOADSID, ProductType.NonConsumable, new IDs () {
			{ noAdsProductID, AppleAppStore.Name },
			{ ThirdPartyManager.AndroidNOADSID, GooglePlay.Name }
		});

		builder.AddProduct (ThirdPartyManager.AndroidGEMSDOUBLERID, ProductType.NonConsumable, new IDs () {
			{ gemDoublerProductID, AppleAppStore.Name },
			{ ThirdPartyManager.AndroidGEMSDOUBLERID, GooglePlay.Name }
		});
		#endif
		///////////// 

		UnityPurchasing.Initialize(this, builder);
	}
    
    
	private bool IsInitialized() {
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}
    
    
	public void PurchaseNoAds() {

		#if UNITY_ANDROID
		PurchaseProductID(ThirdPartyManager.AndroidNOADSID);

				return;
		#endif
		PurchaseProductID(noAdsProductID);
	}
    
    
	public void PurchaseGemDoubler() {
		#if UNITY_ANDROID
		PurchaseProductID(ThirdPartyManager.AndroidGEMSDOUBLERID);

		return;
		#endif
		PurchaseProductID(gemDoublerProductID);
	}
    
    
	void PurchaseProductID(string productId) {
		if (IsInitialized()) {
			Product product = m_StoreController.products.WithID(productId);
			if (product != null && product.availableToPurchase) {
				Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
				purchasedProductTitles.Clear();
				m_StoreController.InitiatePurchase(product);
				#if UNITY_IPHONE
					activitySpinnerShowing = true;
				#endif
				explicitlyPurchasing = true;
			}
			else {
				Debug.Log("PurchaseProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				PurchaseProblemAlert();
			}
		}
		else {
			Debug.Log("PurchaseProductID FAIL. Not initialized.");
			PurchaseProblemAlert();
		}
	}
    
    
	// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
	// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
	public void RestorePurchases()
	{
		if (!IsInitialized()) {
			Debug.Log("RestorePurchases FAIL. Not initialized.");
			RestorePurchasesProblemAlert();
			return;
		}
        
		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer || 
			Application.platform == RuntimePlatform.OSXPlayer) {
			// ... begin restoring purchases
			Debug.Log("RestorePurchases started ...");
			activitySpinnerShowing = true;
			explicitlyPurchasing = false;
			purchasedProductTitles.Clear();
            
			// Fetch the Apple store-specific subsystem.
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
			// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then 
				// no purchases are available to be restored.
				Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
				activitySpinnerShowing = false;
				if (result) {
					if (purchasedProductTitles.Count == 0) {
						NativeUtilities.OpenAlert("Restore", "No previous purchases found.", null, null);
					}
					else {
						string message = "";
						for (int i = 0; i < purchasedProductTitles.Count; i++) {
							if (i > 0) {
								if (purchasedProductTitles.Count == 2) {
									message += " and ";
								}
								else if (purchasedProductTitles.Count > 3 && i == purchasedProductTitles.Count - 1) {
									message += ", and ";
								}
								else {
									message += ", ";
								}
							}
							message += purchasedProductTitles[i];
						}
						message += purchasedProductTitles.Count > 1 ? " were" : " was";
						message += " restored!";
						NativeUtilities.OpenAlert("Restore", message, null, null);
					}
				}
				else {
					RestorePurchasesProblemAlert();
				}
			});
		}
	}
	
	public void RestorePurchasesProblemAlert() {
		NativeUtilities.OpenAlert("Restore", "A problem occured while attempting to restore purchases.", null, null);
	}
    
	public void PurchaseProblemAlert() {
		NativeUtilities.OpenAlert("Purchase", "A problem occured while attempting to purchase this product.", null, null);
	}
    
	//  
	// --- IStoreListener
	//
    
	public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
		Debug.Log("IAP: OnInitialized: PASS");
		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
	}
    
    
	public void OnInitializeFailed(InitializationFailureReason error) {
		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		Debug.Log("IAP:  OnInitializeFailed InitializationFailureReason:" + error);
	}
    
    
	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
	{
		if (String.Equals(args.purchasedProduct.definition.id, noAdsProductID, StringComparison.Ordinal) ||
			String.Equals(args.purchasedProduct.definition.id, gemDoublerProductID, StringComparison.Ordinal) ||
			String.Equals(args.purchasedProduct.definition.id, ThirdPartyManager.AndroidGEMSDOUBLERID, StringComparison.Ordinal) ||
			String.Equals(args.purchasedProduct.definition.id, ThirdPartyManager.AndroidNOADSID, StringComparison.Ordinal)
		
		)
		{
			Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			purchasedProductTitles.Add(args.purchasedProduct.metadata.localizedTitle);
			#if !UNITY_EDITOR && UNITY_IPHONE
			if (explicitlyPurchasing) {
				Tenjin.getInstance(tenjinID).Transaction(
					args.purchasedProduct.definition.id,
					args.purchasedProduct.metadata.isoCurrencyCode,
					1,
					(double)args.purchasedProduct.metadata.localizedPrice,
					null,
					null,
					null);
				/*
				var price = args.purchasedProduct.metadata.localizedPriceString;
				var currencyCode = args.purchasedProduct.metadata.isoCurrencyCode;
				var wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(args.purchasedProduct.receipt);
				if (wrapper != null) {
					var payload   = (string)wrapper["Payload"]; // For Apple this will be the base64 encoded ASN.1 receipt
					var productId = args.purchasedProduct.definition.id;
					double lPrice = 0;
					double.TryParse(price, out lPrice);
					var transactionId = args.purchasedProduct.transactionID;
					Tenjin.getInstance(tenjinID).Transaction(productId, currencyCode, 1, lPrice, transactionId, payload, null);
				}
				*/
			}
			#endif
			if (String.Equals(args.purchasedProduct.definition.id, noAdsProductID, StringComparison.Ordinal) || 
				String.Equals(args.purchasedProduct.definition.id, ThirdPartyManager.AndroidNOADSID, StringComparison.Ordinal)) 
			{
				gameController.NoAdsPurchased();
			}
			else {
				gameController.GemDoublerPurchased();
			}
		}
		else {
			Debug.Log(string.Format("IAP: ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
		}

		activitySpinnerShowing = false;
		explicitlyPurchasing = false;
		
		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		return PurchaseProcessingResult.Complete;
	}
    
    
	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
		// this reason with the user to guide their troubleshooting actions.
		Debug.Log(string.Format("IAP: OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
		if (failureReason != PurchaseFailureReason.UserCancelled) {
			PurchaseProblemAlert();
		}
		activitySpinnerShowing = false;
		explicitlyPurchasing = false;
	}
}
