﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallConfig : MonoBehaviour {
	
	public Texture mainTexture;
	
	public Texture trailTexture;
	public Color trailColor;
	
	public Color[] deathBallColors;
	
	public int cost;
}
