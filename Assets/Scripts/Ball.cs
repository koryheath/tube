﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
	
	public Rigidbody rigidBody;
	
	private Material _ballMaterial;
	public Material ballMaterial {
		get {
			if (_ballMaterial == null) {
				_ballMaterial = GetComponent<MeshRenderer>().material;
			}
			return _ballMaterial;
		}
	}
	
	void OnDestroy() {
		if (_ballMaterial != null) {
			Destroy(_ballMaterial);
		}
	}
	
	public void SetTexture(Texture texture) {
		ballMaterial.mainTexture = texture;
	}
	
	public float squashFactor {
		get {
			return ballMaterial.GetFloat("_SquashFactor");
		}
		set {
			ballMaterial.SetFloat("_SquashFactor", value);
		}
	}
}
