﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MeshType {
	none,
	sphereSparse,
	sphereDense,
	cube,
	cubePillar,
	pyramid,
	pyramidPillar,
	cylinder,
	cylinderPillar,
	gem,
	ring
}

public enum ColliderType {
	none,
	sphere,
	ellipsoid,
	cube,
	pyramid,
	cylinder,
	gem,
	ring
}

public enum ShadingType {
	directional,
	radial,
	gem,
	none
}

public enum ShadowType {
	directional,
	radial,
	none
}

public enum RepeatMode {
	loop,
	pingPong,
	triggeredOnce,
	triggeredLoop,
	triggeredPingPong
}

[DisallowMultipleComponent]
public class TubeObject : MonoBehaviour {
	
	public static Mesh[] meshes;
	public static Material[] shadingMaterials;
	public static Material[] shadowMaterials;
	
	public static float shakingFactor;
	
	public static float ballRadius;
	public static float ballRadiusSquared;
	public static Vector3 ballPosition;
	
	public MeshFilter meshFilter;
	public MeshRenderer meshRenderer;
	
	public int numberOfSlats;
	
	public Func<bool> isColliding;
	
	Vector3[] vertices;
	Vector3[] normals;
	
	List<TubeObject> children = new List<TubeObject>();
	
	private TubeObject _tubeObjectParent = null;
	public TubeObject tubeObjectParent {
		get {
			return _tubeObjectParent;
		}
		set {
			if (_tubeObjectParent != null) {
				_tubeObjectParent.children.Remove(this);
			}
			_tubeObjectParent = value;
			if (_tubeObjectParent != null) {
				_tubeObjectParent.children.Add(this);
			}
		}
	}
	
	// When setting this to ring, numberOfSlats must already be properly set.
	public MeshType _meshType = MeshType.none;
	public MeshType meshType {
		get {
			return _meshType;
		}
		set {
			if (_meshType == value) {
				return;
			}
			if (_meshType == MeshType.ring) {
				Destroy(meshFilter.sharedMesh);
				meshFilter.sharedMesh = null;
				vertices = null;
				normals = null;
			}
			_meshType = value;
			int index = (int)value;
			if (index < meshes.Length) {
				meshFilter.sharedMesh = meshes[index];
			}
			else {
				vertices = new Vector3[((numberOfSlats + 1) * 8) + 8];
				normals = new Vector3[vertices.Length];
				int[] triangles = new int[(numberOfSlats * 24) + 12];
		
				int vertexIndex = 0;
				int triangleIndex = 0;
		
				// Top surface
				for (int i = 0; i < numberOfSlats; i++, vertexIndex += 2) {
					triangles[triangleIndex] = vertexIndex;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 2;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 1;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 2;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 3;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 1;
					triangleIndex++;
				}
				vertexIndex += 2;
		
				// Bottom surface
				for (int i = 0; i < numberOfSlats; i++, vertexIndex += 2) {
					triangles[triangleIndex] = vertexIndex;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 1;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 2;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 2;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 1;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 3;
					triangleIndex++;
				}
				vertexIndex += 2;
		
				// Front surface
		
				for (int i = 0; i < (numberOfSlats + 1) * 2; i++) {
					normals[vertexIndex + i] = Vector3.back;
				}
			
				for (int i = 0; i < numberOfSlats; i++, vertexIndex += 2) {
					triangles[triangleIndex] = vertexIndex;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 1;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 2;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 3;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 2;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 1;
					triangleIndex++;
				}
				vertexIndex += 2;
		
				// Back surface
		
				for (int i = 0; i < (numberOfSlats + 1) * 2; i++) {
					normals[vertexIndex + i] = Vector3.forward;
				}
		
				for (int i = 0; i < numberOfSlats; i++, vertexIndex += 2) {
					triangles[triangleIndex] = vertexIndex;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 2;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 1;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 1;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 2;
					triangleIndex++;
					triangles[triangleIndex] = vertexIndex + 3;
					triangleIndex++;
				}
				vertexIndex += 2;
		
				// Left side
				triangles[triangleIndex] = vertexIndex;
				triangleIndex++;
				triangles[triangleIndex] = vertexIndex + 1;
				triangleIndex++;
				triangles[triangleIndex] = vertexIndex + 2;
				triangleIndex++;
				triangles[triangleIndex] = vertexIndex + 3;
				triangleIndex++;
				triangles[triangleIndex] = vertexIndex + 2;
				triangleIndex++;
				triangles[triangleIndex] = vertexIndex + 1;
				triangleIndex++;
				vertexIndex += 4;
		
				// Right side
				triangles[triangleIndex] = vertexIndex;
				triangleIndex++;
				triangles[triangleIndex] = vertexIndex + 1;
				triangleIndex++;
				triangles[triangleIndex] = vertexIndex + 2;
				triangleIndex++;
				triangles[triangleIndex] = vertexIndex + 3;
				triangleIndex++;
				triangles[triangleIndex] = vertexIndex + 2;
				triangleIndex++;
				triangles[triangleIndex] = vertexIndex + 1;
				triangleIndex++;
		
				Mesh mesh = new Mesh();
				mesh.vertices = vertices;
				mesh.normals = normals;
				mesh.triangles = triangles;
				meshFilter.sharedMesh = mesh;
			}
		}
	}
	
	protected ColliderType _colliderType = ColliderType.none;
	public ColliderType colliderType {
		get {
			return _colliderType;
		}
		set {
			_colliderType = value;
			switch(value) {
			case ColliderType.sphere:
				isColliding = testSphereCollision;
				break;
			case ColliderType.ellipsoid:
				isColliding = testEllipsoidCollision;
				break;
			case ColliderType.cube:
				isColliding = testCubeCollision;
				break;
			case ColliderType.pyramid:
				isColliding = testPyramidCollision;
				break;
			case ColliderType.cylinder:
				isColliding = testCylinderCollision;
				break;
			case ColliderType.gem:
				isColliding = testGemCollision;
				break;
			case ColliderType.ring:
				isColliding = testRingCollision;
				break;
			default:
				isColliding = null;
				break;
			}
		}
	}
	
	protected ShadingType _shadingType = ShadingType.none;
	public ShadingType shadingType {
		get {
			return _shadingType;
		}
		set {
			_shadingType = value;
			UpdateMaterials();
		}
	}
	
	protected ShadowType _shadowType = ShadowType.none;
	public ShadowType shadowType {
		get {
			return _shadowType;
		}
		set {
			_shadowType = value;
			UpdateMaterials();
		}
	}
	
	public float animationDistance;
	
	protected float _tubeAngle;
	protected float _tubeAngleStart;
	public float tubeAngleEnd;
	public float tubeAngleDuration;
	public System.Func<float, float> tubeAngleCurve;
	public RepeatMode tubeAngleRepeatMode;
	public float tubeAngleShakingDistance = -1000000;
	public float tubeAngleShakingAmount;
	public float tubeAngle {
		get { return _tubeAngle; }
		set {
			_tubeAngle = value;
			_tubeAngleStart = value;
			UpdatePositionAndRotation();
		}
	}
	
	protected float _xScale = 1;
	protected float _xScaleStart = 1;
	public float xScaleEnd;
	public float xScaleDuration;
	public System.Func<float, float> xScaleCurve;
	public RepeatMode xScaleRepeatMode;
	public float xScaleShakingDistance = -1000000;
	public float xScaleShakingAmount;
	public float xScale {
		get { return _xScale; }
		set {
			_xScale = value;
			_xScaleStart = value;
			UpdateScale();
		}
	}
	
	protected float _yScale = 1;
	protected float _yScaleStart = 1;
	public float yScaleEnd;
	public float yScaleDuration;
	public System.Func<float, float> yScaleCurve;
	public RepeatMode yScaleRepeatMode;
	public float yScaleShakingDistance = -1000000;
	public float yScaleShakingAmount;
	public float yScale {
		get { return _yScale; }
		set {
			_yScale = value;
			_yScaleStart = value;
			UpdateScale();
		}
	}
	
	protected float _zScale = 1;
	protected float _zScaleStart = 1;
	public float zScaleEnd;
	public float zScaleDuration;
	public System.Func<float, float> zScaleCurve;
	public RepeatMode zScaleRepeatMode;
	public float zScaleShakingDistance = -1000000;
	public float zScaleShakingAmount;
	public float zScale {
		get { return _zScale; }
		set {
			_zScale = value;
			_zScaleStart = value;
			UpdateScale();
		}
	}
	
	protected float _xRotation;
	protected float _xRotationStart;
	public float xRotationEnd;
	public float xRotationDuration;
	public System.Func<float, float> xRotationCurve;
	public RepeatMode xRotationRepeatMode;
	public float xRotationShakingDistance = -1000000;
	public float xRotationShakingAmount;
	public float xRotation {
		get { return _xRotation; }
		set {
			_xRotation = value;
			_xRotationStart = value;
			UpdateRotation();
		}
	}
	
	protected float _yRotation;
	protected float _yRotationStart;
	public float yRotationEnd;
	public float yRotationDuration;
	public System.Func<float, float> yRotationCurve;
	public RepeatMode yRotationRepeatMode;
	public float yRotationShakingDistance = -1000000;
	public float yRotationShakingAmount;
	public float yRotation {
		get { return _yRotation; }
		set {
			_yRotation = value;
			_yRotationStart = value;
			UpdateRotation();
		}
	}
	
	protected float _zRotation;
	protected float _zRotationStart;
	public float zRotationEnd;
	public float zRotationDuration;
	public System.Func<float, float> zRotationCurve;
	public RepeatMode zRotationRepeatMode;
	public float zRotationShakingDistance = -1000000;
	public float zRotationShakingAmount;
	public float zRotation {
		get { return _zRotation; }
		set {
			_zRotation = value;
			_zRotationStart = value;
			UpdateRotation();
		}
	}
	
	protected float _xPosition;
	protected float _xPositionStart;
	public float xPositionEnd;
	public float xPositionDuration;
	public System.Func<float, float> xPositionCurve;
	public RepeatMode xPositionRepeatMode;
	public float xPositionShakingDistance = -1000000;
	public float xPositionShakingAmount;
	public float xPosition {
		get { return _xPosition; }
		set {
			_xPosition = value;
			_xPositionStart = value;
			UpdatePosition();
		}
	}
	
	protected float _yPosition;
	protected float _yPositionStart;
	public float yPositionEnd;
	public float yPositionDuration;
	public System.Func<float, float> yPositionCurve;
	public RepeatMode yPositionRepeatMode;
	public float yPositionShakingDistance = -1000000;
	public float yPositionShakingAmount;
	public float yPosition {
		get { return _yPosition; }
		set {
			_yPosition = value;
			_yPositionStart = value;
			UpdatePosition();
		}
	}
	
	protected float _zPosition;
	protected float _zPositionStart;
	public float zPositionEnd;
	public float zPositionDuration;
	public System.Func<float, float> zPositionCurve;
	public RepeatMode zPositionRepeatMode;
	public float zPositionShakingDistance = -1000000;
	public float zPositionShakingAmount;
	public float zPosition {
		get { return _zPosition; }
		set {
			_zPosition = value;
			_zPositionStart = value;
			UpdatePosition();
		}
	}
	
	protected float _arc = 360;
	protected float _arcStart = 360;
	public float arcEnd;
	public float arcDuration;
	public System.Func<float, float> arcCurve;
	public RepeatMode arcRepeatMode;
	public float arcShakingDistance = -1000000;
	public float arcShakingAmount;
	public float arc {
		get { return _arc; }
	}
	
	protected float _top = Levels.tubeRadius * .5f;
	protected float _topStart = Levels.tubeRadius * .5f;
	public float topEnd;
	public float topDuration;
	public System.Func<float, float> topCurve;
	public RepeatMode topRepeatMode;
	public float topShakingDistance = -1000000;
	public float topShakingAmount;
	public float top {
		get { return _top; }
	}
	
	protected float _bottom;
	protected float _bottomStart;
	public float bottomEnd;
	public float bottomDuration;
	public System.Func<float, float> bottomCurve;
	public RepeatMode bottomRepeatMode;
	public float bottomShakingDistance = -1000000;
	public float bottomShakingAmount;
	public float bottom {
		get { return _bottom; }
	}
	
	public void SetArcTopBottom(float arc, float top, float bottom) {
		_arc = arc;
		_arcStart = _arc;
		_top = top;
		_topStart = top;
		_bottom = bottom;
		_bottomStart = bottom;
		UpdateRingMesh();
	}
	
	public void AnimateParameters(float ballZ, float zPositionStartAnchor) {
		
		zPositionStartAnchor += _zPositionStart;
		
		for(int i = 0; i < children.Count; i++) {
			children[i].AnimateParameters(ballZ, zPositionStartAnchor);
		}
		
		ballZ -= zPositionStartAnchor;
		float animationTime = (ballZ + animationDistance) / 100;
		
		float previousValue;
		
		previousValue = _tubeAngle;
		_tubeAngle = AdjustedFloat(
			ballZ,
			animationTime,
			_tubeAngleStart,
			tubeAngleEnd,
			tubeAngleDuration,
			tubeAngleRepeatMode,
			tubeAngleCurve,
			tubeAngleShakingDistance,
			tubeAngleShakingAmount
		);
		bool tubeAngleChanged = _tubeAngle != previousValue;
		
		previousValue = _xScale;
		_xScale = AdjustedFloat(
			ballZ,
			animationTime,
			_xScaleStart,
			xScaleEnd,
			xScaleDuration,
			xScaleRepeatMode,
			xScaleCurve,
			xScaleShakingDistance,
			xScaleShakingAmount
		);
		bool scaleChanged = _xScale != previousValue;
		
		previousValue = _yScale;
		_yScale = AdjustedFloat(
			ballZ,
			animationTime,
			_yScaleStart,
			yScaleEnd,
			yScaleDuration,
			yScaleRepeatMode,
			yScaleCurve,
			yScaleShakingDistance,
			yScaleShakingAmount
		);
		scaleChanged = scaleChanged || _yScale != previousValue;
		
		previousValue = _zScale;
		_zScale = AdjustedFloat(
			ballZ,
			animationTime,
			_zScaleStart,
			zScaleEnd,
			zScaleDuration,
			zScaleRepeatMode,
			zScaleCurve,
			zScaleShakingDistance,
			zScaleShakingAmount
		);
		scaleChanged = scaleChanged || _zScale != previousValue;
		
		previousValue = _xRotation;
		_xRotation = AdjustedFloat(
			ballZ,
			animationTime,
			_xRotationStart,
			xRotationEnd,
			xRotationDuration,
			xRotationRepeatMode,
			xRotationCurve,
			xRotationShakingDistance,
			xRotationShakingAmount
		);
		bool rotationChanged = _xRotation != previousValue;
		
		previousValue = _yRotation;
		_yRotation = AdjustedFloat(
			ballZ,
			animationTime,
			_yRotationStart,
			yRotationEnd,
			yRotationDuration,
			yRotationRepeatMode,
			yRotationCurve,
			yRotationShakingDistance,
			yRotationShakingAmount
		);
		rotationChanged = rotationChanged || _yRotation != previousValue;
		
		previousValue = _zRotation;
		_zRotation = AdjustedFloat(
			ballZ,
			animationTime,
			_zRotationStart,
			zRotationEnd,
			zRotationDuration,
			zRotationRepeatMode,
			zRotationCurve,
			zRotationShakingDistance,
			zRotationShakingAmount
		);
		rotationChanged = rotationChanged || _zRotation != previousValue;
		
		previousValue = _xPosition;
		_xPosition = AdjustedFloat(
			ballZ,
			animationTime,
			_xPositionStart,
			xPositionEnd,
			xPositionDuration,
			xPositionRepeatMode,
			xPositionCurve,
			xPositionShakingDistance,
			xPositionShakingAmount
		);
		bool positionChanged = _xPosition != previousValue;
		
		previousValue = _yPosition;
		_yPosition = AdjustedFloat(
			ballZ,
			animationTime,
			_yPositionStart,
			yPositionEnd,
			yPositionDuration,
			yPositionRepeatMode,
			yPositionCurve,
			yPositionShakingDistance,
			yPositionShakingAmount
		);
		positionChanged = positionChanged || _yPosition != previousValue;
		
		previousValue = _zPosition;
		_zPosition = AdjustedFloat(
			ballZ,
			animationTime,
			_zPositionStart,
			zPositionEnd,
			zPositionDuration,
			zPositionRepeatMode,
			zPositionCurve,
			zPositionShakingDistance,
			zPositionShakingAmount
		);
		positionChanged = positionChanged || _zPosition != previousValue;
		
		if (tubeAngleChanged || (positionChanged && rotationChanged)) {
			UpdatePositionAndRotation();
		}
		else if (positionChanged) {
			UpdatePosition();
		}
		else if (rotationChanged) {
			UpdateRotation();
		}
		
		if (scaleChanged) {
			UpdateScale();
		}
		
		if (meshType == MeshType.ring) {
			previousValue = _arc;
			_arc = AdjustedFloat(
				ballZ,
				animationTime,
				_arcStart,
				arcEnd,
				arcDuration,
				arcRepeatMode,
				arcCurve,
				arcShakingDistance,
				arcShakingAmount
			);
			bool ringChanged = _arc != previousValue;
		
			previousValue = _top;
			_top = AdjustedFloat(
				ballZ,
				animationTime,
				_topStart,
				topEnd,
				topDuration,
				topRepeatMode,
				topCurve,
				topShakingDistance,
				topShakingAmount
			);
			ringChanged = ringChanged || _top != previousValue;
		
			previousValue = _bottom;
			_bottom = AdjustedFloat(
				ballZ,
				animationTime,
				_bottomStart,
				bottomEnd,
				bottomDuration,
				bottomRepeatMode,
				bottomCurve,
				bottomShakingDistance,
				bottomShakingAmount
			);
			ringChanged = ringChanged || _bottom != previousValue;
			
			if (ringChanged) {
				UpdateRingMesh();
			}
		}
	}
	
	public void AddChild(TubeObject child) {
		child.tubeObjectParent = this;
		child.transform.SetParent(transform, false);
		child.zPosition = 0;
	}
	
	public float AdjustedFloat(
		float ballZ,
		float animationTime,
		float start,
		float end,
		float duration,
		RepeatMode repeatMode,
		System.Func<float, float> curve,
		float shakingDistance,
		float shakingAmount)
	{
		float t;
		float shakingZ = -shakingDistance;
		if (ballZ >= shakingZ && ballZ < -animationDistance) {
			t = Mathf.InverseLerp(shakingZ, -animationDistance, ballZ);
			return start + (((Mathf.PerlinNoise(_zPositionStart + (Time.timeSinceLevelLoad * 222), 0) * 2) - 1) * t * shakingAmount * shakingFactor);
		}
		if (duration == 0) {
			return start;
		}
		t = animationTime / duration;
		if (repeatMode == RepeatMode.triggeredOnce) {
			t = Mathf.Clamp01(t);
		}
		else {
			if (repeatMode == RepeatMode.triggeredLoop || repeatMode == RepeatMode.triggeredPingPong) {
				t = Mathf.Max(0, t);
			}
			if (repeatMode == RepeatMode.pingPong || repeatMode == RepeatMode.triggeredPingPong) {
				t = t % 2;
				if (t < 0) {
					t += 2;
				}
				if (t >= 1) {
					t = 1 - (t - 1);
				}
			}
			else {
				t = t % 1;
				if (t < 0) {
					t += 1;
				}
			}
		}
		if (curve != null) {
			t = curve(t);
		}
		return Mathf.LerpUnclamped(start, end, t);
	}
	
	virtual public void ResetToDefaults() {
		
		tubeObjectParent = null;
		
		_shadingType = ShadingType.none;
		_shadowType = ShadowType.none;
		meshRenderer.sharedMaterials = new Material[0];
		
		numberOfSlats = 0;
		
		meshType = MeshType.none;
		
		isColliding = null;
		_colliderType = ColliderType.none;
		
		animationDistance = 0;
	
		_tubeAngle = 0;
		_tubeAngleStart = 0;
		tubeAngleEnd = 0;
		tubeAngleDuration = 0;
		tubeAngleCurve = null;
		tubeAngleRepeatMode = RepeatMode.loop;
		tubeAngleShakingDistance = -1000000;
		tubeAngleShakingAmount = 0;
		
		_xScale = 1;
		_xScaleStart = 1;
		xScaleEnd = 0;
		xScaleDuration = 0;
		xScaleCurve = null;
		xScaleRepeatMode = RepeatMode.loop;
		xScaleShakingDistance = -1000000;
		xScaleShakingAmount = 0;
		
		_yScale = 1;
		_yScaleStart = 1;
		yScaleEnd = 0;
		yScaleDuration = 0;
		yScaleCurve = null;
		yScaleRepeatMode = RepeatMode.loop;
		yScaleShakingDistance = -1000000;
		yScaleShakingAmount = 0;
		
		_zScale = 1;
		_zScaleStart = 1;
		zScaleEnd = 0;
		zScaleDuration = 0;
		zScaleCurve = null;
		zScaleRepeatMode = RepeatMode.loop;
		zScaleShakingDistance = -1000000;
		zScaleShakingAmount = 0;
		
		_xRotation = 0;
		_xRotationStart = 0;
		xRotationEnd = 0;
		xRotationDuration = 0;
		xRotationCurve = null;
		xRotationRepeatMode = RepeatMode.loop;
		xRotationShakingDistance = -1000000;
		xRotationShakingAmount = 0;
		
		_yRotation = 0;
		_yRotationStart = 0;
		yRotationEnd = 0;
		yRotationDuration = 0;
		yRotationCurve = null;
		yRotationRepeatMode = RepeatMode.loop;
		yRotationShakingDistance = -1000000;
		yRotationShakingAmount = 0;
		
		_zRotation = 0;
		_zRotationStart = 0;
		zRotationEnd = 0;
		zRotationDuration = 0;
		zRotationCurve = null;
		zRotationRepeatMode = RepeatMode.loop;
		zRotationShakingDistance = -1000000;
		zRotationShakingAmount = 0;
		
		_xPosition = 0;
		_xPositionStart = 0;
		xPositionEnd = 0;
		xPositionDuration = 0;
		xPositionCurve = null;
		xPositionRepeatMode = RepeatMode.loop;
		xPositionShakingDistance = -1000000;
		xPositionShakingAmount = 0;
		
		_yPosition = 0;
		_yPositionStart = 0;
		yPositionEnd = 0;
		yPositionDuration = 0;
		yPositionCurve = null;
		yPositionRepeatMode = RepeatMode.loop;
		yPositionShakingDistance = -1000000;
		yPositionShakingAmount = 0;
		
		_zPosition = 0;
		_zPositionStart = 0;
		zPositionEnd = 0;
		zPositionDuration = 0;
		zPositionCurve = null;
		zPositionRepeatMode = RepeatMode.loop;
		zPositionShakingDistance = -1000000;
		zPositionShakingAmount = 0;
		
		_arc = 360;
		_arcStart = 360;
		arcEnd = 0;
		arcDuration = 0;
		arcCurve = null;
		arcRepeatMode = RepeatMode.loop;
		arcShakingDistance = -1000000;
		arcShakingAmount = 0;
		
		_top = Levels.tubeRadius * .5f;
		_topStart = Levels.tubeRadius * .5f;
		topEnd = 0;
		topDuration = 0;
		topCurve = null;
		topRepeatMode = RepeatMode.loop;
		topShakingDistance = -1000000;
		topShakingAmount = 0;
		
		_bottom = 0;
		_bottomStart = 0;
		bottomEnd = 0;
		bottomDuration = 0;
		bottomCurve = null;
		bottomRepeatMode = RepeatMode.loop;
		bottomShakingDistance = -1000000;
		bottomShakingAmount = 0;
	}
	
	public bool testSphereCollision() {
		float size = (_xScale *.5f) + ballRadius;
		return (transform.position - ballPosition).sqrMagnitude < size * size;
	}
	
	public bool testEllipsoidCollision() {
		Vector3 position = transform.InverseTransformPoint(ballPosition);
		position = Vector3.ClampMagnitude(position, .5f);
		position = transform.TransformPoint(position);
		return (position - ballPosition).sqrMagnitude <= ballRadiusSquared;
	}
	
	public bool testCubeCollision() {
		Vector3 position = transform.InverseTransformPoint(ballPosition);
		position.x = Mathf.Clamp(position.x, -.5f, .5f);
		position.y = Mathf.Clamp(position.y, -.5f, .5f);
		position.z = Mathf.Clamp(position.z, -.5f, .5f);
		position = transform.TransformPoint(position);
		return (position - ballPosition).sqrMagnitude <= ballRadiusSquared;
	}
	
	public bool testPyramidCollision() {
		Vector3 position = transform.InverseTransformPoint(ballPosition);
		position.y = Mathf.Clamp(position.y, -.5f, .5f);
		float pyramidScale = (1 - (position.y + .5f)) * .5f;
		position.x = Mathf.Clamp(position.x, -pyramidScale, pyramidScale);
		position.z = Mathf.Clamp(position.z, -pyramidScale, pyramidScale);
		position = transform.TransformPoint(position);
		return (position - ballPosition).sqrMagnitude <= ballRadiusSquared;
	}
	
	public bool testCylinderCollision() {
		Vector3 position = transform.InverseTransformPoint(ballPosition);
		float y = Mathf.Clamp(position.y, -.5f, .5f);
		position.y = 0;
		position = Vector3.ClampMagnitude(position, .5f);
		position.y = y;
		position = transform.TransformPoint(position);
		return (position - ballPosition).sqrMagnitude <= ballRadiusSquared;
	}
	
	public bool testRingCollision() {
		float arcSpread = _arc * Mathf.Deg2Rad * .5f;
		float adjustedBottom = 20 - _bottom;
		float adjustedTop = 20 - _top;
		Vector3 position = transform.InverseTransformPoint(ballPosition);
		position.z = Mathf.Clamp(position.z, -.5f, .5f);
		float length = Mathf.Clamp(((Vector2)position).magnitude, adjustedTop, adjustedBottom);
		float angle = Mathf.Clamp(Mathf.Atan2(position.x, position.y), -arcSpread, arcSpread);
		position.x = Mathf.Sin(angle) * length;
		position.y = Mathf.Cos(angle) * length;
		position = transform.TransformPoint(position);
		return (position - ballPosition).sqrMagnitude <= ballRadiusSquared;
	}
	
	public bool testGemCollision() {
		float size = _yScale + ballRadius;
		return (transform.position - ballPosition).sqrMagnitude < size * size;
	}
	
	public void UpdateMaterials() {
		
		List<Material> materials = new List<Material>();
		
		if (_shadowType != ShadowType.none) {
			materials.Add(shadowMaterials[(int)_shadowType]);
		}
		
		if (_shadingType != ShadingType.none) {
			materials.Add(shadingMaterials[(int)_shadingType]);
		}
		
		meshRenderer.sharedMaterials = materials.ToArray();
	}
	
	public void UpdateTransform() {
		Quaternion rotation = Quaternion.Euler(0, 0, _tubeAngle);
		transform.localPosition = rotation * new Vector3(_xPosition, _yPosition, _zPosition);
		transform.localRotation = rotation * Quaternion.Euler(_xRotation, _yRotation, _zRotation);
		transform.localScale = new Vector3(_xScale, _yScale, _zScale);
	}
	
	public void UpdatePositionAndRotation() {
		Quaternion rotation = Quaternion.Euler(0, 0, _tubeAngle);
		transform.localPosition = rotation * new Vector3(_xPosition, _yPosition, _zPosition);
		transform.localRotation = rotation * Quaternion.Euler(_xRotation, _yRotation, _zRotation);
	}
	
	public void UpdatePosition() {
		Quaternion rotation = Quaternion.Euler(0, 0, _tubeAngle);
		transform.localPosition = rotation * new Vector3(_xPosition, _yPosition, _zPosition);
	}
	
	public void UpdateRotation() {
		Quaternion rotation = Quaternion.Euler(0, 0, _tubeAngle);
		transform.localRotation = rotation * Quaternion.Euler(_xRotation, _yRotation, _zRotation);
	}
	
	public void UpdateScale() {
		transform.localScale = new Vector3(_xScale, _yScale, _zScale);
	}
	
	void UpdateRingMesh() {
		float arcSpread = _arc * Mathf.Deg2Rad;
		float angle = Mathf.PI + (((Mathf.PI * 2) - arcSpread) / 2);
		float angleAdd = arcSpread / numberOfSlats;
		float topRadius = Levels.tubeRadius - _top;
		float bottomRadius = Levels.tubeRadius - _bottom;
		int bottomIndex = (numberOfSlats + 1) * 2;
		int frontIndex = bottomIndex + ((numberOfSlats + 1) * 2);
		int backIndex = frontIndex + ((numberOfSlats + 1) * 2);
		int leftIndex = backIndex+ ((numberOfSlats + 1) * 2);
		int rightIndex = leftIndex + 4;
		for (int i = 0; i < (numberOfSlats + 1) * 2; i += 2, angle += angleAdd) {
			Vector3 v0 = new Vector3(Mathf.Sin(angle) * topRadius, Mathf.Cos(angle) * topRadius, -.5f);
			Vector3 v1 = v0 + Vector3.forward;
			Vector3 v2 = new Vector3(Mathf.Sin(angle) * bottomRadius, Mathf.Cos(angle) * bottomRadius, -.5f);
			Vector3 v3 = v2 + Vector3.forward;
			Vector3 bottomNormal = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0).normalized;
			Vector3 topNormal = -bottomNormal;
			vertices[i] = v0;
			vertices[i + 1] = v1;
			normals[i] = topNormal;
			normals[i + 1] = topNormal;
			vertices[bottomIndex + i] = v2;
			vertices[bottomIndex + i + 1] = v3;
			normals[bottomIndex + i] = bottomNormal;
			normals[bottomIndex + i + 1] = bottomNormal;
			vertices[frontIndex + i] = v0;
			vertices[frontIndex + i + 1] = v2;
			vertices[backIndex + i] = v1;
			vertices[backIndex + i + 1] = v3;
			if (i == 0) {
				Vector3 leftNormal = Quaternion.Euler(0, 0, -90) * topNormal;
				vertices[leftIndex] = v0;
				vertices[leftIndex + 1] = v1;
				vertices[leftIndex + 2] = v2;
				vertices[leftIndex + 3] = v3;
				normals[leftIndex] = leftNormal;
				normals[leftIndex + 1] = leftNormal;
				normals[leftIndex + 2] = leftNormal;
				normals[leftIndex + 3] = leftNormal;
			}
			else if (i == numberOfSlats * 2) {
				Vector3 rightNormal = Quaternion.Euler(0, 0, 90) * topNormal;
				vertices[rightIndex] = v1;
				vertices[rightIndex + 1] = v0;
				vertices[rightIndex + 2] = v3;
				vertices[rightIndex + 3] = v2;
				normals[rightIndex] = rightNormal;
				normals[rightIndex + 1] = rightNormal;
				normals[rightIndex + 2] = rightNormal;
				normals[rightIndex + 3] = rightNormal;
			}
		}
		Mesh mesh = meshFilter.sharedMesh;
		mesh.vertices = vertices;
		mesh.normals = normals;
		float size = bottomRadius * 2;
		mesh.bounds = new Bounds(Vector3.zero, new Vector3(size, size, 1));
	}
	
	public void OnDestroy() {
		if (_meshType == MeshType.ring) {
			Destroy(meshFilter.sharedMesh);
			meshFilter.sharedMesh = null;
		}
	}
	
	/// Instance management
	
	public static TubeObject tubeObjectPrefab;
	public static List<TubeObject> activeTubeObjects = new List<TubeObject>();
	public static Queue<TubeObject> inactiveTubeObjects = new Queue<TubeObject>();
	
	public static TubeObject GetInstance() {
		TubeObject tubeObject;
		if (inactiveTubeObjects.Count > 0) {
			tubeObject = inactiveTubeObjects.Dequeue();
		}
		else {
			tubeObject = Instantiate(tubeObjectPrefab) as TubeObject;
		}
		activeTubeObjects.Add(tubeObject);
		tubeObject.gameObject.SetActive(true);
		return tubeObject;
	}
	
	public static void RecycleInstance(TubeObject tubeObject) {
		while(tubeObject.children.Count > 0) {
			RecycleInstance(tubeObject.children[0]);
		}
		tubeObject.ResetToDefaults();
		tubeObject.gameObject.SetActive(false);
		tubeObject.transform.parent = null;
		activeTubeObjects.Remove(tubeObject);
		inactiveTubeObjects.Enqueue(tubeObject);
	}
	
	public static void SetUp() {
		while(inactiveTubeObjects.Count < 250) {
			TubeObject tubeObject = Instantiate(tubeObjectPrefab) as TubeObject;
			tubeObject.gameObject.SetActive(false);
			inactiveTubeObjects.Enqueue(tubeObject);
		}
	}
	
	public static void CleanUp() {
		activeTubeObjects.Clear();
		inactiveTubeObjects.Clear();
	}
}
