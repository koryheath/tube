﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShareController : MonoBehaviour {
	
	public Camera mainCamera;
	
	[HideInInspector]
	public Texture2D texture2d;
	
	RenderTexture renderTexture;
	
	void Awake() {
		renderTexture = new RenderTexture(Screen.width / 2, Screen.height / 2, 16, RenderTextureFormat.ARGB32);
		renderTexture.antiAliasing = 8;
		renderTexture.Create();
		texture2d = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.RGB24, false);
	}
	
	void OnDestroy() {
		renderTexture.Release();
		Destroy(renderTexture);
	}
	
	public void TakeScreenshot() {
		mainCamera.targetTexture = renderTexture;
		mainCamera.Render();
		RenderTexture.active = renderTexture;
		texture2d.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
		texture2d.Apply();
		RenderTexture.active = null;
		mainCamera.targetTexture = null;
	}
	
	public void ShareScreenshot() {

		string url = " https://itunes.apple.com/app/tricky-tube/id1336004724";


		#if UNITY_ANDROID
		url = " market://details?id=" + Application.identifier; 
		#endif

		NativeShare.ShareTexture(
			texture2d,
			"",
			string.Format(LocalizationManager.GetLocalizedText("ShareLink"), Settings.bestScore.ToString()) +
			url,
			""
		);
	}
}
