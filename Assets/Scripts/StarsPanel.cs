﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarsPanel : MonoBehaviour {
	
	public GameObject[] achievedStars;
	public GameObject[] unachievedStars;

	private int _stars;
	public int stars {
		get {
			return _stars;
		}
		set {
			_stars = value;
			for (int i = 0; i < 3; i++) {
				achievedStars[i].SetActive(i < value);
				unachievedStars[i].SetActive(i >= value);
			}
		}
	}
}
