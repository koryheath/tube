﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBall : MonoBehaviour {
	
	public Rigidbody rigidBody;

	private Material _ballMaterial;
	public Material ballMaterial {
		get {
			if (_ballMaterial == null) {
				_ballMaterial = GetComponent<MeshRenderer>().material;
			}
			return _ballMaterial;
		}
	}
	
	void OnDestroy() {
		if (_ballMaterial != null) {
			Destroy(_ballMaterial);
		}
	}
	
	public void SetColors(Color topColor, Color bottomColor) {
		ballMaterial.SetColor("_TopColor", topColor);
		ballMaterial.SetColor("_BottomColor", bottomColor);
	}
}
