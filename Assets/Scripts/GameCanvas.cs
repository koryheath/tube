﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
#if UNITY_IOS
using UnityEngine.iOS;
#endif
public class GameCanvas : MonoBehaviour {
	
	public GameController gameController;
	public Canvas gameCanvas;
	public Camera mainCamera;
	public FlyingGemAnimator flyingGemPrefab;
	public TextMeshProUGUI flyingGemAmountPrefab;
	public Text bestScoreText;
	public Text bestScoreButtonText;
	public Text gemText;
	public Text tutorialText;
	public Text scoreText;
	public Text previousScoreText;
	public RectTransform highScorePanel;
	public RectTransform gemsPanel;
	public RectTransform gemImage;
	public RectTransform tutorialPanel;
	public FlashPanel flashPanel;
	public GameObject settingsMenu;
	public BallMenu ballMenu;
	public ChallengeMenu challengeMenu;
	public StarsPanel starsPanel;
	public RevivePanel revivePanel;
	public PausePanel pausePanel;
	public ReversibleAnimator mainUIAnimator;
	public CanvasGroup mainUICanvasGroup;
	public Animator highScoreButtonAnimator;
	public Animator gemVideoButtonAnimator;
	public Animator freeGiftButtonAnimator;
	public Animator noAdsButtonAnimator;
	public Animator gemDoublerButtonAnimator;
	public GameObject freeGiftUnderlay;
	public ReversibleAnimator activitySpinnerAnimator;
	public KetchappSquareController ketchappSquareController;
	public Image screenshotImage;
	public RectTransform screenshotTransform;
	public Button shareButton;
	public GameObject menuPanel;
	public GameObject invisibleTouchBlocker;
	public GameObject volumeOn;
	public GameObject volumeOff;
	public GameObject musicOn;
	public GameObject musicOff;
	public GameObject invertedOn;
	public GameObject invertedOff;
	public GameObject hapticOn;
	public GameObject hapticOff;
	public GameObject gdprButton;
	public Slider swipeSensitivitySlider;
	public RectTransform safeArea;
	public GameObject gameplayInput;
	public GameObject text_FPS;
	public GameObject text_LogsStatus;
	
	bool iPhoneX;
	
	void Awake() {
		#if UNITY_EDITOR
		iPhoneX = (Screen.height / (float)Screen.width > 2.1f);
		#elif UNITY_IOS
		iPhoneX = Device.generation == DeviceGeneration.iPhoneX;
		#endif
		if (iPhoneX) {
			highScorePanel.anchoredPosition += Vector2.right * 10;
			gemsPanel.anchoredPosition += Vector2.left * 10;
			safeArea.offsetMax = new Vector2(safeArea.offsetMax.x, -20);
		}
		gameCanvas.worldCamera = mainCamera;
		gameCanvas.planeDistance = 1;
		AdjustForAds();
		UpdateSoundSettings();
		UpdateMusicSettings();
		UpdateInvertedSettings();
		UpdateHapticSettings();
		UpdateSwipeSensitivitySettings();
		
		if (text_FPS)
			text_FPS.SetActive (Settings.onScreenDebugInfoEnabled);
		if (text_LogsStatus)
			text_LogsStatus.SetActive (Settings.onScreenDebugInfoEnabled);
	}
	
	public void AdjustForAds() {
		float yAnchor = Settings.adsEnabled ? .16f : .12f;
		tutorialPanel.anchorMin = new Vector2(tutorialPanel.anchorMin.x, yAnchor);
		tutorialPanel.anchorMax = new Vector2(tutorialPanel.anchorMax.x, yAnchor);
		float yMin = 0;
		if (Settings.adsEnabled) {
			yMin = iPhoneX ? 80 : 40;
		}
		safeArea.offsetMin = new Vector2(safeArea.offsetMin.x, yMin);
	}
	
	private int _score;
	public int score {
		get {
			return _score;
		}
		set {
			if (value <= 0 && _score > 0) {
				scoreText.gameObject.SetActive(false);
			}
			else if (value > 0 && _score <= 0) {
				scoreText.gameObject.SetActive(true);
			}
			_score = value;
			scoreText.text = value.ToString();
		}
	}
	
	private int _previousScore;
	public int previousScore {
		get {
			return _previousScore;
		}
		set {
			if (value <= 0 && _previousScore > 0) {
				previousScoreText.gameObject.SetActive(false);
			}
			else if (value > 0 && _previousScore <= 0) {
				previousScoreText.gameObject.SetActive(true);
			}
			_previousScore = value;
			previousScoreText.text = value.ToString();
		}
	}
	
	public int bestScore {
		get {
			return Settings.bestScore;
		}
		set {
			Settings.bestScore = value;
			bestScoreText.text = value.ToString();
			bestScoreButtonText.text = value.ToString();
		}
	}
	
	public int gems {
		get {
			return Settings.gems;
		}
		set {
			Settings.gems = value;
			gemText.text = Settings.gems.ToString();
		}
	}
	
	public bool mainUIEnabled {
		get {
			return mainUIAnimator.visible;
		}
		set {
			mainUIAnimator.visible = value;
			mainUICanvasGroup.blocksRaycasts = value;
		}
	}
	
	private Material _screenshotMaterial;
	public Material screenshotMaterial {
		get {
			if (_screenshotMaterial == null) {
				_screenshotMaterial = Instantiate(screenshotImage.material);
				screenshotImage.material = _screenshotMaterial;
			}
			return _screenshotMaterial;
		}
	}
	
	void OnDestroy() {
		if (_screenshotMaterial != null) {
			Destroy(_screenshotMaterial);
		}
	}
	
	public void ShowActivitySpinnerWithTimeout(float duration) {
		CancelInvoke("HideActivitySpinner");
		activitySpinnerAnimator.visible = true;
		Invoke("HideActivitySpinner", duration);
	}
	
	public void HideActivitySpinner() {
		CancelInvoke("HideActivitySpinner");
		activitySpinnerAnimator.visible = false;
	}
	
	public void UpdateSoundSettings() {
		volumeOn.SetActive(Settings.soundEnabled);
		volumeOff.SetActive(!Settings.soundEnabled);
	}
	
	public void UpdateMusicSettings() {
		musicOn.SetActive(Settings.musicEnabled);
		musicOff.SetActive(!Settings.musicEnabled);
	}
	
	public void UpdateInvertedSettings() {
		invertedOn.SetActive(Settings.controlsInverted);
		invertedOff.SetActive(!Settings.controlsInverted);
	}
	
	public void UpdateHapticSettings() {
		hapticOn.SetActive(Settings.hapticEnabled);
		hapticOff.SetActive(!Settings.hapticEnabled);
	}
	
	public void UpdateSwipeSensitivitySettings() {
		swipeSensitivitySlider.minValue = gameController.swipeSensitivityMin;
		swipeSensitivitySlider.maxValue = gameController.swipeSensitivityMax;
		swipeSensitivitySlider.value = Settings.swipeSensitivity;
		swipeSensitivitySlider.onValueChanged.AddListener(delegate { SwipeSensitivitySliderChanged(); });
	}
	
	public void SwipeSensitivitySliderChanged() {
		Settings.swipeSensitivity = swipeSensitivitySlider.value;
	}
	
	public void OpenSettingsMenu(float delay) {
		mainUIEnabled = false;
		invisibleTouchBlocker.SetActive(true);
		gameplayInput.SetActive(false);
		if (KetchappPromo.IsKetchappCountryGdpr()) {
			gdprButton.SetActive(true);
		}
		gameController.Delay(delay, () => {
			invisibleTouchBlocker.SetActive(false);
			menuPanel.SetActive(true);
			settingsMenu.SetActive(true);
			highScorePanel.gameObject.SetActive(false);
			gemsPanel.gameObject.SetActive(false);
		});
	}
	
	public void OpenBallMenu(float delay) {
		mainUIEnabled = false;
		invisibleTouchBlocker.SetActive(true);
		gameplayInput.SetActive(false);
		gameController.Delay(delay, () => {
			gameCanvas.renderMode = RenderMode.ScreenSpaceCamera;
			invisibleTouchBlocker.SetActive(false);
			menuPanel.SetActive(true);
			ballMenu.gameObject.SetActive(true);
			highScorePanel.gameObject.SetActive(false);
			gemsPanel.gameObject.SetActive(true);
		});
	}
	
	public void OpenChallengeMenu(float delay) {
		mainUIEnabled = false;
		invisibleTouchBlocker.SetActive(true);
		gameplayInput.SetActive(false);
		gameController.Delay(delay, () => {
			starsPanel.gameObject.SetActive(false);
			invisibleTouchBlocker.SetActive(false);
			menuPanel.SetActive(true);
			challengeMenu.gameObject.SetActive(true);
			highScorePanel.gameObject.SetActive(false);
			gemsPanel.gameObject.SetActive(true);
		});
	}
	
	public void CloseMenu() {
		mainUIEnabled = true;
		gameCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
		invisibleTouchBlocker.SetActive(false);
		gameplayInput.SetActive(true);
		menuPanel.SetActive(false);
		settingsMenu.SetActive(false);
		ballMenu.gameObject.SetActive(false);
		challengeMenu.gameObject.SetActive(false);
		highScorePanel.gameObject.SetActive(true);
		gemsPanel.gameObject.SetActive(true);
		gdprButton.SetActive(false);
		AdjustForAds();
	}
	
	public void DisableSpecialButtons() {
		highScoreButtonAnimator.gameObject.SetActive(false);
		gemVideoButtonAnimator.gameObject.SetActive(false);
		freeGiftButtonAnimator.gameObject.SetActive(false);
		noAdsButtonAnimator.gameObject.SetActive(false);
		gemDoublerButtonAnimator.gameObject.SetActive(false);
	}
	
	public void EnableHighScoreButton() {
		highScoreButtonAnimator.gameObject.SetActive(true);
	}
	
	public void EnableGemVideoButton() {
		gemVideoButtonAnimator.gameObject.SetActive(true);
	}
	
	public void EnableFreeGiftButton() {
		freeGiftButtonAnimator.gameObject.SetActive(true);
		freeGiftUnderlay.SetActive(true);
		invisibleTouchBlocker.SetActive(true);
	}
	
	public void EnableNoAdsButton() {
		noAdsButtonAnimator.gameObject.SetActive(true);
	}
	
	public void EnableGemDoublerButton() {
		gemDoublerButtonAnimator.gameObject.SetActive(true);
	}
	
	public void DismissHighScoreButton() {
		if (highScoreButtonAnimator.isActiveAndEnabled) {
			highScoreButtonAnimator.Play("SpecialButtonFade");
		}
	}
	
	public void DismissGemVideoButton() {
		if (gemVideoButtonAnimator.isActiveAndEnabled) {
			gemVideoButtonAnimator.Play("SpecialButtonFade");
		}
	}
	
	public void DismissFreeGiftButton() {
		if (freeGiftButtonAnimator.isActiveAndEnabled) {
			freeGiftButtonAnimator.Play("SpecialButtonFade");
		}
	}
	
	public void DismissNoAdsButton() {
		if (noAdsButtonAnimator.isActiveAndEnabled) {
			noAdsButtonAnimator.Play("SpecialButtonFade");
		}
	}
	
	public void DismissGemDoublerButton() {
		if (gemDoublerButtonAnimator.isActiveAndEnabled) {
			gemDoublerButtonAnimator.Play("SpecialButtonFade");
		}
	}
	
	public bool IsGemVideoButtonEnabled() {
		return gemVideoButtonAnimator.gameObject.activeSelf;
	}
	
	public void SetUpGame() {
		mainUIEnabled = true;
		highScorePanel.gameObject.SetActive(true);
		gemsPanel.gameObject.SetActive(true);
		starsPanel.gameObject.SetActive(false);
		UpdateTutorialText();
	}
	
	public void StartGame() {
		mainUIEnabled = false;
	}
	
	public void StartChallenge() {
		starsPanel.stars = 0;
		highScorePanel.gameObject.SetActive(false);
		gemsPanel.gameObject.SetActive(false);
		starsPanel.gameObject.SetActive(true);
		gameplayInput.SetActive(true);
		menuPanel.SetActive(false);
		settingsMenu.SetActive(false);
		ballMenu.gameObject.SetActive(false);
		challengeMenu.gameObject.SetActive(false);
		highScorePanel.gameObject.SetActive(false);
		gemsPanel.gameObject.SetActive(false);
	}
	
	public void UpdateTutorialText() {
		tutorialText.gameObject.SetActive(Settings.numberOfTutorialLevelsPassed == 0);
	}
	
	public void AwardGems(
		int numberOfGems,
		float gemScale,
		Vector3 centerPosition,
		AudioSource popAudioSource,
		AudioSource landAudioSource)
	{
		popAudioSource.Play();
		float radius = 200;
		float noise = 100;
		gems += numberOfGems;
		int numberOfFlyingGems = Mathf.Min(50, numberOfGems);
		for (int i = 0; i < numberOfFlyingGems; i++) {
			float angleAdd = i * (360f / numberOfFlyingGems);
			AudioSource audioSource = i == 0 ? landAudioSource : null;
			FlyingGemAnimator animator = Instantiate(flyingGemPrefab);
			animator.transform.SetParent(transform, false);
			animator.startPosition = transform.InverseTransformPoint(centerPosition);
			Vector3 direction = Quaternion.Euler(0, 0, angleAdd) * Vector3.up * radius;
			animator.controlPosition = animator.startPosition + direction;
			animator.controlPosition = new Vector3(
				animator.controlPosition.x + Random.Range(-noise, noise),
				animator.controlPosition.y + Random.Range(-noise, noise),
				0);
			animator.endPosition = transform.InverseTransformPoint(gemImage.position);
			animator.gemAwardAudioSource = audioSource;
			animator.GetComponent<Animator>().SetFloat("SpeedMultiplier", Random.Range(1.2f, .8f));
			animator.transform.localPosition = animator.startPosition;
			animator.transform.localScale = new Vector3(gemScale, gemScale, 1);
		}
		TextMeshProUGUI flyingGemAmount = Instantiate(flyingGemAmountPrefab);
		flyingGemAmount.transform.SetParent(transform, false);
		flyingGemAmount.transform.position = centerPosition;
		flyingGemAmount.text = "+" + numberOfGems.ToString();
	}
}
