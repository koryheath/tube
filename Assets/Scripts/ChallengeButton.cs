﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChallengeButton : MonoBehaviour {
	
	public GameObject lockedPanel;
	public GameObject unlockedPanel;
	public Text levelNumberText;
	public GameObject[] achievedStarIcons;
	public GameObject[] unachievedStarIcons;
	public Text raycastText;
	public BouncyButton bouncyButton;
	public Animator attentionBounceAnimator;
	
	public System.Action<ChallengeButton> pressedAction;
	
	private int _achievedStars;
	public int achievedStars {
		get { return _achievedStars; }
		set {
			_achievedStars = value;
			UpdateUI();
		}
	}
	
	public void UpdateUI() {
		raycastText.raycastTarget = _achievedStars >= 0;
		unlockedPanel.SetActive(_achievedStars >= 0);
		lockedPanel.SetActive(_achievedStars < 0);
		for (int i = 0; i < achievedStarIcons.Length; i++) {
			achievedStarIcons[i].SetActive(i < _achievedStars);
		}
		for (int i = 0; i < unachievedStarIcons.Length; i++) {
			unachievedStarIcons[i].SetActive(i >= _achievedStars);
		}
	}
	
	public void ChallengeButtonPressed() {
		pressedAction(this);
	}
}
