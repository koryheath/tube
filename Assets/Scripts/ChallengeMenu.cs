﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ChallengeMenu : MonoBehaviour {
	
	public GameController gameController;
	public ChallengeButton challengeButtonPrefab;
	public ChallengeButton challengeButtonAttentionPrefab;
	public RectTransform content;
	public Animator bounceAnimator;
	public int[] gemAwards;
	public Text currentStarsText;
	public Text totalStarsText;
	
	[HideInInspector]
	public float t;
	
	Transform bounceTransform;
	float bounceScale;
	
	List<ChallengeButton> challengeButtons = new List<ChallengeButton>();
	
	public bool attentionBounceEnabled {
		get {
			if (challengeButtons.Count > 0 && challengeButtons[0].attentionBounceAnimator != null) {
				return challengeButtons[0].attentionBounceAnimator.enabled;
			}
			else {
				return false;
			}
		}
		set {
			if (challengeButtons.Count > 0 && challengeButtons[0].attentionBounceAnimator != null) {
				challengeButtons[0].attentionBounceAnimator.enabled = value;
				challengeButtons[0].attentionBounceAnimator.gameObject.transform.localScale = Vector3.one;
			}
		}
	}
	
	int _currentStars;
	public int currentStars {
		get {
			return _currentStars;
		}
	}
	
	public void SetUp() {
		
		GameObject buttonGroup = null;
		
		for (int i = 0; i < Levels.masterLevelList.Length; i++) {
			
			if (i % 8 == 0) {
				buttonGroup = new GameObject();
				buttonGroup.transform.SetParent(content, false);
				GridLayoutGroup gridLayoutGroup = buttonGroup.gameObject.AddComponent<GridLayoutGroup>();
				gridLayoutGroup.cellSize = new Vector2(50, 50);
				gridLayoutGroup.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
				gridLayoutGroup.constraintCount = 4;
				gridLayoutGroup.spacing = new Vector2(5, 5);
				gridLayoutGroup.startAxis = GridLayoutGroup.Axis.Horizontal;
				gridLayoutGroup.startCorner = GridLayoutGroup.Corner.UpperLeft;
			}
			ChallengeButton challengeButton = Instantiate(i == 0 ? challengeButtonAttentionPrefab : challengeButtonPrefab);
			challengeButtons.Add(challengeButton);
			challengeButton.transform.SetParent(buttonGroup.transform, false);
			challengeButton.levelNumberText.text = (i + 1).ToString();
			challengeButton.pressedAction = ChallengeButtonPressed;
		}
		UpdateUI();
	}
	
	public void UpdateSettings() {
		// Lock all zero star levels.
		for (int i = 0; i < Levels.masterLevelList.Length; i++) {
			string levelName = Levels.masterLevelList[i].Method.Name;
			if (Settings.GetStarsForLevel(levelName) == 0) {
				Settings.SetStarsForLevel(levelName, -1);
			}
		}
		// Find the highest level with at least one star.
		int levelIndex = -1;
		for (int i = 0; i < Levels.masterLevelList.Length; i++) {
			if (Settings.GetStarsForLevel(Levels.masterLevelList[i].Method.Name) > 0) {
				levelIndex = i;
			}
		}
		// Unlock all levels below that level.
		int numberOfUnlocks = 0;
		for (int i = 0; i < levelIndex; i++) {
			string levelName = Levels.masterLevelList[i].Method.Name;
			if (Settings.GetStarsForLevel(levelName) == -1) {
				Settings.SetStarsForLevel(levelName, 0);
				numberOfUnlocks++;
			}
		}
		// If there's no unlocked zero star level, unlock the next one
		levelIndex++; 
		if (numberOfUnlocks == 0 && levelIndex < Levels.masterLevelList.Length) {
			Settings.SetStarsForLevel(Levels.masterLevelList[levelIndex].Method.Name, 0);
		}
	}
	
	public void UpdateUI() {
		UpdateSettings();
		for (int i = 0; i < challengeButtons.Count; i++) {
			challengeButtons[i].achievedStars = Settings.GetStarsForLevel(Levels.masterLevelList[i].Method.Name);
		}
		UpdateStarCounts();
	}
	
	public void UpdateStarCounts() {
		_currentStars = 0;
		for (int i = 0; i < challengeButtons.Count; i++) {
			_currentStars += Mathf.Max(0, challengeButtons[i].achievedStars);
		}
		currentStarsText.text = _currentStars.ToString();
		totalStarsText.text = (challengeButtons.Count * 3).ToString();
	}
	
	public void AnimateUI(int challengeIndex) {
		ChallengeButton challengeButton = challengeButtons[challengeIndex];
		int previousNumberOfStars = challengeButton.achievedStars;
		int newNumberOfStars = Settings.GetStarsForLevel(Levels.masterLevelList[challengeIndex].Method.Name);
		if (newNumberOfStars <= previousNumberOfStars) {
			return;
		}
		
		gameController.gameCanvas.invisibleTouchBlocker.SetActive(true);
		float duration = .3f;
		
		for (int i = previousNumberOfStars + 1; i <= newNumberOfStars; i++) {
			int starIndex = i;
			gameController.Delay(duration, () => {
				challengeButton.achievedStars = starIndex;
				UpdateStarCounts();
				bounceTransform = challengeButton.achievedStarIcons[starIndex - 1].transform;
				bounceScale = 1.3f;
				bounceAnimator.Play("ChallengeMenuBounce");
				gameController.gameCanvas.AwardGems(
					gemAwards[starIndex - 1],
					3,
					gameController.gameCanvas.transform.TransformPoint(Vector3.zero),
					gameController.challengeStarAudioSources[starIndex - 1],
					null
				);
			});
			duration += 1.15f;
		}
		
		int levelIndex = -1;
		for (int i = 0; i < challengeButtons.Count; i++) {
			int stars = Settings.GetStarsForLevel(Levels.masterLevelList[i].Method.Name);
			if (stars == -1) {
				levelIndex = i;
				break;
			}
			if (stars == 0) {
				break;
			}
		}
		
		if (levelIndex >= 0) {
			duration += .15f;
			gameController.Delay(duration, () => {
				challengeButtons[levelIndex].achievedStars = 0;
				gameController.buttonUnlockedAudioSource.Play();
				bounceTransform = challengeButtons[levelIndex].transform;
				bounceScale = 1.1f;
				bounceAnimator.Play("ChallengeMenuBounce");
			});
			duration += .5f;
		}
		
		gameController.Delay(duration, () => {
			gameController.gameCanvas.invisibleTouchBlocker.SetActive(false);
		});
	}
	
	public void Update() {
		if (bounceTransform != null) {
			float scale = Mathf.LerpUnclamped(1, bounceScale, t);
			bounceTransform.localScale = new Vector3(scale, scale, 1);
		}
	}
	
	public void ChallengeButtonPressed(ChallengeButton challengeButton) {
		gameController.clickAudioSource.Play();
		gameController.SetChallengeIndex(challengeButtons.IndexOf(challengeButton));
	}
}
