using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPSDisplay : MonoBehaviour 
{
	
	// Attach this to a GUIText to make a frames/second indicator.
	//
	// It calculates frames/second over each updateInterval,
	// so the display does not keep changing wildly.
	//
	// It is also fairly accurate at very low FPS counts (<10).
	// We do this not by simply counting frames per interval, but
	// by accumulating FPS for each frame. This way we end up with
	// correct overall FPS even if the interval renders something like
	// 5.5 frames.
	
	public  float updateInterval = 0.5F;
	
	private float accum   = 0; // FPS accumulated over the interval
	private int   frames  = 0; // Frames drawn over the interval
	private float timeleft; // Left time for current interval


	int frameCounter = 0;
	int countsBelow30 = 0;
	float minFPS;
	float maxFPS;
	float avgFPS;

	public Text fpsLabel;

	void Start()
	{
		if(fpsLabel == null)
		{
			fpsLabel = gameObject.GetComponent<Text>();
		}

		if(fpsLabel == null)
		{
			Debug.Log("UtilityFramesPerSecond needs a GUIText component!");
			enabled = false;
			return;
		}

		timeleft = updateInterval; 
	}
	
	void Update()
	{
		timeleft -= Time.deltaTime;
		accum += Time.timeScale/Time.deltaTime;
		++frames;

		// Interval ended - update GUI text and start new interval
		if( timeleft <= 0.0 )
		{
			frameCounter ++;
			// display two fractional digits (f2 format)
			float fps = accum/frames;
			if (fps<30)
			{
				countsBelow30++;	
			}
			if (frameCounter>1) 
			{
				if (fps>maxFPS) 
				{
					maxFPS = fps;				
				}
				else if (fps<minFPS)  
				{
					minFPS = fps;	
				}
				avgFPS = (avgFPS+ fps)/2;
			}
			else
			{
				maxFPS = fps;
				avgFPS = fps;
				minFPS = fps;
			}

//			string format = System.String.Format("FPS: {0:F2}\n MinFPS: {1:F2}\n AvgFPS: {2:F2}\n MaxFPS: {3:F2}\n CountsBelow 30: {4}",
//				fps, minFPS, avgFPS, maxFPS, countsBelow30);
			string format = System.String.Format("FPS: {0:F2}",fps);
//			fpsLabel.text = SystemInfo.graphicsDeviceVersion + "\n" + "Refresh Rate: " + Screen.currentResolution.refreshRate + " Hz\n";
			fpsLabel.text = format;
			
			if(fps < 30)
				fpsLabel.color = Color.yellow;
			else 
				if(fps < 20)
					fpsLabel.color = Color.red;
			else
				fpsLabel.color = Color.green;
			
			timeleft = updateInterval;
			accum = 0.0F;
			frames = 0;
		}

	}
}