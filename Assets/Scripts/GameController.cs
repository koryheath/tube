﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SocialPlatforms;
using TMPro;
#if !UNITY_EDITOR
#if UNITY_IOS
using UnityEngine.SocialPlatforms.GameCenter;
#endif
using Heyzap;
#endif

public enum GameState {
	preGame,
	gemVideo,
	duringGame,
	paused,
	gameOverDelay,
	offeringRevive,
	postGame
}
public enum GamePlayState{
    Idle,
    Playing,
    NotPlaying
}

public class GameController : MonoBehaviour, IInitializePotentialDragHandler {
	
	public int gemsForVideo;
	public int gemsForSharing;
	public int gemsForFreeGift1;
	public int gemsForFreeGift2;
	public int gemsForFreeGift3;
	public int gemsForFreeGift4;
	
	public bool debugDisplay;
	public bool heyzapButtonDisplay;
	public bool allowSecretMenu;
	public bool ballColliderEnabled;
	
	public int iphoneSmoothingFrames;
	public int androidSmoothingFrames;
	
	public float resetTime;
	
	public int numberOfDeathBallRings;
	public int numberOfDeathBallsPerRing;
	public bool deathBallsDoubleRings;
	public float deathGravityStrength;
	public float deathExplosionStrengthMin;
	public float deathExplosionStrengthMax;
	public Vector3 deathExplosionCenterMin;
	public Vector3 deathExplosionCenterMax;
	public float deathSwipeEffectMax;
	public float hapticDelay;
	
	public float swipeSensitivityMin;
	public float swipeSensitivityMax;
	public float swipeSensitivityExpertIncrease;
	
	public float challengeBallSpeed1;
	public float challengeBallSpeed2;
	public float challengeBallSpeed3;
	
	public float cameraRampdownTime;
	public float animationRampdownTime;
	public float dustMotesSpeedFactor;
	public int dustMotesIncreaseCount;
	
	public float earlyAbortTime;
	
	public float expertLevelOverride;

	public float beginnerBallSpeed0Duration;
	public float beginnerBallSpeed0Value;
	public float beginnerBallSpeed1Duration;
	public float beginnerBallSpeed1Value;
	public float beginnerBallSpeed2Duration;
	public float beginnerBallSpeed2Value;
	public float beginnerBallSpeed3Duration;
	public float beginnerBallSpeed3Value;
	public float beginnerLateGameAcceleration;
	public float beginnerDistancePerPoint;
	public int beginnerStartingWindowSize;
	public int beginnerEndingWindowSize;
	public int beginnerEndingWindowLevel;
	
	public float expertBallSpeed0Duration;
	public float expertBallSpeed0Value;
	public float expertBallSpeed1Duration;
	public float expertBallSpeed1Value;
	public float expertBallSpeed2Duration;
	public float expertBallSpeed2Value;
	public float expertBallSpeed3Duration;
	public float expertBallSpeed3Value;
	public float expertLateGameAcceleration;
	public float expertDistancePerPoint;
	public int expertStartingWindowSize;
	public int expertEndingWindowSize;
	public int expertEndingWindowLevel;
	
	public float arcadeInitialLevelSpacingZ;
	public float arcadeLevelSpacingZ;
	
	public float challengeInitialLevelSpacingZ;
	public float challengeLevelSpacingZ;
	
	public float collectableChance;
	public float collectablePosition;
	public int collectableValue;
	
	public float shadowLength;
	public float shadowOpacity;
	public float shadowDropoff;
	
	public float ballColliderSize;
	public float ballDistance;
	public float revivePadding;
	public float trailHeadZ;
	
	public int reviveScoreMin;
	public float reviveScorePercentage;
	
	public float interstitialTime;
	public float rewardedVideoTime;
	
	public float fogNearZ;
	public float fogFarZ;
	public Color fogColor;
	
	public float tubeBendInitial;
	public float tubeBendMax;
	public float tubeBendAcceleration;
	public float tubeBendVelocityMax;
	public float tubeBendStart;
	
	public int levelsBetweenPaletteChange;
	public float paletteChangeDistance;
	public List<Palette> palettes = new List<Palette>();
	public List<Palette> challengePalettes = new List<Palette>();
	
	public Material tubeSegmentMaterial;
	public Transform tubeSegmentPrefab;
	
	public TubeObject tubeObjectPrefab;
	public Mesh[] tubeObjectMeshes;
	
	public Material shadingDirectionalMaterial;
	public Material shadingRadialMaterial;
	public Material gemMaterial;
	
	public Material shadowDirectionalMaterial;
	public Material shadowRadialMaterial;
	
	public Transform cameraTransform;
	public GameCanvas gameCanvas;
	public IAPManager IAPManager;
	public ShareController shareController;
	public GameObject tubeCap;
	public Ball ball;
	public GameObject ballShadow;
	public BallTrail ballTrail;
	public GameObject[] tubeLines;
	public Transform deathCollidersContainer;
	public ParticleSystem dustMotesPrefab;
	public GameObject gemExplosionPrefab;
	public DeathBall deathBallPrefab;
	public GameObject deathColliderPrefab;
	public GameObject giftBoxAnimationPrefab;
	public GameObject heyzapButton;
	public Animator challengeAttentionBounceAnimator;
	
	public PaletteAnimator paletteAnimator;
	
	public AudioSource gameMusicAudioSource;
	public AudioSource menuMusicAudioSource;
	public AudioSource gemCollectedAudioSource;
	public AudioSource deathAudioSource;
	public AudioSource clickAudioSource;
	public AudioSource buttonLockedAudioSource;
	public AudioSource buttonUnlockedAudioSource;
	public AudioSource highScoreAudioSource;
	public AudioSource flyingGemLandAudioSource;
	public AudioSource flyingGemPopAudioSource;
	public AudioSource giftBoxAudioSource;
	public AudioSource[] challengeStarAudioSources;
	
	public Animator gameMusicAnimator;
	public Animator menuMusicAnimator;
	
	public string leaderboardIDiOS;
	
	List<System.Action> tutorialLevels = new List<System.Action>();
	List<System.Action> avoidLevels = new List<System.Action>();
	
	List<float> triggerDistances = new List<float>();
	List<System.Action> triggerFunctions = new List<System.Action>();
	
	System.Action currentLevelFunction;
	int currentLevelIndex;
	int currentLevelNumber;
	int currentPaletteLevelNumber;
	
	GameStyle gameStyle;
	
	float postGameTime;
	float gameOverDelayRemaining;
	float tubeSegmentLength = 100;
	List<Transform> tubeSegments = new List<Transform>();
	ParticleSystem dustMotes;
	GameState gameState;
	bool isDragging;
	float initialPointerX;
	float initialSpinAngle;
	float dragTime;
	float spinAngle;
	float[] spinAngles;
	int spinAngleIndex;
	float averageSpinAngle;
	float previousSpinAngle;
	float spinVelocity;
	float sideDistance;
	float gameTime;
	float gameDistance;
	float cameraSpeed;
	float ballSpeed;
	float deathSpeed;
	float swipeMomentum;
	int challengeIndex;
	int createdLevelCount;
	float screenWidth;
	float screenAspect;
	float swipeSensitivity;
	Quaternion ballInitialRotation;
	GameObject gemExplosion;
	List<DeathBall> deathBalls = new List<DeathBall>();
	List<GameObject> deathColliders = new List<GameObject>();
	
	int[] levelPassedCounts;
	float expertLevel;
	
	float[] ballSpeedDuration;
	float[] ballSpeedValue;
	int ballSpeedIndex;
	float ballSpeedTime;
	float lateGameAcceleration;
	float distancePerPoint;
	float startingWindowSize;
	float endingWindowSize;
	float endingWindowLevel;
	
	Vector2 tubeBend;
	Vector2 tubeBendVector;
	float tubeBendMagnitude;
	float tubeBendVelocity;
	float tubeBendDisplacement;
	
	int secretMenuCount;
	
	// This keeps interstitials from showing until the player passes up the rewarded button
	bool blockInterstitials;
	// This blocks the revive popup once after the player rejects it.
	bool blockRevive;
	float interstitialTimer;
	float rewardedVideoTimer;
	int freeGiftCount;
	int rewardedVideoCount;
	int ketchappSquareCount;
	int rateThisAppCount;
	int IAPEnticeCount;
	bool willShowRateThisApp;
	bool hasShownRateThisApp;
	bool isShareButtonClicked;
	bool requestPermission;
	bool shouldTriggerKetchappPromoDone;
	bool shouldTriggerKetchappGDPR;
	bool ketchappPromoDoneRun;

	public Text text_Status;
	int testSegmentCount;
	public int testSegmentNum = 0;

    [System.Serializable]
    public class DragThreshold{
        public GamePlayState gamePlayState;
        public int threshold;
    }
    public List<DragThreshold> dragThresholds = new List<DragThreshold>();
    public GamePlayState currentGamePlayState;
    public Text dragThresholdText;



	#if !UNITY_EDITOR
	private bool _bannerAdsEnabled;
	bool bannerAdsEnabled {
		get {
			return _bannerAdsEnabled;
		}
		set {
			if (_bannerAdsEnabled != value) {
				_bannerAdsEnabled = value;
				if (_bannerAdsEnabled) {
					HZBannerShowOptions showOptions = new HZBannerShowOptions();
					showOptions.Position = HZBannerShowOptions.POSITION_BOTTOM;
					HZBannerAd.ShowWithOptions(showOptions);
				}
				else {
					HZBannerAd.Destroy();
				}
			}
		}
	}
	#endif
	
	void OnDestroy() {
		TubeObject.CleanUp();
		Destroy(Palette.tubeSegmentMaterial);
		Destroy(TubeObject.shadingMaterials[0]);
		Destroy(TubeObject.shadingMaterials[1]);
		#if !UNITY_EDITOR
		bannerAdsEnabled = false;
		#endif
	}
	
	void Awake () {
		
		Debug.Log("Tricky Tube Version " + Application.version);
		
		//PlayerPrefs.DeleteAll();

		testSegmentCount = 1;
        dragThresholdText.text = "DragThreshold: " + EventSystem.current.pixelDragThreshold;
		Application.targetFrameRate = 60;
		Input.multiTouchEnabled = false;
		swipeMomentum = Settings.swipeMomentum / 100f;
		screenWidth = Screen.width;
		screenAspect = (Screen.width / (float)Screen.height) > .70f ? 4.5f : 3f;
		
		heyzapButton.SetActive(heyzapButtonDisplay);
		
		/*
		GameObject meshObject = new GameObject("Mesh");
		MeshFilter meshFilter = meshObject.AddComponent<MeshFilter>();
		meshFilter.mesh = MeshManager.CreateSphereMesh(2);
		meshObject.AddComponent<MeshRenderer>();
		*/
		
		Palette.tubeSegmentMaterial = Instantiate(tubeSegmentMaterial) as Material;
		
		TubeObject.tubeObjectPrefab = tubeObjectPrefab;
		TubeObject.meshes = tubeObjectMeshes;
		
		TubeObject.shadingMaterials = new Material[] {
			Instantiate(shadingDirectionalMaterial) as Material,
			Instantiate(shadingRadialMaterial) as Material,
			gemMaterial
		};
		
		TubeObject.shadowMaterials = new Material[] {
			shadowDirectionalMaterial,
			shadowRadialMaterial
		};
		
		TubeObject.ballRadius = ballColliderSize * .5f;
		TubeObject.ballRadiusSquared = TubeObject.ballRadius * TubeObject.ballRadius;
		
		tubeCap.GetComponent<MeshRenderer>().material.color = fogColor;
		
		gameStyle = Settings.gameStyle;
		
		/*
		// If version number is different than last time the app was run, reset progressions.
		if (PlayerPrefs.GetString("MostRecentVersion", "") != Application.version.ToString()) {
			Debug.Log("Resetting progression because of new version.");
			PlayerPrefs.DeleteKey("SwipeSensitivity");
			Settings.numberOfTutorialLevelsPassed = 0;
			for (int i = 0; i < Levels.masterLevelList.Length; i++) {
				Settings.SetStarsForLevel(Levels.masterLevelList[i].Method.Name, -1);
			}
		}
		*/
		
		// Save the current version number.
		PlayerPrefs.SetString("MostRecentVersion", Application.version.ToString());
		
		Settings.SetBallPurchased(gameCanvas.ballMenu.ballConfigs[0].gameObject.name.ToLower(), true);
		
		challengeIndex = -1;
		gameCanvas.challengeMenu.SetUp();
		
		tubeCap.SetActive(true);
		
		#if UNITY_ANDROID
		spinAngles = new float[androidSmoothingFrames];
		#else
		spinAngles = new float[iphoneSmoothingFrames];
		#endif
		
		gameCanvas.bestScore = Settings.bestScore;
		gameCanvas.gems = Settings.gems;
		
		levelPassedCounts = Settings.GetLevelInts("LevelPassedCounts", 0);
		
		TubeObject.SetUp();
		
		UpdateSoundSettings();
		
		Shader.SetGlobalFloat("_ShadowLength", shadowLength);
		Shader.SetGlobalFloat("_ShadowOpacity", shadowOpacity);
		Shader.SetGlobalFloat("_ShadowDropoff", shadowDropoff);
		Shader.SetGlobalFloat("_ShadowTubeRadius", 19.5f);
		
		Shader.SetGlobalFloat("_TubeBendStart", tubeBendStart);
		Shader.SetGlobalFloat("_TubeBendUnit", 60);
		Shader.SetGlobalVector("_QOffset", Vector4.zero);
		
		Shader.SetGlobalFloat("_CustomFogNearZ", fogNearZ);
		Shader.SetGlobalFloat("_CustomFogFarZ", fogFarZ);
		Shader.SetGlobalColor("_CustomFogColor", fogColor);
		
		tubeBendAcceleration *= tubeBendMax;
		tubeBendVelocityMax *= tubeBendMax;
		
		gameCanvas.revivePanel.reviveAction = Revive;
		gameCanvas.revivePanel.noReviveAction = NoRevive;
		
		cameraTransform.GetComponent<Camera>().farClipPlane = fogFarZ + 1;
		// Create tube segments
		int numberOfTubeSegments = Mathf.CeilToInt(fogFarZ / tubeSegmentLength) + 1;
		// Make sure there's an even number of tube segments
		numberOfTubeSegments += (numberOfTubeSegments % 2);
		for (int i = 0; i < numberOfTubeSegments; i++) {
			Transform tubeSegment = Instantiate(tubeSegmentPrefab) as Transform;
			tubeSegment.localScale = new Vector3(Levels.tubeRadius * 2, Levels.tubeRadius * 2, tubeSegmentLength);
			MeshRenderer meshRenderer = tubeSegment.GetComponent<MeshRenderer>();
			Material[] materials = meshRenderer.sharedMaterials;
			materials[1] = Palette.tubeSegmentMaterial;
			meshRenderer.sharedMaterials = materials;
			tubeSegments.Add(tubeSegment);
		}
		
		gameState = GameState.preGame;
		
		ChooseRandomPalette();
		
		gemExplosion = Instantiate(gemExplosionPrefab);
		
		// Set up DeathBalls
		
		int numberOfDeathBalls = (((numberOfDeathBallRings - 2) * numberOfDeathBallsPerRing) + 2) * (deathBallsDoubleRings ? 2 : 1);
		for (int i = 0; i < numberOfDeathBalls; i++) {
			DeathBall deathBall = Instantiate(deathBallPrefab) as DeathBall;
			deathBalls.Add(deathBall);
		}
		int numberOfDeathColliders = 40;
		for (int i = 0; i < numberOfDeathColliders; i++) {
			deathColliders.Add(Instantiate(deathColliderPrefab));
			deathColliders[i].transform.parent = deathCollidersContainer;
			float degrees1 = 360 * (i / (float)numberOfDeathColliders);
			float degrees2 = 360 * ((i + 1) / (float)numberOfDeathColliders);
			Vector3 vector1 = Quaternion.Euler(0, 0, degrees1) * Vector3.up * Levels.tubeRadius;
			Vector3 vector2 = Quaternion.Euler(0, 0, degrees2) * Vector3.up * Levels.tubeRadius;
			Vector3 vector = Vector3.Lerp(vector1, vector2, .5f);
			deathColliders[i].transform.position =
				vector +
				(vector.normalized * deathColliders[i].transform.localScale.y * .5f) +
				(Vector3.forward * deathColliders[i].transform.localScale.z * .5f);
			deathColliders[i].transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(degrees1, degrees2, .5f));
		}
		
		SetUpBall();
	}
	
	void Start() {
		
		SetUpGame();
		gameCanvas.SetUpGame();
		
		gameCanvas.screenshotMaterial.mainTexture = shareController.texture2d;
		gameCanvas.screenshotMaterial.mainTextureScale = new Vector2(
			1,
			(shareController.texture2d.width / (float)shareController.texture2d.height) *
			(gameCanvas.screenshotTransform.sizeDelta.y / gameCanvas.screenshotTransform.sizeDelta.x)
		);
		gameCanvas.screenshotMaterial.mainTextureOffset = new Vector2(
			0,
			(1 - gameCanvas.screenshotMaterial.mainTextureScale.y) / 2
		);

		shouldTriggerKetchappPromoDone = true;
		isShareButtonClicked = false;
		Invoke("KetchappPromoTimeout", 20);
		gameCanvas.ShowActivitySpinnerWithTimeout(20);
		
		#if !UNITY_EDITOR
		if (!Settings.gdprShown && KetchappPromo.IsKetchappCountryGdpr()) {
			#if UNITY_IOS
			if (KetchappPromo.RequestKetchappPromo()) {
				shouldTriggerKetchappGDPR = true;
			}
			else {
				KetchappPromoStarted("");
				KetchappPromoDone("");
			}
			#elif UNITY_ANDROID
			shouldTriggerKetchappGDPR = true;
			KetchappPromo.OpenKetchappGdprWindow();
			#else
			KetchappPromoStarted("");
			KetchappPromoDone("");
			#endif
		}
		else if (Settings.adsEnabled && Application.internetReachability != NetworkReachability.NotReachable) {
			if (!KetchappPromo.RequestKetchappPromo()) {
				KetchappPromoStarted("");
				KetchappPromoDone("");
			}
		}
		else {
			KetchappPromoStarted("");
			KetchappPromoDone("");
		}
		#else
		KetchappPromoStarted("");
		KetchappPromoDone("");
		#endif
		
		#if UNITY_EDITOR
		Levels.DisplayWarnings();
		#endif

		// The first time this is instantiated, there's a minor performance hitch, so we do it now.
		TextMeshProUGUI flyingGemAmount = Instantiate(gameCanvas.flyingGemAmountPrefab);
		flyingGemAmount.transform.SetParent(gameCanvas.transform, false);
		flyingGemAmount.transform.position = Vector3.left * 5000;
		flyingGemAmount.text = "TEST";
	}
	
	void OnApplicationPause(bool pauseStatus) {
		if (pauseStatus == true) {
			if (gameState == GameState.duringGame || gameState == GameState.paused) {
				if (challengeIndex < 0 && gameTime < earlyAbortTime) {
					AbortGame();
				}
				else {
					gameState = GameState.paused;
					gameCanvas.pausePanel.OpenPanel();
				}
			}
		}
		else {
			
			if (gameState == GameState.paused) {
				gameCanvas.pausePanel.animator.Play("PausePanelFadeIn");
			}
			
			#if UNITY_ANDROID
			
			if (shouldTriggerKetchappGDPR) {
				isGdpr("");
			}

			if (shouldTriggerKetchappPromoDone) {
				KetchappPromoStarted("");
				KetchappPromoDone("");
			}
			
			if (isShareButtonClicked) {
				isShareButtonClicked = false;
				ShareDone("sent");
			}
			
			print ("Plugin: OnApplicationPause called" + pauseStatus);

			if (requestPermission) {
				print ("Plugin: OnApplicationPause called requestPermission" + "true");
				requestPermission = false;
				onPermissionResult(ThirdPartyManager.checkStoragePermission().ToString());
			}
			#endif
			
			#if !UNITY_EDITOR && UNITY_IOS
			Tenjin.getInstance(IAPManager.tenjinID).Connect();
			#endif
		}
	}
	
	public void UnpauseGame() {
		gameState = GameState.duringGame;
	}
	
	public void KetchappPromoStarted(string message) {
		CancelInvoke("KetchappPromoTimeout");
	}
	
	public void KetchappPromoDone(string message) {
		
		shouldTriggerKetchappPromoDone = false;
		
		// Make sure this function doesn't run twice.
		if (ketchappPromoDoneRun) {
			return;
		}
		
		ketchappPromoDoneRun = true;
		
		menuMusicAnimator.Play("PlayAndFadeIn");
		gameCanvas.HideActivitySpinner();

		#if !UNITY_EDITOR
		Debug.Log("----------------------------------- KetchappPromoDone GDPR " + CheckGDPR());
		HeyzapAds.SetGdprConsent(CheckGDPR());
			// HeyzapAds.Start("4928152512ae0686f8d054027e35165c", HeyzapAds.FLAG_NO_OPTIONS); // com.sizzlecorp.tube
			HeyzapAds.Start("ff4d63e68db9518cacc9a4b5d8e375d8", HeyzapAds.FLAG_NO_OPTIONS); // com.ketchapp.trickytube
			if (Settings.adsEnabled) {
				bannerAdsEnabled = true;
			}
			HZIncentivizedAd.Fetch();
			#if UNITY_IOS
				BaseTenjin instance = Tenjin.getInstance(IAPManager.tenjinID);
				if (CheckGDPR()) {
					instance.OptIn();
				}
				else {
					instance.OptOut();
				}
				instance.Connect();
			#endif
		#endif
		Delay(2, () => {
			shareController.TakeScreenshot();
			gameCanvas.shareButton.interactable = true;
		});
	}
	
	public void KetchappPromoTimeout() {
		KetchappPromoDone("");
	}
	
	public bool CheckGDPR() {
		return Settings.gdprShown ? KetchappPromo.IsKetchappGdprOptin() : true;
	}
	
	public void OpenKetchappGdprWindow() {
		#if !UNITY_EDITOR
		shouldTriggerKetchappGDPR = true;
		gameCanvas.ShowActivitySpinnerWithTimeout(20);
		KetchappPromo.OpenKetchappGdprWindow();
		#endif
	}
	
	public void isGdpr(string message) {
		if (shouldTriggerKetchappGDPR) {
			shouldTriggerKetchappGDPR = false;
			gameCanvas.HideActivitySpinner();
			Settings.gdprShown = true;
			#if !UNITY_EDITOR
			Debug.Log("----------------------------------- isGdpr " + CheckGDPR());
			HeyzapAds.SetGdprConsent(CheckGDPR());
				#if UNITY_IOS
				BaseTenjin instance = Tenjin.getInstance(IAPManager.tenjinID);
				if (CheckGDPR()) {
					instance.OptIn();
				}
				else {
					instance.OptOut();
				}
				#endif
			#endif
		}
	}
	
	public void HeyzapButtonPressed() {
		#if !UNITY_EDITOR
		HeyzapAds.ShowMediationTestSuite();
		#endif
	}
	public void KEtchappButtonPressed() {
		KetchappPromo.RequestKetchappPromo();
	}
	public void SettingsButtonPressed() {
		gameCanvas.OpenSettingsMenu(.6f);
	}
	
	public void BallButtonPressed() {
		gameCanvas.OpenBallMenu(.6f);
	}
	
	public void ChallengeButtonPressed() {
		gameCanvas.challengeMenu.UpdateUI();
		gameCanvas.OpenChallengeMenu(.6f);
	}
	
	public void LeaderboardButtonPressed() {
		#if !UNITY_EDITOR


			#if UNITY_ANDROID
				
		GPGS_LeaderboardManager.singleton.ShowLeaderboard(GPGSIds.leaderboard_score);

			#elif UNITY_IOS
		NativeUtilities.OpenLeaderboard(leaderboardIDiOS);
			gameCanvas.activitySpinnerAnimator.visible = true;
			#endif
		#endif
	}
	
	public void LeaderboardDone(string message) {
		gameCanvas.HideActivitySpinner();
	}

	public void FacebookButtonPressed() {

		#if UNITY_ANDROID
			if (ThirdPartyManager.isFacebookInstalled ()) {
				Application.OpenURL ("fb://page/503287153144438");
			} else {
				Application.OpenURL ("https://www.facebook.com/ketchappgames?fref=ts");
			}
			return;
		#endif
		NativeUtilities.OpenURLWithFallback("fb://profile/503287153144438", "https://www.facebook.com/ketchappgames?fref=ts");
	}
	
	public void InstagramButtonPressed() {


		#if UNITY_ANDROID
			if (ThirdPartyManager.isInstagramInstalled ()) {
				Application.OpenURL ("instagram://user?username=ketchapp");
			} else {
				Application.OpenURL ("https://www.instagram.com/ketchapp/");
			}
			return;
		#endif

		NativeUtilities.OpenURLWithFallback("instagram://user?username=ketchapp", "https://www.instagram.com/ketchapp/");
	}
	
	public void RatingButtonPressed() {
		#if UNITY_IOS && !UNITY_EDITOR
		NativeUtilities.OpenURLWithFallback(
			"itms-apps://itunes.apple.com/us/app/tricky-tube/id1336004724?mt=8&action=write-review",
			"https://itunes.apple.com/us/app/tricky-tube/id1336004724?mt=8&action=write-review"
		);
		#endif
		#if UNITY_ANDROID
		Application.OpenURL("market://details?id=" + Application.identifier);
		#endif
	}
	
	public void AppStoreDone(string message) {
		gameCanvas.HideActivitySpinner();
	}
	
	public void NoAdsButtonPressed() {
		#if !UNITY_EDITOR
		IAPManager.PurchaseNoAds();
		#endif
	}
	
	public void GemDoublerButtonPressed() {
		#if !UNITY_EDITOR
		IAPManager.PurchaseGemDoubler();
		#endif
	}
	
	public void RestorePurchasesButtonPressed() {
		#if !UNITY_EDITOR
		IAPManager.RestorePurchases();
		#endif
	}
	
	public void NoAdsPurchased() {
		Settings.adsEnabled = false;
		gameCanvas.DismissNoAdsButton();
		#if !UNITY_EDITOR
		bannerAdsEnabled = false;
		#endif
	}
	
	public void GemDoublerPurchased() {
		Settings.gemMultiplier = 2;
		Settings.adsEnabled = false;
		gameCanvas.DismissGemDoublerButton();
		#if !UNITY_EDITOR
		bannerAdsEnabled = false;
		#endif
	}
	
	public void GemVideoButtonPressed() {


		Debug.LogError ("UNITY: GemVideoButtonPressed called");

		gameCanvas.DismissGemVideoButton();
		Vector3 gemAwardPosition = gameCanvas.ballMenu.gameObject.activeSelf ?
			gameCanvas.ballMenu.gemVideoButton.transform.position : gameCanvas.transform.TransformPoint(Vector3.zero);

		#if !UNITY_EDITOR
		gameCanvas.ShowActivitySpinnerWithTimeout(5);
		gameState = GameState.gemVideo;
		menuMusicAnimator.Play("FadeOutAndPause");
		Debug.LogError ("UNITY: GemVideoButtonPressed called Displaying");
		Delay(.5f, () => {
			HZIncentivizedAd.SetDisplayListener(delegate(string adState, string adTag) {
				if (adState.Equals("failed") ||
					adState.Equals("fetch_failed") ||
					adState.Equals("incentivized_result_incomplete")) {


		text_Status.text = "Rewared Video Call " + adState;
		gameCanvas.HideActivitySpinner();

		Debug.LogError ("UNITY: GemVideoButtonPressed called " + adState);
					HZIncentivizedAd.SetDisplayListener(null);
					GemVideoDone(false, gemAwardPosition);
					}
				else if (adState.Equals("incentivized_result_complete")) {


		text_Status.text = "Rewared Video Call completed";

		gameCanvas.HideActivitySpinner();
		Debug.LogError ("UNITY: GemVideoButtonPressed called " + adState);
					HZIncentivizedAd.SetDisplayListener(null);
					GemVideoDone(true, gemAwardPosition);
				}
			});
			HZIncentivizedAd.Show();
			text_Status.text = "Rewared Video Call Sent";
		});
		#else
		interstitialTimer = 0;
		blockInterstitials = true;
		Delay(.2f, () => {
			gameCanvas.AwardGems(gemsForVideo, 1.5f, gemAwardPosition, flyingGemPopAudioSource, flyingGemLandAudioSource);
		});
		#endif
	}
	
	public void GemVideoDone(bool complete, Vector3 gemAwardPosition) {
		if (gameState != GameState.gemVideo) {


			gameCanvas.HideActivitySpinner();
			Debug.LogError ("UNITY: GemVideoDone called not gemvideo state");
			return;

		}

		Debug.LogError ("UNITY: GemVideoDone called 1111");
		gameState = GameState.preGame;
		menuMusicAnimator.Play("UnPauseAndFadeIn");
		gameCanvas.HideActivitySpinner();
		rewardedVideoTimer = 0;
		if (complete) {
			interstitialTimer = 0;
			blockInterstitials = true;
			Delay(.6f, () => {
				gameCanvas.AwardGems(gemsForVideo, 1.5f, gemAwardPosition, flyingGemPopAudioSource, flyingGemLandAudioSource);
			});
		}
	}
	
	public void FreeGiftButtonPressed() {
		Settings.freeGiftAwardedCount++;
		gameCanvas.DismissFreeGiftButton();
		gameCanvas.invisibleTouchBlocker.SetActive(true);
		GameObject giftBoxAnimation = Instantiate(giftBoxAnimationPrefab, gameCanvas.transform);
		giftBoxAnimation.transform.localPosition = new Vector3(0, -70, 0);
		Delay(.3f, () => {
			gameCanvas.gameCanvas.renderMode = RenderMode.ScreenSpaceCamera;
			giftBoxAnimation.SetActive(true);
		});
		Delay(.6f, () => {
			giftBoxAudioSource.Play();
		});
		Delay(2.6f, () => {
			Vector3 position = gameCanvas.transform.TransformPoint(Vector3.up * 50);
			if (Settings.freeGiftAwardedCount == 1) {
				gameCanvas.AwardGems(gemsForFreeGift1, 1.5f, position, flyingGemPopAudioSource, flyingGemLandAudioSource);
			}
			else if (Settings.freeGiftAwardedCount == 2) {
				gameCanvas.AwardGems(gemsForFreeGift2, 1.5f, position, flyingGemPopAudioSource, flyingGemLandAudioSource);
			}
			else if (Settings.freeGiftAwardedCount == 3) {
				gameCanvas.AwardGems(gemsForFreeGift3, 1.5f, position, flyingGemPopAudioSource, flyingGemLandAudioSource);
			}
			else {
				gameCanvas.AwardGems(gemsForFreeGift4, 1.5f, position, flyingGemPopAudioSource, flyingGemLandAudioSource);
			}
		});
		Delay(3.5f, () => {
			Destroy(giftBoxAnimation);
			gameCanvas.gameCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
			gameCanvas.invisibleTouchBlocker.SetActive(false);
		});
	}
	
	public void ShareButtonPressed(bool clickedFromHomeScreen) {

		print ("Plugin: ShareButtonPressed called clickedFromHomeScreen" + clickedFromHomeScreen);

		#if !UNITY_EDITOR
//		gameCanvas.activitySpinnerAnimator.visible = true;

		#if UNITY_ANDROID 

		if (!ThirdPartyManager.checkStoragePermission ()) {

				print ("Plugin: ShareButtonPressed No permission");
			
			ThirdPartyManager.askForPermission (gameObject.name,"onPermissionResult");
			requestPermission = true;
			return;
		}
		#endif

		shareController.ShareScreenshot();
		#endif
		if(!clickedFromHomeScreen)
			isShareButtonClicked = true;
	}

	public void onPermissionResult(string str)
	{

		print ("Plugin: onPermissionResult called" + str);
		if (str.ToLower() == "true") {
			print ("Plugin: onPermissionResult called11111111" + str);
			shareController.ShareScreenshot();
			isShareButtonClicked = true;
		}
	}


	// Called by native iOS code
	public void ShareDone(string message) {
		gameCanvas.HideActivitySpinner();
		if (message == "sent" && gameCanvas.highScoreButtonAnimator.isActiveAndEnabled) {
			gameCanvas.DismissHighScoreButton();
			Delay(.2f, () => {
				gameCanvas.AwardGems(
					gemsForSharing,
					1.5f,
					gameCanvas.highScoreButtonAnimator.transform.position,
					flyingGemPopAudioSource,
					flyingGemLandAudioSource
				);
			});
		}
	}
	
	public void VolumeButtonPressed() {
		Settings.soundEnabled = !Settings.soundEnabled;
		gameCanvas.UpdateSoundSettings();
		UpdateSoundSettings();
		if (Settings.soundEnabled) {
			clickAudioSource.Play();
		}
	}
	
	public void MusicButtonPressed() {
		Settings.musicEnabled = !Settings.musicEnabled;
		UpdateSoundSettings();
		gameCanvas.UpdateMusicSettings();
	}
	
	public void UpdateSoundSettings() {
		gameMusicAudioSource.mute = !Settings.musicEnabled;
		menuMusicAudioSource.mute = !Settings.musicEnabled;
		gemCollectedAudioSource.mute = !Settings.soundEnabled;
		deathAudioSource.mute = !Settings.soundEnabled;
		clickAudioSource.mute = !Settings.soundEnabled;
		buttonLockedAudioSource.mute = !Settings.soundEnabled;
		buttonUnlockedAudioSource.mute = !Settings.soundEnabled;
		flyingGemPopAudioSource.mute = !Settings.soundEnabled;
		flyingGemLandAudioSource.mute = !Settings.soundEnabled;
		giftBoxAudioSource.mute = !Settings.soundEnabled;
		highScoreAudioSource.mute = !Settings.soundEnabled;
		challengeStarAudioSources[0].mute = !Settings.soundEnabled;
		challengeStarAudioSources[1].mute = !Settings.soundEnabled;
		challengeStarAudioSources[2].mute = !Settings.soundEnabled;
	}
	
	public void HapticButtonPressed() {
		Settings.hapticEnabled = !Settings.hapticEnabled;
		gameCanvas.UpdateHapticSettings();
	}
	
	public void InvertedButtonPressed() {
		Settings.controlsInverted = !Settings.controlsInverted;
		gameCanvas.UpdateInvertedSettings();
	}
	
	void SetUpGame() {

		SetUpDifficulty();
		SetUpChallengeAttentionBounce();
		
		RecycleObjects(float.MaxValue);
		ballShadow.SetActive(true);
		ball.gameObject.SetActive(true);
		for (int i = 0; i < deathBalls.Count; i++) {
			deathBalls[i].gameObject.SetActive(false);
		}
		deathCollidersContainer.gameObject.SetActive(false);
		deathBalls.Shuffle();
		if (dustMotes) {
			GameObject.Destroy(dustMotes.gameObject);
		}
		dustMotes = Instantiate(dustMotesPrefab) as ParticleSystem;
		dustMotes.Play();
		for (int i = 0; i < tubeLines.Length; i++) {
			tubeLines[i].SetActive(false);
		}
		if (challengeIndex < 0 && Settings.bestScore >= 5) {
			tubeLines[0].SetActive(true);
			tubeLines[0].transform.position = Vector3.forward * (Settings.bestScore + 1) * distancePerPoint;
		}
		ResetBall();
		UpdateCurrentSpeed();
		Levels.z = 0;
		gameCanvas.score = 0;
		gameCanvas.revivePanel.Reset();
		SetUpTubeSegments();
		triggerDistances.Clear();
		triggerFunctions.Clear();
		paletteAnimator.SetToEndPalette();
		avoidLevels.Clear();
		if (currentLevelFunction != null) {
			avoidLevels.Add(currentLevelFunction);
		}
		currentLevelFunction = null;
		currentLevelIndex = -1;
		tutorialLevels.Clear();
		for (int i = Settings.numberOfTutorialLevelsPassed; i < Levels.tutorialLevels.Length; i++) {
			tutorialLevels.Add(Levels.tutorialLevels[i]);
		}
		currentLevelNumber = 0;
		createdLevelCount = 0;
		CreateLevels();
		tubeBend = Quaternion.Euler(0, 0, Random.Range(0, 360f)) * (Vector2.up * tubeBendInitial);
		RandomizeTubeBendTarget();
		UpdateTubeBend();
		ApplyTubeBend();
		Update();
		gameState = GameState.preGame;
	}
	
	public void SetUpDifficulty() {
		
		int numberOfLevelsPassed = 0;
		for (int i = 0; i < levelPassedCounts.Length; i++) {
			numberOfLevelsPassed += levelPassedCounts[i] > 0 ? 1 : 0;
		}
		
		expertLevel = expertLevelOverride > 0 ? expertLevelOverride : numberOfLevelsPassed / (float)levelPassedCounts.Length;
		
		int ballSpeedLength = 0;
		if (beginnerBallSpeed0Duration > 0) {
			ballSpeedLength++;
			if (beginnerBallSpeed1Duration > 0) {
				ballSpeedLength++;
				if (beginnerBallSpeed2Duration > 0) {
					ballSpeedLength++;
					if (beginnerBallSpeed3Duration > 0) {
						ballSpeedLength++;
					}
				}
			}
		}
		ballSpeedValue = new float[ballSpeedLength];
		ballSpeedDuration = new float[ballSpeedLength];
		if (ballSpeedLength > 0) {
			ballSpeedValue[0] = Mathf.Lerp(beginnerBallSpeed0Value, expertBallSpeed0Value, expertLevel);
			ballSpeedDuration[0] = Mathf.Lerp(beginnerBallSpeed0Duration, expertBallSpeed0Duration, expertLevel);
			if (ballSpeedLength > 1) {
				ballSpeedValue[1] = Mathf.Lerp(beginnerBallSpeed1Value, expertBallSpeed1Value, expertLevel);
				ballSpeedDuration[1] = Mathf.Lerp(beginnerBallSpeed1Duration, expertBallSpeed1Duration, expertLevel);
				if (ballSpeedLength > 2) {
					ballSpeedValue[2] = Mathf.Lerp(beginnerBallSpeed2Value, expertBallSpeed2Value, expertLevel);
					ballSpeedDuration[2] = Mathf.Lerp(beginnerBallSpeed2Duration, expertBallSpeed2Duration, expertLevel);
					if (ballSpeedLength > 3) {
						ballSpeedValue[3] = Mathf.Lerp(beginnerBallSpeed3Value, expertBallSpeed3Value, expertLevel);
						ballSpeedDuration[3] = Mathf.Lerp(beginnerBallSpeed3Duration, expertBallSpeed3Duration, expertLevel);
					}
				}
			}
		}
		
		lateGameAcceleration = Mathf.Lerp(beginnerLateGameAcceleration, expertLateGameAcceleration, expertLevel);
		distancePerPoint = Mathf.Lerp(beginnerDistancePerPoint, expertDistancePerPoint, expertLevel);
		startingWindowSize = Mathf.Lerp(beginnerStartingWindowSize, expertStartingWindowSize, expertLevel);
		endingWindowSize = Mathf.Lerp(beginnerEndingWindowSize, expertEndingWindowSize, expertLevel);
		endingWindowLevel = Mathf.Lerp(beginnerEndingWindowLevel, expertEndingWindowLevel, expertLevel);
	}
	
	public void SetUpTubeSegments() {
		for (int i = 0; i < tubeSegments.Count; i++) {
			tubeSegments[i].position = cameraTransform.position + (Vector3.forward * ((tubeSegmentLength / 2) + (tubeSegmentLength * i)));
			tubeSegments[i].rotation = Quaternion.Euler(0, 0, i % 2 == 0 ? 46 : 136);
		}
	}
	
	public void SetUpBall() {
		BallConfig ballConfig = gameCanvas.ballMenu.GetBallConfig(Settings.currentBall);
		if (ballConfig == null) {
			ballConfig = gameCanvas.ballMenu.ballConfigs[0];
			Settings.currentBall = ballConfig.gameObject.name.ToLower();
		}
		ball.SetTexture(ballConfig.mainTexture);
		ballTrail.SetTextureAndColor(ballConfig.trailTexture, ballConfig.trailColor);
		if (ballConfig.deathBallColors.Length > 0) {
			for (int i = 0; i < deathBalls.Count; i++) {
				Color topColor = ballConfig.deathBallColors[i % ballConfig.deathBallColors.Length];
				Color bottomColor = topColor * .5f;
				bottomColor.a = 1;
				deathBalls[i].SetColors(topColor, bottomColor);
			}
		}
		ballInitialRotation = ballConfig.transform.rotation;
		ball.transform.rotation = ballInitialRotation;
		SetBallPosition();
	}
	
	public void SetUpChallengeAttentionBounce() {
		bool enableBounce = (Settings.numberOfTutorialLevelsPassed >= Levels.tutorialLevels.Length &&
			!Settings.challengeAttentionBounceFinished &&
			gameCanvas.challengeMenu.currentStars == 0
		);
		challengeAttentionBounceAnimator.enabled = enableBounce;
		gameCanvas.challengeMenu.attentionBounceEnabled = enableBounce;
		challengeAttentionBounceAnimator.gameObject.transform.localScale = Vector3.one;
	}
	
	public void ResetBall() {
		ball.transform.position = Vector3.zero + (Vector3.forward * ballDistance);
		ball.transform.rotation = ballInitialRotation;
		spinVelocity = 0;
		spinAngle = 0;
		ballTrail.Reset(spinAngle);
		for (int i = 0; i < spinAngles.Length; i++) {
			spinAngles[i] = spinAngle;
		}
		spinAngleIndex = 0;
		previousSpinAngle = spinAngle;
		averageSpinAngle = spinAngle;
		gameTime = 0;
		ballSpeedIndex = 0;
		ballSpeedTime = 0;
		cameraTransform.position = Vector3.zero;
		cameraTransform.rotation = Quaternion.identity;
		postGameTime = 0;
		gameDistance = 0;
	}
	
	public void ChooseRandomPalette() {
		paletteAnimator.startPalette = paletteAnimator.endPalette;
		List<Palette> palettesToChooseFrom = new List<Palette>(palettes);
		if (palettesToChooseFrom.Count > 1) {
			palettesToChooseFrom.Remove(paletteAnimator.endPalette);
		}
		paletteAnimator.endPalette = palettesToChooseFrom[Random.Range(0, palettesToChooseFrom.Count)];
	}
	
	public void HueShift() {
		paletteAnimator.gameObject.SetActive(true);
	}
	
	public void HueShiftToRandomPalette() {
		ChooseRandomPalette();
		HueShift();
	}
	
	public void StartGame() {
		currentGamePlayState = GamePlayState.Playing;
		SetUpDifficulty();
		if (gameCanvas.IsGemVideoButtonEnabled()) {
			// The player was shown the rewarded video button and didn't press it, so we can show interstitials again.
			blockInterstitials = false;
		}
        EventSystem.current.pixelDragThreshold = dragThresholds.Find(x => x.gamePlayState == currentGamePlayState).threshold;
        dragThresholdText.text = "DragThreshold: " + EventSystem.current.pixelDragThreshold;
		gameCanvas.StartGame();
		gameState = GameState.duringGame;
		menuMusicAnimator.Play("FadeOutAndStop");
		gameMusicAnimator.Play("PlayAndFadeIn");
		swipeSensitivity = Settings.swipeSensitivity + Mathf.Lerp(0, swipeSensitivityExpertIncrease, expertLevel);
		if(dustMotes != null) {
			((ParticleSystem.MainModule)dustMotes.main).maxParticles = dustMotes.main.maxParticles + dustMotesIncreaseCount;
		}
	}
	
	public void SetChallengeIndex(int index) {
		currentGamePlayState = GamePlayState.Playing;
		SetUpDifficulty();
		blockInterstitials = false;
		EventSystem.current.pixelDragThreshold = dragThresholds.Find(x => x.gamePlayState == currentGamePlayState).threshold;
		dragThresholdText.text = "DragThreshold: " + EventSystem.current.pixelDragThreshold;
		challengeIndex = index;
		paletteAnimator.endPalette = challengePalettes[0];
		SetUpGame();
		gameCanvas.StartChallenge();
		menuMusicAnimator.Play("FadeOutAndStop");
		gameMusicAnimator.Play("PlayAndFadeIn");
		gameState = GameState.duringGame;
		swipeSensitivity = Settings.swipeSensitivity + Mathf.Lerp(0, swipeSensitivityExpertIncrease, expertLevel);
	}
	
	// This is a big hack. If the game gets backgrounded right after the game starts, we abort the game
	public void AbortGame() {
		currentGamePlayState = GamePlayState.NotPlaying;
		EventSystem.current.pixelDragThreshold = dragThresholds.Find(x => x.gamePlayState == currentGamePlayState).threshold;
		dragThresholdText.text = "DragThreshold: " + EventSystem.current.pixelDragThreshold;
		gameCanvas.mainUIEnabled = true;
		gameState = GameState.preGame;
		gameMusicAnimator.Play("FadeOutAndStop");
		menuMusicAnimator.Play("PlayAndFadeIn");
		gameCanvas.score = 0;
		ResetBall();
		UpdateCurrentSpeed();
		SetUpTubeSegments();
		if(dustMotes != null) {
			((ParticleSystem.MainModule)dustMotes.main).maxParticles = dustMotes.main.maxParticles - dustMotesIncreaseCount;
		}
	}
	
	public void OnPointerDown(BaseEventData baseEventData) {
		
		PointerEventData data = baseEventData as PointerEventData;
		
		isDragging = true;
		spinVelocity = 0;
		
		if (allowSecretMenu) {
			if (data.position.x > Screen.width * .6f && data.position.y > Screen.height * .9f) {
				secretMenuCount++;
			}
			else {
				secretMenuCount = 0;
			}
			if (secretMenuCount == 7) {
				SceneManager.LoadScene("SettingsScene");
				return;
			}
		}
		
		if (challengeIndex < 0 && gameState == GameState.preGame) {
			StartGame();
		}
		
		if (gameState == GameState.gameOverDelay) {
			AfterGameOverDelay();
		}
	}
	
	public void OnBeginDrag(BaseEventData baseEventData) {
		PointerEventData data = baseEventData as PointerEventData;
		initialPointerX = data.position.x;
		initialSpinAngle = spinAngle;
		dragTime = Time.unscaledTime;
	}
	
	public void OnDrag(BaseEventData baseEventData) {
		PointerEventData data = baseEventData as PointerEventData;
		if (gameState == GameState.duringGame) {
			int swipeDirection = (Settings.controlsInverted ? -1 : 1);
			float spinStrength = ((data.position.x - initialPointerX) / screenWidth) * screenAspect * swipeSensitivity * swipeDirection;
			float oldSpinAngle = spinAngle;
			spinAngle = initialSpinAngle + spinStrength;
			float deltaTime = Time.unscaledTime - dragTime;
			spinVelocity = deltaTime > 0 ? (spinAngle - oldSpinAngle) / deltaTime : 0;
			dragTime = Time.unscaledTime;
		}
	}
	
	public void OnEndDrag(BaseEventData baseEventData) {
		isDragging = false;
	}
	
	public void OnInitializePotentialDrag(PointerEventData data) {
		data.useDragThreshold = false;
	}
	
	void UpdateCurrentSpeed() {
		if (gameState == GameState.paused) {
			cameraSpeed = 0;
		} else if (challengeIndex >= 0) {
			if (currentLevelNumber == 2) {
				cameraSpeed = challengeBallSpeed2;
			}
			else if (currentLevelNumber == 3) {
				cameraSpeed = challengeBallSpeed3;
			}
			else {
				cameraSpeed = challengeBallSpeed1;
			}
		}
		else if (ballSpeedIndex < ballSpeedDuration.Length) {
			float t = ballSpeedTime / ballSpeedDuration[ballSpeedIndex];
			float startingSpeed = ballSpeedIndex < 1 ? 0 : ballSpeedValue[ballSpeedIndex - 1];
			cameraSpeed = Mathf.Lerp(startingSpeed, ballSpeedValue[ballSpeedIndex], t);
			if (ballSpeedTime >= ballSpeedDuration[ballSpeedIndex]) {
				ballSpeedIndex++;
				ballSpeedTime = 0;
			}
		}
		else {
			cameraSpeed = ballSpeedValue[ballSpeedValue.Length - 1] + (lateGameAcceleration * ballSpeedTime);
		}
		Levels.gameSpeed = Mathf.Max(cameraSpeed, ballSpeedValue[0]);
		if (gameState == GameState.gameOverDelay || gameState == GameState.postGame || gameState == GameState.offeringRevive) {
			postGameTime += Time.deltaTime;
			if (challengeIndex < 0) {
				cameraSpeed = Mathf.Lerp(deathSpeed, 0, Levels.EaseOutExpo(Mathf.Min(1, postGameTime / cameraRampdownTime)));
				ballSpeed = Mathf.Lerp(deathSpeed, 0, Levels.EaseOutQuad(Mathf.Min(1, postGameTime / animationRampdownTime)));
			}
			else {
				cameraSpeed = Mathf.Lerp(deathSpeed, 0, Levels.EaseOutExpo(Mathf.Min(1, postGameTime / 1.6f)));
			}
		}
		TubeObject.shakingFactor = ((
			gameState == GameState.gameOverDelay ||
			gameState == GameState.postGame ||
			gameState == GameState.offeringRevive) ? ballSpeed : cameraSpeed) / 100;
	}
	
	void Update() {
		
		#if UNITY_EDITOR
		if (Input.GetKeyDown("space")) {
			for (int i = 0; i < levelPassedCounts.Length; i++) {
				print((levelPassedCounts[i] > 0 ? levelPassedCounts[i].ToString() : "") + "\t" + Levels.masterLevelList[i].Method.Name);
			}
		}
		#endif
		
		if (gameState == GameState.gameOverDelay) {
			gameOverDelayRemaining -= Time.deltaTime;
			if (gameOverDelayRemaining < 0) {
				AfterGameOverDelay();
				return;
			}
		}
		
		if (gameState == GameState.duringGame) {
			ballSpeedTime += Time.deltaTime;
			interstitialTimer += Time.deltaTime;
			rewardedVideoTimer += Time.deltaTime;
		}
		
		UpdateCurrentSpeed();
		
        if (dustMotes != null){
            ParticleSystem.VelocityOverLifetimeModule module = dustMotes.velocityOverLifetime;
            module.z = -cameraSpeed * dustMotesSpeedFactor;
        }
		
		
		// Move the camera
		float forwardDistance = Time.deltaTime * cameraSpeed;
		cameraTransform.position += Vector3.forward * forwardDistance;
		
        if (dustMotes != null)
        {
            // Move the dust mote emitter along with the camera
            dustMotes.gameObject.transform.position = cameraTransform.position + (Vector3.forward * (dustMotes.shape.scale.z / 2));
        }
		// Move tubeCap
		tubeCap.transform.position = cameraTransform.position + (Vector3.forward * fogFarZ);
		
		// Update the current spin angle
		if (gameState == GameState.duringGame) {
			
			// Slow down tube spin after lifting finger
			if (!isDragging && spinVelocity != 0) {
				spinAngle += (spinVelocity * Time.deltaTime);
				spinVelocity *= swipeMomentum;
				if (Mathf.Abs(spinVelocity) < 1) {
					spinVelocity = 0;
				}
			}
			
			spinAngles[spinAngleIndex] = spinAngle;
			spinAngleIndex = (spinAngleIndex + 1) % spinAngles.Length;
			averageSpinAngle = 0;
			for (int i = 0; i < spinAngles.Length; i++) {
				averageSpinAngle += (spinAngles[i] / spinAngles.Length);
			}
		}
		
		// Update the camera rotation based on new spin angle
		cameraTransform.rotation = Quaternion.Euler(0, 0, averageSpinAngle);
		
		// Update Tube Segments
		UpdateTubeSegments();
		
		// Set ball position based on camera, and update ball trail
		SetBallPosition();
		
		AnimateTubeObjects();
		
		// Make the ball look like it's rolling
		if (gameState == GameState.duringGame || ((gameState == GameState.gameOverDelay || gameState == GameState.postGame) && challengeIndex >= 0)) {
			ball.rigidBody.maxAngularVelocity = cameraSpeed * .2f;
			sideDistance = ((averageSpinAngle - previousSpinAngle) / 360) * (40 * Mathf.PI);
			Vector3 motionVector = cameraTransform.rotation * new Vector3(forwardDistance, 0, -sideDistance);
			ball.rigidBody.AddTorque(motionVector, ForceMode.VelocityChange);
			previousSpinAngle = averageSpinAngle;
		}
		else {
			ball.rigidBody.angularVelocity = Vector3.zero;
		}
		
		// Things that need to happen during gameplay after the ball position has been updated
		if (gameState == GameState.duringGame) {
			
			UpdateTubeBend();
			
			gameTime += Time.deltaTime;
			
			if (challengeIndex < 0) {
				gameDistance = ball.transform.position.z / distancePerPoint;
				int previousScore = gameCanvas.score;
				gameCanvas.score = (int)gameDistance;
				if (previousScore <= Settings.bestScore && gameCanvas.score > Settings.bestScore && Settings.bestScore >= 5) {
					highScoreAudioSource.Play();
				}
			}
			
			if (ballColliderEnabled) {
				TubeObject.ballPosition = ball.transform.position;
				for (int i = 0; i < TubeObject.activeTubeObjects.Count; i++) {
					TubeObject tubeObject = TubeObject.activeTubeObjects[i];
					if (tubeObject.isColliding != null && tubeObject.isColliding()) {
						if (tubeObject.meshType == MeshType.gem) {
							DisplayGemExplosion(tubeObject.transform.position);
							TubeObject.RecycleInstance(tubeObject);
							gemCollectedAudioSource.Play();
							gameCanvas.gems += collectableValue * Mathf.Min(Settings.gemMultiplier, 2);
						}
						else {
							EndGame(true);
							return;
						}
					}
				}
			}
			
			// Execute any triggers that the ball has reached
			for (int i = 0; i < triggerDistances.Count; i++) {
				if (ball.transform.position.z >= triggerDistances[i]) {
					triggerFunctions[i]();
					triggerDistances.RemoveAt(i);
					triggerFunctions.RemoveAt(i);
					i--;
				}
			}
		}
		
		ApplyTubeBend();
		
		// Apply death ball gravity
		if (gameState == GameState.gameOverDelay || gameState == GameState.postGame || gameState == GameState.offeringRevive) {
			for (int i = 0; i < deathBalls.Count; i++) {
				Vector3 gravityVector = deathBalls[i].transform.position;
				gravityVector.z = 0;
				gravityVector.Normalize();
				deathBalls[i].rigidBody.AddForce(gravityVector * deathGravityStrength, ForceMode.Acceleration);
			}
		}
		
		CreateLevels();
		RecycleObjects(cameraTransform.position.z - 100);
	}
	
	void DisplayGemExplosion(Vector3 position) {
		gemExplosion.transform.position = position;
		gemExplosion.transform.rotation = cameraTransform.rotation;
		gemExplosion.SetActive(false);
		gemExplosion.SetActive(true);
		Delay(2.5f, () => { gemExplosion.SetActive(false); });
	}
	
	void SetBallPosition() {
		
		Vector3 ballRoot = cameraTransform.position + (Vector3.forward * ballDistance);
		if ((gameState == GameState.gameOverDelay || gameState == GameState.postGame || gameState == GameState.offeringRevive) && challengeIndex < 0) {
			ballRoot = new Vector3(0, 0, ball.transform.position.z + (Time.deltaTime * ballSpeed));
		}
		
		if (gameState == GameState.duringGame || challengeIndex > 0) {
			ballTrail.transform.position = ballRoot + (Vector3.forward * trailHeadZ);
			ballTrail.AddAngle(averageSpinAngle, ballRoot.z - ball.transform.position.z);
		}
		
		if (gameState != GameState.duringGame) {
			ballTrail.ReduceLength(Time.deltaTime * deathSpeed);
		}
		
		Vector3 ballDown = cameraTransform.rotation * Vector3.down;
		ball.transform.position = ballRoot + (ballDown * (Levels.tubeRadius - (ball.transform.localScale.y / 2)));
		
		ballShadow.transform.position = ballRoot + (ballDown * (Levels.tubeRadius - .3f));
		ballShadow.transform.rotation = Quaternion.LookRotation(ballDown, Vector3.forward);
	}
	
	void UpdateTubeSegments() {
		while(tubeSegments[tubeSegments.Count - 1].position.z + (tubeSegmentLength / 2) < cameraTransform.position.z + fogFarZ) {
			Transform tubeSegment = tubeSegments[0];
			tubeSegment.position = new Vector3(0, 0, tubeSegments[tubeSegments.Count - 1].position.z + tubeSegmentLength);
			tubeSegments.RemoveAt(0);
			tubeSegments.Add(tubeSegment);
		}
	}
	
	void AnimateTubeObjects() {
		for (int i = 0; i < TubeObject.activeTubeObjects.Count; i++) {
			// Only animate root objects. They will recursively call AnimateParameters on their own children.
			TubeObject tubeObject = TubeObject.activeTubeObjects[i];
			if (tubeObject.transform.parent == null) {
				tubeObject.AnimateParameters(ball.transform.localPosition.z, 0);
			}
		}
	}
	
	void RandomizeTubeBendTarget() {
		tubeBendVelocity = 0;
		tubeBendDisplacement = 0;
		for (int i = 0; i < 100; i++) {
			Vector2 tubeBendTarget = Quaternion.Euler(0, 0, Random.Range(0, 360f)) * (Vector2.up * Random.Range(0, tubeBendMax));
			tubeBendVector = (tubeBendTarget - tubeBend);
			tubeBendMagnitude = tubeBendVector.magnitude;
			tubeBendVector.Normalize();
			if (tubeBendMagnitude > tubeBendMax * 1.5f) {
				return;
			}
		}
		tubeBendVector = Vector2.down;
		tubeBendMagnitude = 1;
	}
	
	void UpdateTubeBend() {
		float acceleration = tubeBendAcceleration * (cameraSpeed / 100);
		float velocityMax = tubeBendVelocityMax * (cameraSpeed / 100);
		if (tubeBendVelocity < 0) {
			RandomizeTubeBendTarget();
		}
		if (tubeBendDisplacement + ((tubeBendVelocity * tubeBendVelocity) / (2 * acceleration)) < tubeBendMagnitude) {
			tubeBendVelocity = Mathf.Min(velocityMax, tubeBendVelocity + (acceleration * Time.deltaTime));
		}
		else {
			tubeBendVelocity = Mathf.Min(velocityMax, tubeBendVelocity - (acceleration * Time.deltaTime));
		}
		
		float deltaDisplacement = (tubeBendVelocity * Time.deltaTime);
		tubeBendDisplacement += deltaDisplacement;
		tubeBend += (tubeBendVector * deltaDisplacement);
	}
	
	void ApplyTubeBend() {
		Vector3 rotatedTubeBend = Quaternion.Euler(0, 0, -averageSpinAngle) * (Vector3)tubeBend;
		Shader.SetGlobalVector("_QOffset", new Vector4(rotatedTubeBend.x, rotatedTubeBend.y, 0, 0));
	}
	
	void CreateLevels() {
		// Create obstacles and collectables for levels
		while(cameraTransform.position.z + fogFarZ >= Levels.z) {
			System.Action level = null;
			int levelIndex = -1;
			createdLevelCount++;
			float spacing;
			if (challengeIndex < 0) {
				spacing = createdLevelCount == 1 ? arcadeInitialLevelSpacingZ : arcadeLevelSpacingZ;
			}
			else {
				spacing = createdLevelCount == 1 ? challengeInitialLevelSpacingZ : challengeLevelSpacingZ;
			}
			if (challengeIndex >= 0) {
				level = createdLevelCount < 4 ? Levels.masterLevelList[challengeIndex] : Levels.EmptyLevel;
			}
			else if (gameStyle == GameStyle.fullGame && tutorialLevels.Count > 0) {
				level = tutorialLevels[0];
				tutorialLevels.RemoveAt(0);
				createdLevelCount = 0;
			}
			else if (gameStyle == GameStyle.fullGame) {
				
				float windowPosition = Mathf.Clamp01(Mathf.InverseLerp(1, endingWindowLevel, createdLevelCount));
				
				int lowerEdge = (int)(Mathf.Lerp(0, Levels.masterLevelList.Length - endingWindowSize, windowPosition));
				int upperEdge = (int)(Mathf.Lerp(startingWindowSize, Levels.masterLevelList.Length, windowPosition));
				
				lowerEdge = (int)Mathf.Clamp((float)lowerEdge, 0, Levels.masterLevelList.Length);
				upperEdge = (int)Mathf.Clamp((float)upperEdge, 0, Levels.masterLevelList.Length);
				
				/*
				print("Creating level " + createdLevelCount + " with a level between (" +
					lowerEdge.ToString() + " " + Levels.masterLevelList[lowerEdge].Method.Name + ") and (" +
				(upperEdge - 1).ToString() + " " + Levels.masterLevelList[upperEdge - 1].Method.Name + ")");
				*/
				
				List<int> levelIndexesToChooseFrom = new List<int>();
				for (int i = lowerEdge; i < upperEdge; i++) {
					if (!avoidLevels.Contains(Levels.masterLevelList[i])) {
						levelIndexesToChooseFrom.Add(i);
					}
				}
				if (levelIndexesToChooseFrom.Count == 0 && avoidLevels.Count > 0) {
					System.Action lastAvoidLevel = avoidLevels[avoidLevels.Count - 1];
					avoidLevels.Clear();
					avoidLevels.Add(lastAvoidLevel);
					for (int i = lowerEdge; i < upperEdge; i++) {
						if (!avoidLevels.Contains(Levels.masterLevelList[i])) {
							levelIndexesToChooseFrom.Add(i);
						}
					}
				}
				if (levelIndexesToChooseFrom.Count == 0) {
					// This should never happen, but it's here to be safe
					levelIndex = Random.Range(0,  Levels.masterLevelList.Length);
					avoidLevels.Clear();
				}
				else {
					levelIndex = levelIndexesToChooseFrom[Random.Range(0, levelIndexesToChooseFrom.Count)];
				}
				level = Levels.masterLevelList[levelIndex];
				avoidLevels.Add(level);
				//print("Created level " + level.Method.Name);
			}
			else if (gameStyle == GameStyle.testLevels1) {
				level = Levels.testLevels1[Random.Range(0, Levels.testLevels1.Length)];
			}
			else if (gameStyle == GameStyle.testLevels2) {
				level = Levels.testLevels2[Random.Range(0, Levels.testLevels2.Length)];
			}
			else if (gameStyle == GameStyle.testLevels3) {
				level = Levels.testLevels3[Random.Range(0, Levels.testLevels3.Length)];
			}
			else if (gameStyle == GameStyle.testLevels4) {
				level = Levels.testLevels4[Random.Range(0, Levels.testLevels4.Length)];
			}
			else if (gameStyle == GameStyle.testLevels5) {
				level = Levels.testLevels5[Random.Range(0, Levels.testLevels5.Length)];
			}
			else if (gameStyle == GameStyle.testLevels6) {

				if (testSegmentCount++ % 4 == 0) {
					level = Levels.fpsDroppingLevels [Random.Range (0, Levels.fpsDroppingLevels.Length - 1)];
				} else {
					level = Levels.fpsDroppingLevels [Levels.fpsDroppingLevels.Length - 1];
				}
//				Debug.LogError ("Segment Name: " + Levels.fpsDroppingLevels[testSegmentNum].Method.Name);
			}
			
			// Add palette change trigger
			CreateTrigger(Levels.z + paletteChangeDistance, () => {
				if (challengeIndex < 0 && currentLevelNumber != 0) {
					currentPaletteLevelNumber = (currentPaletteLevelNumber + 1) % levelsBetweenPaletteChange;
					if (currentPaletteLevelNumber == 0) {
						HueShiftToRandomPalette();
					}
				}
			});
			
			// Add level change trigger
			float levelChangeZ = Levels.z + (spacing * ((challengeIndex >= 0 && createdLevelCount == 4) ? .54f : .3f));
			CreateTrigger(levelChangeZ, () => {
				// Increase passed count for the level that was just passed
				if (currentLevelIndex >= 0) {
					levelPassedCounts[currentLevelIndex]++;
				}
				// Check whether we just passed a tutorial level
				if (challengeIndex < 0 && currentLevelFunction != null) {
					if (System.Array.IndexOf(Levels.tutorialLevels, currentLevelFunction) >= 0) {
						Settings.numberOfTutorialLevelsPassed++;
					}
				}
				currentLevelFunction = level;
				currentLevelIndex = levelIndex;
				if (challengeIndex >= 0 && currentLevelNumber > 0) {
					// Level passed during a challenge
					challengeStarAudioSources[currentLevelNumber - 1].Play();
					paletteAnimator.startPalette = challengePalettes[currentLevelNumber - 1];
					paletteAnimator.endPalette = challengePalettes[Mathf.Min(currentLevelNumber, challengePalettes.Count - 1)];
					HueShift();
					gameCanvas.starsPanel.stars = currentLevelNumber;
					string levelName = Levels.masterLevelList[challengeIndex].Method.Name;
					Settings.SetStarsForLevel(levelName, Mathf.Max(currentLevelNumber, Settings.GetStarsForLevel(levelName)));
					if (currentLevelNumber == 3) {
						EndGame(false);
					}
				}
				currentLevelNumber++;
			});
			
			// Add screenshot trigger
			if (challengeIndex < 0) {
				CreateTrigger(Levels.z + spacing - 20, () => {
					gameCanvas.scoreText.enabled = false;
					gameCanvas.highScorePanel.gameObject.SetActive(false);
					gameCanvas.gemsPanel.gameObject.SetActive(false);
					shareController.TakeScreenshot();
					gameCanvas.scoreText.enabled = true;
					gameCanvas.highScorePanel.gameObject.SetActive(true);
					gameCanvas.gemsPanel.gameObject.SetActive(true);
				});
			}
			
			// Set up challenge tube lines
			if (challengeIndex >= 0 && createdLevelCount >= 2 && createdLevelCount <= 4) {
				GameObject tubeLine = tubeLines[createdLevelCount - 2];
				tubeLine.SetActive(true);
				tubeLine.transform.position = new Vector3(0, 0, levelChangeZ);
			}
			
			// Create collectable
			Levels.z += spacing * collectablePosition;
			if (challengeIndex < 0 && createdLevelCount > 1 && Random.Range(0, .9999f) < collectableChance) {
				Levels.RandomizeTubeAngle();
				Levels.CreateGem();
			}
			Levels.z += spacing * (1 - collectablePosition);
			
			// Create level
			level();
		}
	}
	
	void CreateTrigger(float z, System.Action action) {
		triggerDistances.Add(z);
		triggerFunctions.Add(action);
	}
	
	void RecycleObjects(float obstacleCutoffZ) {
		// Only recycle root objects. They will recursively call RecycleObjects on their own children.
		List<TubeObject> tubeObjectsToRecycle = new List<TubeObject>();
		for (int i = 0; i < TubeObject.activeTubeObjects.Count; i++) {
			TubeObject tubeObject = TubeObject.activeTubeObjects[i];
			if (tubeObject.transform.parent == null && tubeObject.transform.position.z < obstacleCutoffZ) {
				tubeObjectsToRecycle.Add(tubeObject);
			}
		}
		for (int i = 0; i < tubeObjectsToRecycle.Count; i++) {
			TubeObject.RecycleInstance(tubeObjectsToRecycle[i]);
		}
	}
	
	void EndGame(bool death) {
		
		/*
		Debug.Log("");
		Debug.Log("--------------------beginnerBallSpeed0Duration\t\t" + beginnerBallSpeed0Duration);
		Debug.Log("--------------------beginnerBallSpeed0Value\t\t" + beginnerBallSpeed0Value);
		Debug.Log("--------------------beginnerBallSpeed1Duration\t\t" + beginnerBallSpeed1Duration);
		Debug.Log("--------------------beginnerBallSpeed1Value\t\t" + beginnerBallSpeed1Value);
		Debug.Log("--------------------beginnerBallSpeed2Duration\t\t" + beginnerBallSpeed2Duration);
		Debug.Log("--------------------beginnerBallSpeed2Value\t\t" + beginnerBallSpeed2Value);
		Debug.Log("--------------------beginnerBallSpeed3Duration\t\t" + beginnerBallSpeed3Duration);
		Debug.Log("--------------------beginnerBallSpeed3Value\t\t" + beginnerBallSpeed3Value);
		Debug.Log("--------------------beginnerStartingWindowSize\t\t" + beginnerStartingWindowSize);
		Debug.Log("--------------------beginnerEndingWindowSize\t\t" + beginnerEndingWindowSize);
		Debug.Log("--------------------beginnerEndingWindowLevel\t\t" + beginnerEndingWindowLevel);
		Debug.Log("--------------------beginnerLateGameAcceleration\t\t" + beginnerLateGameAcceleration);
		Debug.Log("--------------------beginnerDistancePerPoint\t\t" + beginnerDistancePerPoint);
		
		Debug.Log("");
		Debug.Log("--------------------expertBallSpeed0Duration\t\t" + expertBallSpeed0Duration);
		Debug.Log("--------------------expertBallSpeed0Value\t\t" + expertBallSpeed0Value);
		Debug.Log("--------------------expertBallSpeed1Duration\t\t" + expertBallSpeed1Duration);
		Debug.Log("--------------------expertBallSpeed1Value\t\t" + expertBallSpeed1Value);
		Debug.Log("--------------------expertBallSpeed2Duration\t\t" + expertBallSpeed2Duration);
		Debug.Log("--------------------expertBallSpeed2Value\t\t" + expertBallSpeed2Value);
		Debug.Log("--------------------expertBallSpeed3Duration\t\t" + expertBallSpeed3Duration);
		Debug.Log("--------------------expertBallSpeed3Value\t\t" + expertBallSpeed3Value);
		Debug.Log("--------------------expertStartingWindowSize\t\t" + expertStartingWindowSize);
		Debug.Log("--------------------expertEndingWindowSize\t\t" + expertEndingWindowSize);
		Debug.Log("--------------------expertEndingWindowLevel\t\t" + expertEndingWindowLevel);
		Debug.Log("--------------------expertLateGameAcceleration\t\t" + expertLateGameAcceleration);
		Debug.Log("--------------------expertDistancePerPoint\t\t" + expertDistancePerPoint);
		Debug.Log("");
		*/
        
        currentGamePlayState = GamePlayState.NotPlaying;
        EventSystem.current.pixelDragThreshold = dragThresholds.Find(x => x.gamePlayState == currentGamePlayState).threshold;
        dragThresholdText.text = "DragThreshold: " + EventSystem.current.pixelDragThreshold;
		deathSpeed = cameraSpeed;
		gameCanvas.previousScore = gameCanvas.score;
		spinVelocity = 0;
		gameMusicAnimator.Play("FadeOutAndPause");
		
		if (death) {
			deathAudioSource.Play();
			ball.gameObject.SetActive(false);
			ballShadow.SetActive(false);
			LaunchDeathBalls();
			if (Settings.hapticEnabled) {
				Delay(hapticDelay, () => { NativeUtilities.TriggerHaptic(); });
			}
        }

        gameState = GameState.gameOverDelay;
        gameOverDelayRemaining = resetTime;
    }

    public void AfterGameOverDelay() {

	    gameState = GameState.postGame;
	    // Set up post game interstitials and special offers
	    
	    #if !UNITY_EDITOR
	    bool incentivizedAdIsAvailable = Heyzap.HZIncentivizedAd.IsAvailable();
	    bool interstitialAdIsAvailable = Heyzap.HZInterstitialAd.IsAvailable();
	    #else
	    bool incentivizedAdIsAvailable = true;
	    bool interstitialAdIsAvailable = false;
	    #endif
	    
	    if (gameDistance >= reviveScoreMin &&
		    gameDistance >= Settings.bestScore * reviveScorePercentage &&
		    incentivizedAdIsAvailable &&
		    !gameCanvas.revivePanel.alreadyEnabled)
	    {
	    	if (blockRevive) {
	    		blockRevive = false;
	    	}
	    	else {
		    	gameState = GameState.offeringRevive;
		    	gameCanvas.revivePanel.Enable();
		    	text_Status.text += "Showing Revive Panel" + "\n";
		    	return;
	    	}
	    }

	    gameCanvas.ketchappSquareController.animationEnabled = false;
	    gameCanvas.DisableSpecialButtons();
			
	    freeGiftCount++;
	    rewardedVideoCount++;
	    ketchappSquareCount++;
	    rateThisAppCount++;
	    IAPEnticeCount++;

	    text_Status.text = "";
	    text_Status.text += "freeGiftCount: " + freeGiftCount + "\n";
	    text_Status.text += "rewardedVideoCount: " + rewardedVideoCount + "\n";
	    text_Status.text += "ketchappSquareCount: " + ketchappSquareCount + "\n";
	    text_Status.text += "rateThisAppCount: " + rateThisAppCount + "\n";
	    text_Status.text += "IAPEnticeCount: " + IAPEnticeCount + "\n";

	    #if !UNITY_EDITOR && !UNITY_ANDROID
	    if (Settings.adsEnabled && ketchappSquareCount == 1) {
	    KetchappPromo.FetchKetchappSquare();
	    }
	    #endif
			
	    bool willShowInterstitial = false;
	    if (Settings.adsEnabled &&
	    	!blockInterstitials &&
	    	!gameCanvas.revivePanel.alreadyEnabled &&
	    	interstitialTimer >= interstitialTime &&
		    interstitialAdIsAvailable &&
	    	Application.internetReachability != NetworkReachability.NotReachable)
	    {
	    	willShowInterstitial = true;
		    interstitialTimer = 0;
		    ShowInterstitialAd();
	    }
			
	    if (challengeIndex >= 0) {
		    #if !UNITY_EDITOR
		    if (!willShowInterstitial) {
		    	GameOverFlash();
		    }
		    #else
		    GameOverFlash();
		    #endif
		    return;
	    }
			
	    bool willShowHighScore = false;
	    if (gameCanvas.score >= 5 && gameCanvas.score > Settings.bestScore) {
		    willShowHighScore = true;
		    gameCanvas.EnableHighScoreButton();
		    text_Status.text += "Showing High ScoreButton" + "\n";
	    }
			
	    bool willShowFreeGift = false;
	    if (!willShowHighScore) {
		    int threshhold = Settings.freeGiftAwardedCount == 0 ? 5 : 15;
		    if (freeGiftCount >= threshhold) {
			    freeGiftCount = 0;
			    willShowFreeGift = true;
			    gameCanvas.EnableFreeGiftButton();
			    text_Status.text += "Showing Free Gift Button" + "\n";
		    }
	    }
			
	    bool willShowRewardedVideo = false;
	    if (incentivizedAdIsAvailable &&
		    !willShowHighScore &&
		    !willShowFreeGift &&
	    	(rewardedVideoCount >= 3 || rewardedVideoTimer >= rewardedVideoTime))
	    {
		    rewardedVideoCount = 0;
		    rewardedVideoTimer = 0;
		    willShowRewardedVideo = true;
		    gameCanvas.EnableGemVideoButton();
		    text_Status.text += "Showing Gem Button" + "\n";
	    }
			
	    bool willShowKetchappSquare = false;
	    if (Settings.adsEnabled &&
	    	!willShowInterstitial &&
		    !willShowHighScore &&
		    !willShowFreeGift &&
		    !willShowRewardedVideo &&
		    ketchappSquareCount >= 4)
	    {
		    ketchappSquareCount = 0;
		    willShowKetchappSquare = true;
		    gameCanvas.ketchappSquareController.animationEnabled = true;
	    }
	    
	    bool willShowIAPEntice = false;
	    int IAPindex = 0;
	    if (Settings.gemMultiplier <= 1) {
	    	if (Settings.adsEnabled) {
		    	IAPindex = Random.value < .8f ? 1 : 2;
	    	}
	    	else {
		    	IAPindex = 1;
	    	}
	    }
	    if (!willShowHighScore &&
		    !willShowFreeGift &&
		    !willShowRewardedVideo &&
		    !willShowKetchappSquare &&
		    IAPindex != 0 &&
		    IAPEnticeCount >= 5)
	    {
		    IAPEnticeCount = 0;
		    willShowIAPEntice = true;
	    	if (IAPindex == 1) {
			    gameCanvas.EnableGemDoublerButton();
	    	}
	    	else if (IAPindex == 2) {
			    gameCanvas.EnableNoAdsButton();
		    }
	    }
			
	    willShowRateThisApp = false;
	    if (!willShowInterstitial &&
		    !willShowFreeGift &&
		    !hasShownRateThisApp &&
		    rateThisAppCount >= 10)
	    {
		    willShowRateThisApp = true;
	    }

	    text_Status.text += "willShowInterstitial: " + willShowInterstitial + "\n";
	    
	    #if !UNITY_EDITOR
	    if (!willShowInterstitial) {
		    GameOverFlash();
	    }
	    #else
	    GameOverFlash();
	    #endif
    }
	
	public void ShowInterstitialAd() {
		#if !UNITY_EDITOR
		HZInterstitialAd.SetDisplayListener(delegate(string adState, string adTag) {
			if (gameState != GameState.postGame) {
				return;
			}
			if (adState.Equals("failed") || adState.Equals("fetch_failed") || adState.Equals("hide")) {
				text_Status.text += "Interstitial Call " + adState + "\n";
				GameOverFlash();
			}
		});
		gameCanvas.activitySpinnerAnimator.visible = true;
		Invoke("GameOverFlash", 10);
		text_Status.text += "Interstitial Call sent" + "\n";
		HZInterstitialAd.Show();
		#endif
	}
	
	public void LaunchDeathBalls() {

		Debug.Log ("LaunchDeathBalls Called");
		deathCollidersContainer.position = cameraTransform.position;
		deathCollidersContainer.gameObject.SetActive(true);
		for (int i = 0, j = 0; j < numberOfDeathBallRings; j++) {
			float angle1 = 180 * (j / (float)(numberOfDeathBallRings - 1));
			int numberOfBallsInRing = angle1 == 0 || angle1 == 180 ? 1 : numberOfDeathBallsPerRing;
			for (int k = 0; k < numberOfBallsInRing; k++) {
				float angle2 = 360 * (k / (float)numberOfBallsInRing);
				Vector3 vector = Quaternion.Euler(0, angle2, angle1) * Vector3.up;
				LaunchDeathBall(
					deathBalls[i],
					vector * ((ball.transform.localScale.x * .5f) - (deathBalls[i].transform.localScale.x * .5f))
				);
				i++;
				if (deathBallsDoubleRings) {
					LaunchDeathBall(
						deathBalls[i],
						vector * ((ball.transform.localScale.x * .25f) - (deathBalls[i].transform.localScale.x * .5f))
					);
					i++;
				}
			}
		}
	}
	
	public void LaunchDeathBall(DeathBall deathBall, Vector3 vector) {
		deathBall.transform.position = ball.transform.position + vector;
		deathBall.gameObject.SetActive(true);
		Vector3 left = Vector3.left * (Random.Range(deathExplosionCenterMin.x, deathExplosionCenterMax.x) +
			Mathf.Clamp(sideDistance, -deathSwipeEffectMax, deathSwipeEffectMax));
		Vector3 forward = Vector3.forward * Random.Range(deathExplosionCenterMin.z, deathExplosionCenterMax.z);
		Vector3 up = Vector3.up * Random.Range(deathExplosionCenterMin.y, deathExplosionCenterMax.y);
		deathBall.rigidBody.AddExplosionForce(
			Random.Range(deathExplosionStrengthMin, deathExplosionStrengthMax),
			ball.transform.position + forward + (cameraTransform.rotation * (up + left)),
			0);
		//rigidBody.AddForce((vector * 2) + (ballVelocity * 10), ForceMode.Force);
	}
	
	public void Revive() {
		if (gameState != GameState.offeringRevive) {
			return;
		}
		gameState = GameState.postGame;
		gameCanvas.flashPanel.Flash(paletteAnimator.endPalette.tubeBottomColor, () => {
			interstitialTimer = 0;
			blockInterstitials = true;
			gameState = GameState.duringGame;
			ballShadow.SetActive(true);
			ball.gameObject.SetActive(true);
			for (int i = 0; i < deathBalls.Count; i++) {
				deathBalls[i].gameObject.SetActive(false);
			}
			deathCollidersContainer.gameObject.SetActive(false);
			triggerDistances.Clear();
			triggerFunctions.Clear();
			RecycleObjects(float.MaxValue);
			Levels.z = ball.transform.position.z + revivePadding;
			CreateLevels();
			SetBallPosition();
			ballTrail.Reset(spinAngle);
			for (int i = 0; i < spinAngles.Length; i++) {
				spinAngles[i] = spinAngle;
			}
			spinAngleIndex = 0;
			previousSpinAngle = spinAngle;
			averageSpinAngle = spinAngle;
			UpdateCurrentSpeed();
			postGameTime = 0;
			gameMusicAnimator.Play("UnPauseAndFadeIn");
		});
	}
	
	public void NoRevive() {
		blockInterstitials = false;
		blockRevive = true;
		AfterGameOverDelay();
	}
	
	public void GameOverFlash() {

		text_Status.text += "GameOverFlash called " + "\n";

		Debug.Log ("GameOverFlash");
		CancelInvoke ("GameOverFlash");
		gameCanvas.HideActivitySpinner();
		#if !UNITY_EDITOR
		HZInterstitialAd.SetDisplayListener(null);
		#endif
		
		if (willShowRateThisApp) {
			willShowRateThisApp = false;
			hasShownRateThisApp = true;
			NativeUtilities.OpenRateThisAppPopup();
		}

		if (challengeIndex < 0) {
			currentPaletteLevelNumber = (currentPaletteLevelNumber + 1) % levelsBetweenPaletteChange;
			if (currentPaletteLevelNumber == 0) {
				ChooseRandomPalette();
			}
		}
		else {
			paletteAnimator.endPalette = challengePalettes[0];
		}
		
		gameCanvas.flashPanel.Flash(paletteAnimator.endPalette.tubeBottomColor, () => {
			
			Settings.SetLevelInts("LevelPassedCounts", levelPassedCounts);
			
			#if !UNITY_EDITOR

			#if UNITY_ANDROID

			GPGS_LeaderboardManager.singleton.ReportScore (gameCanvas.score, GPGSIds.leaderboard_score);
			#elif UNITY_IOS

					if (gameCanvas.score > gameCanvas.bestScore) {
					if (!Social.localUser.authenticated) {
					Social.localUser.Authenticate(success => {
					if (success) {

			Social.ReportScore(gameCanvas.score, leaderboardIDiOS, null);
					}
					});
					}
					else {
			Social.ReportScore(gameCanvas.score, leaderboardIDiOS, null);
					}
					}
			#endif



			#endif
			
			if (gameCanvas.score > gameCanvas.bestScore) {
				gameCanvas.bestScore = gameCanvas.score;
			}
			SetUpGame();
			if (challengeIndex < 0) {
				gameCanvas.SetUpGame();
			}
			else {
				Settings.challengeAttentionBounceFinished = true;
				gameCanvas.OpenChallengeMenu(0);
				gameCanvas.challengeMenu.AnimateUI(challengeIndex);
				challengeIndex = -1;
				SetUpGame();
			}
			menuMusicAnimator.Play("PlayAndFadeIn");
		});
	}
	
	public void Delay(float duration, System.Action completion) {
		if (duration == 0) {
			completion();
		}
		else {
			StartCoroutine(WaitBeforeAction(duration, completion));
		}
	}
	
	IEnumerator WaitBeforeAction(float duration, System.Action completion) {
		yield return new WaitForSeconds(duration);
		completion();
	}
	
	void OnGUI()
	{
		if (debugDisplay) {
			int w = Screen.width;
			int h = Screen.height;
			GUIStyle style = new GUIStyle();
			Rect rect = new Rect(0, 0, w, h * 2 / 100);
			style.alignment = TextAnchor.UpperLeft;
			style.fontSize = h * 2 / 100;
			style.normal.textColor = Color.white;
			GUI.Label(rect, "Camera Speed: " + (int)cameraSpeed, style);
			style.alignment = TextAnchor.UpperCenter;
			GUI.Label(rect, "Expert Level: " + expertLevel, style);
			if (currentLevelFunction != null) {
				style.alignment = TextAnchor.UpperRight;
				GUI.Label(rect, currentLevelFunction.Method.Name, style);
			}
		}
	}
}

static class ExtensionMethods {
	
	public static void Shuffle<T>(this IList<T> list)  
	{  
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = UnityEngine.Random.Range(0, n + 1);
			if (k != n) {
				T value = list[k];  
				list[k] = list[n];  
				list[n] = value;  
			}
		}  
	}
}
