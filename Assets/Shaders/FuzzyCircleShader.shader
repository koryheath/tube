Shader "Tube/FuzzyCircleShader" {

	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_InnerRadius ("Inner Radius", float) = 0
		_OuterRadius ("Outer Radius", float) = .5
	}
	
	SubShader {
	
		Tags {
			"Queue" = "Geometry+4"
			"PreviewType"="Plane"
		}
		
		Pass {
		
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			
			CGPROGRAM
			
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				
				#include "UnityCG.cginc"
				#include "ShaderUtils.cginc"
	
				struct appdata
				{
					float4 vertex : POSITION;
					float2 texcoord : TEXCOORD0;
				};
	
				struct v2f
				{
					float4 vertex : SV_POSITION;
					float2 texcoord : TEXCOORD0;
				};
				
				sampler2D _MainTex;
				float4 _Color;
				float _InnerRadius;
				float _OuterRadius;

				v2f vert (appdata v)
				{
					v2f o;
					o.texcoord = float2(v.texcoord.x - .5, v.texcoord.y - .5);
					o.vertex = UnityObjectToClipPos(v.vertex.xyz);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					return fixed4(
						_Color.r,
						_Color.g,
						_Color.b,
						_Color.a * (1 - smoothstep(_InnerRadius, _OuterRadius, length(i.texcoord)))
					);
				}
				
			ENDCG
		}
	} 
}
