Shader "Tube/DustMoteShader" {

	Properties {
		_InvisibleRadius ("Invisible Radius", Float) = 5
		_Dropoff ("Dropoff", Float) = .2
		_InnerRadius ("Inner Radius", float) = 0
		_OuterRadius ("Outer Radius", float) = .5
	}
	
	SubShader {
	
		Tags {
			"Queue" = "Geometry+5"
			"PreviewType"="Plane"
		}

		Pass {
		
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			
			CGPROGRAM
			
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				
				#include "UnityCG.cginc"
				#include "TubeBend.cginc"
				#include "ShaderUtils.cginc"
	
				struct appdata
				{
					float4 vertex : POSITION;
					float2 texcoord : TEXCOORD0;
					fixed4 color : COLOR;
				};
	
				struct v2f
				{
					float4 vertex : SV_POSITION;
					float2 texcoord : TEXCOORD0;
					fixed4 color : COLOR;
				};
				
				float _InvisibleRadius;
				float _Dropoff;
				float _InnerRadius;
				float _OuterRadius;

				TUBE_BEND_PROPERTIES
	
				v2f vert (appdata v)
				{
					v2f o;
					o.texcoord = float2(v.texcoord.x - .5, v.texcoord.y - .5);
					o.color = v.color;
					o.vertex = float4(UnityObjectToViewPos(v.vertex.xyz), 1);
					o.color.a *= saturate((length(o.vertex.xy) - _InvisibleRadius) * _Dropoff);
					APPLY_TUBE_BEND(o.vertex)
					o.vertex = mul(UNITY_MATRIX_P, o.vertex);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					return fixed4(
						i.color.r,
						i.color.g,
						i.color.b,
						i.color.a * (1 - smoothstep(_InnerRadius, _OuterRadius, length(i.texcoord))) * saturate(2 - (ddx(i.texcoord.x) * 3))
					);
				}
				
			ENDCG
		}
	} 
}
