Shader "Tube/TubeSegmentShader" {

	Properties {
		_TopColor ("Top Color", Color) = (1,1,1,1)
		_BottomColor ("Bottom Color", Color) = (1,1,1,1)
	}
	
	SubShader {
	
		Tags {
			"Queue" = "Geometry+2"
		}

		Pass {
		
			Blend OneMinusDstAlpha DstAlpha
			Offset -1, -1
			ZWrite Off
			
			CGPROGRAM
			
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
	
				#include "UnityCG.cginc"
				#include "TubeBend.cginc"
				#include "CustomFog.cginc"
				
				struct appdata
				{
					float4 vertex : POSITION;
					fixed4 color : COLOR;
				};
	
				struct v2f
				{
					float4 vertex : SV_POSITION;
					fixed4 color : COLOR;
				};
				
				float4 _TopColor;
				float4 _BottomColor;
				
				TUBE_BEND_PROPERTIES
				CUSTOM_FOG_PROPERTIES
				
				v2f vert (appdata v)
				{
					v2f o;
					o.vertex = float4(UnityObjectToViewPos(v.vertex.xyz), 1);
					o.color = lerp(_TopColor, _BottomColor, (cos(((o.vertex.y + 20) / 40) * 3.1415926) + 1) * .5) * v.color;
					APPLY_CUSTOM_FOG(o.color, -o.vertex.z);
					APPLY_TUBE_BEND(o.vertex)
					o.vertex = mul(UNITY_MATRIX_P, o.vertex);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					return i.color;
				}
				
			ENDCG
		}
	} 
}
