Shader "Tube/ShadowRadialShader" {

	SubShader {
	
		Tags {
			"Queue" = "Geometry+1"
		}

		Pass {
		
			BlendOp Max
			Cull Front
			ZWrite Off
			
			CGPROGRAM
				
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
	
				#include "UnityCG.cginc"
				#include "TubeBend.cginc"
				#include "CustomFog.cginc"
				#include "ShaderUtils.cginc"
				
				struct appdata
				{
					float4 vertex : POSITION;
				};
	
				struct v2f
				{
					float4 vertex : SV_POSITION;
					fixed4 color : COLOR;
					float2 shadow : TEXCOORD0;
				};
				
				float _ShadowLength;
				float _ShadowOpacity;
				float _ShadowDropoff;
				float _ShadowTubeRadius;
				
				TUBE_BEND_PROPERTIES
				CUSTOM_FOG_PROPERTIES
				
				v2f vert (appdata v)
				{
					v2f o;
					o.vertex = float4(UnityObjectToViewPos(v.vertex.xyz), 1);
					
					float height = (_ShadowTubeRadius - length(o.vertex.xy)) * _ShadowLength;
					o.vertex.xy = normalize(o.vertex.xy) * _ShadowTubeRadius;
					o.vertex.z += height;
					
					o.shadow = float2(_ShadowOpacity * (1 - (height / _ShadowDropoff)), 0);
					o.color = fixed4(0, 0, 0, 1);
					
					APPLY_CUSTOM_FOG(o.color, -o.vertex.z);
					APPLY_TUBE_BEND(o.vertex)
					
					o.vertex = mul(UNITY_MATRIX_P, o.vertex);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					i.color.a = saturate(i.shadow.x) * LESSER_THAN_OR_EQUAL_TO(i.shadow.x, _ShadowOpacity);
					return i.color;
				}
				
			ENDCG
		}
	} 
}
