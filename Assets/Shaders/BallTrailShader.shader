Shader "Tube/BallTrail"
{
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Texture", 2D) = "white" {}
		_HeadPercentage ("HeadPercentage", Float) = .05
	}
	
	SubShader {
	
		Tags {
			"Queue" = "Geometry+4"
			"PreviewType"="Plane"
		}
		
		Pass {
		
			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Always
			ZWrite Off
			
			CGPROGRAM
			
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				
				#include "UnityCG.cginc"
				#include "TubeBend.cginc"
	
				sampler2D _MainTex;
				float4 _MainTex_ST;
				fixed4 _Color;
				float _HeadPercentage;
				
				TUBE_BEND_PROPERTIES
				
				struct appdata {
					float4 vertex : POSITION;
					float2 texcoord : TEXCOORD0;
				};
	
				struct v2f {
					float4 vertex : SV_POSITION;
					float2 texcoord : TEXCOORD0;
					fixed2 head : TEXCOORD1;
				};
					
				v2f vert (appdata v)
				{
					v2f o;
					o.vertex = float4(UnityObjectToViewPos(v.vertex.xyz), 1);
					APPLY_TUBE_BEND(o.vertex)
					o.vertex = mul(UNITY_MATRIX_P, o.vertex);
					o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
					o.head = fixed2(0, 1 - v.texcoord.y);
					return o;
				}
	
				fixed4 frag (v2f i) : SV_Target
				{
					fixed4 col = tex2D(_MainTex, i.texcoord) * _Color;
					col.a *= saturate(i.head.y / _HeadPercentage);
					return col;
				}
				
			ENDCG
		}
	}
}
