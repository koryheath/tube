Shader "Tube/VertexColorShader" {

	Properties {
		_Color ("Color", Color) = (1,1,1,1)
	}
	
	SubShader {
	
		Tags {
			"Queue" = "Geometry+3"
			"PreviewType"="Plane"
		}

		Pass {
		
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			
			CGPROGRAM
			
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0

				#include "UnityCG.cginc"
				#include "TubeBend.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					fixed4 color : COLOR;
				};
	
				struct v2f
				{
					float4 vertex : SV_POSITION;
					fixed4 color : COLOR;
				};
				
				fixed4 _Color;
				
				TUBE_BEND_PROPERTIES
	
				v2f vert (appdata v)
				{
					v2f o;
					o.color = v.color;
					o.vertex = float4(UnityObjectToViewPos(v.vertex.xyz), 1);
					APPLY_TUBE_BEND(o.vertex)
					o.vertex = mul(UNITY_MATRIX_P, o.vertex);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					return saturate(_Color * i.color);
				}
				
			ENDCG
		}
	} 
}
