#ifndef CUSTOM_FOG_INCLUDED
#define CUSTOM_FOG_INCLUDED

#define CUSTOM_FOG_PROPERTIES \
	float _CustomFogNearZ; \
	float _CustomFogFarZ; \
	float4 _CustomFogColor;

#define APPLY_CUSTOM_FOG(color, z) \
	float _customFogStrength = saturate((z - _CustomFogNearZ) / (_CustomFogFarZ - _CustomFogNearZ)); \
	_customFogStrength = (_customFogStrength * _customFogStrength); \
	color = lerp(color, _CustomFogColor, _customFogStrength);
#endif
