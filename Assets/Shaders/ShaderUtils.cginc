#ifndef SHADER_UTILS_INCLUDED
#define SHADER_UTILS_INCLUDED

#define EQUAL_TO(a, b) ((a) == (b) ? 1 : 0)
#define NOT_EQUAL_TO(a, b) ((a) != (b) ? 1 : 0)
#define GREATER_THAN(a, b) ((a) > (b) ? 1 : 0)
#define LESSER_THAN(a, b) ((a) < (b) ? 1 : 0)
#define GREATER_THAN_OR_EQUAL_TO(a, b) ((a) >= (b) ? 1 : 0)
#define LESSER_THAN_OR_EQUAL_TO(a, b) ((a) <= (b) ? 1 : 0)
#define LOGICAL_AND(a, b) ((a) * (b))
#define LOGICAL_OR(a, b) min((a) + (b), 1)
#define LOGICAL_XOR(a, b) (((a) + (b)) % 2)
#define LOGICAL_NOT(a) (1 - (a))
#define INVERSE_LERP(from, to, value) (((value) - (from)) / ((to) - (from)))
#define INVERSE_LERP_CLAMP(from, to, value) saturate(((value) - (from)) / ((to) - (from)))

#endif
