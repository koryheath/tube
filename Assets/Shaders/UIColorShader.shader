Shader "Tube/UIColorShader" {

	Properties {
		_MainTex ("Sprite Texture", 2D) = "white" {}
		_ColorIndex ("Color Index", int) = 0
	}
	
	SubShader {
	
		Tags { 
			"RenderType" = "Opaque"
			"Queue" = "Transparent"
			"PreviewType"="Plane"
		}
		
		Pass {
		
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha 
			
			CGPROGRAM
			
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				
				sampler2D _MainTex;
				int _ColorIndex;
				float4 _UIColors[2];
				
				struct Vertex
				{
					float4 vertex : POSITION;
					float2 uv_MainTex : TEXCOORD0;
				};
				
				struct Fragment
				{
					float4 vertex : POSITION;
					float2 uv_MainTex : TEXCOORD0;
				};
				
				Fragment vert(Vertex v)
				{
					Fragment o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv_MainTex = v.uv_MainTex;
					return o;
				}
				
				fixed4 frag(Fragment IN) : COLOR
				{
					return tex2D(_MainTex, IN.uv_MainTex) * _UIColors[_ColorIndex];
				}
			
			ENDCG
		}
	}
}
