Shader "Tube/ZWriteShader" {

	SubShader {
	
		Tags {
			"Queue" = "Geometry"
		}

		Pass {
			
			CGPROGRAM
			
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
	
				#include "UnityCG.cginc"
				#include "TubeBend.cginc"
				
				struct appdata
				{
					float4 vertex : POSITION;
				};
	
				struct v2f
				{
					float4 vertex : SV_POSITION;
				};
				
				TUBE_BEND_PROPERTIES
				
				v2f vert (appdata v)
				{
					v2f o;
					o.vertex = float4(UnityObjectToViewPos(v.vertex.xyz), 1);
					APPLY_TUBE_BEND(o.vertex)
					o.vertex = mul(UNITY_MATRIX_P, o.vertex);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					return fixed4(0, 0, 0, 0);
				}
				
			ENDCG
		}
	} 
}
