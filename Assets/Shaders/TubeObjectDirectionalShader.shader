Shader "Tube/TubeObjectDirectionalShader" {

	Properties {
		_TopColor ("Facing Toward Tube Center", Color) = (1,1,1,1)
		_BottomColor ("Facing Away From Tube Center", Color) = (1,1,1,1)
		_ZOffset ("Z Offset", Float) = 1
	}
	
	SubShader {
	
		Tags {
			"Queue" = "Geometry+5"
			"DisableBatching"="True"
		}

		Pass {
		
			CGPROGRAM
			
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				
				#include "UnityCG.cginc"
				#include "TubeBend.cginc"
				#include "CustomFog.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float3 normal : NORMAL;
				};
	
				struct v2f
				{
					float4 vertex : SV_POSITION;
					fixed4 color : COLOR;
				};
				
				float4 _TopColor;
				float4 _BottomColor;
				float _ZOffset;
				
				TUBE_BEND_PROPERTIES
				CUSTOM_FOG_PROPERTIES
				
				v2f vert (appdata v)
				{
					v2f o;
					float4 objectCenter = mul(UNITY_MATRIX_M, float4(0, 0, 0, 1));
					float3 lightVector = float3(-objectCenter.x, -objectCenter.y, 0);
					// In case objectCenter is at exactly xy 0,0
					lightVector.z -= length(lightVector) == 0 ? 1 : 0;
					lightVector = normalize(lightVector);
					lightVector = normalize(float3(lightVector.x, lightVector.y, _ZOffset));
					float3 worldNormal = UnityObjectToWorldNormal(v.normal);
					float nl = saturate(dot(worldNormal, lightVector));
					o.color = lerp(_BottomColor, _TopColor, nl);
					o.vertex = float4(UnityObjectToViewPos(v.vertex.xyz), 1);
					APPLY_CUSTOM_FOG(o.color, -o.vertex.z);
					APPLY_TUBE_BEND(o.vertex)
					o.vertex = mul(UNITY_MATRIX_P, o.vertex);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					return i.color;
				}
				
			ENDCG
		}
	} 
}
