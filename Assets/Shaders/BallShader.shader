Shader "Tube/BallShader" {

	Properties {
		_BrightestColor ("Brightest Color", Color) = (1,1,1,1)
		_DarkestColor ("Darkest Color", Color) = (.5,.5,.5,1)
		_LightDirection ("Light Direction", Vector) = (0, 1, 1, 1)
		_MainTex ("Texture", 2D) = "white" {}
		_SquashFactor ("Squash Factor", float) = 1
	}
	
	SubShader {
	
		Tags {
			"Queue" = "Geometry+7"
		}

		Pass {
		
			CGPROGRAM
			
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				
				#include "UnityCG.cginc"
	
				struct appdata
				{
					float4 vertex : POSITION;
					float3 normal : NORMAL;
					float4 texcoord : TEXCOORD0;
				};
	
				struct v2f
				{
					float4 vertex : SV_POSITION;
					float2 texcoord : TEXCOORD0;
					fixed4 color : COLOR;
				};
				
				float4 _BrightestColor;
				float4 _DarkestColor;
				float4 _LightDirection;
				sampler2D _MainTex;
				float4 _MainTex_ST;
				float _SquashFactor;

				v2f vert (appdata v)
				{
					v2f o;
					float light = max(0, dot(normalize(mul((float3x3)UNITY_MATRIX_IT_MV, v.normal)), normalize(_LightDirection.xyz)));
					o.color = lerp(_DarkestColor, _BrightestColor, light);
					o.vertex = float4(UnityObjectToViewPos(v.vertex.xyz), 1);
					o.vertex.y = ((o.vertex.y + 20) * _SquashFactor) - 20;
					o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
					o.vertex = mul(UNITY_MATRIX_P, o.vertex);
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					return tex2D(_MainTex, i.texcoord) * i.color;
				}
				
			ENDCG
		}
	} 
}
