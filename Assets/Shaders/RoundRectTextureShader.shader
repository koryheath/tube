Shader "Tube/RoundRectTextureShader" {

	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_CornerParameters ("Corner Parameters", Vector) = (.5, .5, .1, 1.5)
	}
	
	SubShader {
	
		Tags {
			"Queue" = "Transparent"
			"PreviewType"="Plane"
		}
		
		Pass {
		
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			
			CGPROGRAM
			
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				
				#include "UnityCG.cginc"
				#include "ShaderUtils.cginc"
	
				struct appdata
				{
					float4 vertex : POSITION;
					float2 texcoord : TEXCOORD0;
					fixed4 color : COLOR;
				};
	
				struct v2f
				{
					float4 vertex : SV_POSITION;
					float2 roundrectcoord : TEXCOORD0;
					float2 texcoord : TEXCOORD1;
					fixed4 color : COLOR;
				};
				
				sampler2D _MainTex;
				float4 _MainTex_ST;
				fixed4 _CornerParameters;

				v2f vert (appdata v)
				{
					v2f o;
					o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
					o.roundrectcoord = float2((v.texcoord.x - .5) * 2, (v.texcoord.y - .5) * 2);
					o.vertex = UnityObjectToClipPos(v.vertex.xyz);
					o.color = v.color;
					return o;
				}
				
				fixed4 frag (v2f i) : SV_Target
				{
					float2 coord = abs(i.roundrectcoord) - _CornerParameters.xy;
					float xIsPositive = GREATER_THAN(coord.x, 0);
					float yIsPositive = GREATER_THAN(coord.y, 0);
					
					float d = -INVERSE_LERP(1 - _CornerParameters.x, 0, coord.x) * LOGICAL_AND(xIsPositive, LOGICAL_NOT(yIsPositive));
					d -= INVERSE_LERP(1 - _CornerParameters.y, 0, coord.y) * LOGICAL_NOT(xIsPositive);
					coord.x /= (1 - _CornerParameters.x);
					coord.y /= (1 - _CornerParameters.y);
					d += (length(coord) - 1) * LOGICAL_AND(xIsPositive, yIsPositive);
					
					fixed4 col = lerp(
						tex2D(_MainTex, i.texcoord),
						i.color,
						INVERSE_LERP_CLAMP(-_CornerParameters.z, -_CornerParameters.z + _CornerParameters.w, d)
					);
					col.a *= INVERSE_LERP_CLAMP(0, -_CornerParameters.w, d);
					
					return col;
				}
				
			ENDCG
		}
	} 
}
