#ifndef TUBEBEND_INCLUDED
#define TUBEBEND_INCLUDED

#define TUBE_BEND_PROPERTIES \
	float4 _QOffset; \
	float _TubeBendStart; \
	float _TubeBendUnit;

#define APPLY_TUBE_BEND(vertex) \
	float zOff = max(0, -vertex.z - _TubeBendStart) / _TubeBendUnit; \
	vertex += _QOffset * zOff * zOff;

#endif
