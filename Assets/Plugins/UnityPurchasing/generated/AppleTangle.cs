#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("GxD0girJBdVamLm9RUqBw0Ca51++DIo1vgyNLS6NjI+MjI+MvoOIh4uOjQyPgY6+DI+EjAyPj45qHyeH7OLrrv367+Dq7/zqrvrr/OP9ru9Hl/x704Bb8dEVfKuNNNsBw9ODf/rm4fzn+ve/mL6aiI3bio2dg8/+6rutm8Wb15M9Gnl4EhBB3jRP1t4FlwdQd8Xie4klrL6MZpawdt6HXZi+moiN24qNnYPP/v7i667c4eH6/uLrrs3r/Prn6Oft7/rn4eCuz/uIvoGIjduTnY+PcYqLvo2Pj3G+k/zv7frn7euu/frv+uvj6+D6/aC+9L4Mj/i+gIiN25OBj49xioqNjI+iru3r/Prn6Oft7/rrrv7h4uft9+fo5+3v+ufh4K7P+/rm4fzn+ve/+fmg7/7+4uug7eHjoe/+/uLr7e/HVvgRvZrrL/kaR6OMjY+Ojy0Mj1e48U8J21cpFze8zHVWW/8Q8C/cP77WYtSKvALmPQGTUOv9cenQ6zK+n4iN24qEnYTP/v7i667H4O2gv6psZV85/lGBy2+pRH/j9mNpO5mZ4uuux+DtoL+ovqqIjduKhZ2Tz/5O7b35ebSJothlVIGvgFQ0/ZfBO6i+qoiN24qFnZPP/v7i667N6/z6+ufo5+3v+uuu7Peu7+D3rv7v/PqzqOmuBL3keYMMQVBlLaF33eTV6ukBhjqueUUioq7h/jixj74COc1BiWLztw0F3a5dtko/MRTBhOVxpXLc6+Ln7+Dt667h4K765uf9ru3r/PHPJhZ3X0ToEqrln14tNWqVpE2Rrs3PvgyPrL6DiIekCMYIeYOPj485lTMdzKqcpEmBkzjDEtDtRsUOmQH9D+5IldWHoRw8dsrGfu62EJt7O7QjeoGAjhyFP6+YoPpbsoNV7JglLf8cyd3bTyGhzz12dW3+Q2gtwuDqru3h4Orn+ufh4P2u4eiu+/3r967v/f374+v9ru/t7ev++u/g7eu4F8Kj9jljAhVSffkVfPhc+b7BT4qInYzb3b+dvp+IjduKhJ2Ez/7+DI+OiIekCMYIee3qi4++D3y+pIiu4eiu+ubrrvrm6+Cu7/7+4uft76G+D02IhqWIj4uLiYyMvg84lA89vbjUvuy/hb6HiI3bioidjNvdv52G0L4Mj5+IjduTrooMj4a+DI+KvoaliI+Li4mMj5iQ5vr6/v20oaH5g4iHpAjGCHmDj4+Li46NDI+PjtLL8JHC5d4YzwdK+uyFng3PCb0ED9cpi4fymc7Yn5D6XTkFrbXJLVvhJlLwrLtEq1tXgVjlWiyqrZ95LyKRCw0LlRezybl8JxXOAKJaPx6cVru8v7q+vbjUmYO9u768vre8v7q+oM4oecnD8YbQvpGIjduTrYqWvpgOmqVe58ka+IdweuUDoM4oecnD8ZEfVZDJ3mWLY9D3CqNluCzZwtti/uLrrtzh4fquzc++kJmDvri+urykCMYIeYOPj4uLjr7sv4W+h4iN24ETs32lx6aURnBAOzeAV9CSWEWzru/g6q7t6/z65+jn7e/65+Hgrv6IjduTgIqYipqlXufJGviHcHrlAzB6/RVgXOqBRffBulYssHf2ceVG3iQEW1Rqcl6Hibk++/uv");
        private static int[] order = new int[] { 18,18,14,42,19,5,20,16,12,19,29,46,28,44,46,36,27,58,26,19,53,42,57,29,39,56,42,51,38,58,44,59,55,58,39,43,40,48,46,39,41,48,46,58,58,47,56,52,54,49,51,59,55,54,54,56,57,57,58,59,60 };
        private static int key = 142;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
