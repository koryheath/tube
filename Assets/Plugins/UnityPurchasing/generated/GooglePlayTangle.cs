#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("/huCUeq0QOCrYD+2vWjPeTeILwkIujkaCDU+MRK+cL7PNTk5OT04O+mC3CB8m5cetuQa32rf/O95Lt3xujk3OAi6OTI6ujk5OI/fyWQ/lb+t/g7UMVZ46yPP8TAuXazjgekaGoNFwnETXr2UfQuJ+7XnA68etLNniQ7zW2TIg1MwZKlS2rqauRvxJcpG4OsIjwuYEzCbfMHUlN/Kl1KAL0MXg5ce3r0EWJFPT0jieXtlqkGJfERuzTzlIfnR0Fm6IxQng4MoJzeeE1FnFTFA1g+uCVfh+2PlCHtbaknzQmOwC3YC2nlBHb2l4HGPTIIvpT16hlXjNfZZ3Np7KquWLLaJIvFFkwH23xeWBOyIJ4T4NU5szvjzOeD+9FvzMx1cDzo7OTg5");
        private static int[] order = new int[] { 6,6,13,6,4,5,10,7,9,10,10,11,13,13,14 };
        private static int key = 56;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
