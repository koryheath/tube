//
//  Interstitiel.h
//
//  Created by Michel Morcos on 13/07/13.
//
////

#import <UIKit/UIKit.h>
#import <StoreKit/SKStoreProductViewController.h>
#import <Mediaplayer/Mediaplayer.h>
//@import MediaPlayer;
#import <AVFoundation/AVFoundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
//#import <AVKit/AVKit.h>
#import <AdSupport/AdSupport.h>
#import "Square.h"

@class Interstitiel;             //define class, so protocol can see MyClass
@protocol InterstitielDelegate   //define delegate protocol
- (void) interstitielDidFailToLoad;  //define delegate method to be
- (void) interstitielDidLoad: (Interstitiel *) sender withvideo:(BOOL) hasvideo;  //define delegate method to be
- (void) interstitielDidClose: (Interstitiel *) sender withvideo:(BOOL) hasvideo;  //define delegate method to be implemented within another class
- (void) squareIsReady;
- (void) isGdpr:(BOOL) optin;


@end //end protocol

@interface Interstitiel : UIView <SquareDelegate, SKStoreProductViewControllerDelegate/*, NSURLConnectionDelegate, NSURLConnectionDownloadDelegate, NSURLConnectionDataDelegate*/>
{
    
    Square *theSquare;
    
	NSMutableDictionary *adDico;
    NSMutableDictionary *langDico;
    
    NSUserDefaults *standardUserDefaults;
    
    NSString *theAdId;
    
    bool openAd;
    bool playvideo;
    bool isSurvey;
    
    UIActivityIndicatorView *loadingAlert;
    
    UIView *generalView;
    UIView *bgdview;
    //UIButton *adButton;
    //UIImageView *imgValidate;
    //UIButton *closeButton;
    
    NSString *appName;
    NSString *orientation;
    
    NSTimeInterval start;
    
    //double time_speed;
    bool isClosing;
    bool storeLoad;
    bool openad;
    NSTimer *_timer;
    NSURLConnection *conn;
    
    CGRect videoFrame;
    CGRect windowFrame;
    
    UIScrollView *gdprscrollView;
    
    //double time_speed;
    
    NSMutableArray *paramArraySave;
    
    bool isGdpr;
}

//@property (nonatomic, assign) UIViewController *rootViewController;

@property (nonatomic, assign) id <InterstitielDelegate> delegate; //define MyClassDelegate as delegate
@property(strong,nonatomic)MPMoviePlayerController *moviePlayer;
//@property(strong,nonatomic) AVPlayerViewController *moviePlayer;


//@property(nonatomic,retain) NSDictionary *adDico;

- (void) updateView:(CGRect)theFrame;
- (void) displaySite;
- (void) displaySiteVideo;
- (bool) hasAd;
- (void) createAd;
- (void) show:(UIView*) topview;
- (void) showSquare:(UIView*) topview atPoint:(CGPoint) thePoint;
- (id)initWithFrame:(CGRect)frame appName:(NSString*)theappName orientation:(NSString*)theorientation;
-(void) fetchSquare;
-(void) removeSquare;
-(void) openGdprWindow;
-(BOOL) isCountryGdpr;
-(BOOL) isGdprOptin;

@end
