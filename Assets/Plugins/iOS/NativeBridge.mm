//
//  iOSUtility.mm
//  Unity-iPhone
//
//  Created by Anas on 8/Sep/16.
//
//

#import <Foundation/Foundation.h>
#import "UnityAppController.h"
#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

#define MakeStringCopy( _x_ ) ( _x_ != NULL && [_x_ isKindOfClass:[NSString class]] ) ? strdup( [_x_ UTF8String] ) : NULL

extern "C"
{
    
    bool isDevelopmentMode = false; // set it to false for Release
    // Set the final values for release
    float cubeShooterPlayerSpeedIphone = 0.9f;
    float cubeShooterPlayerSpeedIpad = 0.45f;
    // Leaderboard ID's
   
    NSString *ballboost = @"com.yabado.kingofgamesuk.leaderboard.ballboost"; // add leaderboard id for ballboost here
    NSString *ColorTower = @"com.yabado.kingofgamesuk.leaderboard.colortower"; // add leaderboard id for ColorTower here
    NSString *ColorCells_Mode1 = @"com.yabado.kingofgamesuk.leaderboard.colorcells"; // add leaderboard id for ColorCells_Mode1 here
    NSString *ColorCells_Mode2 = @"com.yabado.kingofgamesuk.leaderboard.colorcells2"; // add leaderboard id for ColorCells_Mode2 here
    NSString *Balance_basic = @"com.yabado.kingofgamesuk.leaderboard.balance"; // add leaderboard id for Balance_basic here
    NSString *Balance_fun = @"com.yabado.kingofgamesuk.leaderboard.balance2"; // add leaderboard id for Balance_fun here
    NSString *Balance_wave = @"com.yabado.kingofgamesuk.leaderboard.balance3"; // add leaderboard id for Balance_wave here
    NSString *Balance_egg = @"com.yabado.kingofgamesuk.leaderboard.balance4"; // add leaderboard id for Balance_egg here
    NSString *CubeShooter = @"com.yabado.kingofgamesuk.leaderboard.cubeshooter"; // add leaderboard id for CubeShooter here
    NSString *Just64 = @"com.yabado.kingofgamesuk.leaderboard.just64"; // add leaderboard id for Just64 here
    NSString *BridgeBall = @"com.yabado.kingofgamesuk.leaderboard.bridgeball"; // add leaderboard id for BridgeBall here
    NSString *LineBoost = @"com.yabado.kingofgamesuk.leaderboard.lineboost"; // add leaderboard id for LineBoost here
    NSString *JustWave_sunny = @"com.yabado.kingofgamesuk.leaderboard.justwave"; // add leaderboard id for JustWave_sunny here
    NSString *JustWave_dark = @"com.yabado.kingofgamesuk.leaderboard.justwave2"; // add leaderboard id for JustWave_dark here
    NSString *ColorCode_3x3_SwipeMode = @"com.yabado.kingofgamesuk.leaderboard.colorcodeswipe3x3"; // add leaderboard id for ColorCode_3x3_SwipeMode here
    NSString *ColorCode_3x3_NormalMode = @"com.yabado.kingofgamesuk.leaderboard.colorcodetap3x3"; // add leaderboard id for ColorCode_3x3_NormalMode here
    NSString *ColorCode_4x4_SwipeMode = @"com.yabado.kingofgamesuk.leaderboard.colorcodeswipe4x4"; // add leaderboard id for ColorCode_4x4_SwipeMode here
    NSString *ColorCode_4x4_NormalMode = @"com.yabado.kingofgamesuk.leaderboard.colorcodetap4x4"; // add leaderboard id for ColorCode_4x4_NormalMode here
    NSString *BallRush = @"com.yabado.kingofgamesuk.leaderboard.ballrush"; // add leaderboard id for BallRush here
    NSString *AllGameRounds = @"com.yabado.kingofgamesuk.leaderboard.allgamerounds"; // add leaderboard id for all game rounds here
    
     NSString *AppStoreID = @"1338202819"; // add apple app id here
    
    /////////////////helper methods/////////////////////////////////////////////////////////////////////
    char* cStringCopy(const char* string)
    {
        if (string == NULL)
            return NULL;
        
        char* res = (char*)malloc(strlen(string) + 1);
        strcpy(res, string);
        
        return res;
    }
    static NSString* CreateNSString(const char* string)
    {
        if (string != NULL)
            return [NSString stringWithUTF8String:string];
        else
            return [NSString stringWithUTF8String:""];
    }
    /////////////////////////////////////////////////////////////////////
    
    char* _GetAppleAppID()
    {
        return cStringCopy([AppStoreID UTF8String]);
    }
    
	void _CleanIconBadge()
	{
		[UIApplication sharedApplication].applicationIconBadgeNumber = -1;
	}
    void _RateAndReviewApp(char* _id,int _showCount,bool _buttonClicked=false)
    {
       
        NSString *appUrl = [NSString stringWithUTF8String:_id];
        if(_showCount < 3)
        {
            if([UIDevice currentDevice].systemVersion.floatValue >= 10.3 && !_buttonClicked)
            {
                NSLog(@"Show Dialogue IOS");
                [SKStoreReviewController requestReview];
            }
            else{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: appUrl]];
            }
        }
        else{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: appUrl]];
        }
    }
    
    bool _EnableQAMenu()
    {
        return isDevelopmentMode; // please change true to false for disabling QAMenu
    }
    
    bool _isInDevelopmentMode()
    {
        return isDevelopmentMode;
    }
    
    char* _GetIAPIds()
    {
        
        if(isDevelopmentMode)
        {
            NSString *ids = @"com.binexsolutions.kogdemo.2000,com.binexsolutions.kogdemo.5500,com.binexsolutions.kogdemo.15000,com.binexsolutions.kogdemo.50000,com.binexsolutions.kogdemo.100000,com.binexsolutions.kogdemo.500000";
            
            return cStringCopy([ids UTF8String]);
        }
        else
        {
            NSString *ids = @"com.yabado.kingofgamesuk.iap.2000gems,com.yabado.kingofgamesuk.iap.5500gems,com.yabado.kingofgamesuk.iap.15000gems,com.yabado.kingofgamesuk.iap.50000gems,com.yabado.kingofgamesuk.iap.100000gems,com.yabado.kingofgamesuk.iap.500000gems";
            
            return cStringCopy([ids UTF8String]);
        }
    }
    
    char* _GetIAPButtonId(const char* _id)
    {
         NSString *temp = [NSString stringWithUTF8String:_id];
        if(isDevelopmentMode)
        {
            if ([temp isEqualToString:@"2000"])
            {
                NSString *helloString = @"com.binexsolutions.kogdemo.2000"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }
            else   if ([temp isEqualToString:@"5500"])
            {
                NSString *helloString = @"com.binexsolutions.kogdemo.5500"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }
            else   if ([temp isEqualToString:@"15000"])
            {
                NSString *helloString = @"com.binexsolutions.kogdemo.15000"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }else   if ([temp isEqualToString:@"50000"])
            {
                NSString *helloString = @"com.binexsolutions.kogdemo.50000"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }else   if ([temp isEqualToString:@"100000"])
            {
                NSString *helloString = @"com.binexsolutions.kogdemo.100000"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }else   if ([temp isEqualToString:@"500000"])
            {
                NSString *helloString = @"com.binexsolutions.kogdemo.500000"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }
            else
            {
                NSString *helloString = @"com.binexsolutions.kogdemo.2000"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }
        }
        else
        {
            
            
            if ([temp isEqualToString:@"2000"])
            {
                NSString *helloString = @"com.yabado.kingofgamesuk.iap.2000gems"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }
            else   if ([temp isEqualToString:@"5500"])
            {
                NSString *helloString = @"com.yabado.kingofgamesuk.iap.5500gems"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }
            else   if ([temp isEqualToString:@"15000"])
            {
                NSString *helloString = @"com.yabado.kingofgamesuk.iap.15000gems"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }else   if ([temp isEqualToString:@"50000"])
            {
                NSString *helloString = @"com.yabado.kingofgamesuk.iap.50000gems"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }else   if ([temp isEqualToString:@"100000"])
            {
                NSString *helloString = @"com.yabado.kingofgamesuk.iap.100000gems"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }else   if ([temp isEqualToString:@"500000"])
            {
                NSString *helloString = @"com.yabado.kingofgamesuk.iap.500000gems"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }
            else
            {
                NSString *helloString = @"com.yabado.kingofgamesuk.iap.2000gems"; // add button id for in app here
                return cStringCopy([helloString UTF8String]);
            }
        }
    }
    char* _GetAllGameRoundID()
    {
        return cStringCopy([AllGameRounds UTF8String]);
    }
    
    char* _GetId(const char* _id)
    {
        NSLog(@"aa gya ");
        
        NSString *temp = [NSString stringWithUTF8String:_id];
        
//        NSString* leaderBoardID = @"Shafqat";
//        NSString *temp = @"ballboost";
        
        if(isDevelopmentMode)
        {
             return cStringCopy([temp UTF8String]);
        }
        
      //  NSString *temp = [NSString stringWithUTF8String:_id];
       
        if ([temp isEqualToString:@"ballboost"])
        {
            return cStringCopy([ballboost UTF8String]);
        }
        else if ([temp isEqualToString:@"ColorTower"])
        {
            return cStringCopy([ColorTower UTF8String]);
        }
        else if ([temp isEqualToString:@"ColorCells_Mode1"])
        {
            return cStringCopy([ColorCells_Mode1 UTF8String]);
        }
        else if ([temp isEqualToString:@"ColorCells_Mode2"])
        {
            
            return cStringCopy([ColorCells_Mode2 UTF8String]);
            
        }
        else if ([temp isEqualToString:@"Balance_basic"])
        {
            
            return cStringCopy([Balance_basic UTF8String]);
            
        }
        else if ([temp isEqualToString:@"Balance_fun"])
        {
            
            return cStringCopy([Balance_fun UTF8String]);
            
        }
        else if ([temp isEqualToString:@"Balance_wave"])
        {
            
            return cStringCopy([Balance_wave UTF8String]);
            
        }
        else if ([temp isEqualToString:@"Balance_egg"])
        {
            
            return cStringCopy([Balance_egg UTF8String]);
            
        }
        else if ([temp isEqualToString:@"CubeShooter"])
        {
            
            return cStringCopy([CubeShooter UTF8String]);
            
        }
        else if ([temp isEqualToString:@"Just64"])
        {
            
            return cStringCopy([Just64 UTF8String]);
            
        }
        else if ([temp isEqualToString:@"BridgeBall"])
        {
            
            return cStringCopy([BridgeBall UTF8String]);
            
        }
        else if ([temp isEqualToString:@"LineBoost"])
        {
            
            return cStringCopy([LineBoost UTF8String]);
            
        }
        else if ([temp isEqualToString:@"JustWave_sunny"])
        {
            
            return cStringCopy([JustWave_sunny UTF8String]);
            
        }
        else if ([temp isEqualToString:@"JustWave_dark"])
        {
            
            return cStringCopy([JustWave_dark UTF8String]);
            
        }
        else if ([temp isEqualToString:@"ColorCode_3x3_SwipeMode"])
        {
            
            return cStringCopy([ColorCode_3x3_SwipeMode UTF8String]);
            
        }
        else if ([temp isEqualToString:@"ColorCode_3x3_NormalMode"])
        {
            
            return cStringCopy([ColorCode_3x3_NormalMode UTF8String]);
            
        }
        else if ([temp isEqualToString:@"ColorCode_4x4_SwipeMode"])
        {
            
            return cStringCopy([ColorCode_4x4_SwipeMode UTF8String]);
            
        }
        else if ([temp isEqualToString:@"ColorCode_4x4_NormalMode"])
        {
            
            return cStringCopy([ColorCode_4x4_NormalMode UTF8String]);
            
        }
        else if ([temp isEqualToString:@"BallRush"])
        {
            
            return cStringCopy([BallRush UTF8String]);
            
        }
        else
        {
            return cStringCopy([ballboost UTF8String]);
        }
    }
    
    char* _GetAllLeaderboardID()
    {
      if(!isDevelopmentMode)
        {
            NSString *allID = [@[ballboost,ColorTower,ColorCells_Mode1,ColorCells_Mode2,Balance_basic,Balance_fun,Balance_wave,Balance_egg,CubeShooter,Just64,BridgeBall,LineBoost,JustWave_sunny,JustWave_dark,ColorCode_3x3_SwipeMode,ColorCode_3x3_NormalMode,ColorCode_4x4_SwipeMode,ColorCode_4x4_NormalMode,BallRush] componentsJoinedByString:@"\n"];
            
            return cStringCopy([allID UTF8String]);
        }
        else
        {
            NSString *error = @"Internal Id's is being used.";
            return cStringCopy([error UTF8String]);
        }
    }
    
    char* _GetHeyzapID()
    {
        NSString *heyzapID = @"b9362b2c3ede81fb8169e3601e7dd64f"; // add heyzap id here
        return cStringCopy([heyzapID UTF8String]);
    }
    float _OSVersionFloat()
    {
        return [UIDevice currentDevice].systemVersion.floatValue;
    }
    float _CubeShooterPlayerSpeedValueiPhone(float value)
    {
        if(!isDevelopmentMode)
            return cubeShooterPlayerSpeedIphone;
        else
            return value;
    }
    float _CubeShooterplayerSpeedValueiPad(float value)
    {
        if(!isDevelopmentMode)
            return cubeShooterPlayerSpeedIpad;
        else
            return value;
    }
    char* _BalanceLeaderBoardID()
    {
        if(isDevelopmentMode)
        {
            NSString *leaderBoardID = @"com.binexsolutions.balancedemo";
            return cStringCopy([leaderBoardID UTF8String]);
        }
        else
        {
            NSString *leaderBoardID = @"com.yabado.balance.leaderboard";
            return cStringCopy([leaderBoardID UTF8String]);
        }
    }

}






















