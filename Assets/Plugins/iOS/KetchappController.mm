
//
//  KetchappController.mm
//

#import "UnityAppController.h"
#import "UnityAppController+ViewHandling.h"

#import <Ketchapp/Ketchapp.h>

extern void UnitySendMessage(const char *, const char *, const char *);

@interface KetchappController : UnityAppController<InterstitielDelegate>
{
    Interstitiel *theAd;
}
@end

@implementation KetchappController

- (void)showKetchappPromo
{
    //Change the bundle to the one we will use on our account. The orientation can be Portrait or Landscape
    
    theAd = [[Interstitiel alloc] initWithFrame:[[UIScreen mainScreen] bounds] appName:@"com.ketchapp.trickytube" orientation:@"portrait"];
    theAd.delegate = self;
    
    
}

- (void) interstitielDidLoad:(Interstitiel*) theInterstitiel withvideo:(BOOL)hasvideo
{
    //If hasvideo is Yes then pause the sound
    
    if(theInterstitiel != nil)
    {
        //If hasvideo is Yes then pause the sound
        
        [theInterstitiel show:self.rootView];
        UnitySendMessage("GameController", "KetchappPromoStarted", "");
    }
    else {
        UnitySendMessage("GameController", "KetchappPromoStarted", "");
        UnitySendMessage("GameController", "KetchappPromoDone", "");
    }
}

-(void) interstitielDidFailToLoad
{
    //If there is no cross promo (don't use that to display the banner)
    UnitySendMessage("GameController", "KetchappPromoStarted", "");
    UnitySendMessage("GameController", "KetchappPromoDone", "");
}

- (void) interstitielDidClose:(Interstitiel*) theInterstitiel withvideo:(BOOL)hasvideo
{
    //the user has close the cross promo
    //If hasvideo is Yes then play the sound back
    UnitySendMessage("GameController", "KetchappPromoDone", "");
}

- (void) isGdpr:(BOOL)optin
{
    //the user has close the cross promo
    //If hasvideo is Yes then play the sound back
    
    if(optin)
        UnitySendMessage("GameController", "isGdpr", "true");
    else
        UnitySendMessage("GameController", "isGdpr", "false");
    
}


-(void) fetchKetchappSquare
{
    [theAd fetchSquare];
}

-(void) showKetchappSquare:(float) positionX andPosition:(float) positionY
{
    positionX *= [UIScreen mainScreen].bounds.size.width;
    positionY *= [UIScreen mainScreen].bounds.size.height;
    [theAd showSquare:self.rootView atPoint:CGPointMake(positionX,positionY)];
}

- (void) removeKetchappSquare
{
    [theAd removeSquare];
}

- (void) openKetchappGdprWindow
{
    [theAd openGdprWindow];
}

- (BOOL) isKetchappCountryGdpr
{
    return [theAd isCountryGdpr];
}

- (BOOL) isKetchappGdprOptin
{
    return [theAd isGdprOptin];
}

-(void) squareIsReady
{
    //After fetching this methode is called to let you know there is a square cross promo for game over screen.
    //Use this on the game over screen : [theAd showSquare:theView atPoint:CGPointMake(x,y)];
}

@end


extern "C" void _ShowKetchappPromo()
{
    [(KetchappController*)GetAppController () showKetchappPromo];
}


extern "C" void _FetchKetchappSquare()
{
    [(KetchappController*)GetAppController () fetchKetchappSquare];
}

extern "C" void _ShowKetchappSquare(float positionX, float positionY)
{
    [(KetchappController*)GetAppController () showKetchappSquare:positionX andPosition:positionY];
}

extern "C" void _RemoveKetchappSquare()
{
    [(KetchappController*)GetAppController () removeKetchappSquare];
}

extern "C" void _OpenKetchappGdprWindow()
{
    [(KetchappController*)GetAppController () openKetchappGdprWindow];
}


extern "C" bool _IsKetchappCountryGdpr()
{
    return [(KetchappController*)GetAppController () isKetchappCountryGdpr];
}


extern "C" bool _IsKetchappGdprOptin()
{
    return [(KetchappController*)GetAppController () isKetchappGdprOptin];
}


IMPL_APP_CONTROLLER_SUBCLASS(KetchappController)
