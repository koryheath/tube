﻿using UnityEngine;
using System.Runtime.InteropServices;

public class KetchappPromo
{
	private static int showCount = 0;
	#if UNITY_IOS && !UNITY_EDITOR
	
	[DllImport ("__Internal")]private static extern void _ShowKetchappPromo ();
    [DllImport ("__Internal")]private static extern void _FetchKetchappSquare ();
    [DllImport ("__Internal")]private static extern void _ShowKetchappSquare (float positionX, float positionY);
    [DllImport ("__Internal")]private static extern void _RemoveKetchappSquare ();
    [DllImport ("__Internal")]private static extern void _OpenKetchappGdprWindow ();
    [DllImport ("__Internal")]private static extern bool _IsKetchappCountryGdpr ();
    [DllImport ("__Internal")]private static extern bool _IsKetchappGdprOptin ();

	#endif

	public static bool RequestKetchappPromo()
	{
		if (showCount != 0) {
			// Only show the promo once
			return false;
		}
		showCount++;

        #if UNITY_IOS && !UNITY_EDITOR
		_ShowKetchappPromo ();
		return true;
        #elif UNITY_ANDROID && !UNITY_EDITOR
		KetchappPromotionAndroid.Show();
		return true;
        #else
		return false;
        #endif
	}

    public static void FetchKetchappSquare ()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        _FetchKetchappSquare ();
        #endif
    }

    public static void ShowKetchappSquare (float positionX, float positionY)
    {
        #if UNITY_IOS && !UNITY_EDITOR
        _ShowKetchappSquare (positionX, positionY);
        #endif
    }

    public static void RemoveKetchappSquare()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        _RemoveKetchappSquare ();
        #endif
    }

	public static void OpenKetchappGdprWindow()
	{
        #if UNITY_IOS && !UNITY_EDITOR
        _OpenKetchappGdprWindow ();
        #elif UNITY_ANDROID && !UNITY_EDITOR
		KetchappPromotionAndroid.OpenKetchappGdprWindow();
        #endif
	}

    public static bool IsKetchappCountryGdpr()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        return _IsKetchappCountryGdpr ();
	    #elif UNITY_ANDROID && !UNITY_EDITOR
	    //return KetchappPromotionAndroid.IsKetchappCountryGdpr();
	    return false;
	    #else
	    return false;
        #endif
    }

    public static bool IsKetchappGdprOptin()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        return _IsKetchappGdprOptin ();
	    #elif UNITY_ANDROID && !UNITY_EDITOR
	    //return KetchappPromotionAndroid.IsKetchappGdprOptin();
	    return true;
	    #else
        return true;
        #endif
    }


    public void interstitielDidLoad(string parameter)
	{
		
		#if UNITY_IOS && !UNITY_EDITOR
        
	    #endif
    }

    public void interstitielDidFailToLoad(string parameter)
    {
	    #if UNITY_IOS && !UNITY_EDITOR
	    
	    #endif
    }

    public void interstitielDidClose(string parameter)
    {
	    #if UNITY_IOS && !UNITY_EDITOR
	    
	    #endif
    }

    public void squareIsReady(string parameter)
    {
        #if UNITY_IOS && !UNITY_EDITOR

        #endif
    }

    public void isGdpr(bool optin)
    {
        #if UNITY_IOS && !UNITY_EDITOR

        #endif
    }


}

#if UNITY_ANDROID && !UNITY_EDITOR
class KetchappPromotionAndroid {

    public static void Show() {
		// Java public static void showPromotion()
		using (AndroidJavaClass jc = new AndroidJavaClass("com.ketchapp.promotion.Unity3d.Unity3dHelper")) {
            jc.CallStatic("showPromotion");
        }
    }

    public static void OpenKetchappGdprWindow() {
    	// For the moment this is disabled since we're still using Android Ketchapp sdk 1.0. Just show the promo instead.
		using (AndroidJavaClass jc = new AndroidJavaClass("com.ketchapp.promotion.Unity3d.Unity3dHelper")) {
            jc.CallStatic("showPromotion");
        }
    	/*
		// Java public static void OpenKetchappGdprWindow()
		using (AndroidJavaClass jc = new AndroidJavaClass("com.ketchapp.promotion.Unity3d.Unity3dHelper")) {
            jc.CallStatic("OpenKetchappGdprWindow");
        }
        */
    }

	public static bool IsKetchappGdprOptin()
	{
		return true;
		/*
		bool result = false;
		using (AndroidJavaClass jc = new AndroidJavaClass("com.ketchapp.promotion.Unity3d.Unity3dHelper")) {
			result = jc.CallStatic<bool>("IsKetchappGdprOptin");
		}
		return result;
		*/
	}

	public static bool IsKetchappCountryGdpr()
	{
		return false;
		/*
		bool result = false;
		using (AndroidJavaClass jc = new AndroidJavaClass("com.ketchapp.promotion.Unity3d.Unity3dHelper")) {
			result = jc.CallStatic<bool>("IsKetchappCountryGdpr");
		}
		return result;
		*/
	}
}
#endif

