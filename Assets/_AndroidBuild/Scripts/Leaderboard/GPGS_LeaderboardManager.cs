﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
#if UNITY_ANDROID
using GooglePlayGames;
#endif


public class GPGS_LeaderboardManager : MonoBehaviour {

	public static GPGS_LeaderboardManager singleton;

		private string SessionCountLeaderboardName = "gamesessions";

		public enum Leaderboards
		{
			HighestDistance
		}

		void Awake()
		{
			if(!singleton)
			{
			singleton = this as GPGS_LeaderboardManager;
				Intialize ();
			}
			else
			{
				DestroyObject(gameObject);
			}

			DontDestroyOnLoad(gameObject);


		}

		public void Intialize()
		{
			#if UNITY_ANDROID
			if(!PlayGamesPlatform.Instance.IsAuthenticated())
				PlayGamesPlatform.Activate ();
			#endif

			Social.localUser.Authenticate (ProcessAuthentication);

		}
		void ProcessAuthentication (bool success) 
		{
			if (success) 
			{
				Debug.Log ("Authenticated");

			}
			else
			{
				Debug.Log ("Failed to authenticate");
			}

		}
		public void ShowLeaderboard(string id)
		{
			
			if (Application.platform == RuntimePlatform.IPhonePlayer) {
//				id = NativeUtilityBinding.Instance.GetID (id);
//				GameCenterPlatform.ShowLeaderboardUI (id,UnityEngine.SocialPlatforms.TimeScope.AllTime);

			

			} else if (Application.platform == RuntimePlatform.Android) {
				
				#if UNITY_ANDROID
//				id = GameIDs.Instance.GetLeaderboardID (id);
				PlayGamesPlatform.Instance.ShowLeaderboardUI(id);
				#endif

			}
		}




		public void ShowGameCentreScreen()
		{
			if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				if (!Social.localUser.authenticated) {
				


				} else {
					Social.ShowLeaderboardUI ();
				}
			}
			else if (Application.platform == RuntimePlatform.Android)
			{
				//  Social.ShowLeaderboardUI ();
				#if UNITY_ANDROID
				if (!Social.localUser.authenticated) {


					Intialize();

				} else {
					PlayGamesPlatform.Instance.ShowLeaderboardUI();
				}
				#endif

			}
		}

		public void ReportScore(long score,string BoardID)
		{
			



			if (Application.platform == RuntimePlatform.IPhonePlayer) {
			//	BoardID = NativeUtilityBinding.Instance.GetID (BoardID);
				Social.ReportScore(score,BoardID.ToString(),HandleReportScoreResponse);

			} else if (Application.platform == RuntimePlatform.Android) {

				#if UNITY_ANDROID
			//	BoardID = NativeUtilityBinding.Instance.GetID (BoardID);
				PlayGamesPlatform.Instance.ReportScore(score,BoardID,HandleReportScoreResponse);
				#endif
			}


		}
		void HandleReportSessionCountResponse(bool success)
		{
			if (success)
			{
				Debug.Log("HandleReportSessionCountResponse  Reported successfully");
			}
			else
			{
				Debug.Log("HandleReportSessionCountResponse Reported failed");
			}
		}
		void HandleReportScoreResponse(bool success)
		{
			if (success)
			{
				Debug.Log("Score Reported successfully");
			}
			else
			{
				Debug.Log("Score Reported failed");
			}
		}


}