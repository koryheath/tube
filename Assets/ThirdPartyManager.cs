﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPartyManager : MonoBehaviour {


	public static string HyezapID = "";
	public static string AndroidSharingString = "";
	public static string AndroidNOADSID = "com.ketchapp.game.trickytube.removeads";
	public static string AndroidGEMSDOUBLERID = "com.ketchapp.game.trickytube.gemdoubler";



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void askForPermission(string gameObjectName, string functionName)
	{

		AndroidJavaClass share = new AndroidJavaClass ("kog.binexsolutions.com.mylibrary.PluginClass");
		share.CallStatic("askForStoragePermission",gameObjectName,functionName);
	}


	public static bool checkStoragePermission()
	{

		AndroidJavaClass share = new AndroidJavaClass ("kog.binexsolutions.com.mylibrary.PluginClass");
		return share.CallStatic<bool> ("checkPermission");

//		return false;
	}

	public static bool isFacebookInstalled()
	{
		//com.facebook.katana

		AndroidJavaClass share = new AndroidJavaClass ("kog.binexsolutions.com.mylibrary.PluginClass");
		return share.CallStatic<bool> ("isAppInstalled","com.facebook.katana");

	}

	public static bool isInstagramInstalled()
	{
		//com.facebook.katana

		AndroidJavaClass share = new AndroidJavaClass ("kog.binexsolutions.com.mylibrary.PluginClass");
		return share.CallStatic<bool> ("isAppInstalled","com.instagram.android");

	}
}
