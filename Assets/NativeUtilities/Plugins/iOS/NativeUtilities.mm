#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import <GameKit/GameKit.h>

@interface StoreProductViewController : SKStoreProductViewController <SKStoreProductViewControllerDelegate>
@end
@interface GameCenterViewController : GKGameCenterViewController <GKGameCenterControllerDelegate>
@end

static UIImpactFeedbackGenerator* impactFeedbackGenerator = nil;

extern "C"
{
    void _openAlert(const char* title, const char* message, const char* unityObject, const char* unityMethod) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithUTF8String:title]
                                                                       message:[NSString stringWithUTF8String:message]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  if (unityObject != nil && unityMethod != nil) {
                                                                      UnitySendMessage(unityObject, unityMethod, "");
                                                                  }
                                                              }];
        
        [alert addAction:defaultAction];
        [UnityGetGLViewController() presentViewController:alert animated:YES completion:nil];
    }
    
    void _openURLWithFallback(const char* primaryString, const char* secondaryString) {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *primaryURL = [NSURL URLWithString:[NSString stringWithUTF8String:primaryString]];
        NSURL *secondaryURL = [NSURL URLWithString:[NSString stringWithUTF8String:secondaryString]];
        
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [application openURL:primaryURL options:@{} completionHandler:^(BOOL success) {
                if (!success) {
                    [application openURL:secondaryURL options:@{} completionHandler:nil];
                }
            }];
        } else {
            BOOL success = [application openURL:primaryURL];
            if (!success) {
                [application openURL:secondaryURL];
            }
        }
    }
    
    void _openAppStore(int itemIdentifier) {
        StoreProductViewController* spvc = [[StoreProductViewController alloc] init];
        [spvc setDelegate:spvc];
        [spvc loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier:[NSNumber numberWithInteger:itemIdentifier]}
                        completionBlock:nil];
        [UnityGetGLViewController() presentViewController:spvc animated:YES completion:nil];
    }
    
    void _openRateThisAppPopup() {
        if (NSClassFromString(@"SKStoreReviewController")) {
            [SKStoreReviewController requestReview];
        }
    }
    
    void _openLeaderboard(const char* leaderboardID) {
        GameCenterViewController *gameCenterViewController = [[GameCenterViewController alloc] init];
        if (gameCenterViewController != nil) {
            gameCenterViewController.gameCenterDelegate = gameCenterViewController;
            gameCenterViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
            gameCenterViewController.leaderboardTimeScope = GKLeaderboardTimeScopeAllTime;
            gameCenterViewController.leaderboardIdentifier = [NSString stringWithUTF8String:leaderboardID];
            [UnityGetGLViewController() presentViewController: gameCenterViewController animated:YES completion:nil];
        }
        else {
            UnitySendMessage("GameController", "LeaderboardDone", "");
        }
    }
    
    void _triggerHaptic() {
        if (NSClassFromString(@"UIImpactFeedbackGenerator")) {
            if (impactFeedbackGenerator == nil) {
                impactFeedbackGenerator = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleMedium];
            }
            [impactFeedbackGenerator prepare];
            [impactFeedbackGenerator impactOccurred];
        }
    }
}


@implementation StoreProductViewController

-(void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
    UnitySendMessage("GameController", "AppStoreDone", "");
}

@end


@implementation GameCenterViewController

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
    UnitySendMessage("GameController", "LeaderboardDone", "");
}

@end
