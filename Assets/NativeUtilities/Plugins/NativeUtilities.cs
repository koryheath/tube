﻿using UnityEngine;
using System.Runtime.InteropServices;

public class NativeUtilities {
	
	#if UNITY_IOS && !UNITY_EDITOR
	[DllImport ("__Internal")]
	private static extern void _openAlert(string title, string message, string unityObject, string unityMethod);
	[DllImport ("__Internal")]
	private static extern void _openURLWithFallback(string primaryString, string secondaryString);
	[DllImport ("__Internal")]
	private static extern void _openAppStore(int itemIdentifier);
	[DllImport ("__Internal")]
	private static extern void _openRateThisAppPopup();
	[DllImport ("__Internal")]
	private static extern void _openLeaderboard(string leaderboardID);
	[DllImport ("__Internal")]
	private static extern void _triggerHaptic();
	#endif
	
	public static void OpenAlert(string title, string message, string unityObject, string unityMethod) {
		#if UNITY_IOS && !UNITY_EDITOR
		_openAlert(title, message, unityObject, unityMethod);
		#endif
	}
	
	public static void OpenURLWithFallback(string primaryString, string secondaryString) {
		#if UNITY_IOS && !UNITY_EDITOR
		_openURLWithFallback(primaryString, secondaryString);
		#else
		Application.OpenURL(primaryString);
		#endif
	}
	
	public static void OpenAppStore(int itemIdentifier) {
		#if UNITY_IOS && !UNITY_EDITOR
		_openAppStore(itemIdentifier);
		#endif
	}
	
	public static void OpenRateThisAppPopup() {
		#if UNITY_IOS && !UNITY_EDITOR
		_openRateThisAppPopup();
		#endif
	}
	
	public static void OpenLeaderboard(string leaderboardID) {
		#if UNITY_IOS && !UNITY_EDITOR
		_openLeaderboard(leaderboardID);
		#endif
	}
	
	public static void TriggerHaptic() {
		#if UNITY_IOS && !UNITY_EDITOR
		_triggerHaptic();
		#endif
	}
}
